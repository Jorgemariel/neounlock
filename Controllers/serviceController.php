<?php namespace Controllers;

	//Definiciones
	use Models\Service as Service;
	use Models\Brand as Brand;
	use Models\Model as Model;
	use Models\Country as Country;
	use Models\Company as Company;
	use Models\Language as Language;
	use Models\User as User;

	class serviceController
	{
		private $service;
		private $template;

		private $brand;
		private $model;
		private $country;
		private $company;
		private $user;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->service = new Service();
			$this->user = new User();
			$this->template = new \Smarty();
			$this->brand = new Brand();
			$this->model = new Model();
			$this->country = new Country();
			$this->company = new Company();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(6);
			
			$languages = $this->language->toList();
			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['ServiceRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['ServiceAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['ServiceEdited']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'brands')
				{
					$this->template->assign('message', $this->lang['CompaniesUpdated']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'descriptions')
				{
					$this->template->assign('message', $this->lang['DescriptionsUpdated']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->service->set('name', $_GET['searchName']);
				if (isset($_GET['searchDescription']) and ($_GET['searchDescription'] === '1' or $_GET['searchDescription'] === '0')) $this->service->set('description', $_GET['searchDescription']);
				if (isset($_GET['searchPrice']) and !empty($_GET['searchPrice'])) $this->service->set('price', $_GET['searchPrice']);
				if (isset($_GET['searchDelay']) and !empty($_GET['searchDelay'])) $this->service->set('delay', $_GET['searchDelay']);
				if (isset($_GET['searchDelayType']) and !empty($_GET['searchDelayType'])) $this->service->set('id_delay_type', $_GET['searchDelayType']);
				if (isset($_GET['searchStatus']) and ($_GET['searchStatus'] === '1' or $_GET['searchStatus'] === '0')) $this->service->set('status', $_GET['searchStatus']);
				if (isset($_GET['searchBrand']) and !empty($_GET['searchBrand'])) $this->service->set('brand', $_GET['searchBrand']);
				if (isset($_GET['searchModel']) and !empty($_GET['searchModel'])) $this->service->set('model', $_GET['searchModel']);
				if (isset($_GET['searchCountry']) and !empty($_GET['searchCountry'])) $this->service->set('country', $_GET['searchCountry']);
				if (isset($_GET['searchCompany']) and !empty($_GET['searchCompany'])) $this->service->set('company', $_GET['searchCompany']);

				if (isset($_GET['order']) and !empty($_GET['order'])) $this->service->set('order', $_GET['order']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->service->set('sort', $_GET['sort']);

				$list = $this->service->search();

				$this->template->assign(array(
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'list' 		=> $list
				));

				$this->template->display('Admin/Service/_list.tpl');

			} else {
				$list = $this->service->search();
				$brands = $this->brand->toListModels();
				$countries = $this->country->search();
				$companies = $this->company->search();
				$delayTypes = $this->service->toListDelayTypes();

				//var_dump($list);

				$this->template->assign(array(
					'title'		=> $this->lang['Services'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'services',
					'list' 		=> $list,
					'brands'	=> $brands,
					'countries'	=> $countries,
					'companies' => $companies,
					'delayTypes'=> $delayTypes
				));
				$this->template->display('Admin/Service/index.tpl');
			}
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(24);
			
			if ($_POST)
			{
				if (
					isset($_POST['name']) and !empty($_POST['name']) and 
					isset($_POST['price']) and !empty($_POST['price']) and 
					isset($_POST['delay']) and !empty($_POST['delay']) and 
					isset($_POST['delayType']) and
					isset($_POST['status']) and
					isset($_POST['brands']) and
					isset($_POST['models']) and
					isset($_POST['countries']) and 
					isset($_POST['companies'])
					)
				{
					//Carga todo el POST en la clase
					$this->service->set('name', $_POST['name']);
					$this->service->set('price', $_POST['price']);
					$this->service->set('delay', $_POST['delay']);
					$this->service->set('id_delay_type', $_POST['delayType']);
					$this->service->set('status', $_POST['status']);

					$this->service->add(
						$_POST['brands'],
						$_POST['models'],
						$_POST['countries'],
						$_POST['companies']
					);

					die('1');
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
			else
			{
				$brands = $this->brand->toListModels();
				$delayTypes = $this->service->toListDelayTypes();
				$countries = $this->country->search();
				$companies = $this->company->search();

				$title = $this->lang['Add'] . ' ' . $this->lang['Service'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'services',
					'delayTypes'	=> $delayTypes,
					'brands'	=> $brands,
					'countries' => $countries,
					'companies' => $companies
				));

				$this->template->display('Admin/Service/item.tpl');
				
			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(25);
			
			if (!$_POST)
			{
				$this->service->set('id', $id);
				$service = $this->service->view();
				$brands = $this->service->toListBrandsAndModelsAllAndCheck();
				$countries = $this->service->toListCountriesAllAndCheck();
				$companies = $this->service->toListCompaniesAllAndCheck();
				$delayTypes = $this->service->toListDelayTypes();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Service'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'service'	=> $service,
					'title' 	=> $title,
					'nav'		=> 'services',
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'delayTypes'=> $delayTypes,
					'brands'	=> $brands,
					'countries'	=> $countries,
					'companies'	=> $companies
				));

				$this->template->display('Admin/Service/item.tpl');
			}
			else
			{
				if (
					isset($_POST['name']) and !empty($_POST['name']) and 
					isset($_POST['price']) and !empty($_POST['price']) and 
					isset($_POST['delay']) and !empty($_POST['delay']) and 
					isset($_POST['delayType']) and
					isset($_POST['status']) and
					isset($_POST['brands']) and
					isset($_POST['models']) and
					isset($_POST['countries']) and 
					isset($_POST['companies'])
					)
				{
					//Carga todo el POST en la clase
					$this->service->set('name', $_POST['name']);
					$this->service->set('price', $_POST['price']);
					$this->service->set('delay', $_POST['delay']);
					$this->service->set('id_delay_type', $_POST['delayType']);
					$this->service->set('status', $_POST['status']);
					$this->service->set('id', $id);

					$this->service->edit(
						$_POST['brands'],
						$_POST['models'],
						$_POST['countries'],
						$_POST['companies']
					);

					die('1'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(26);
			
			$this->service->set('id', $id);
			$this->service->delete();

			header("Location: " . URL . "service/?message=delete");
		}

		public function export()
		{
			$this->user->checkSession();
			echo 'falta terminar';
		}

		public function descriptions($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(36);
			
			if (!$_POST) {
				//die('2');
				$this->service->set('id', $id);
				$descriptions = $this->service->toListDescriptions();
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $this->lang['Descriptions'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'services',
					'id'		=> $id,
					'descriptions'	=> $descriptions
				));

				$this->template->display('Admin/Service/descriptions.tpl');
			} else {
				if (isset($_POST['data']) and !empty($_POST['data']))
				{
					foreach ($_POST['data'] as $d) {
						$this->service->set('id', $id);
						$this->service->set('id_language', $d['id_language']);
						$this->service->set('description', $d['content']);
						$this->service->editDescription();
					}
					die('1');
				}
				die('Error');
			}
		}
	}

?>