<?php namespace Controllers;

	//Definiciones
	use Models\User as User;
	use Models\Language as Language;

	class userController
	{
		private $user;
		private $template;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->user = new User();
			$this->template = new \Smarty();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(10);
			
			$languages = $this->language->toList();
			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['UserRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['UserAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'noimage')
				{
					$this->template->assign('message', $this->lang['UserAddedNoImage']);
					$this->template->assign('messageColor', 'yellow darken-2');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['UserEdited']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->user->set('name', $_GET['searchName']);
				if (isset($_GET['searchPosition']) and !empty($_GET['searchPosition'])) $this->user->set('position', $_GET['searchPosition']);
				if (isset($_GET['searchImage']) and ($_GET['searchImage'] === '1' or $_GET['searchImage'] === '0')) $this->user->set('img', $_GET['searchImage']);
				if (isset($_GET['searchStatus']) and ($_GET['searchStatus'] === '1' or $_GET['searchStatus'] === '0')) $this->user->set('status', $_GET['searchStatus']);
				if (isset($_GET['searchModel']) and ($_GET['searchModel'] === '1' or $_GET['searchModel'] === '0')) $this->user->set('showModel', $_GET['searchModel']);
				if (isset($_GET['searchCountry']) and ($_GET['searchCountry'] === '1' or $_GET['searchCountry'] === '0')) $this->user->set('showCountry', $_GET['searchCountry']);
				if (isset($_GET['searchCompany']) and ($_GET['searchCompany'] === '1' or $_GET['searchCompany'] === '0')) $this->user->set('showCompany', $_GET['searchCompany']);
				if (isset($_GET['searchPRD']) and ($_GET['searchPRD'] === '1' or $_GET['searchPRD'] === '0')) $this->user->set('showPRD', $_GET['searchPRD']);
				if (isset($_GET['searchMEP']) and ($_GET['searchMEP'] === '1' or $_GET['searchMEP'] === '0')) $this->user->set('showMEP', $_GET['searchMEP']);

				if (isset($_GET['order']) and !empty($_GET['order'])) $this->user->set('order', $_GET['order']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->user->set('sort', $_GET['sort']);

				$list = $this->user->search();

				$this->template->assign(array(
					'title'		=> $this->lang['Users'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'users',
					'list' 		=> $list
				));

				$this->template->display('Admin/User/_list.tpl');

			} else {
				$list = $this->user->toList();
				$this->template->assign(array(
					'title'		=> $this->lang['Users'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'users',
					'list' 		=> $list
				));
				$this->template->display('Admin/User/index.tpl');
			}
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(31);
			
			if ($_POST)
			{
				// var_dump($_POST);
				if (
					isset($_POST['username']) and !empty($_POST['username']) and 
					isset($_POST['email']) and !empty($_POST['email']) and
					isset($_POST['status']) and 
					isset($_POST['language'])
					)
				{
					$privileges = $this->user->toListPrivileges();

					foreach ($privileges as &$p) {
						$p['status'] = $_POST['privilege_'.$p['id']] == 'true' ? true : false;
					}

					// var_dump($privileges);
					

					//Carga todo el POST en la clase
					$this->user->set('username', $_POST['username']);
					$this->user->set('email', $_POST['email']);
					$this->user->set('password', $_POST['password'] ? password_hash($_POST['password'], PASSWORD_DEFAULT) : "");
					$this->user->set('id_status', $_POST['status']);
					$this->user->set('id_language', $_POST['language']);
					$this->user->setPrivileges($privileges);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->user->set('img', end($temp));

							// Guarda y obtengo el id para asignarselo al nombre en el server
							$id = $this->user->add();
							if ($id == 0) {
								die($this->lang['UserAllreadyExists']);
							}
							$this->user->set('id', $id);

							$fileName = $this->user->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "users" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);

							die('1');//Marca agregada con imagen
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}
					}

					//Guarda en la base de datos
					$id = $this->user->add();
					if ($id == 0) {
						die($this->lang['UserAllreadyExists']);
					}

					die('2'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
			else
			{

				$title = $this->lang['Add'] . ' ' . $this->lang['User'];
				$languages = $this->language->toList();
				$privileges = $this->user->toListPrivileges();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'users',
					'privileges'=> $privileges
				));

				$this->template->display('Admin/User/item.tpl');
				
			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(32);
			
			if (!$_POST)
			{
				$this->user->set('id', $id);
				$user = $this->user->view();
				$privileges = $this->user->toListPrivileges();

				$title = $this->lang['Edit'] . ' ' . $this->lang['User'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'user'	=> $user,
					'privileges'=>$privileges,
					'title' => $title,
					'languages'	=> $languages,
					'nav'	=> 'users',
					'lang'	=> $this->lang
				));

				$this->template->display('Admin/User/item.tpl');
			}
			else
			{
				// var_dump($_POST);
				if (
					isset($_POST['username']) and !empty($_POST['username']) and 
					isset($_POST['email']) and !empty($_POST['email']) and
					isset($_POST['status']) and 
					isset($_POST['language'])
					)
				{
					$privileges = $this->user->toListPrivileges();

					foreach ($privileges as &$p) {
						$p['status'] = $_POST['privilege_'.$p['id']] == 'true' ? true : false;
					}

					// var_dump($privileges);
					

					//Carga todo el POST en la clase
					$this->user->set('username', $_POST['username']);
					$this->user->set('email', $_POST['email']);
					$this->user->set('password', $_POST['password'] ? password_hash($_POST['password'], PASSWORD_DEFAULT) : "");
					$this->user->set('id_status', $_POST['status']);
					$this->user->set('id_language', $_POST['language']);
					$this->user->setPrivileges($privileges);
					$this->user->set('img', $_GET['img']);

					$this->user->set('id', $id);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->user->set('img', end($temp));

							$fileName = $this->user->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "users" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
							die('1');//Marca editada con imagen
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}
					}

					//Guarda en la base de datos
					$this->user->edit();

					die('1'); //Marca editada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(33);
			
			$this->user->set('id', $id);
			$this->user->delete();

			header("Location: " . URL . "user/?message=delete");
		}

		public function export()
		{
			echo 'falta terminar';
		}
	}

?>