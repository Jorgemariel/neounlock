<?php namespace Controllers;

	use Models\Language as Language;
	use Models\User as User;
	use Models\Admin as Admin;

	class adminController
	{
		private $language;
		private $lang;
		private $user;
		private $admin;

		public function __construct()
		{
			$this->template = new \Smarty();
			$this->user = new User();
			$this->admin = new Admin();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$languages = $this->language->toList();

			$profitTotal = $this->admin->profitTotal();
			$profitYear = $this->admin->profitYear();
			$profitMonth = $this->admin->profitMonth();

			$profitYearCompare = $this->admin->profitYearCompare();
			$profitYearCompare = $profitYearCompare ? ($profitYear/$profitYearCompare-1)*100 : 100;

			$profitMonthCompare = $this->admin->profitMonthCompare();
			$profitMonthCompare = $profitMonthCompare ? ($profitMonth/$profitMonthCompare-1)*100 : 100;

			$profitChart = $this->admin->profitChart();


			$this->template->assign(array(
				'tabTitle'	=> 'Admin NeoUnlock',
				'title'		=> 'Admin',
				'languages'	=> $languages,
				'lang'		=> $this->lang,
				'profitTotal'=> $profitTotal,
				'profitYear'=> $profitYear,
				'profitMonth'=> $profitMonth,
				'profitYearCompare'	=> $profitYearCompare,
				'profitMonthCompare'=> $profitMonthCompare
			));

			$this->template->display('Admin/Index/index.tpl');
		}
	}
?>
