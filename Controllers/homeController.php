<?php namespace Controllers;

	use Models\Brand as Brand;
	use Models\Model as Model;
	use Models\Country as Country;
	use Models\Company as Company;
	use Models\Service as Service;
	use Models\Order as Order;
	use Models\Language as Language;
	use Libs\Validation as Validation;
	use Libs\Mail as Mail;

	class homeController
	{
		private $template;
		private $brand;
		private $model;
		private $country;
		private $company;
		private $service;
		private $order;
		private $validation;
		private $mail;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->template = new \Smarty();
			$this->brand = new Brand();
			$this->model = new Model();
			$this->country = new Country();
			$this->company = new Company();
			$this->service = new Service();
			$this->order = new Order();
			$this->validation = new Validation();
			$this->mail = new Mail();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			//var_dump($_SESSION);
			$this->brand->set('status', '1');
			$this->brand->set('order', 'position');
			$this->brand->set('sort', 'asc');
			$brands = $this->brand->search();
			$languages = $this->language->toList();

			$this->template->assign(array(
				'nav'		=> 'home',
				'lang'		=> $this->lang,
				'title'		=> $this->lang['Home'],
				'languages'	=> $languages,
				'brands'	=> $brands
			));

			$this->template->display('Home/index.tpl');
		}

		public function models($id = null)
		{
			if ($id)
			{
				$this->brand->set('id', $id);
				$brand = $this->brand->view();
				$this->model->set('id_brand', $id);
				$models = $this->model->search();

				$title = $brand['name'] . ': ' . $this->lang['Models'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'nav'		=> 'home',
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'title'		=> $title,
					'brand'		=> $brand,
					'models'	=> $models
				));

				$this->template->display('Home/models.tpl');
			}
			else
			{
				header("Location: " . URL);
			}
		}

		public function form($id_brand, $id_model = null)
		{
			if (!$_POST)
			{
				if (isset($_GET['data']) and !empty($_GET['data']))
				{
					$this->order->set('tracking', $_GET['data']);
					$o = $this->order->view();

					$this->template->assign(array(
						'name'		=> $order['name'],
						'email'		=> $order['email'],
						'imei'		=> $order['imei'],
						'mep'		=> $order['mep'],
						'prd'		=> $order['prd'],
						'id_country'=> $order['id_country'],
						'id_company'=> $order['id_company']
					));
				}
				if ($id_brand)
				{
					$m = null;
					$countries = null;

					$this->brand->set('id', $id_brand);
					// Existe la marca?
					if ($brand = $this->brand->view())
					{
						// Marca pide modelo?
						if ($brand['showModel'])
						{
							// Tengo un modelo?
							if ($id_model)
							{
								// Chequeo si existe ese modelo
								$this->model->set('id', $id_model);
								$this->model->set('id_brand', $id_brand);

								// Si el modelo NO existe -> Vuelvo al inicio
								if (!$m = $this->model->search())
								{
									header("Location: " . URL);
								} else {
								}
							} else {
								header("Location: " . URL);
							}
						}

						if ($brand['showCountry'] or $brand['showCompany'] or $m[0]['showCountry'] or $m[0]['showCompany'])
						{
							$countries = $this->country->toListCompanies();
						}
						$languages = $this->language->toList();

						$this->template->assign(array(
							'nav'		=> 'home',
							'lang'		=> $this->lang,
							'title'		=> $this->lang['UnlockingForm'],
							'languages'	=> $languages,
							'brand'		=> $brand,
							'model'		=> $m[0],
							'countries' => $countries
						));

						$this->template->display('Home/form.tpl');
					}
					else
					{
						header("Location: " . URL);
					}
				}
				else
				{
					header("Location: " . URL);
				}
			}
			else
			{
				if (isset($_POST['brand']) and !empty($_POST['brand']) and $this->validation->numberValidate($_POST['brand']) and
					isset($_POST['email']) and !empty($_POST['email']) and $this->validation->emailValidate($_POST['email']) and
					isset($_POST['imei']) and !empty($_POST['imei']) and $this->validation->imeiValidate($_POST['imei']) and
					isset($_POST['name']) and !empty($_POST['name']) and $this->validation->nameValidate($_POST['name']))
				{
					$id_brand = $_POST['brand'];
					$email = $_POST['email'];
					$imei = $_POST['imei'];
					$name = $_POST['name'];

					$this->brand->set('id', $id_brand);
					$m = null;

					// Existe la marca?
					if ($brand = $this->brand->view())
					{
						$this->service->set('brand', $id_brand);

						// Marca pide modelo?
						if ($brand['showModel'])
						{
							// Tengo un modelo?
							if (isset($_POST['model']) and !empty($_POST['model']) and $this->validation->numberValidate($_POST['model']))
							{
								$id_model = $_POST['model'];

								// Chequeo si existe ese modelo
								$this->model->set('id', $id_model);
								$this->model->set('id_brand', $id_brand);

								// Si el modelo NO existe -> Vuelvo al inicio
								if ($m = $this->model->search())
								{
									$this->service->set('model', $id_model);
								} else {
									die($this->lang['ModelDoesntExists']);
								}
							} else {
								die($this->lang['MissingData']);
							}
						}

						// Si se pide el pais, se asegura que exista
						if ((isset($brand['showCountry']) and $brand['showCountry']) or (isset($m[0]['showCountry']) and $m[0]['showCountry']))
						{
							if (isset($_POST['country']) and !empty($_POST['country']) and $this->validation->numberValidate($_POST['country'])) {
								$id_country = $_POST['country'];
								$this->service->set('country', $id_country);
							} else {
								die($this->lang['MissingData']);
							}
						}

						// Si se pide la empresa, se asegura que exista
						if ((isset($brand['showCompany']) and $brand['showCompany']) or (isset($m[0]['showCompany']) and $m[0]['showCompany']))
						{
							if (isset($_POST['company']) and !empty($_POST['company']) and $this->validation->numberValidate($_POST['company']))
							{
								$id_company = $_POST['company'];
								$this->service->set('company', $id_company);
							} else {
								die($this->lang['MissingData']);
							}
						}

						if (isset($_POST['prd']) and !empty($_POST['prd'])) $prd = $_POST['prd'];
						if (isset($_POST['mep']) and !empty($_POST['mep'])) $mep = $_POST['mep'];

						$this->service->set('status', '1');
						$services = $this->service->search();
						if ($c = count($services))
						{
							if ($c == 1)
							{
								$ip = $_SERVER['REMOTE_ADDR'];

								// Cargar datos del servicio
								$this->order->set('id_service', $services[0]['id']);
								$this->order->set('service_price', $services[0]['price']);
								$this->order->set('service_delay', $services[0]['delay']);
								$this->order->set('service_delay_type', $services[0]['id_delay_type']);

								// Cargar datos del cliente
								$this->order->set('ip', $ip);
								$this->order->set('name', $name);
								$this->order->set('email', $email);
								$this->order->set('imei', $imei);
								$this->order->set('id_brand', $id_brand);
								if (isset($id_model) and !empty($id_model)) $this->order->set('id_model', $id_model);
								if (isset($id_country) and !empty($id_country)) $this->order->set('id_country', $id_country);
								if (isset($id_company) and !empty($id_company)) $this->order->set('id_company', $id_company);
								if (isset($prd) and !empty($prd)) $this->order->set('prd', $prd);
								if (isset($mep) and !empty($mep)) $this->order->set('mep', $mep);

								do {
									$tracking = $this->generateTrackingCode(6);
									$this->order->set('tracking', $tracking);
								} while ($this->order->add());

								$return = json_encode(array(
									'response' => '1',
									'tracking' => $tracking
								));
								die($return);
							}
							else
							{
								// Hay 2 o mas servicios disponibles

								$_SESSION['form']['name'] = $_POST['name'];
								$_SESSION['form']['email'] = $_POST['email'];
								$_SESSION['form']['imei'] = $_POST['imei'];
								$_SESSION['form']['id_brand'] = $_POST['brand'];
								if (isset($_POST['model']) and !empty($_POST['model'])) $_SESSION['form']['id_model'] = $_POST['model'];
								if (isset($_POST['country']) and !empty($_POST['country'])) $_SESSION['form']['id_country'] = $_POST['country'];
								if (isset($_POST['company']) and !empty($_POST['company'])) $_SESSION['form']['id_company'] = $_POST['company'];
								if (isset($_POST['prd']) and !empty($_POST['prd'])) $_SESSION['form']['prd'] = $_POST['prd'];
								if (isset($_POST['mep']) and !empty($_POST['mep'])) $_SESSION['form']['mep'] = $_POST['mep'];

								$_SESSION['services'] = $services;

								$return = json_encode(array(
									'response' => '2'
								));
								die($return);
							}
						}
						else
						{
							$return = json_encode(array(
								'response' => '0'
							));
							die($return);
						}

						// Hay que chequear que exista el servicio.
						// Hay que contar los servicios, ver si pasa directo al resumen (en caso de 1)
						// En caso de que sea un solo servicio se cargan los datos y se los guarda en la base de datos
						// En caso de contar mas de un servicio, hay que mostrarlos
							// Guardo los datos del pedido? antes de seleccionar el servicio?

					} else {
						// echo "no existe la marca";
						die($this->lang['BrandDoesntExists']);
					}

				} else {
					// echo 'faltan datos basicos';
					die($this->lang['MissingData']);
				}
			}
		}

		public function services()
		{
			if (isset($_SESSION['services']) and !empty($_SESSION['services']) and !$_POST)
			{
				$languages = $this->language->toList();
				$this->template->assign(array(
					'nav'		=> 'home',
					'lang'		=> $this->lang,
					'title'		=> $this->lang['Services'],
					'languages'	=> $languages,
					'services'	=> $_SESSION['services']
				));

				unset($_SESSION['services']);

				$this->template->display('Home/services.tpl');
			}
			else if ($_POST['id_service'])
			{
				$this->service->set('id', $_POST['id_service']);
				$service = $this->service->view();

				$ip = $_SERVER['REMOTE_ADDR'];

				// Cargar datos del servicio
				$this->order->set('id_service', $service['id']);
				$this->order->set('service_price', $service['price']);
				$this->order->set('service_delay', $service['delay']);
				$this->order->set('service_delay_type', $service['id_delay_type']);

				// Cargar datos del cliente
				$this->order->set('ip', $ip);
				$this->order->set('name', $_SESSION['form']['name']);
				$this->order->set('email', $_SESSION['form']['email']);
				$this->order->set('imei', $_SESSION['form']['imei']);
				$this->order->set('id_brand', $_SESSION['form']['id_brand']);
				if (isset($_SESSION['form']['id_model']) and !empty($_SESSION['form']['id_model'])) $this->order->set('id_model', $_SESSION['form']['id_model']);
				if (isset($_SESSION['form']['id_country']) and !empty($_SESSION['form']['id_country'])) $this->order->set('id_country', $_SESSION['form']['id_country']);
				if (isset($_SESSION['form']['id_company']) and !empty($_SESSION['form']['id_company'])) $this->order->set('id_company', $_SESSION['form']['id_company']);
				if (isset($_SESSION['form']['prd']) and !empty($_SESSION['form']['prd'])) $this->order->set('prd', $_SESSION['form']['prd']);
				if (isset($_SESSION['form']['mep']) and !empty($_SESSION['form']['mep'])) $this->order->set('mep', $_SESSION['form']['mep']);

				do {
					$tracking = $this->generateTrackingCode(6);
					$this->order->set('tracking', $tracking);
				} while ($this->order->add());

				unset($_SESSION['form']);

				$return = json_encode(array(
					'response' => '1',
					'tracking' => $tracking
				));
				die($return);
			}
			else
			{
				header("Location: " . URL);
			}
		}

		public function resume($tracking)
		{
			$this->mail->pending($tracking);
			//$this->mail->paid($tracking);

			$this->order->set('tracking', $tracking);
			if ($order = $this->order->view() and $order['id_status'] == '1')
			{
				$languages = $this->language->toList();
				$this->template->assign(array(
					'nav'		=> 'home',
					'lang'		=> $this->lang,
					'title'		=> $this->lang['OrderResume'],
					'languages'	=> $languages,
					'order'		=> $order
				));

				$this->template->display('Home/resume.tpl');
			}
			else
			{
				header("Location: " . URL);
			}

		}

		public function payment($tracking)
		{
			$this->order->set('tracking', $tracking);
			if ($order = $this->order->view() and $order['id_status'] == 1)
			{
				$languages = $this->language->toList();

				$this->template->assign(array(
					'nav'		=> 'home',
					'lang'		=> $this->lang,
					'title'		=> $this->lang['PaymentMethod'],
					'languages'	=> $languages,
					'order'		=> $order
				));

				$this->template->display('Home/payment.tpl');
			}
			else
			{
				header("Location: " . URL);
			}

		}

		public function finished($tracking)
		{
			echo 'FALTA TERMINAR';
		}

		public function language($short_name)
		{
			if(isset($short_name) and !empty($short_name))
			{
				$_SESSION['language'] = strtolower($short_name);
				//var_dump($_SESSION);
				die('1');
			}
			else
			{
				die($this->lang['ErrorOcurred']);
			}
		}

		private function generateTrackingCode($length, $numbers = true, $simbols = false)
		{
		    $hash = '';

		    $abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    if ($numbers) $abc .= '1234567890';
		    if ($simbols) $abc .= '.-,@#~/';

		    for ($i=0; $i<$length; $i++)
		    {
		        $rand = rand(1, strlen($abc)) - 1;
		        $hash .= substr($abc, $rand, 1);
		    }

		    return $hash;
		}
	}
?>
