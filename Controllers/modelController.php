<?php namespace Controllers;

	//Definiciones
	use Models\Model as Model;
	use Models\Brand as Brand;
	use Models\Language as Language;
	use Models\User as User;

	class modelController
	{
		private $model;
		private $brand;
		private $template;
		private $user;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->model = new Model();
			$this->brand = new Brand();
			$this->user = new User();
			$this->template = new \Smarty();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(2);
			
			$languages = $this->language->toList();
			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['ModelRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['ModelAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'noimage')
				{
					$this->template->assign('message', $this->lang['ModelAddedNoImage']);
					$this->template->assign('messageColor', 'yellow darken-2');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['ModelEdited']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->model->set('name', $_GET['searchName']);
				if (isset($_GET['searchBrand']) and !empty($_GET['searchBrand'])) $this->model->set('id_brand', $_GET['searchBrand']);
				if (isset($_GET['searchPosition']) and !empty($_GET['searchPosition'])) $this->model->set('position', $_GET['searchPosition']);
				if (isset($_GET['searchImage']) and ($_GET['searchImage'] === '1' or $_GET['searchImage'] === '0')) $this->model->set('img', $_GET['searchImage']);
				if (isset($_GET['searchStatus']) and ($_GET['searchStatus'] === '1' or $_GET['searchStatus'] === '0')) $this->model->set('status', $_GET['searchStatus']);
				if (isset($_GET['searchCountry']) and ($_GET['searchCountry'] === '1' or $_GET['searchCountry'] === '0')) $this->model->set('showCountry', $_GET['searchCountry']);
				if (isset($_GET['searchCompany']) and ($_GET['searchCompany'] === '1' or $_GET['searchCompany'] === '0')) $this->model->set('showCompany', $_GET['searchCompany']);
				if (isset($_GET['searchPRD']) and ($_GET['searchPRD'] === '1' or $_GET['searchPRD'] === '0')) $this->model->set('showPRD', $_GET['searchPRD']);
				if (isset($_GET['searchMEP']) and ($_GET['searchMEP'] === '1' or $_GET['searchMEP'] === '0')) $this->model->set('showMEP', $_GET['searchMEP']);

				if (isset($_GET['order']) and !empty($_GET['order'])) $this->model->set('order', $_GET['order']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->model->set('sort', $_GET['sort']);

				$list = $this->model->search();
				$listBrands = $this->brand->toList();

				$this->template->assign(array(
					'title'		=> $this->lang['Models'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'models',
					'list' 		=> $list,
					'listBrands'=> $listBrands
				));

				$this->template->display('Admin/Model/_list.tpl');
				die();

			} else {

				$list = $this->model->toList();
				$listBrands = $this->brand->toList();

				$this->template->assign(array(
					'title'		=> $this->lang['Models'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'models',
					'list' 		=> $list,
					'listBrands'=> $listBrands
				));
			}

			$this->template->display('Admin/Model/index.tpl');
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(15);
			
			$listBrands = $this->brand->toList();

			if ($_POST)
			{
				if (
					isset($_POST['brand']) and 
					isset($_POST['name']) and !empty($_POST['name']) and 
					isset($_POST['status']) and 
					isset($_POST['position']) and !empty($_POST['position']) and
					isset($_POST['showCountry']) and 
					isset($_POST['showCompany']) and 
					isset($_POST['showPRD']) and 
					isset($_POST['showMEP'])
					)
				{
					$status = (($_POST['status']) ? 1 : 0);
					$showCountry = (($_POST['showCountry']) == 'true' ? 1 : 0);
					$showCompany = (($_POST['showCompany']) == 'true' ? 1 : 0);
					$showPRD = (($_POST['showPRD']) == 'true' ? 1 : 0);
					$showMEP = (($_POST['showMEP']) == 'true' ? 1 : 0);

					//Carga todo el POST en la clase
					$this->model->set('id_brand', $_POST['brand']);
					$this->model->set('name', $_POST['name']);
					$this->model->set('status', $status);
					$this->model->set('position', $_POST['position']);
					$this->model->set('showCountry', $showCountry);
					$this->model->set('showCompany', $showCompany);
					$this->model->set('showPRD', $showPRD);
					$this->model->set('showMEP', $showMEP);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->model->set('img', end($temp));

							//Guarda y obtengo el id para asignarselo al nombre en el server
							$id = $this->model->add();
							$this->model->set('id', $id);

							$fileName = $this->model->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "models" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}

						die('1'); //Modelo agregada con imagen
					}

					//Guarda en la base de datos
					$this->model->add();

					die('2'); //Modelo agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
			else
			{
				$title = $this->lang['Add'] . ' ' . $this->lang['Model'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'models',
					'listBrands'=> $listBrands
				));

				$this->template->display('Admin/Model/item.tpl');
				
			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(16);
			
			if (!$_POST)
			{
				$this->model->set('id', $id);
				$model = $this->model->view();
				$listBrands = $this->brand->toList();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Model'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'lang'	=> $this->lang,
					'languages'	=> $languages,
					'model'	=> $model,
					'title' => $title,
					'nav'	=> 'models',
					'listBrands' => $listBrands
				));

				$this->template->display('Admin/Model/item.tpl');
			}
			else
			{
				if (
					isset($_POST['brand']) and 
					isset($_POST['name']) and !empty($_POST['name']) and 
					isset($_POST['status']) and 
					isset($_POST['position']) and !empty($_POST['position']) and
					isset($_POST['showModel']) and 
					isset($_POST['showCountry']) and 
					isset($_POST['showCompany']) and 
					isset($_POST['showPRD']) and 
					isset($_POST['showMEP'])
					)
				{
					$status = (($_POST['status']) ? 1 : 0);
					$showModel = (($_POST['showModel']) == 'true' ? 1 : 0);
					$showCountry = (($_POST['showCountry']) == 'true' ? 1 : 0);
					$showCompany = (($_POST['showCompany']) == 'true' ? 1 : 0);
					$showPRD = (($_POST['showPRD']) == 'true' ? 1 : 0);
					$showMEP = (($_POST['showMEP']) == 'true' ? 1 : 0);

					//Carga todo el POST en la clase
					$this->model->set('id_brand', $_POST['brand']);
					$this->model->set('name', $_POST['name']);
					$this->model->set('status', $status);
					$this->model->set('position', $_POST['position']);
					$this->model->set('showCountry', $showCountry);
					$this->model->set('showCompany', $showCompany);
					$this->model->set('showPRD', $showPRD);
					$this->model->set('showMEP', $showMEP);
					$this->model->set('img', $_GET['img']);

					$this->model->set('id', $id);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->model->set('img', end($temp));

							$fileName = $this->model->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "models" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}
					}

					//Guarda en la base de datos
					$this->model->edit();

					die('1'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(17);
			
			$this->model->set('id', $id);
			$this->model->delete();

			header("Location: " . URL . "model/?message=delete");
		}

		public function export()
		{
			$this->user->checkSession();
			echo 'falta terminar';
		}
	}

?>