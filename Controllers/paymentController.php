<?php namespace Controllers;
/**
* Esta clase se encarga de recibir y administrar los pagos
*/
	use Models\Order as Order;
	use Models\Service as Service;
	use Models\Language as Language;
	use Libs\MercadoPago as MercadoPago;

	class paymentController
	{
		private $order;
		private $service;
		private $mercadoPago;

		private $payPal;
		private $payPalApiContext;
		private $paypalOAuthTokenCredential;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->order = new Order();
			$this->service = new Service();
			$this->mercadoPago = new MercadoPago(MP_CLIENT_ID, MP_CLIENT_SECRET);

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		// MERCADOPAGO ------------------------------------------------------------------------------------------------

		public function mercadopago($tracking)
		{
			if ($_GET)
			{
				$this->order->set('tracking', $tracking);
				if (!$o = $this->order->view()) 	die($this->lang['ErrorOcurred']); //ERROR: El pedido es inexistente
				$this->service->set('id', $o['id_service']);
				if (!$s = $this->service->view()) 	die($this->lang['ErrorOcurred']); //ERROR: El servicio es inexistente

				if ($o['id_status'] != 1 or !$s['status'] or $o['service_price'] != $s['price']) 	die($this->lang['ErrorOcurred']);
				//ERROR: El pedido no esta pendiente de pago o el servicio no esta activo o el precio no coincide entre pedido y servicio
				$preference_data = array(
					"items" => array(
						array(
							"title" 		=> 'NeoUnlock - '.$this->lang['UnlockingService'].' '.$o['brand'].' '.$o['tracking'],
							"quantity" 		=> 1,
							"currency_id" 	=> 'USD', // Available currencies at: https://api.mercadopago.com/currencies
							"unit_price" 	=> (float)$s['price']
						)
					),

					"payer" => array(
						'name' 	=> $o['name'],
						'email'	=> $o['email']
					),

					"brack_urls" => array(
						'success'	=> URL.'home/finished/'.$o['tracking'],
						'pending'	=> URL.'home/finished/'.$o['tracking'],
						'failure'	=> URL.'home/finished/'.$o['tracking']
					),
					"external_reference" => $o['tracking'],
					"notification_url"	=> URL.'payment/mercadopago_request'
				);
				$preference = $this->mercadoPago->create_preference($preference_data);

				// var_dump($preference);

				$mp = (DEV) ? $preference['response']['sandbox_init_point'] : $preference['response']['init_point'];
				die($mp);
			}
		}

		public function mercadopago_request()
		{
			try
			{
				//ERROR: 400
				if (!isset($_GET["id"], $_GET["topic"]) || !ctype_digit($_GET["id"]))
				{
					http_response_code(400);
					die($this->lang['ErrorOcurred']);
				}

				$payment_data = $this->mercadoPago->get_payment($_REQUEST['id']); // Busco el pedido que se pagó

				// var_dump($payment_data['response']);

				if (!isset($payment_data['status']) or $payment_data['status'] != 200) die($this->lang['ErrorOcurred']);
				// ERROR: El pedido no esta pagado

				$this->order->set('tracking', $payment_data['response']['collection']['external_reference']);

				if (!$this->order->view()) die($this->lang['ErrorOcurred']); // ERROR: El pedido no existe

				$this->order->set('status', '3'); // 3 = Pagado
				$this->order->editStatus();
			}
			catch (Exception $e)
			{
				echo $e;
			}
		}

		public function mp_test()
		{
			echo "Area de testing";
		}

		// PAYPAL -----------------------------------------------------------------------------------------------------------

		public function paypal($tracking)
		{
			$this->order->set('tracking', $tracking);
			if (!$o = $this->order->view()) 	die($this->lang['ErrorOcurred']); //ERROR: El pedido es inexistente
			$this->service->set('id', $o['id_service']);
			if (!$s = $this->service->view()) 	die($this->lang['ErrorOcurred']); //ERROR: El servicio es inexistente

			if ($o['id_status'] != 1 or !$s['status'] or $o['service_price'] != $s['price']) 	die($this->lang['ErrorOcurred']);
			//ERROR: El pedido no esta pendiente de pago o el servicio no esta activo o el precio no coincide entre pedido y servicio

			// Paypal querystring
			$paypal_qs = '';
			$paypal_qs .= '&business='.PP_EMAIL;
			$paypal_qs .= '&item_name='.'NeoUnlock - '.$this->lang['UnlockingService'].' '.$o['brand'].' '.$o['tracking'];
			$paypal_qs .= '&item_number='.$o['id_service'];
			$paypal_qs .= '&amount='.$o['service_price'];
			$paypal_qs .= '&currency_code=USD';
			$paypal_qs .= '&quantity=1';
			$paypal_qs .= '&cbt=Neounlock';
			$paypal_qs .= '&notify_url='.urlencode(URL.'payment/paypal_request');
			$paypal_qs .= '&invoice='.$o['tracking'];
			$paypal_qs .= '&rm=2';
			$paypal_qs .= '&return='.urlencode(URL.'home/finished/'.$o['tracking']);
			$paypal_qs .= '&cancel_return='.urlencode(URL.'home/payment/'.$o['tracking']);
			$paypal_qs .= '&image_url='.urlencode(URL.'Views/img/logo.png');
			//$paypal_qs .= '&lc='.$_SESSION['language']; // Pais (no idioma)

			$pp = (DEV) ? 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick' : 'https://www.paypal.com/cgi-bin/webscr?cmd=_xclick';
			$pp .= $paypal_qs;

			header('Location: '.$pp);
		}

		public function paypal_request()
		{
			$this->headers = "From: " . strip_tags(EMAIL) . "\r\n";
			$this->headers .= "Reply-To: ". strip_tags(EMAIL) . "\r\n";
			$this->headers .= "MIME-Version: 1.0\r\n";
			$this->headers .= "Content-Type: text/html; charset=UTF-8\r\n";

			mail(
				EMAIL_CLIENT,
				'se recibio un pago por paypal - NeoUnlock',
				var_dump($_POST),
				$this->headers
			);

			die('1');
		}
	}
?>
