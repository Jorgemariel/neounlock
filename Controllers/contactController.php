<?php namespace Controllers;

	use Models\Language as Language;
	use Models\User as User;

	use Libs\Mail as Mail;

	class contactController
	{
		private $template;
		private $language;
		private $lang;
		private $mail;

		public function __construct()
		{
			$this->mail = new Mail();

			$this->template = new \Smarty();
			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$languages = $this->language->toList();

			$this->template->assign(array(
				'tabTitle'	=> 'NeoUnlock',
				'title'		=> $this->lang['Contact'],
        'nav'		=> 'contact',
				'languages'	=> $languages,
				'lang'		=> $this->lang
			));

			$this->template->display('Contact/index.tpl');
		}

		public function send()
		{
			if(!$_POST)
			{
				die($this->lang['ErrorOcurred']);
			}
			else if(
				!isset($_POST['email']) or empty($_POST['email']) or
				!isset($_POST['textarea']) or empty($_POST['textarea'])
				)
			{
				die($this->lang['MissingData']);
			}
			else
			{
				$email = $_POST['email'];
				$text = $_POST['textarea'];

				$this->mail->contact($email, $text);

				die('1');
			}
		}
	}
?>
