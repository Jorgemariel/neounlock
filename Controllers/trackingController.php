<?php namespace Controllers;

	use Models\Language as Language;
	use Models\Order as Order;

	class trackingController
	{
		private $template;
		private $language;
		private $lang;
		private $order;

		public function __construct()
		{
			$this->template = new \Smarty();
			$this->order = new Order();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function resume($o)
		{
			// var_dump($_SESSION);
			if(!$o) header("Location: " . URL ); // Si no se cargo la variable
			$o = trim($o);
			if(strlen($o) != 6) header("Location: " . URL ); // Si el codigo de seguimiento no es de 6 digitos

			$languages = $this->language->toList();
			$this->order->set('tracking', $o);

			if (!$data = $this->order->view()) { // Si no se encuentra el pedido
				$this->template->assign(array(
					'nav'		=> 'tracking',
					'title'		=> $this->lang['Tracking'].' '.strtoupper($o),
					'languages'	=> $languages,
					'lang'		=> $this->lang,
					'tracking' => $o
				));

				$this->template->display('Tracking/not_found.tpl');
				die();
			}

			$data['delay_time'] = $this->getDelay($data['payment_date'], $data['delivery_date']);
			$data['time_left'] = $this->getTimeLeft($data['payment_date'], $data['service_delay'], $data['service_delay_type']);

			$this->template->assign(array(
				'nav'		=> 'tracking',
				'title'		=> $this->lang['Tracking'].' '.strtoupper($o),
				'languages'	=> $languages,
				'lang'		=> $this->lang,
				'order'		=> $data
			));

			$view = 'error';
			switch ($data['id_status']) {
				case '1':
					// Pendiente
					$view = 'pending';
					break;
				case '2':
					// Expirado
					$view = 'expired';
					break;
				case '3':
					// Pagado
					$view = 'paid';
					break;
				case '4':
					// Completado
					$view = 'completed';
					break;
				case '5':
					// Ocurrio un problema
					$view = 'problem';
					break;
				case '6':
					// Cancelado
					$view = 'cancelled';
					break;

				default:
					//
					$view = 'error';
					break;
			}

			$this->template->display('Tracking/'.$view.'.tpl');
		}

		private function getDelay($payment, $deliver) {

			//var_dump($payment);

			$datetime1 = date_create($payment);

			//var_dump($datetime1);

			$datetime2 = date_create($deliver);
			$interval = date_diff($datetime1, $datetime2);
			$result = '';

			if ($interval->format('%a') > 0) $result .= $interval->format('%a') . ' ' . strtolower($this->lang['Days'] . ' ');
			if ($interval->format('%h') > 0) $result .= $interval->format('%h') . ' ' . strtolower($this->lang['Hours'] . ' ');
			if ($interval->format('%m') > 0) $result .= $interval->format('%m') . ' ' . strtolower($this->lang['Minutes']);

			return $result;
		}

		private function getTimeLeft($payment, $service, $type) {
			date_default_timezone_set("America/Argentina/Cordoba");
			// var_dump(date_default_timezone_get());
			// echo '<br/>';
			// print_r($payment);
			// echo '<br/>';

			$WorkingDays = false;
			switch ($type) {
				case '1':
					// Minutes
					$service = $service * 60;
					break;
				case '2':
					// Hours
					$service = $service * 60 * 60;
					break;
				case '3':
					// WorkingDays
					$days = $service;
					$service = $service * 60 * 60 * 24;
					$count = 0;

					for ($i=0; $i < $days; $i++) {
						$dayofweek = date('w', strtotime($payment)+(86400*$i));
						if ($dayofweek == 0 || $dayofweek == 6) {
							$count++;
						}
					}

					$payment = date("Y-m-d", strtotime($payment)+(86400*$count)+86340);
					//print_r($payment);

					break;
				case '4':
					// Days
					$service = $service * 60 * 60 * 24;
					$payment = date("Y-m-d", strtotime($payment)+86340);
					//print_r($payment);
					break;

				default:
					//
					break;
			}

			// print_r("\n Duracion del servicio: ".$service);
			// echo '<br/>';

			$paymentDateInt = strtotime($payment);

			// print_r("\n Fecha de pago: ".$payment.' - '.$paymentDateInt);
			// echo '<br/>';

			$datetime1 = $paymentDateInt + $service;
			$datetime2 = time();
			// print_r("\n Fecha de entrega: ".date("Y-m-d H:i:s", $datetime1).' - '.$datetime1);
			// echo '<br/>';
			// print_r("\n Fecha de actual: ".date("Y-m-d H:i:s", $datetime2).' - '.$datetime2);
			// echo '<br/>';

			$diff = $datetime1 - $datetime2;

			$result = '';
			$days = floor($diff / (60*60*24));
			$hours = floor(($diff - $days*60*60*24)/ (60*60));
			$minutes = floor(($diff - $days*60*60*24 - $hours*60*60)/ 60);

			if ($days > 0) $result .= $days .' '.strtolower($this->lang['Days']).' ';
			if ($hours > 0) $result .= $hours .' '.strtolower($this->lang['Hours']).' ';
			if ($minutes > 0) $result .= $minutes .' '.strtolower($this->lang['Minutes']).' ';

			return $result;

		}
	}
?>
