<?php namespace Controllers;

	use Models\Language as Language;
	use Models\User as User;

	class loginController
	{
		private $user;
		private $template;
		private $language;
		private $lang;
		
		public function __construct()
		{
			$this->template = new \Smarty();

			$this->user = new User();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			// var_dump($_SESSION);
			if(!$_POST) {
				if(isset($_SESSION['user'])) header("Location: " . URL . "admin");
				$languages = $this->language->toList();
				
				$this->template->assign(array(
					'tabTitle'	=> 'Admin NeoUnlock',
					'title'		=> 'Admin',
					'languages'	=> $languages,
					'lang'		=> $this->lang
				));

				$this->template->display('Admin/Login/index.tpl');
			} else {
				//var_dump($_POST);

				if (
					isset($_POST['username']) and !empty($_POST['username']) and 
					isset($_POST['password']) and !empty($_POST['password'])
					)
				{
					$this->user->set('username', $_POST['username']);
					$this->user->set('password', $_POST['password']);

					if($this->user->login()) {
						die('1');
					} else {
						die($this->lang['WrongUserPassword']);
					}

				} else {
					die($this->lang['MissingData']);
				}
			}
		}

		public function logout() {
			$this->user->logout();
			header("Location: " . URL);
		}
	}
?>