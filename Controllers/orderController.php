<?php namespace Controllers;

	//Definiciones
	use Models\Order as Order;
	use Models\Language as Language;
	use Models\Brand as Brand;
	use Models\Model as Model;
	use Models\Country as Country;
	use Models\Company as Company;
	use Models\Service as Service;
	use Models\User as User;

	class orderController
	{
		private $order;
		private $brand;
		private $model;
		private $country;
		private $company;
		private $service;
		private $template;
		private $user;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->order = new Order();
			$this->brand = new Brand();
			$this->model = new Model();
			$this->country = new Country();
			$this->company = new Company();
			$this->service = new Service();
			$this->user = new User();
			$this->template = new \Smarty();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(8);
			
			$languages = $this->language->toList();
			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'codes')
				{
					$this->template->assign('message', $this->lang['CodesDelivered']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['OrderEdited']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->order->set('general', $_GET['searchName']);
				if (isset($_GET['searchStatus']) and !empty($_GET['searchStatus'])) $this->order->set('id_status', $_GET['searchStatus']);
				if (isset($_GET['searchService']) and !empty($_GET['searchService'])) $this->order->set('id_service', $_GET['searchService']);
				if (isset($_GET['searchLanguage']) and !empty($_GET['searchLanguage'])) $this->order->set('id_language', $_GET['searchLanguage']);
				if (isset($_GET['searchBrand']) and !empty($_GET['searchBrand'])) $this->order->set('id_brand', $_GET['searchBrand']);
				if (isset($_GET['searchModel']) and !empty($_GET['searchModel'])) $this->order->set('id_model', $_GET['searchModel']);
				if (isset($_GET['searchCountry']) and !empty($_GET['searchCountry'])) $this->order->set('id_country', $_GET['searchCountry']);
				if (isset($_GET['searchCompany']) and !empty($_GET['searchCompany'])) $this->order->set('id_company', $_GET['searchCompany']);

				if (isset($_GET['order']) and !empty($_GET['order'])) $this->order->set('order', $_GET['order']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->order->set('sort', $_GET['sort']);

				$list = $this->order->search();

				$this->template->assign(array(
					'lang'	=> $this->lang,
					'list' 		=> $list
				));

				$this->template->display('Admin/Order/_list.tpl');

			} else {
				$status = $this->order->statusList();
				$list = $this->order->search();
				$brands = $this->brand->search();
				$models = $this->model->search();
				$countries = $this->country->search();
				$companies = $this->company->search();
				$services = $this->service->search();
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $this->lang['Orders'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'orders',
					'list' 		=> $list,
					'status'	=> $status,
					'brands'	=> $brands,
					'models'	=> $models,
					'countries'	=> $countries,
					'companies'	=> $companies,
					'services'	=> $services,
					'languages'	=> $languages
				));
				$this->template->display('Admin/Order/index.tpl');
			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(27);
			
			if (!$_POST)
			{
				$this->order->set('id', $id);
				$order = $this->order->view();
				$languages = $this->language->toList();
				$brands = $this->brand->toListModels();
				$countries = $this->country->search();
				$companies = $this->company->search();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Order'] . ' ' . $order['tracking'];

				$this->template->assign(array(
					'order'		=> $order,
					'title' 	=> $title,
					'languages'	=> $languages,
					'nav'		=> 'orders',
					'lang'		=> $this->lang,
					'brands'	=> $brands,
					'countries'	=> $countries,
					'companies'	=> $companies
				));

				$this->template->display('Admin/Order/item.tpl');
			}
			else
			{
				if (
					isset($_POST['name']) and !empty($_POST['name']) and 
					isset($_POST['status']) and 
					isset($_POST['position']) and !empty($_POST['position']) and
					isset($_POST['showModel']) and 
					isset($_POST['showCountry']) and 
					isset($_POST['showCompany']) and 
					isset($_POST['showPRD']) and 
					isset($_POST['showMEP'])
					)
				{
					$status = (($_POST['status']) ? 1 : 0);
					$showModel = (($_POST['showModel']) == 'true' ? 1 : 0);
					$showCountry = (($_POST['showCountry']) == 'true' ? 1 : 0);
					$showCompany = (($_POST['showCompany']) == 'true' ? 1 : 0);
					$showPRD = (($_POST['showPRD']) == 'true' ? 1 : 0);
					$showMEP = (($_POST['showMEP']) == 'true' ? 1 : 0);

					//Carga todo el POST en la clase
					$this->order->set('name', $_POST['name']);
					$this->order->set('id_status', $status);
					$this->order->set('position', $_POST['position']);
					$this->order->set('showModel', $showModel);
					$this->order->set('showCountry', $showCountry);
					$this->order->set('showCompany', $showCompany);
					$this->order->set('showPRD', $showPRD);
					$this->order->set('showMEP', $showMEP);
					$this->order->set('img', $_GET['img']);

					$this->order->set('id', $id);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->order->set('img', end($temp));

							$fileName = $this->order->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "orders" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}
					}

					//Guarda en la base de datos
					$this->order->edit();

					die('1'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function completed($tracking)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(37);
			
			if (!$_POST) die($this->lang['MissingData']);

			$this->order->set('tracking', $tracking);
			$o = $this->order->view();

			if ($o['id_status'] != 3) die($this->lang['CheckOrderStatus']);

			if (isset($_POST['code1'])) $this->order->set('code1', $_POST['code1']);
			if (isset($_POST['code2'])) $this->order->set('code2', $_POST['code2']);
			if (isset($_POST['code3'])) $this->order->set('code3', $_POST['code3']);
			if (isset($_POST['code4'])) $this->order->set('code4', $_POST['code4']);
			if (isset($_POST['code5'])) $this->order->set('code5', $_POST['code5']);
			if (isset($_POST['code6'])) $this->order->set('code6', $_POST['code6']);
			$this->order->set('id_status', '4');

			$this->order->editCodes();

			die('1');
		}

		public function export()
		{
			$this->user->checkSession();
			echo 'falta terminar';
		}
	}

?>