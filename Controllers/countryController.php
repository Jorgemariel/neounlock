<?php namespace Controllers;

	//Definiciones
	use Models\Country as Country;
	use Models\Language as Language;
	use Models\User as User;

	class countryController
	{
		private $country;
		private $template;
		private $user;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->country = new Country();
			$this->user = new User();
			$this->template = new \Smarty();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(3);
			
			$languages = $this->language->toList();

			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['CountryRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['CountryAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'noimage')
				{
					$this->template->assign('message', $this->lang['CountryAddedNoImage']);
					$this->template->assign('messageColor', 'yellow darken-2');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['CountryEdited']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'companies')
				{
					$this->template->assign('message', $this->lang['CompaniesUpdated']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->country->set('name_en', $_GET['searchName']);
				if (isset($_GET['searchLanguage']) and !empty($_GET['searchLanguage'])) $this->country->set('id_language', $_GET['searchLanguage']);
				if (isset($_GET['searchImage']) and ($_GET['searchImage'] === '1' or $_GET['searchImage'] === '0')) $this->country->set('img', $_GET['searchImage']);
				if (isset($_GET['searchHighlighted']) and ($_GET['searchHighlighted'] === '1' or $_GET['searchHighlighted'] === '0')) $this->country->set('highlighted', $_GET['searchHighlighted']);

				if (isset($_GET['order']) and !empty($_GET['order'])) $this->country->set('order', $_GET['order']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->country->set('sort', $_GET['sort']);

				$list = $this->country->search();

				foreach ($list as &$r) {
					$this->country->set('id', $r['id']);
					$data = $this->country->companiesPerCountry();
					$r['companies'] = $data;
				}

				$this->template->assign(array(
					'title'		=> $this->lang['Countries'],
					'lang'		=> $this->lang,
					'nav'		=> 'countries',
					'list' 		=> $list,
					'languages'	=> $languages
				));

				$this->template->display('Admin/Country/_list.tpl');

			} else {
				$list = $this->country->search();

				foreach ($list as &$r) {
					$this->country->set('id', $r['id']);
					$data = $this->country->companiesPerCountry();
					$r['companies'] = $data;
				}

				$this->template->assign(array(
					'title'		=> $this->lang['Countries'],
					'lang'		=> $this->lang,
					'nav'		=> 'countries',
					'list' 		=> $list,
					'languages'	=> $languages
				));
				$this->template->display('Admin/Country/index.tpl');
			}
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(18);
			
			if ($_POST)
			{
				if (
					isset($_POST['name_en']) and !empty($_POST['name_en']) and 
					isset($_POST['name_es']) and !empty($_POST['name_es']) and 
					isset($_POST['iso2']) and !empty($_POST['iso2']) and 
					isset($_POST['iso3']) and !empty($_POST['iso3']) and 
					isset($_POST['coin']) and !empty($_POST['coin']) and 
					isset($_POST['language']) and 
					isset($_POST['highlighted'])
					)
				{
					$highlighted = (($_POST['highlighted']) == 'true' ? 1 : 0);

					//Carga todo el POST en la clase
					$this->country->set('name_en', $_POST['name_en']);
					$this->country->set('name_es', $_POST['name_es']);
					$this->country->set('iso2', $_POST['iso2']);
					$this->country->set('iso3', $_POST['iso3']);
					$this->country->set('coin', $_POST['coin']);
					$this->country->set('id_language', $_POST['language']);
					$this->country->set('highlighted', $highlighted);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->country->set('img', end($temp));

							//Guarda y obtengo el id para asignarselo al nombre en el server
							$id = $this->country->add();
							$this->country->set('id', $id);

							$fileName = $id . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "countries" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}

						die('1'); //Marca agregada con imagen
					}

					//Guarda en la base de datos
					$this->country->add();

					die('2'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
			else
			{
				$title = $this->lang['Add'] . ' ' . $this->lang['Country'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'nav'		=> 'countries',
					'languages'	=> $languages
				));

				$this->template->display('Admin/Country/item.tpl');
				
			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(19);
			
			if (!$_POST)
			{
				$this->country->set('id', $id);
				$country = $this->country->view();
				$languages = $this->language->toList();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Country'];

				$this->template->assign(array(
					'country'	=> $country,
					'title' 	=> $title,
					'nav'		=> 'countries',
					'lang'		=> $this->lang,
					'languages'	=> $languages
				));

				$this->template->display('Admin/Country/item.tpl');
			}
			else
			{
				if (
					isset($_POST['name_en']) and !empty($_POST['name_en']) and 
					isset($_POST['name_es']) and !empty($_POST['name_es']) and 
					isset($_POST['iso2']) and !empty($_POST['iso2']) and 
					isset($_POST['iso3']) and !empty($_POST['iso3']) and 
					isset($_POST['coin']) and !empty($_POST['coin']) and 
					isset($_POST['language']) and 
					isset($_POST['highlighted'])
					)
				{
					$highlighted = (($_POST['highlighted']) == 'true' ? 1 : 0);

					//Carga todo el POST en la clase
					$this->country->set('name_en', $_POST['name_en']);
					$this->country->set('name_es', $_POST['name_es']);
					$this->country->set('iso2', $_POST['iso2']);
					$this->country->set('iso3', $_POST['iso3']);
					$this->country->set('coin', $_POST['coin']);
					$this->country->set('id_language', $_POST['language']);
					$this->country->set('highlighted', $highlighted);

					$this->country->set('id', $id);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->country->set('img', end($temp));

							$fileName = $this->country->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "countries" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}

						die('1'); //Marca agregada con imagen
					}

					//Guarda en la base de datos
					$this->country->edit();

					die('1'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(20);
			
			$this->country->set('id', $id);
			$this->country->delete();

			header("Location: " . URL . "country/?message=delete");
		}

		public function export()
		{
			$this->user->checkSession();
			echo 'falta terminar';
		}

		public function companies($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(34);
			
			if (!$_POST)
			{
				$this->country->set('id', $id);
				$country = $this->country->view();
				$companies = $this->country->companiesAllAndChecked();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Companies'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'countries',
					'companies'	=> $companies,
					'id'		=> $id,
					'countryName'	=> $country['name_en']
				));

				$this->template->display('Admin/Country/companies.tpl');
			} else {

				if (isset($_POST['companies']))
				{
					$this->country->set('id', $id);
					$this->country->updateCompanies($_POST['companies']);

					die('1');
				} else {
					die($this->lang['MissingData']);
				}
			}
		}
	}

?>