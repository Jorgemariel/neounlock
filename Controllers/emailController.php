<?php namespace Controllers;

	//Definiciones
	use Models\Email as Email;
	use Models\Language as Language;
	use Models\User as User;

	use Libs\Mail as Mail;
	use Libs\Crp as Crp; // Para encriptar

	class emailController
	{
		private $email;
		private $template;
		private $user;
		private $mail;
		private $crp;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->email = new Email();
			$this->user = new User();
			$this->mail = new Mail();
			$this->crp = new Crp();
			$this->template = new \Smarty();
			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(9);

			$languages = $this->language->toList();
			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['EmailRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['EmailAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['EmailEdited']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'brands')
				{
					$this->template->assign('message', $this->lang['CompaniesUpdated']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'emails')
				{
					$this->template->assign('message', $this->lang['EmailsUpdated']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'success')
				{
					$this->template->assign('message', $this->lang['EmailContentUpdated']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->email->set('name', $_GET['searchName']);
				if (isset($_GET['searchDescription']) and ($_GET['searchDescription'] === '1' or $_GET['searchDescription'] === '0')) $this->email->set('description', $_GET['searchDescription']);
				if (isset($_GET['searchPrice']) and !empty($_GET['searchPrice'])) $this->email->set('price', $_GET['searchPrice']);
				if (isset($_GET['searchDelay']) and !empty($_GET['searchDelay'])) $this->email->set('delay', $_GET['searchDelay']);
				if (isset($_GET['searchDelayType']) and !empty($_GET['searchDelayType'])) $this->email->set('id_delay_type', $_GET['searchDelayType']);
				if (isset($_GET['searchStatus']) and ($_GET['searchStatus'] === '1' or $_GET['searchStatus'] === '0')) $this->email->set('status', $_GET['searchStatus']);
				if (isset($_GET['searchBrand']) and !empty($_GET['searchBrand'])) $this->email->set('brand', $_GET['searchBrand']);
				if (isset($_GET['searchModel']) and !empty($_GET['searchModel'])) $this->email->set('model', $_GET['searchModel']);
				if (isset($_GET['searchCountry']) and !empty($_GET['searchCountry'])) $this->email->set('country', $_GET['searchCountry']);
				if (isset($_GET['searchCompany']) and !empty($_GET['searchCompany'])) $this->email->set('company', $_GET['searchCompany']);

				if (isset($_GET['email']) and !empty($_GET['email'])) $this->email->set('email', $_GET['email']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->email->set('sort', $_GET['sort']);

				$list = $this->email->search();

				$this->template->assign(array(
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'list' 		=> $list
				));

				$this->template->display('Admin/Email/_list.tpl');

			} else {
				$list = $this->email->toList();

				//print_r($list);

				$this->template->assign(array(
					'title'		=> $this->lang['Emails'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'emails',
					'list' 		=> $list
				));
				$this->template->display('Admin/Email/index.tpl');
			}
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(28);

			if ($_POST)
			{
				if (
					isset($_POST['name']) and !empty($_POST['name']) and
					isset($_POST['description']) and !empty($_POST['description'])
					)
				{
					//Carga todo el POST en la clase
					$this->email->set('name', $_POST['name']);
					$this->email->set('description', $_POST['description']);

					$this->email->add();

					die('1');
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
			else
			{
				$title = $this->lang['Add'] . ' ' . $this->lang['Email'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'emails'
				));

				$this->template->display('Admin/Email/item.tpl');

			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(29);

			if (!$_POST)
			{
				$this->email->set('id', $id);
				$email = $this->email->view();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Email'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'email'		=> $email,
					'title' 	=> $title,
					'nav'		=> 'emails',
					'lang'		=> $this->lang,
					'languages'	=> $languages
				));

				$this->template->display('Admin/Email/item.tpl');
			}
			else
			{
				if (
					isset($_POST['name']) and !empty($_POST['name']) and
					isset($_POST['description']) and !empty($_POST['description'])
					)
				{
					//Carga todo el POST en la clase
					$this->email->set('id', $id);
					$this->email->set('name', $_POST['name']);
					$this->email->set('description', $_POST['description']);

					$this->email->edit();

					die('1');
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(30);

			$this->email->set('id', $id);
			$this->email->delete();

			header("Location: " . URL . "email/?message=delete");
		}

		public function editContent($id_email)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(39);

			if (!$_POST || !isset($_POST['id_content']) || !isset($_POST['content']) || !isset($_POST['id_language']) || empty($_POST['id_language']))
			{
				die($this->lang['MissingData']);
			}
			else
			{
				// Decrypt el mensaje
				$content = $this->crp->decrypte($_POST['content'], 'neounlock');
				// Decodificar el mensaje
				$content = urldecode($content);

				$this->email->set('id', $id_email);
				$this->email->editContent($_POST['id_content'], $content, $_POST['id_language']);
			}
		}

		public function testContent()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(39);

			if ($_POST and isset($_POST['content']) and !empty($_POST['content']))
			{
				// Decrypt el mensaje
				$content = $this->crp->decrypte($_POST['content'], 'neounlock');
				// Decodificar el mensaje
				$content = urldecode($content);

				$this->mail->testEmail($content);
			}
			else
			{
				die("$this->lang['MissingData']");
			}
		}

		public function export()
		{
			$this->user->checkSession();
			echo 'falta terminar';
		}

		public function descriptions($id)
		{
			$this->user->checkSession(); //////////////////////////////////////////////////ver en donde se usa esto. faltan permisos
			if (!$_POST) {
				//die('2');
				$this->email->set('id', $id);
				$descriptions = $this->email->toListDescriptions();
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $this->lang['Descriptions'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'emails',
					'id'		=> $id,
					'descriptions'	=> $descriptions
				));

				$this->template->display('Admin/Email/descriptions.tpl');
			} else {
				if (isset($_POST['data']) and !empty($_POST['data']))
				{
					foreach ($_POST['data'] as $d) {
						$this->email->set('id', $id);
						$this->email->set('id_language', $d['id_language']);
						$this->email->set('description', $d['content']);
						$this->email->editDescription();
					}
					die('1');
				}
				die('Error');
			}
		}
	}

?>
