<?php namespace Controllers;

	use Models\Faq as Faq;
	use Models\Language as Language;
	use Models\User as User;

	class faqsController
	{
		private $faq;

		private $template;
		private $language;
		private $lang;

		public function __construct()
		{
			$this->faq = new Faq();

			$this->template = new \Smarty();
			$this->user = new User();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$languages = $this->language->toList();
			$this->faq->set('language', strtoupper($_SESSION['language']));

			$faqs = $this->faq->toList();

			$this->template->assign(array(
				'tabTitle'	=> 'NeoUnlock',
				'title'		=> 'FAQs',
				'nav'		=> 'faqs',
				'faqs'	=> $faqs,
				'languages'	=> $languages,
				'lang'		=> $this->lang
			));

			$this->template->display('Faq/index.tpl');

		}

		public function list()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(40);

			$languages = $this->language->toList();

			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['FaqRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['FaqAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['FaqEdited']);
					$this->template->assign('messageColor', 'green');
				}
			}

			$faqs = $this->faq->search();

			$this->template->assign(array(
				'title'		=> $this->lang['Questions'],
				'lang'		=> $this->lang,
				'list'	=> $faqs,
				'languages'	=> $languages,
				'nav'		=> 'faqs',
			));

			$this->template->display('Admin/Faq/index.tpl');
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(41);

			if(!$_POST) {
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $this->lang['Add'] . ' ' . $this->lang['Question'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'faqs',
				));

				$this->template->display('Admin/Faq/item.tpl');

			} else {
				if (
					isset($_POST['language']) and !empty($_POST['language']) and
					isset($_POST['status']) and
					isset($_POST['question']) and !empty($_POST['question']) and
					isset($_POST['answer']) and !empty($_POST['answer'])
					)
				{
					$this->faq->set('id_language', $_POST['language']);
					$this->faq->set('status', $_POST['status']);
					$this->faq->set('question', $_POST['question']);
					$this->faq->set('answer', $_POST['answer']);

					$this->faq->add();

					die('1');
				} else {
					die($lang['MissingData']);
				}
			}

		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(42);

			$languages = $this->language->toList();
			$this->faq->set('id', $id);

			if(!$_POST)
			{
				$faq = $this->faq->view();

				$this->template->assign(array(
					'title'		=> $this->lang['Questions'],
					'lang'		=> $this->lang,
					'faq'	=> $faq,
					'languages'	=> $languages,
					'nav'		=> 'faqs',
				));

				$this->template->display('Admin/Faq/item.tpl');
			} else {
				if (
					isset($_POST['language']) and !empty($_POST['language']) and
					isset($_POST['status']) and
					isset($_POST['question']) and !empty($_POST['question']) and
					isset($_POST['answer']) and !empty($_POST['answer'])
					)
				{
					$this->faq->set('id_language', $_POST['language']);
					$this->faq->set('status', $_POST['status']);
					$this->faq->set('question', $_POST['question']);
					$this->faq->set('answer', $_POST['answer']);

					$this->faq->edit();

					die('1');
				} else {
					die($lang['MissingData']);
				}
			}

		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(43);

			$this->faq->set('id', $id);
			$this->faq->delete();

			header("Location: " . URL . "faqs/list/?message=delete");
		}
	}
?>
