<?php namespace Controllers;

	//Definiciones
	use Models\Brand as Brand;
	use Models\Language as Language;
	use Models\User as User;

	class brandController
	{
		private $brand;
		private $template;
		private $user;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->brand = new Brand();
			$this->user = new User();
			$this->template = new \Smarty();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(1);

			$languages = $this->language->toList();
			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['BrandRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['BrandAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'noimage')
				{
					$this->template->assign('message', $this->lang['BrandAddedNoImage']);
					$this->template->assign('messageColor', 'yellow darken-2');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['BrandEdited']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->brand->set('name', $_GET['searchName']);
				if (isset($_GET['searchPosition']) and !empty($_GET['searchPosition'])) $this->brand->set('position', $_GET['searchPosition']);
				if (isset($_GET['searchImage']) and ($_GET['searchImage'] === '1' or $_GET['searchImage'] === '0')) $this->brand->set('img', $_GET['searchImage']);
				if (isset($_GET['searchStatus']) and ($_GET['searchStatus'] === '1' or $_GET['searchStatus'] === '0')) $this->brand->set('status', $_GET['searchStatus']);
				if (isset($_GET['searchModel']) and ($_GET['searchModel'] === '1' or $_GET['searchModel'] === '0')) $this->brand->set('showModel', $_GET['searchModel']);
				if (isset($_GET['searchCountry']) and ($_GET['searchCountry'] === '1' or $_GET['searchCountry'] === '0')) $this->brand->set('showCountry', $_GET['searchCountry']);
				if (isset($_GET['searchCompany']) and ($_GET['searchCompany'] === '1' or $_GET['searchCompany'] === '0')) $this->brand->set('showCompany', $_GET['searchCompany']);
				if (isset($_GET['searchPRD']) and ($_GET['searchPRD'] === '1' or $_GET['searchPRD'] === '0')) $this->brand->set('showPRD', $_GET['searchPRD']);
				if (isset($_GET['searchMEP']) and ($_GET['searchMEP'] === '1' or $_GET['searchMEP'] === '0')) $this->brand->set('showMEP', $_GET['searchMEP']);

				if (isset($_GET['order']) and !empty($_GET['order'])) $this->brand->set('order', $_GET['order']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->brand->set('sort', $_GET['sort']);

				$list = $this->brand->search();

				$this->template->assign(array(
					'title'		=> $this->lang['Brands'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'brands',
					'list' 		=> $list
				));

				$this->template->display('Admin/Brand/_list.tpl');

			} else {
				$list = $this->brand->toList();
				$this->template->assign(array(
					'title'		=> $this->lang['Brands'],
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'brands',
					'list' 		=> $list
				));
				$this->template->display('Admin/Brand/index.tpl');
			}
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(12);
			if ($_POST)
			{
				if (
					isset($_POST['name']) and !empty($_POST['name']) and
					isset($_POST['status']) and
					isset($_POST['position']) and !empty($_POST['position']) and
					isset($_POST['showModel']) and
					isset($_POST['showCountry']) and
					isset($_POST['showCompany']) and
					isset($_POST['showPRD']) and
					isset($_POST['showMEP'])
					)
				{
					$status = (($_POST['status']) ? 1 : 0);
					$showModel = (($_POST['showModel']) == 'true' ? 1 : 0);
					$showCountry = (($_POST['showCountry']) == 'true' ? 1 : 0);
					$showCompany = (($_POST['showCompany']) == 'true' ? 1 : 0);
					$showPRD = (($_POST['showPRD']) == 'true' ? 1 : 0);
					$showMEP = (($_POST['showMEP']) == 'true' ? 1 : 0);

					// Carga todo el POST en la clase
					$this->brand->set('name', $_POST['name']);
					$this->brand->set('status', $status);
					$this->brand->set('position', $_POST['position']);
					$this->brand->set('showModel', $showModel);
					$this->brand->set('showCountry', $showCountry);
					$this->brand->set('showCompany', $showCompany);
					$this->brand->set('showPRD', $showPRD);
					$this->brand->set('showMEP', $showMEP);

					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					// Chequea de que exista la imagen
					if ($_FILES)
					{
						// Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->brand->set('img', end($temp));

							// Guarda y obtengo el id para asignarselo al nombre en el server
							$id = $this->brand->add();
							$this->brand->set('id', $id);

							$fileName = $this->brand->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "brands" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}

						die('1'); // Marca agregada con imagen
					}

					// Guarda en la base de datos
					$this->brand->add();

					die('2'); // Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
			else
			{

				$title = $this->lang['Add'] . ' ' . $this->lang['Brand'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'brands'
				));

				$this->template->display('Admin/Brand/item.tpl');

			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(13);

			if (!$_POST)
			{
				$this->brand->set('id', $id);
				$brand = $this->brand->view();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Brand'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'brand'	=> $brand,
					'title' => $title,
					'languages'	=> $languages,
					'nav'	=> 'brands',
					'lang'	=> $this->lang
				));

				$this->template->display('Admin/Brand/item.tpl');
			}
			else
			{
				if (
					isset($_POST['name']) and !empty($_POST['name']) and
					isset($_POST['status']) and
					isset($_POST['position']) and !empty($_POST['position']) and
					isset($_POST['showModel']) and
					isset($_POST['showCountry']) and
					isset($_POST['showCompany']) and
					isset($_POST['showPRD']) and
					isset($_POST['showMEP'])
					)
				{
					$status = (($_POST['status']) ? 1 : 0);
					$showModel = (($_POST['showModel']) == 'true' ? 1 : 0);
					$showCountry = (($_POST['showCountry']) == 'true' ? 1 : 0);
					$showCompany = (($_POST['showCompany']) == 'true' ? 1 : 0);
					$showPRD = (($_POST['showPRD']) == 'true' ? 1 : 0);
					$showMEP = (($_POST['showMEP']) == 'true' ? 1 : 0);

					//Carga todo el POST en la clase
					$this->brand->set('name', $_POST['name']);
					$this->brand->set('status', $status);
					$this->brand->set('position', $_POST['position']);
					$this->brand->set('showModel', $showModel);
					$this->brand->set('showCountry', $showCountry);
					$this->brand->set('showCompany', $showCompany);
					$this->brand->set('showPRD', $showPRD);
					$this->brand->set('showMEP', $showMEP);
					$this->brand->set('img', $_GET['img']);

					$this->brand->set('id', $id);

					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->brand->set('img', end($temp));

							$fileName = $this->brand->get('id') . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "brands" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}
					}

					//Guarda en la base de datos
					$this->brand->edit();

					die('1'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(14);

			$this->brand->set('id', $id);
			$this->brand->delete();

			header("Location: " . URL . "brand/?message=delete");
		}

		public function export()
		{
			$this->user->checkSession();
			echo 'falta terminar';
		}
	}

?>
