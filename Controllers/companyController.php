<?php namespace Controllers;

	//Definiciones
	use Models\Company as Company;
	use Models\Language as Language;
	use Models\User as User;

	class companyController
	{
		private $company;
		private $template;
		private $user;

		private $language;
		private $lang;

		public function __construct()
		{
			$this->company = new Company();
			$this->user = new User();
			$this->template = new \Smarty();

			$this->language = new Language();
			$this->lang = $this->language->lang;
		}

		public function index()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(5);
			
			$languages = $this->language->toList();
			if (isset($_GET['message']))
			{
				if ($_GET['message'] == 'delete')
				{
					$this->template->assign('message', $this->lang['CompanyRemoved']);
					$this->template->assign('messageColor', 'red');
				}
				else if ($_GET['message'] == 'added')
				{
					$this->template->assign('message', $this->lang['CompanyAdded']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'noimage')
				{
					$this->template->assign('message', $this->lang['CompanyAddedNoImage']);
					$this->template->assign('messageColor', 'yellow darken-2');
				}
				else if ($_GET['message'] == 'edited')
				{
					$this->template->assign('message', $this->lang['CompanyEdited']);
					$this->template->assign('messageColor', 'green');
				}
				else if ($_GET['message'] == 'countries')
				{
					$this->template->assign('message', $this->lang['CountriesUpdated']);
					$this->template->assign('messageColor', 'green');
				}
			}

			if(isset($_GET['searchName']))
			{
				if (isset($_GET['searchName']) and !empty($_GET['searchName'])) $this->company->set('name', $_GET['searchName']);
				if (isset($_GET['searchImage']) and ($_GET['searchImage'] === '1' or $_GET['searchImage'] === '0')) $this->company->set('img', $_GET['searchImage']);
				if (isset($_GET['searchStatus']) and ($_GET['searchStatus'] === '1' or $_GET['searchStatus'] === '0')) $this->company->set('status', $_GET['searchStatus']);

				if (isset($_GET['order']) and !empty($_GET['order'])) $this->company->set('order', $_GET['order']);
				if (isset($_GET['sort']) and !empty($_GET['sort'])) $this->company->set('sort', $_GET['sort']);

				$list = $this->company->search();

				foreach ($list as &$r) {
					$this->company->set('id', $r['id']);
					$data = $this->company->countriesPerCompany();
					$r['countries'] = $data;
				}

				$this->template->assign(array(
					'title'		=> $this->lang['Companies'],
					'lang'		=> $this->lang,
					'nav'		=> 'companies',
					'languages'	=> $languages,
					'list' 		=> $list
				));

				$this->template->display('Admin/Company/_list.tpl');

			} else {
				$list = $this->company->search();

				foreach ($list as &$r) {
					$this->company->set('id', $r['id']);
					$data = $this->company->countriesPerCompany();
					$r['countries'] = $data;
				}

				$this->template->assign(array(
					'title'		=> $this->lang['Companies'],
					'lang'		=> $this->lang,
					'nav'		=> 'companies',
					'languages'	=> $languages,
					'list' 		=> $list
				));
				$this->template->display('Admin/Company/index.tpl');
			}
		}

		public function add()
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(21);
			
			if ($_POST)
			{
				if (isset($_POST['name']) and !empty($_POST['name']) and isset($_POST['status']))
				{
					//Carga todo el POST en la clase
					$this->company->set('name', $_POST['name']);
					$this->company->set('status', $_POST['status']);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->company->set('img', end($temp));

							//Guarda y obtengo el id para asignarselo al nombre en el server
							$id = $this->company->add();
							$this->company->set('id', $id);

							$fileName = $id . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "companies" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}

						die('1'); //Marca agregada con imagen
					}

					//Guarda en la base de datos
					$this->company->add();

					die('2'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
			else
			{

				$title = $this->lang['Add'] . ' ' . $this->lang['Company'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'companies'
				));

				$this->template->display('Admin/Company/item.tpl');
				
			}
		}

		public function edit($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(22);
			
			if (!$_POST)
			{
				$this->company->set('id', $id);
				$company = $this->company->view();
				$languages = $this->language->toList();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Company'];

				$this->template->assign(array(
					'company'	=> $company,
					'title' 	=> $title,
					'nav'		=> 'companies',
					'languages'	=> $languages,
					'lang'		=> $this->lang
				));

				$this->template->display('Admin/Company/item.tpl');
			}
			else
			{
				if (isset($_POST['name']) and !empty($_POST['name']) and isset($_POST['status']))
				{

					//Carga todo el POST en la clase
					$this->company->set('name', $_POST['name']);
					$this->company->set('status', $_POST['status']);
					$this->company->set('img', $_GET['img']);

					$this->company->set('id', $id);
					
					$permitted = array("image/jpeg", "image/png", "image/gif", "image/ipg");
					$limit = 700 * 1024;

					//Chequea de que exista la imagen
					if ($_FILES)
					{
						//Chequea de que cumpla las condiciones y carga la extensión en la clase
						if (in_array($_FILES['img']['type'], $permitted) and $_FILES['img']['size'] <= $limit)
						{
							$temp = explode(".", $_FILES['img']['name']);
							$this->company->set('img', end($temp));

							$fileName = $id . '.' . end($temp);
							$route = "Views" . DS . "img" . DS . "companies" . DS;
							move_uploaded_file($_FILES['img']['tmp_name'], $route . $fileName);
						}
						else
						{
							die('El archivo ingresado es incompatible.');
						}
					}

					//Guarda en la base de datos
					$this->company->edit();

					die('1'); //Marca agregada sin imagen
				}
				else
				{
					die($this->lang['MissingData']);
				}
			}
		}

		public function delete($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(23);
			
			$this->company->set('id', $id);
			$this->company->delete();

			header("Location: " . URL . "company/?message=delete");
		}

		public function export()
		{
			$this->user->checkSession();
			echo 'falta terminar';
		}

		public function countries($id)
		{
			$this->user->checkSession();
			$this->user->checkPrivilege(35);
			
			if (!$_POST)
			{
				$this->company->set('id', $id);
				$company = $this->company->view();
				$countries = $this->company->countriesAllAndChecked();

				$title = $this->lang['Edit'] . ' ' . $this->lang['Countries'];
				$languages = $this->language->toList();

				$this->template->assign(array(
					'title'		=> $title,
					'lang'		=> $this->lang,
					'languages'	=> $languages,
					'nav'		=> 'companies',
					'countries'	=> $countries,
					'id'		=> $id,
					'companyName'	=> $company['name']
				));

				$this->template->display('Admin/Company/countries.tpl');
			} else {

				if (isset($_POST['countries']))
				{
					$this->company->set('id', $id);
					$this->company->updateCountries($_POST['countries']);

					die('1');
				} else {
					die($this->lang['MissingData']);
				}
			}
		}
	}

?>