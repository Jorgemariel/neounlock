<?php namespace Libs;

	use Models\Order as Order;
	use Models\Email as Email;

/**
* Esta librería se encarga del envio de emails.
* bool mail ( string $to , string $subject , string $message [, string $additional_headers [, string $additional_parameters ]] )
*/
class Mail
{
	private $to;
	private $subject;
	private $headers;

	private $template;
	private $order;
	private $email;

	public function __construct()
	{
		$this->order = new Order();
		$this->email = new Email();
		$this->template = new \Smarty();
	}

	public function pending($tracking)
	{
		$this->order->set('tracking', $tracking);
		$o = $this->order->view();

		include('Lang/'.strtolower($o['lang_short']).'.php');


		$this->email->set('id', 1);
		$content = $this->email->viewContentByIds($o['id_language']);

		//var_dump($content);

		if ($o['id_status'] != '1') die($lang['EmailStatusError']);

		$this->headers = "From: " . strip_tags(EMAIL) . "\r\n";
		$this->headers .= "Reply-To: ". strip_tags(EMAIL) . "\r\n";
		$this->headers .= "MIME-Version: 1.0\r\n";
		$this->headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$this->template->assign(array(
			'lang'				=> $lang,
			'tracking'			=> $o['tracking'],
			'name'				=> $o['name'],
			'email'				=> $o['email'],
			'ip'				=> $o['ip'],
			'imei'				=> $o['imei'],
			'brand'				=> $o['brand'],
			'model' 			=> $o['model'],
			'country'			=> $o['country'],
			'company'			=> $o['company'],
			'mep'				=> $o['mep'],
			'prd'				=> $o['prd'],
			'status'			=> $lang[$o['status']],
			'service'			=> $o['service'],
			'service_price'		=> $o['service_price'],
			'service_delay'		=> $o['service_delay'],
			'delay_type'		=> $lang[$o['delay_type']],
			'entry_date'		=> $o['entry_date'],
			'payment_date'		=> $o['payment_date'],
			'delivery_date'		=> $o['delivery_date'],
			'payment_method'	=> $o['payment_method'],
			'code1'				=> $o['code1'],
			'code2'				=> $o['code2'],
			'code3'				=> $o['code3'],
			'code4'				=> $o['code4'],
			'code5'				=> $o['code5'],
			'code6'				=> $o['code6']
		));

		mail(
			$o['email'],
			$lang['OrderPending'].' - NeoUnlock',
			$this->template->fetch('eval:'.$content),
			$this->headers
		);
	}

	public function expired($tracking)
	{
		$this->order->set('tracking', $tracking);
		$o = $this->order->view();

		if ($o['id_status'] != '2') die($this->lang['EmailStatusError']);

	}

	public function paid($tracking)
	{
		$this->order->set('tracking', $tracking);
		$o = $this->order->view();

		include('Lang/'.strtolower($o['lang_short']).'.php');

		if ($o['id_status'] != '3') die($lang['EmailStatusError']);

		$this->headers = "From: " . strip_tags(EMAIL) . "\r\n";
		$this->headers .= "Reply-To: ". strip_tags(EMAIL) . "\r\n";
		$this->headers .= "MIME-Version: 1.0\r\n";
		$this->headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$this->template->assign(array(
			'lang'				=> $lang,
			'name'				=> $o['name'],
			'tracking'			=> $o['tracking'],
			'imei'				=> $o['imei'],
			'payment_method'	=> $o['payment_method'],
			'service_price'		=> $o['service_price']
		));

		mail(
			$o['email'],
			$lang['OrderPaid'].' - NeoUnlock',
			$this->template->fetch('Email/'.strtolower($o['lang_short']).'/paid.tpl'),
			$this->headers
		);

	}

	public function completed($tracking)
	{
		$this->order->set('tracking', $tracking);
		$o = $this->order->view();

		if ($o['id_status'] != '4') die($this->lang['EmailStatusError']);

	}

	public function problem($tracking)
	{
		$this->order->set('tracking', $tracking);
		$o = $this->order->view();

		if ($o['id_status'] != '5') die($this->lang['EmailStatusError']);

	}

	public function cancelled($tracking)
	{
		$this->order->set('tracking', $tracking);
		$o = $this->order->view();

		if ($o['id_status'] != '6') die($this->lang['EmailStatusError']);

	}

	public function testEmail($content)
	{
		$this->headers = "From: " . strip_tags(EMAIL) . "\r\n";
		$this->headers .= "Reply-To: ". strip_tags(EMAIL) . "\r\n";
		$this->headers .= "MIME-Version: 1.0\r\n";
		$this->headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$this->template->assign(array(
			'tracking'		=> 'TRACKING',
			'name'				=> 'NAME',
			'email'				=> 'EMAIL@EMAIL.COM',
			'ip'					=> '192.168.1.1',
			'imei'				=> '000000000000000',
			'brand'				=> 'BRAND',
			'model' 			=> 'MODEL',
			'country'			=> 'COUNTRY',
			'company'			=> 'COMPANY',
			'mep'					=> '456456465',
			'prd'					=> '456456466',
			'status'			=> 'STATUS',
			'service'			=> 'SERVICE',
			'service_price'		=> '200',
			'service_delay'		=> '24',
			'delay_type'			=> 'HOURS',
			'entry_date'			=> '20/02/2020',
			'payment_date'		=> '21/02/2020',
			'delivery_date'		=> 'DELIVERY DATE',
			'payment_method'	=> 'PAYMENT METHOD',
			'code1'				=> '123123123',
			'code2'				=> '123123123',
			'code3'				=> '123123123',
			'code4'				=> '123123123',
			'code5'				=> '123123123',
			'code6'				=> '123123123'
		));

		mail(
			EMAIL_CLIENT,
			'THIS IS A TEST - NeoUnlock',
			$this->template->fetch("eval:".$content),
			$this->headers
		);

		die('1');
	}

	public function contact($mail, $text)
	{
		$this->headers = "From: " . strip_tags(EMAIL) . "\r\n";
		$this->headers .= "Reply-To: ". strip_tags($mail) . "\r\n";
		$this->headers .= "MIME-Version: 1.0\r\n";
		$this->headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		//print_r($_SESSION['language']);

		include('Lang/'.$_SESSION['language'].'.php');

		mail(
			EMAIL,
			$lang['WebMessage'].' - NeoUnlock',
			$this->template->fetch("eval:".$text),
			$this->headers
		);

		die('1');
	}
}

?>
