<?php
/* Smarty version 3.1.31, created on 2017-02-21 03:26:30
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Country\item.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58abb366254d50_96175847',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e2ab560e451d634bc1642639a9fceaf68e6f1f36' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Country\\item.tpl',
      1 => 1487634032,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/sidenav.tpl' => 1,
    'file:Public/navbar.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58abb366254d50_96175847 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12 m6 l6">
							<input id="name_en" name="name_en" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['country']->value['name_en'])) {
echo $_smarty_tpl->tpl_vars['country']->value['name_en'];
}?>">
							<label for="name_en" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
 (English)</label>
						</div>
						<div class="input-field col s12 m6 l6">
							<input id="name_es" name="name_es" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['country']->value['name_es'])) {
echo $_smarty_tpl->tpl_vars['country']->value['name_es'];
}?>">
							<label for="name_es" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
 (Español)</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="iso2" name="iso2" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['country']->value['iso2'])) {
echo $_smarty_tpl->tpl_vars['country']->value['iso2'];
}?>">
							<label for="iso2" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
">ISO (2)</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="iso3" name="iso3" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['country']->value['iso3'])) {
echo $_smarty_tpl->tpl_vars['country']->value['iso3'];
}?>">
							<label for="iso3" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
">ISO (3)</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="coin" name="coin" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['country']->value['coin'])) {
echo $_smarty_tpl->tpl_vars['country']->value['coin'];
}?>">
							<label for="coin" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Coin'];?>
</label>
						</div>
						<div class="file-field input-field col s12 m6">
							<div class="btn blue">
								<span><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<div class="input-field col s12 m6">
							<select id="language" name="language" value="">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['country']->value['id_language']) && $_smarty_tpl->tpl_vars['country']->value['id_language'] == $_smarty_tpl->tpl_vars['l']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</label>
						</div>
						<p class="input-field col s12">
							<input type="checkbox" name="highlighted" id="highlighted" <?php if (isset($_smarty_tpl->tpl_vars['country']->value['highlighted']) && $_smarty_tpl->tpl_vars['country']->value['highlighted'] == 1) {?>checked<?php }?>/>
							<label for="highlighted"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Highlighted'];?>
</label>
						</p>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="<?php echo URL;?>
country" class="waves-effect waves-light btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
							<?php if (!isset($_smarty_tpl->tpl_vars['country']->value)) {?>
							<a id="save" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
							<?php } else { ?>
							<a id="update" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Update'];?>
</a>
							<?php }?>
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name_en		= $('#name_en');
				var name_es		= $('#name_es');
				var iso2 		= $('#iso2');
				var iso3 		= $('#iso3');
				var coin 		= $('#coin');
				var language 	= $('#language');
				var highlighted = $('#highlighted');

				if (!name_en.val())
				{
					name_en.addClass('invalid');
					name_en.focus();
					return false;
				}

				if (!name_es.val())
				{
					name_es.addClass('invalid');
					name_es.focus();
					return false;
				}

				if (!iso2.val())
				{
					iso2.addClass('invalid');
					iso2.focus();
					return false;
				}

				if (!iso3.val())
				{
					iso3.addClass('invalid');
					iso3.focus();
					return false;
				}

				if (!coin.val())
				{
					coin.addClass('invalid');
					coin.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name_en', name_en.val());
				formData.append('name_es', name_es.val());
				formData.append('iso2', iso2.val());
				formData.append('iso3', iso3.val());
				formData.append('coin', coin.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('language', language.val());
				formData.append('highlighted', highlighted.is(':checked'));

				$.ajax({
					url: '<?php echo URL;?>
country/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
country/?message=added';
						}
						else if (data == 2)
						{
							window.location = '<?php echo URL;?>
country/?message=noimage';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			<?php if (isset($_smarty_tpl->tpl_vars['country']->value)) {?>
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name_en		= $('#name_en');
				var name_es		= $('#name_es');
				var iso2 		= $('#iso2');
				var iso3 		= $('#iso3');
				var coin 		= $('#coin');
				var language 	= $('#language');
				var highlighted = $('#highlighted');

				if (!name_en.val())
				{
					name_en.addClass('invalid');
					name_en.focus();
					return false;
				}

				if (!name_es.val())
				{
					name_es.addClass('invalid');
					name_es.focus();
					return false;
				}

				if (!iso2.val())
				{
					iso2.addClass('invalid');
					iso2.focus();
					return false;
				}

				if (!iso3.val())
				{
					iso3.addClass('invalid');
					iso3.focus();
					return false;
				}

				if (!coin.val())
				{
					coin.addClass('invalid');
					coin.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name_en', name_en.val());
				formData.append('name_es', name_es.val());
				formData.append('iso2', iso2.val());
				formData.append('iso3', iso3.val());
				formData.append('coin', coin.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('language', language.val());
				formData.append('highlighted', highlighted.is(':checked'));

				$.ajax({
					url: '<?php echo URL;?>
country/edit/<?php echo $_smarty_tpl->tpl_vars['country']->value['id'];?>
/?img=<?php echo $_smarty_tpl->tpl_vars['country']->value['img'];?>
',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
country/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			<?php }?>
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
