<?php
/* Smarty version 3.1.31, created on 2018-05-29 22:06:30
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Contact/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b0db2c668b653_40849681',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '233385966b2a26dda07ff630e8d504a8b5748bbc' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Contact/index.tpl',
      1 => 1527624386,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Contact/_social.tpl' => 1,
    'file:Contact/_email.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b0db2c668b653_40849681 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px" class="animated fadeInLeft"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Contact'];?>
</h2>
			<!-- <p>
				Navega entre las preguntas mas realizadas por nuestros clientes y busca aquellas que sean de tu interés. <br>
				Si no encuentras la respuesta a tu pregunta en el listado, te invitamos a realizarla al final de la página. <br>
				Estamos para ayudarte.
			</p> -->
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Contact/_social.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Contact/_email.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){

		});

		$('#send').click(function(){
			$('#contactForm').hide();
			$('#sendLoading').fadeIn();
			var formData = $('#contactForm').serialize();
			console.log(formData);

			$.ajax({
				url: '<?php echo URL;?>
contact/send',
				type: 'POST',
				data: formData,
				cache: false,
				success: function(data) {
					if (data == 1)
					{
						$('#sendLoading').hide();
						$('#success').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
						$('#contactForm')[0].reset();
					}
					else
					{
						$('#sendLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
					}
				},
				error: function(data) {
					$('#sendLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('#contactForm').fadeIn();
				}
			});
			return false;
		});
	<?php echo '</script'; ?>
>

</body>
</html>
<?php }
}
