<?php
/* Smarty version 3.1.31, created on 2017-02-21 17:25:58
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Company\item.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58ac78264be321_80265208',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '06539287584ded46c98e4f56aedc9d35f7f71538' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Company\\item.tpl',
      1 => 1487697944,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/sidenav.tpl' => 1,
    'file:Public/navbar.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58ac78264be321_80265208 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12 m8 l9">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12">
							<input id="name" name="name" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['company']->value['name'])) {
echo $_smarty_tpl->tpl_vars['company']->value['name'];
}?>">
							<label for="name" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
</label>
						</div>
						<div class="file-field input-field col s12 m6">
							<div class="btn blue">
								<span><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<div class="input-field col s12 m6">
							<select id="status" name="status" value="<?php if (isset($_smarty_tpl->tpl_vars['company']->value['status'])) {
echo $_smarty_tpl->tpl_vars['company']->value['status'];
}?>">
								<option value="1" <?php if (isset($_smarty_tpl->tpl_vars['company']->value['status']) && $_smarty_tpl->tpl_vars['company']->value['status'] == 1) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Active'];?>
</option>
								<option value="0" <?php if (isset($_smarty_tpl->tpl_vars['company']->value['status']) && $_smarty_tpl->tpl_vars['company']->value['status'] == 0) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Inactive'];?>
</option>
							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</label>
						</div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="<?php echo URL;?>
company" class="waves-effect waves-light btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
							<?php if (!isset($_smarty_tpl->tpl_vars['company']->value)) {?>
							<a id="save" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
							<?php } else { ?>
							<a id="update" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Update'];?>
</a>
							<?php }?>
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['company']->value['img'])) {
$_smarty_tpl->_assignInScope('image', "Views/img/companies/".((string)$_smarty_tpl->tpl_vars['company']->value['id']).".".((string)$_smarty_tpl->tpl_vars['company']->value['img']));
}?>
			<div class="col s12 m4 l3">
				<h4 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Preview'];?>
</h4>
				<div class="brand card hoverable waves-effect" id="preview" style="min-height: 100px; margin: 10px; min-width: 100%;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php if (isset($_smarty_tpl->tpl_vars['company']->value['img']) && file_exists($_smarty_tpl->tpl_vars['image']->value)) {
echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;
}?>"  id="img-prev" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" id="name-prev" style="margin-top: 20px"><?php if (isset($_smarty_tpl->tpl_vars['company']->value['name'])) {
echo $_smarty_tpl->tpl_vars['company']->value['name'];
} else {
echo $_smarty_tpl->tpl_vars['lang']->value['Company'];
}?></h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Carga la vista previo del nombre de la marca
			$('#name').on('keyup keypress blur change', function(){
				var content = $('#name').val();
				$('#name-prev').html(content);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name 	= $('#name');
				var status 	= $('#status');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('status', status.val());

				$.ajax({
					url: '<?php echo URL;?>
company/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
company/?message=added';
						}
						else if (data == 2)
						{
							window.location = '<?php echo URL;?>
company/?message=noimage';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			<?php if (isset($_smarty_tpl->tpl_vars['company']->value)) {?>
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name	= $('#name');
				var status 	= $('#status');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#preview').parent().hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('status', status.val());

				$.ajax({
					url: '<?php echo URL;?>
company/edit/<?php echo $_smarty_tpl->tpl_vars['company']->value['id'];?>
/?img=<?php echo $_smarty_tpl->tpl_vars['company']->value['img'];?>
',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
company/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			<?php }?>
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
