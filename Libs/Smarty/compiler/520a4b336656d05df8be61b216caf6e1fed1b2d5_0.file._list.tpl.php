<?php
/* Smarty version 3.1.31, created on 2017-11-29 08:02:08
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Country/_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a1e5b706120f2_51435961',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '520a4b336656d05df8be61b216caf6e1fed1b2d5' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Country/_list.tpl',
      1 => 1498935105,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a1e5b706120f2_51435961 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m6 l6 strong">
				<a href="#" class="orderByIso3"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
</a>
			</div>
			<div class="col hide-on-small-only m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByLanguage"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByLanguage"><i class="material-icons">language</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByHighlighted"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Highlighted'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByHighlighted"><i class="material-icons">star</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<?php ob_start();
echo mb_strtolower($_smarty_tpl->tpl_vars['l']->value['iso2'], 'UTF-8');
$_prefixVariable1=ob_get_clean();
$_smarty_tpl->_assignInScope('image', "Views/img/countries/".$_prefixVariable1.".png");
?>
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m6 l6"><strong><?php echo $_smarty_tpl->tpl_vars['l']->value['iso3'];?>
</strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['name_en'];?>
</div>
				<div class="col hide-on-small-only m2 l2 center"><?php echo $_smarty_tpl->tpl_vars['l']->value['language'];?>
</div>
				<div class="col s2 m2 l2 center" style="max-height: 42px">
					<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
					<img src="<?php echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;?>
" class="responsive-img" style="max-height: 35px;">
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
				<div class="col s2 m2 l2">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['highlighted']) {?>
					<i class="material-icons center-icon amber-text text-accent-3">star</i>
					<?php } else { ?>
					<i class="material-icons center-icon">star_border</i>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px 20px 0; margin-bottom: 0;">
				<div class="col s12 m6">
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['language'];?>
 <br>
					<strong>English: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['name_en'];?>
 <br>
					<strong>Español: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['name_es'];?>
 <br>
					<strong>ISO 2: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['iso2'];?>
 <br>
					<strong>ISO 3: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['iso3'];?>
 <br>
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Coin'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['coin'];?>
 <br>
				</div>
				<div class="col s12 m6">
					<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['companies'])) {?>
						<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
:</strong><br>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['companies'], 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
<br>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php }?>
				</div>
				<div class="col s12 right-align" style="margin: 10px 0">
					<?php if (array_search(34,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
country/companies/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['AdminCompanies'];?>
</a>
					<?php }?>
					<?php if (array_search(19,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
country/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
					<?php }?>
					<?php if (array_search(20,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
country/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
					<?php }?>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
