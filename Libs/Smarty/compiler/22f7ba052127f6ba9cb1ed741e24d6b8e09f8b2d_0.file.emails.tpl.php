<?php
/* Smarty version 3.1.31, created on 2018-04-23 21:07:11
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/emails.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ade2edf8f3579_34931151',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '22f7ba052127f6ba9cb1ed741e24d6b8e09f8b2d' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/emails.tpl',
      1 => 1494797824,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5ade2edf8f3579_34931151 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text orange">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<div id="message">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['VariablesAvailable'];?>
:</h5>
					<ul>
						<li>
							{$tracking}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingCode'];?>

						</li>
						<li>
							{$name}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['ClientName'];?>

						</li>
						<li>
							{$email}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['ClientEmail'];?>

						</li>
						<li>
							{$ip}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['ClientIp'];?>

						</li>
						<li>
							{$imei}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PhoneImei'];?>

						</li>
						<li>
							{$brand}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PhoneBrand'];?>

						</li>
						<li>
							{$model}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PhoneModel'];?>

						</li>
						<li>
							{$country}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PhoneCountry'];?>

						</li>
						<li>
							{$company}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PhoneCompany'];?>

						</li>
						<li>
							{$mep}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PhoneMep'];?>

						</li>
						<li>
							{$prd}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PhonePrd'];?>

						</li>
						<li>
							{$status}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderStatus'];?>

						</li>
						<li>
							{$service}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['ServiceName'];?>

						</li>
						<li>
							{$service_price}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderPrice'];?>

						</li>
						<li>
							{$service_delay}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderDelay'];?>

						</li>
						<li>
							{$delay_type}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderDelayType'];?>

						</li>
						<li>
							{$entry_date}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['EntryDate'];?>

						</li>
						<li>
							{$payment_date}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['PaymentDate'];?>

						</li>
						<li>
							{$delivery_date}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['DeliveryDate'];?>

						</li>
						<li>
							{$payment_method}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['WayToPay'];?>

						</li>
						<li>
							{$code1/2/3/4/5/6}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCode'];?>

						</li>
						<li>
							{$lang['XXXXX']}	
							<i class="material-icons" style="vertical-align: middle;">keyboard_arrow_right</i>
							<?php echo $_smarty_tpl->tpl_vars['lang']->value['LanguageText'];?>
 (<?php echo $_smarty_tpl->tpl_vars['lang']->value['NotRecommended'];?>
)
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="admin-container">
		<div class="row" id="list">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<ul class="collapsible popout" data-collapsible="accordion">
					<li class="collapsible-header">
						<div class="row" style="padding:0px;margin:0px;">
							<div class="col s10 strong">
								<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</a>
							</div>
							<div class="col s2 strong center">
								<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
								<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
							</div>
						</div>
					</li>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
					<?php $_smarty_tpl->_assignInScope('image', "Views/img/languages/".((string)$_smarty_tpl->tpl_vars['l']->value['img']).".png");
?>
					<li>
						<div class="collapsible-header">
							<div class="row" style="margin: 0;">
								<div class="col s10">
									<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?><img src="<?php echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;?>
" style="margin-right: 10px;"><?php }?> <?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>

								</div>
								<div class="col s2">
									<?php if (isset($_smarty_tpl->tpl_vars['l']->value['content']) && !empty($_smarty_tpl->tpl_vars['l']->value['content'])) {?>
									<i class="material-icons center-icon green-text">check</i>
									<?php } else { ?>
									<i class="material-icons center-icon red-text">block</i>
									<?php }?>
								</div>
							</div>
						</div>
						<div class="collapsible-body">
							<textarea class="editor" data-language="<?php echo $_smarty_tpl->tpl_vars['l']->value['short_name'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['l']->value['short_name'];?>
">
								<?php if (isset($_smarty_tpl->tpl_vars['l']->value['content']) && !empty($_smarty_tpl->tpl_vars['l']->value['content'])) {?>
									<?php echo $_smarty_tpl->tpl_vars['l']->value['content'];?>

								<?php } else { ?>
									hola
								<?php }?>
							</textarea>
						</div>
					</li>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

				</ul>
			</div>
			<div class="col s12 center">
				<a href="<?php echo URL;?>
email" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
				<a id="save" class="waves-effect waves-light btn-large"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
			</div>
		</div>
		<div class="center" id="loading" style="display: none; height: 200px;">
			<div class="preloader-wrapper big active" style="margin-top: 68px">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php echo '<script'; ?>
 src="http://cloud.tinymce.com/stable/tinymce.min.js?apiKey=835mpwvoa9jxi8di3gqker4w8h54myafuvydw171zjztih33"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo URL;?>
Views/js/tinymce.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			//var data = tinymce.get('editor').getContent();
		});

		$('#messageClose').click(function(){
			$('#messageArea').fadeOut();
		});

		var data = [];

		$('#save').click(function(){
			data = [];
			$('textarea').each(function(i, el){
				// console.log($(el).attr('id'));
				// console.log(tinymce.get($(el).attr('id')).getContent());
				var desc = '';

				if (tinymce.get($(el).attr('id')).getContent() != '<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['AddDescription'];?>
</p>') {
					desc = tinymce.get($(el).attr('id')).getContent();
				}

				data.push({
					language: $(el).attr('data-language'),
					content: desc
				});
			});

			$('#list').hide();
			$('#loading').fadeIn();

			$.ajax({
				url: '<?php echo URL;?>
email/emails/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
',
				type: 'POST',
				data: {data},
				success: function(data) {
					if (data == 1)
					{
						window.location = '<?php echo URL;?>
email/?message=emails';
					}
					else
					{
						$('#loading').hide();
						$('#list').fadeIn();
					}
				},
				error: function(data) {
					alert(data);
				}
			});

			return false;
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
