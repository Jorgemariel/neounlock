<?php
/* Smarty version 3.1.31, created on 2018-04-26 21:13:38
  from "c2c1d6dbbb880a1b95379b02245339d87c5a551f" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ae224e2aa5fd1_22101316',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ae224e2aa5fd1_22101316 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>A Simple Responsive HTML Email</title>
	
	<style type="text/css">
		body {margin: 0; padding: 0; min-width: 100%!important;}
		img {height: auto;}
		.content {width: 100%; max-width: 600px;}
		.header {padding: 40px 30px 20px 30px;}
		.innerpadding {padding: 30px 30px 30px 30px;}
		.borderbottom {border-bottom: 1px solid #f2eeed;}
		.subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
		.h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
		.h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
		.h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
		.bodycopy {font-size: 16px; line-height: 22px;}
		.button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
		.button a {color: #ffffff; text-decoration: none;}
		.footer {padding: 20px 30px 15px 30px;}
		.footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
		.footercopy a {color: #ffffff; text-decoration: underline;}
		@media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
			body[yahoo] .hide {display: none!important;}
			body[yahoo] .buttonwrapper {background-color: transparent!important;}
			body[yahoo] .button {padding: 0px!important;}
			body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
			body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
		}
/*@media only screen and (min-device-width: 601px) {
.content {width: 600px !important;}
.col425 {width: 425px!important;}
.col380 {width: 380px!important;}
}*/
	</style>
	
</head>

<body yahoo bgcolor="#f6f8f1">
<table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
	<!--[if (gte mso 9)|(IE)]>
	<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
	<![endif]-->     
	<table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td bgcolor="#1e88e5" class="header">
				<table width="70" align="left" border="0" cellpadding="0" cellspacing="0">  
					<tr>
						<td height="70">
							<img class="" src="<?php echo URL;?>
Views/img/logo.png" width="90" height="70" border="0" alt="" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="innerpadding borderbottom">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="h2">
							Su pedido de liberación expiró
						</td>
					</tr>
					<tr>
						<td class="bodycopy">
							<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
,<br>
							No realizaste el pago a tiempo. Puedes realizar el pedido nuevamente y disfrutar de todos los beneficios de tener tu celular liberado al mejor precio y de la forma mas segura.

							<div class="button" style="margin: 20px; margin-bottom: 0; padding: 20px; text-align: center; background-color: green; color: white;">
								<a href="<?php echo URL;?>
order/tracking/<?php echo $_smarty_tpl->tpl_vars['tracking']->value;?>
">Código de seguimiento: <strong><?php echo $_smarty_tpl->tpl_vars['tracking']->value;?>
</strong></a>
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding: 20px 0 0 0; text-align: center;">
							<table class="buttonwrapper" bgcolor="#1565c0" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="button" height="45">
										<a href="<?php echo URL;?>
home/form/<?php echo $_smarty_tpl->tpl_vars['brand']->value;?>
/<?php if (isset($_smarty_tpl->tpl_vars['model']->value) && !empty($_smarty_tpl->tpl_vars['model']->value)) {
echo $_smarty_tpl->tpl_vars['model']->value;?>
/<?php }?>?data=<?php echo $_smarty_tpl->tpl_vars['tracking']->value;?>
">Liberar ahora</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="innerpadding bodycopy">
				En NeoUnlock brindamos un servicio de liberación garantizado.
			</td>
		</tr>
		<tr>
			<td class="footer" bgcolor="#1e88e5">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" class="footercopy">
							NeoUnlock - Liberación de celulares<br/>
							<span class="hide"><?php echo strftime("%Y");?>
</span>
						</td>
					</tr>
					<tr>
						<td align="center" style="padding: 20px 0 0 0;">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="37" style="text-align: center; padding: 0 10px 0 10px;">
										<a href="http://www.facebook.com/neounlockofficial">
											<img src="<?php echo URL;?>
Views/img/facebook.png" width="37" height="37" alt="Facebook" border="0" />
										</a>
									</td>
									<td width="37" style="text-align: center; padding: 0 10px 0 10px;">
										<a href="tel: +5493515144316">
											<img src="<?php echo URL;?>
Views/img/whatsapp.png" width="37" height="37" alt="Whatsapp" border="0" />
										</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
			<!--[if (gte mso 9)|(IE)]>
      		</td>
    	</tr>
	</table>
	<![endif]-->
</td>
</tr>
</table>
</body>
</html><?php }
}
