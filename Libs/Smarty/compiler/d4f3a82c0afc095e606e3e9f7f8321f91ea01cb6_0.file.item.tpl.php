<?php
/* Smarty version 3.1.31, created on 2018-06-04 08:07:18
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Faq/item.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b14d7162e0004_15449199',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd4f3a82c0afc095e606e3e9f7f8321f91ea01cb6' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Faq/item.tpl',
      1 => 1528092435,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b14d7162e0004_15449199 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
            <div class="input-field col s12 m6">
							<select id="language" name="language" value="">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['faq']->value['id_language']) && $_smarty_tpl->tpl_vars['faq']->value['id_language'] == $_smarty_tpl->tpl_vars['l']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</label>
						</div>
            <div class="input-field col s12 m6">
							<select id="status" name="status" value="<?php if (isset($_smarty_tpl->tpl_vars['faq']->value['status'])) {
echo $_smarty_tpl->tpl_vars['faq']->value['status'];
}?>">
								<option value="1" <?php if (isset($_smarty_tpl->tpl_vars['faq']->value['status']) && $_smarty_tpl->tpl_vars['faq']->value['status'] == 1) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Active'];?>
</option>
								<option value="0" <?php if (isset($_smarty_tpl->tpl_vars['faq']->value['status']) && $_smarty_tpl->tpl_vars['faq']->value['status'] == 0) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Inactive'];?>
</option>
							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</label>
						</div>
						<div class="input-field col s12">
							<input id="question" name="question" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['faq']->value['question'])) {
echo $_smarty_tpl->tpl_vars['faq']->value['question'];
}?>">
							<label for="question" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['QuestionRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Question'];?>
</label>
						</div>
            <div class="input-field col s12">
              <textarea id="answer" name="answer" class="materialize-textarea"><?php if (isset($_smarty_tpl->tpl_vars['faq']->value['answer'])) {
echo $_smarty_tpl->tpl_vars['faq']->value['answer'];
}?></textarea>
              <label for="answer"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Answer'];?>
</label>
            </div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="<?php echo URL;?>
faqs/list" class="waves-effect waves-light btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
							<?php if (!isset($_smarty_tpl->tpl_vars['faq']->value)) {?>
							<a id="save" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
							<?php } else { ?>
							<a id="update" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Update'];?>
</a>
							<?php }?>
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault();

				var language 		= $('#language');
        var status 		= $('#status');
				var question 		= $('#question');
				var answer 		= $('#answer');

				if (!question.val())
				{
					question.addClass('invalid');
					question.focus();
					return false;
				}

        if (!answer.val())
				{
          $('#error').html("<?php echo $_smarty_tpl->tpl_vars['lang']->value['AnswerRequired'];?>
");
          $('#errorArea').fadeIn();
					answer.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('language', language.val());
        formData.append('status', status.val());
				formData.append('question', question.val());
				formData.append('answer', answer.val());

				$.ajax({
					url: '<?php echo URL;?>
faqs/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
faqs/list/?message=added';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			<?php if (isset($_smarty_tpl->tpl_vars['faq']->value)) {?>
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault();

        var language 		= $('#language');
        var status 		= $('#status');
				var question 		= $('#question');
				var answer 		= $('#answer');

				if (!question.val())
				{
					question.addClass('invalid');
					question.focus();
					return false;
				}

				if (!answer.val())
				{
          $('#error').html("<?php echo $_smarty_tpl->tpl_vars['lang']->value['AnswerRequired'];?>
");
          $('#errorArea').fadeIn();
					answer.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

        formData.append('language', language.val());
        formData.append('status', status.val());
				formData.append('question', question.val());
				formData.append('answer', answer.val());

				$.ajax({
					url: '<?php echo URL;?>
faqs/edit/<?php echo $_smarty_tpl->tpl_vars['faq']->value['id'];?>
',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
faqs/list/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			<?php }?>
		});
	<?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
