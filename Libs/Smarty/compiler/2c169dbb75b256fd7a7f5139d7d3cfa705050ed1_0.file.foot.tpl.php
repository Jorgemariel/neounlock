<?php
/* Smarty version 3.1.31, created on 2017-03-28 16:55:12
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Public\foot.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58da9570ed3606_49505804',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2c169dbb75b256fd7a7f5139d7d3cfa705050ed1' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Public\\foot.tpl',
      1 => 1490720085,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58da9570ed3606_49505804 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="http://code.jquery.com/jquery-3.1.1.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
$(document).ready(function(){
	$(".button-collapse").sideNav();
	$(".dropdown-button").dropdown();
});

$('.lan').click(function(){
	$('.dropdown-button').dropdown('open');
});

$('.lang').click(function(){
	$.ajax({
		url: $(this).data('url'),
		type: 'POST',
		success: function(data) {
			if (data == '1')
			{
				location.reload();
			}
			else
			{
				$('#loading').hide();
				$('form').fadeIn();
				$('#error-text').html(data);
				$('#error').fadeIn().delay(5000).slideUp('slow');
				var errorDiv =  $("#error").offset().top - 20;
				$(window).scrollTop(errorDiv);
			}
		},
		error: function(data) {
			alert(data);
		}
	});
	return false;
});
<?php echo '</script'; ?>
><?php }
}
