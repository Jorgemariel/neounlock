<?php
/* Smarty version 3.1.31, created on 2017-07-05 04:16:07
  from "/opt/lampp/htdocs/neounlock/Views/template/Faq/_search.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_595c4be79c3f23_04645370',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4581271b686370c3e38cd71707334c00ab7f2786' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Faq/_search.tpl',
      1 => 1499220966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_595c4be79c3f23_04645370 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container">
	<div class="card-panel" style="margin-top: -100px;">
		<div class="row valign-wrapper title">
			<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Search'];?>
</h5>
			<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
				<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
				<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
				<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
			</div>
		</div>
		<form class="row" style="margin: 25px">
			<!-- <div id="help" class="card-panel red white-text valign-wrapper" style="margin: 0 20px;">
				<i class="material-icons valign" style="margin-right: 5px">info</i>
				<h6 id="help-text">Ingresa las palabras claves de tu consulta para filtrar el listado de preguntas frecuentes</h6>
			</div> -->
			<div class="input-field col m10 l11">
				<input id="search" type="text" class="validate" required>
				<label for="search">Ingrese palabras claves</label>
			</div>
			<div class="input-field col m2 l1">
				<div class="center">
					<a class="waves-effect waves-light btn-floating btn-large green darken-2"><i class="material-icons left">search</i></a>
				</div>
			</div>
		</form>
	</div>
</div><?php }
}
