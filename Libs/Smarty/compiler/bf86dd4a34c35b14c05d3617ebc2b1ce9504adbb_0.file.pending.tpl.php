<?php
/* Smarty version 3.1.31, created on 2018-07-01 01:40:07
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/pending.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b385b272fd696_28652073',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bf86dd4a34c35b14c05d3617ebc2b1ce9504adbb' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/pending.tpl',
      1 => 1530419977,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Tracking/_help.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b385b272fd696_28652073 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingStatus'];?>
</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Order'];?>
 <strong><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['order']->value['tracking'], 'UTF-8');?>
</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel orange white-text valign-wrapper" style="margin: 0 20px">
				<i class="material-icons valign" style="margin-right: 5px">access_time</i>
				<h6><?php echo $_smarty_tpl->tpl_vars['lang']->value['WaitingPayment'];?>
</h6>
			</div>

			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><div>IMEI<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['imei'];?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Service'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['service'];?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Price'];?>
<a href="#!" class="secondary-content">USD <?php echo $_smarty_tpl->tpl_vars['order']->value['service_price'];?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delay'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['service_delay'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['order']->value['delay_type']];?>
</a></div></li>
			</ul>

			<div class="row" style="margin-top: 20px">
				<div class="col s12 m6 l4 center">
					<a href="<?php echo URL;?>
home/payment/<?php echo $_smarty_tpl->tpl_vars['order']->value['tracking'];?>
" target="_blank" class="waves-effect waves-light btn-large blue tracking-button" style="margin: 10px; width: 85%">
						<i class="material-icons left">attach_money</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['Pay'], 'UTF-8');?>

					</a>
				</div>
				<div class="col s12 m6 l4 center">
					<a class="waves-effect waves-light btn-large green tracking-button" style="margin: 10px; width: 85%"><i class="material-icons left">attach_money</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['ReportPayment'], 'UTF-8');?>
</a>
				</div>
				<div class="col s12 m6 l4 offset-m3 center">
					<a class="waves-effect waves-light btn-large red tracking-button modal-trigger" href="#helpingmodal" style="margin: 10px; width: 85%"><i class="material-icons left">help</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['Help'], 'UTF-8');?>
</a>
				</div>
			</div>

			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderDetail'];?>
</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>
			<ul class="collection" style="margin: 20px">

				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['brand']) && !empty($_smarty_tpl->tpl_vars['order']->value['brand'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brand'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['brand'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['model']) && !empty($_smarty_tpl->tpl_vars['order']->value['model'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['model'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['country']) && !empty($_smarty_tpl->tpl_vars['order']->value['country'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['country'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['company']) && !empty($_smarty_tpl->tpl_vars['order']->value['company'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['company'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['mep']) && !empty($_smarty_tpl->tpl_vars['order']->value['mep'])) {?>
				<li class="collection-item"><div>MEP<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['mep'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['prd']) && !empty($_smarty_tpl->tpl_vars['order']->value['prd'])) {?>
				<li class="collection-item"><div>PRD<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['prd'];?>
</a></div></li>
				<?php }?>
			</ul>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Tracking/_help.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		
		$('#helping-go').click(function(){
			helpingGo(); 
			return false;
		});
		

		function helpingGo() {
			console.log('falta terminar consulta');
		}
	<?php echo '</script'; ?>
>

</body>
</html>
<?php }
}
