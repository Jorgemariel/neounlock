<?php
/* Smarty version 3.1.31, created on 2017-03-24 22:49:36
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Home\services.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58d5a280869233_72483012',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef650c92e58ad0ee363a4a60d10edccb5d1e21d5' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Home\\services.tpl',
      1 => 1490395770,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/_loading.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58d5a280869233_72483012 (Smarty_Internal_Template $_smarty_tpl) {
?>




<?php $_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['AvailableServices'];?>
</h2>
		</div>
	</div>
	<div class="" style="padding: 15px;">
		<div class="row" style="margin-top: -120px;">
			<div id="error" class="col s12 row" style="display: none;">
				<div class="col s12 m8 offset-m2">
					<div class="card-panel red white-text valign-wrapper">
						<i class="material-icons valign" style="margin-right: 5px">info</i>
						<h6 id="error-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['ErrorOcurred'];?>
</h6>
					</div>
				</div>
			</div>
			<?php if (count($_smarty_tpl->tpl_vars['services']->value) == 2) {?> <div class="col m2"></div> <?php }?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 's', false, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['s']->value) {
?>
			<div class="col s12 m4" style="padding: 0 20px;">
				<div class="card hoverable service" data-id="<?php echo $_smarty_tpl->tpl_vars['s']->value['id'];?>
">
					<div class="<?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>purple<?php } elseif ($_smarty_tpl->tpl_vars['i']->value == 1) {?>cyan<?php } else { ?>teal<?php }?> waves-effect" style="width: 100%;">
						<div class="card-title white-text">FALTA TERMINAR</div>
						<div class="center white-text">
							<div style="display: inline-flex; margin-top: 10px;">
								<h4 style="margin-right: 5px">USD</h4>
								<h2 style="margin-top: 13px"><?php echo $_smarty_tpl->tpl_vars['s']->value['price'];?>
</h2>
							</div>
						</div>
					</div>
					<div class="card-content">
						<ul class="collection">
							<li class="collection-item">
								<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['AverageDelay'];?>
</strong><br>
								FALTA TERMINAR
							</li>
							<li class="collection-item">
								<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['MaxDelay'];?>
</strong><br>
								<?php echo $_smarty_tpl->tpl_vars['s']->value['delay'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['s']->value['delay_type']];?>

							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

			<div id="loading" class="col s8 m6 l4 offset-m2 offset-l4" style="display: none;">
				<div class="card-panel center" style="padding: 100px;"><?php $_smarty_tpl->_subTemplateRender('file:Public/_loading.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
			</div>
		</div>
	</div>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$('.service').click(function(){
			$('.service').hide();
			$('#loading').fadeIn();
			var id = $(this).data('id');

			$.ajax({
				url: '<?php echo URL;?>
home/services',
				type: 'POST',
				data: { id_service : id },
				dataType: 'json',
				success: function(data) {
					if (data.response == '1')
					{
						window.location = '<?php echo URL;?>
home/resume/' + data.tracking;
					}
					else
					{
						$('#loading').hide();
						$('.service').fadeIn();
						$('#error-text').html(data.response);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						var errorDiv =  $("#error").offset().top - 20;
						$(window).scrollTop(errorDiv);
					}
				},
				error: function(data) {
					alert(data.response);
					$('#loading').hide();
					$('.service').fadeIn();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					var errorDiv =  $("#error").offset().top - 20;
					$(window).scrollTop(errorDiv);
				}
			});
			return false;
		});
	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
