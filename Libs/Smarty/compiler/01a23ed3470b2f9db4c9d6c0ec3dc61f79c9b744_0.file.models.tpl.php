<?php
/* Smarty version 3.1.31, created on 2018-05-30 05:09:57
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Home/models.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b0e16051390e1_45990774',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '01a23ed3470b2f9db4c9d6c0ec3dc61f79c9b744' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Home/models.tpl',
      1 => 1499291238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b0e16051390e1_45990774 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row valign-wrapper title">
		<h4 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['brand']->value['name'];?>
 <a href="<?php echo URL;?>
" style="font-size: 0.7em;">(<?php echo $_smarty_tpl->tpl_vars['lang']->value['Change'];?>
)</a></h4>
		<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
			<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
			<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
			<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
		</div>
	</div>

	<div class="row">
		<?php if (isset($_smarty_tpl->tpl_vars['models']->value) && !empty($_smarty_tpl->tpl_vars['models']->value)) {?>
		<h5 class="col s12">
			<div class="card-panel white-text lime darken-1 center">
				<?php echo $_smarty_tpl->tpl_vars['lang']->value['SelectModel'];?>

			</div>
		</h5>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['models']->value, 'm');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['m']->value) {
?>
		<div class="col s12 m4 l3">
			<div data-id="<?php echo $_smarty_tpl->tpl_vars['m']->value['id'];?>
" class="model card hoverable waves-effect" style="margin: 15px 5px">
				<div class="card-content" style="text-align: center; padding-bottom: 1px">
					<img src="<?php echo URL;?>
Views/img/models/<?php echo $_smarty_tpl->tpl_vars['m']->value['id'];?>
.<?php echo $_smarty_tpl->tpl_vars['m']->value['img'];?>
" style="max-width: 90%">
					<h6 class="blue-text darken-text-1" style="margin-top: 20px"><?php echo $_smarty_tpl->tpl_vars['m']->value['name'];?>
</h6>
				</div>
			</div>
		</div>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

		<?php } else { ?>
		<div class="col m12 center">
			<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoResult'];?>
</h3>
		</div>
		<?php }?>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){

			$('.model').click(function(){
				window.location = '<?php echo URL;?>
home/form/<?php echo $_smarty_tpl->tpl_vars['brand']->value['id'];?>
/' + $(this).data('id');
				return false;
			});
		});
	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
