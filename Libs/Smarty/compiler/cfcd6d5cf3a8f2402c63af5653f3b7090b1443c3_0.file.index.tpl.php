<?php
/* Smarty version 3.1.31, created on 2017-11-29 08:02:09
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Service/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a1e5b71f086a9_64855044',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cfcd6d5cf3a8f2402c63af5653f3b7090b1443c3' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Service/index.tpl',
      1 => 1498935336,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Service/_list.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5a1e5b71f086a9_64855044 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<h5 class="col s10 expand" style="cursor: pointer;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Filter'];?>
</h5>
					<h5 class="col s2 right-align expand" style="cursor: pointer;"><i class="material-icons small expandArrow">keyboard_arrow_down</i></h5>
					<div class="input-field col s12 m9" style="display: none;">
						<input id="searchName" name="searchName" type="text" class="validate">
						<label for="searchName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Service'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<select id="searchDescription" name="searchDescription">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Descriptions'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<input id="searchPrice" name="searchPrice" type="number" class="validate">
						<label for="searchPrice"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Price'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<input id="searchDelay" name="searchDelay" type="number" class="validate">
						<label for="searchDelay"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delay'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<select id="searchDelayType" name="searchDelayType">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['delayTypes']->value, 'd');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['d']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['d']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['d']->value['name']];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['DelayType'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<select id="searchStatus" name="searchStatus">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<select id="searchBrand" name="searchBrand">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brands']->value, 'b');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['b']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['b']->value['id'];?>
" data-models='<?php echo json_encode($_smarty_tpl->tpl_vars['b']->value['models']);?>
'><?php echo $_smarty_tpl->tpl_vars['b']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brands'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<select id="searchModel" name="searchModel">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Models'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<select id="searchCountry" name="searchCountry">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['countries']->value, 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['c']->value['name_en'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
</label>
					</div>
					<div class="input-field col s6 m3" style="display: none;">
						<select id="searchCompany" name="searchCompany">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['companies']->value, 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
</label>
					</div>
					<div class="col s12 right-align" style="display: none;"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	<?php if (isset($_GET['message'])) {?>
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text <?php echo $_smarty_tpl->tpl_vars['messageColor']->value;?>
">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h6>
			</div>
		</div>
	</div>
	<?php }?>
	
	<div id="list"><?php $_smarty_tpl->_subTemplateRender('file:Admin/Service/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="<?php echo URL;?>
service/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			<?php if (array_search(24,$_SESSION['user']['privileges'])) {?>
			<li><a href="<?php echo URL;?>
service/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
			<?php }?>
		</ul>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});
		});

		var sort = 'desc';
		var orderBy = 'name';

		$('#list').on('click', 'a.orderByName', function(e){
			orderBy = 'name';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByDescription', function(e){
			orderBy = 'checkDescriptions';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		$('#searchForm').children().change(function(){
			search();
		});

		$('.expand').click(function(){
			expand();
		});

		$('#searchBrand').change(function(){
			showModels();
		});

		function expand(){
			if ($('#searchForm').children().not('.expand').is(":visible")) {
				$('#searchForm').children().not('.expand').hide();
				$('#searchForm').css("marginBottom", '-5px');
				$('.expandArrow').text('keyboard_arrow_up');
			} else {
				$('#searchForm').children().not('.expand').show();
				$('#searchForm').css("marginBottom", '-40px');
				$('.expandArrow').text('keyboard_arrow_down');
			}
		};

		function showModels(){
				$('#searchModel').material_select('destroy');

				var models = '<option value=""><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>';
				var data = $('#searchBrand :selected').attr('data-models');
				data = JSON.parse(data);
				$.each(data, function(im, model) {
					models += "<option value=" + data[im].id + ">" + data[im].name + "</option>";
				});
				$('#searchModel').html(models);
				$('#searchModel').material_select();
			};

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '<?php echo URL;?>
service/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
