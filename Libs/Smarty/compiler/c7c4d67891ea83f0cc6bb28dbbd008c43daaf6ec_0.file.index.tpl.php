<?php
/* Smarty version 3.1.31, created on 2017-07-07 16:04:56
  from "/opt/lampp/htdocs/neounlock/Views/template/Faq/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_595f9508b76336_23343445',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7c4d67891ea83f0cc6bb28dbbd008c43daaf6ec' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Faq/index.tpl',
      1 => 1499282160,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Faq/_search.tpl' => 1,
    'file:Faq/_list.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_595f9508b76336_23343445 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px" class="animated fadeInLeft">Preguntas frecuentes</h2>
			<!-- <p>
				Navega entre las preguntas mas realizadas por nuestros clientes y busca aquellas que sean de tu interés. <br>
				Si no encuentras la respuesta a tu pregunta en el listado, te invitamos a realizarla al final de la página. <br>
				Estamos para ayudarte.
			</p> -->
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Faq/_search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Faq/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="container" style="margin-top: 50px">
		<div class="card-panel">
			<h3>¿Tienes otra consulta?</h3>
			<p>Haz tu pregunta y te contestaremos a la brevedad.</p><br>

			<form class="row">
				<div class="input-field col s12">
					<i class="material-icons prefix">email</i>
					<input id="icon_prefix" type="email" class="validate">
					<label for="icon_prefix">Mail de contacto</label>
				</div>
				<div class="input-field col s12">
					<i class="material-icons prefix">mode_edit</i>
					<textarea id="textarea" class="materialize-textarea"></textarea>
					<label for="textarea">Su consulta</label>
				</div>
				<div class="center">
					<button class="btn waves-effect waves-light" type="submit" name="action">Enviar
						<i class="material-icons right">send</i>
					</button>
				</div>
			</form>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){
			
		});
	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
