<?php
/* Smarty version 3.1.31, created on 2017-02-25 21:07:06
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Country\companies.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58b1f1fa3247f5_76467899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '47f14428d703a62725ff345ef6651be78043f12d' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Country\\companies.tpl',
      1 => 1488056813,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/sidenav.tpl' => 1,
    'file:Public/navbar.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58b1f1fa3247f5_76467899 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12" id="errorArea" hidden>
			<div class="card-panel red white-text">
			<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
			</div>
		</div>
		<div class="col s12"><h3><?php echo $_smarty_tpl->tpl_vars['countryName']->value;?>
</h3></div>
		<div class="col s12 m6 l6">
			<div class="card-panel">
				<form class="">
					<div class="input-field">
						<select multiple id="companies" name="companies" value="">
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['companies']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['l']->value['checked']) && $_smarty_tpl->tpl_vars['l']->value['checked'] == '1') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label class="left-align"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
</label>
					</div>
					<div class="right-align">
						<a href="<?php echo URL;?>
country" class="waves-effect waves-light btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Back'];?>
</a>
						<a id="save" class="waves-effect waves-light btn"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
					</div>
				</form>
			</div>
		</div>
		<div class="col s12 m6 l6">
			<div class="card-panel">
				<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['Selected'];?>
</h5>
				<ul id="list"></ul>
			</div>
		</div>
	</div>
	
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			$('select').material_select();
			loadList();
		});

		$('#companies').change(function(){
			loadList();
		});

		//Muestra el listado de paises seleccionados
		function loadList(){
			var list = '';
			$('#companies :selected').each(function(i, el){
				list += '<li> ' + $(el).text() + '</li> ';
			});
			$('#list').html(list);
		};

		$('#save').click(function(){

			$('form').parent().parent().parent().hide();
			$('#loading').fadeIn();
			var formData = $('#companies').val();

			$.ajax({
					url: '<?php echo URL;?>
country/companies/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
',
					type: 'POST',
					data:  {'companies': formData},
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
country/?message=companies';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').parent().parent().parent().hide();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
