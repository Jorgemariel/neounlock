<?php
/* Smarty version 3.1.31, created on 2017-03-07 22:53:02
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Admin\Model\_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58bf39ce13d327_93931709',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '38ee5c667e44346384a4b9e01116651729cfd0f9' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Admin\\Model\\_list.tpl',
      1 => 1488481731,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58bf39ce13d327_93931709 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m6 l6 strong">
				<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
</a>
			</div>
			<div class="col hide-on-small-only m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByPosition"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Position'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByPosition"><i class="material-icons">swap_vert</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<?php $_smarty_tpl->_assignInScope('image', "Views/img/models/".((string)$_smarty_tpl->tpl_vars['l']->value['id']).".".((string)$_smarty_tpl->tpl_vars['l']->value['img']));
?>
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m6 l6"><strong class="hide-on-small-only"><?php echo $_smarty_tpl->tpl_vars['l']->value['brand'];?>
 </strong><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</div>
				<div class="col hide-on-small-only m2 l2 center"><?php echo $_smarty_tpl->tpl_vars['l']->value['position'];?>
</div>
				<div class="col s2 m2 l2">
					<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
				<div class="col s2 m2 l2">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['status']) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12 m4">
					<strong>Position: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['position'];?>

				</div>
				<div class="col s12 m4">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['showCountry'] || $_smarty_tpl->tpl_vars['l']->value['showCompany'] || $_smarty_tpl->tpl_vars['l']->value['showPRD'] || $_smarty_tpl->tpl_vars['l']->value['showMEP']) {?>
					<strong>Show:</strong>
					<ul>
						<?php if ($_smarty_tpl->tpl_vars['l']->value['showCountry']) {?><li><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
</li><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['l']->value['showCompany']) {?><li><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
</li><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['l']->value['showPRD']) {?><li>PRD</li><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['l']->value['showMEP']) {?><li>MEP</li><?php }?>
					</ul>
					<?php }?>
				</div>
				<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
				<div class="col s12 m4 center" style="margin-top: 3%">
					<img src="<?php echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;?>
" style="max-height:70px;">
				</div>
				<?php }?>
				<div class="col s12 m6">
					<a href="<?php echo URL;?>
model/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="col waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
					<a href="<?php echo URL;?>
model/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class=" col waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
