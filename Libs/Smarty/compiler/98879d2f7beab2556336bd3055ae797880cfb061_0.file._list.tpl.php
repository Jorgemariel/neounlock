<?php
/* Smarty version 3.1.31, created on 2017-04-08 04:08:50
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Admin\Order\_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58e86252cc8341_01176972',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98879d2f7beab2556336bd3055ae797880cfb061' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Admin\\Order\\_list.tpl',
      1 => 1491624528,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58e86252cc8341_01176972 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header" style="padding-left: 0;">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col hide-on-small-only m1 strong center">
				<a href="#" class="orderById">ID</a>
			</div>
			<div class="col s4 m2 strong center-align">
				<a href="#" class="hide-on-small-only orderByTracking"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Tracking'];?>
</a>
				<a href="#" class="hide-on-med-and-up center-icon orderByTracking"><i class="material-icons">code</i> </a>
			</div>
			<div class="col s6 m5 strong">
				<a href="#" class="hide-on-small-only orderByService"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Service'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByService center-align"><i class="material-icons">book</i></a>
			</div>
			<div class="col s2 m3 strong right-align">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">content_paste</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<li data-id="<?php echo $_smarty_tpl->tpl_vars['l']->value['tracking'];?>
">
		<div class="collapsible-header" style="padding-left: 0;">
			<div class="row" style="margin: 0;">
				<div class="col hide-on-small-only m1 center"><?php echo sprintf("%05d",$_smarty_tpl->tpl_vars['l']->value['id']);?>
</div>
				<div class="col s4 m2 center-align"><?php echo $_smarty_tpl->tpl_vars['l']->value['tracking'];?>
</div>
				<div class="col s6 m5 truncate"><?php echo $_smarty_tpl->tpl_vars['l']->value['service'];?>
</div>
				<div class="col s2 m3 right">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['id_status'] == 1) {?>
					<i class="material-icons yellow-text">timer</i>
					<?php } elseif ($_smarty_tpl->tpl_vars['l']->value['id_status'] == 2) {?>
					<i class="material-icons orange-text">timer_off</i>
					<?php } elseif ($_smarty_tpl->tpl_vars['l']->value['id_status'] == 3) {?>
					<i class="material-icons green-text">check</i>
					<?php } elseif ($_smarty_tpl->tpl_vars['l']->value['id_status'] == 4) {?>
					<i class="material-icons blue-text">done_all</i>
					<?php } elseif ($_smarty_tpl->tpl_vars['l']->value['id_status'] == 5) {?>
					<i class="material-icons red-text">report_problem</i>
					<?php } elseif ($_smarty_tpl->tpl_vars['l']->value['id_status'] == 6) {?>
					<i class="material-icons black-text">cancel</i>
					<?php }?>
					<div class="hide-on-med-and-down"><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['l']->value['status']];?>
</div>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 5px 20px; margin-bottom: 0;">
				<div class="col s12 m6">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['PhoneData'];?>
</h5>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['imei'])) {?> <strong>IMEI: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['imei'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['brand'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brand'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['brand'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['model'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['model'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['country'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['country'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['company'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['company'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['mep']) && !empty($_smarty_tpl->tpl_vars['l']->value['mep'])) {?> <strong>MEP: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['mep'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['prd']) && !empty($_smarty_tpl->tpl_vars['l']->value['prd'])) {?> <strong>PRD: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['prd'];?>
 <br> <?php }?>
				</div>
				<div class="col s12 m6">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderData'];?>
</h5>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['entry_date'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['EntryDate'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['entry_date'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['tracking'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Tracking'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['tracking'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['dalivery_date'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['DeliveryDate'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['dalivery_date'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['survey_date'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['SurveyDate'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['survey_date'];?>
 <br> <?php }?>
				</div>
			</div>
			<div class="row" style="padding: 5px 20px; margin-bottom: 0;">
				<div class="col s12 m6">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['ClientData'];?>
</h5>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['name'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Name'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['email'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['eMail'];?>
: </strong> <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['l']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['l']->value['email'];?>
</a> <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['ip'])) {?> <strong>IP: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['ip'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['language'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['language'];?>
 <br> <?php }?>
				</div>
				<div class="col s12 m6">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['ServiceData'];?>
</h5>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['service'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Service'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['service'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['service_price'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Price'];?>
: </strong>USD <?php echo $_smarty_tpl->tpl_vars['l']->value['service_price'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['service_delay'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delay'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['service_delay'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['l']->value['delay_type']];?>
<br> <?php }?>
				</div>
			</div>
			<div class="row" style="padding: 5px 20px; margin-bottom: 0;">
				<?php if ($_smarty_tpl->tpl_vars['l']->value['id_status'] >= 3) {?>
				<div class="col s12 m6">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['PaymentData'];?>
</h5>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['payment_date'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['PaymentDate'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['payment_date'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['payment_method'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['PaymentMethod'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['payment_method'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['id_mercadopago'])) {?> <strong>ID (MP): </strong>USD <?php echo $_smarty_tpl->tpl_vars['l']->value['id_mercadopago'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['id_paypal'])) {?> <strong>ID (PP): </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['id_paypal'];?>
 <br> <?php }?>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['l']->value['id_status'] >= 4) {?>
				<div class="col s12 m6">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['Codes'];?>
</h5>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['code1'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 1: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['code1'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['code2'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 2: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['code2'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['code3'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 3: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['code3'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['code4'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 4: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['code4'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['code5'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 5: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['code5'];?>
 <br> <?php }?>
					<?php if (isset($_smarty_tpl->tpl_vars['l']->value['code6'])) {?> <strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 6: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['code6'];?>
 <br> <?php }?>
				</div>
				<?php }?>
			</div>
			<div class="row">
				<div class="col s12 m12">
					<div class="right">
						<?php if ($_smarty_tpl->tpl_vars['l']->value['id_status'] == 3) {?>
						<a href="#codes" class="waves-effect waves-teal btn-flat modal-trigger"><?php echo $_smarty_tpl->tpl_vars['lang']->value['SendCodes'];?>
</a>
						<?php }?>
						<a href="<?php echo URL;?>
order/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
					</div>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<h5 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['ResultsFound'];?>
: <?php echo count($_smarty_tpl->tpl_vars['list']->value);?>
</h5>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
