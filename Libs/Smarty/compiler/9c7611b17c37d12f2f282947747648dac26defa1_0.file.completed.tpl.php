<?php
/* Smarty version 3.1.31, created on 2018-04-24 02:00:31
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/completed.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5adeb9ef6e9ba3_90014679',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c7611b17c37d12f2f282947747648dac26defa1' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/completed.tpl',
      1 => 1499588432,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5adeb9ef6e9ba3_90014679 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/home/jorge/Dev/Proyectos/neounlock/Libs/Smarty/plugins/modifier.date_format.php';
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingStatus'];?>
</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Order'];?>
 <strong><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['order']->value['tracking'], 'UTF-8');?>
</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel green white-text valign-wrapper" style="margin: 0 20px">
				<i class="material-icons valign" style="margin-right: 5px">done_all</i>
				<h6><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCodeAvailable'];?>
</h6>
			</div>

			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCode'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['code1'];?>
</a></div></li>
				<li class="collection-item"><div>IMEI<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['imei'];?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['DeliveryDate'];?>
<a href="#!" class="secondary-content"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['order']->value['delivery_date'],"%D %H:%M");?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['PaymentMethod'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['payment_method'];?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['DelayTime'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['delay_time'];?>
</a></div></li>
			</ul>

			<div class="row" style="margin-top: 20px">
				<div class="col s12 m6 l4 center">
					<a class="waves-effect waves-light btn-large blue" style="margin: 10px; width: 85%"><i class="material-icons left">info</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['Tutorial'], 'UTF-8');?>
</a>
				</div>
				<div class="col s12 m6 l4 center">
					<a class="waves-effect waves-light btn-large red" style="margin: 10px; width: 85%"><i class="material-icons left">help</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['Help'], 'UTF-8');?>
</a>
				</div>
				<div class="col s12 m6 l4 offset-m3 center">
					<a class="waves-effect waves-light btn-large green" style="margin: 10px; width: 85%"><i class="material-icons left">book</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['Survey'], 'UTF-8');?>
</a>
				</div>
			</div>

			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderDetail'];?>
</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<ul class="collection" style="margin: 20px">
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['code2']) && !empty($_smarty_tpl->tpl_vars['order']->value['code2'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCode'];?>
 2<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['code2'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['code3']) && !empty($_smarty_tpl->tpl_vars['order']->value['code3'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCode'];?>
 3<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['code3'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['code4']) && !empty($_smarty_tpl->tpl_vars['order']->value['code4'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCode'];?>
 4<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['code4'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['code5']) && !empty($_smarty_tpl->tpl_vars['order']->value['code5'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCode'];?>
 5<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['code5'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['code6']) && !empty($_smarty_tpl->tpl_vars['order']->value['code6'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockCode'];?>
 6<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['code6'];?>
</a></div></li>
				<?php }?>
			</ul>
			<ul class="collection" style="margin: 20px">

				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['brand']) && !empty($_smarty_tpl->tpl_vars['order']->value['brand'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brand'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['brand'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['model']) && !empty($_smarty_tpl->tpl_vars['order']->value['model'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['model'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['country']) && !empty($_smarty_tpl->tpl_vars['order']->value['country'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['country'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['company']) && !empty($_smarty_tpl->tpl_vars['order']->value['company'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['company'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['mep']) && !empty($_smarty_tpl->tpl_vars['order']->value['mep'])) {?>
				<li class="collection-item"><div>MEP<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['mep'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['prd']) && !empty($_smarty_tpl->tpl_vars['order']->value['prd'])) {?>
				<li class="collection-item"><div>PRD<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['prd'];?>
</a></div></li>
				<?php }?>
			</ul>
			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Service'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['service'];?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Price'];?>
<a href="#!" class="secondary-content">USD <?php echo $_smarty_tpl->tpl_vars['order']->value['service_price'];?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delay'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['service_delay'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['order']->value['delay_type']];?>
</a></div></li>
			</ul>
			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['EntryDate'];?>
<a href="#!" class="secondary-content"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['order']->value['entry_date'],"%D %H:%M");?>
</a></div></li>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['PaymentDate'];?>
<a href="#!" class="secondary-content"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['order']->value['payment_date'],"%D %H:%M");?>
</a></div></li>

				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Survey'];?>

					<?php if ($_smarty_tpl->tpl_vars['order']->value['survey_score'] != null) {?>
						<div href="#!" class="secondary-content">
						<?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['order']->value['survey_score']+1 - (1) : 1-($_smarty_tpl->tpl_vars['order']->value['survey_score'])+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
							<i class="material-icons yellow-text">star</i>
						<?php }
}
?>

						</div>
					<?php } else { ?>
						<div href="#!" class="secondary-content" style="padding-bottom: 9px;">
							<a class="btn right green" href="<?php echo URL;?>
order/survey/<?php echo $_smarty_tpl->tpl_vars['order']->value['tracking'];?>
">
								<i class="material-icons left">book</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['Survey'], 'UTF-8');?>

							</a>
						</div>
					<?php }?>
				</div></li>
			</ul>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){

		});
	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
