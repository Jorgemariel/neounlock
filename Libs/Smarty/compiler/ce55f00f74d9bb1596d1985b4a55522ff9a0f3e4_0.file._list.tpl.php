<?php
/* Smarty version 3.1.31, created on 2017-11-29 08:02:16
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Email/_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a1e5b78ccc0d5_13970817',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ce55f00f74d9bb1596d1985b4a55522ff9a0f3e4' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Email/_list.tpl',
      1 => 1498935568,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a1e5b78ccc0d5_13970817 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m8 l8 strong">
				<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Name'];?>
</a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderById"><?php echo $_smarty_tpl->tpl_vars['lang']->value['File'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderById"><i class="material-icons">insert_drive_file</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<?php $_smarty_tpl->_assignInScope('countFiles', 0);
?>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['files'], 'f');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['f']->value) {
?>
		<?php if ($_smarty_tpl->tpl_vars['f']->value == '1') {?> 
			<?php $_smarty_tpl->_assignInScope('countFiles', $_smarty_tpl->tpl_vars['countFiles']->value+1);
?> 
		<?php }?>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m8 l8"><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</div>
				<div class="col s2 m2 l2 center"><?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
.tpl</div>
				<div class="col s2 m2 l2 center"><?php echo $_smarty_tpl->tpl_vars['countFiles']->value;?>
/<?php echo count($_smarty_tpl->tpl_vars['languages']->value);?>
</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12" style="margin-bottom: 10px;">
					<p><?php echo $_smarty_tpl->tpl_vars['l']->value['description'];?>
</p>
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['Files'];?>
:</h5>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'la');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['la']->value) {
?>
					<h6>
						<?php echo $_smarty_tpl->tpl_vars['la']->value['name'];?>
 
						<i class="material-icons icon-center-text <?php if ($_smarty_tpl->tpl_vars['l']->value['files'][$_smarty_tpl->tpl_vars['la']->value['id']] == '1') {?>green-text">check<?php } else { ?>red-text">close<?php }?></i>
					</h6>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

				</div>
				<div class="col s12">
					<div class="right">
						<?php if (array_search(38,$_SESSION['user']['privileges'])) {?>
						<a href="<?php echo URL;?>
email/emails/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="col waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Emails'];?>
</a>
						<?php }?>
						<?php if (array_search(29,$_SESSION['user']['privileges'])) {?>
						<a href="<?php echo URL;?>
email/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="col waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
						<?php }?>
						<?php if (array_search(30,$_SESSION['user']['privileges'])) {?>
						<a href="<?php echo URL;?>
email/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class=" col waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
						<?php }?>
						
					</div>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
