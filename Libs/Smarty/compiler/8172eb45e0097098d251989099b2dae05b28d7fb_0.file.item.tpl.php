<?php
/* Smarty version 3.1.31, created on 2018-04-24 23:42:34
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/User/item.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5adfa4cac66174_04617656',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8172eb45e0097098d251989099b2dae05b28d7fb' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/User/item.tpl',
      1 => 1498763536,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5adfa4cac66174_04617656 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12 m8 l9">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12">
							<input id="username" name="username" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['user']->value['username'])) {
echo $_smarty_tpl->tpl_vars['user']->value['username'];
}?>">
							<label for="username" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['User'];?>
</label>
						</div>
						<div class="input-field col s12">
							<input id="email" name="email" type="email" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['user']->value['email'])) {
echo $_smarty_tpl->tpl_vars['user']->value['email'];
}?>">
							<label for="email" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['EmailRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Email'];?>
</label>
						</div>
						<div class="input-field col s12">
							<input id="password" name="password" type="password" class="validate" <?php if (isset($_smarty_tpl->tpl_vars['user']->value['password'])) {?>placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['LeaveEmptyNotChange'];?>
"<?php }?>>
							<label for="password" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['PasswordRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Password'];?>
</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="status" name="status" value="<?php if (isset($_smarty_tpl->tpl_vars['user']->value['status'])) {
echo $_smarty_tpl->tpl_vars['user']->value['status'];
}?>">
								<option value="1" <?php if (isset($_smarty_tpl->tpl_vars['user']->value['status']) && $_smarty_tpl->tpl_vars['user']->value['status'] == 1) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Active'];?>
</option>
								<option value="0" <?php if (isset($_smarty_tpl->tpl_vars['user']->value['status']) && $_smarty_tpl->tpl_vars['user']->value['status'] == 0) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Inactive'];?>
</option>
							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="language" name="language" value="<?php if (isset($_smarty_tpl->tpl_vars['user']->value['id_language'])) {
echo $_smarty_tpl->tpl_vars['user']->value['id_language'];
}?>">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['user']->value['id_language']) && $_smarty_tpl->tpl_vars['user']->value['id_language'] == $_smarty_tpl->tpl_vars['l']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</label>
						</div>
						<div class="file-field input-field col s12">
							<div class="btn blue">
								<span><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<h5 class="col s12" style="margin-top: 20px;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Privileges'];?>
</h5>
						<?php if (isset($_smarty_tpl->tpl_vars['user']->value)) {?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['user']->value['privileges'], 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
						<p class="input-field col s12 m6 l4">
							<input class="privileges" type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['p']->value['checked']) && $_smarty_tpl->tpl_vars['p']->value['checked'] == '1') {?>checked<?php }?>/>
							<label for="<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['p']->value['area']];?>
: </strong><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['p']->value['name']];?>
</label>
						</p>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						<?php } else { ?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['privileges']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
						<p class="input-field col s12 m6 l4">
							<input class="privileges" type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
" id="<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
"/>
							<label for="<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['p']->value['area']];?>
: </strong><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['p']->value['name']];?>
</label>
						</p>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						<?php }?>

						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="<?php echo URL;?>
user" class="waves-effect waves-light btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
							<?php if (!isset($_smarty_tpl->tpl_vars['user']->value)) {?>
							<a id="save" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
							<?php } else { ?>
							<a id="update" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Update'];?>
</a>
							<?php }?>
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['user']->value['img'])) {
$_smarty_tpl->_assignInScope('image', "Views/img/users/".((string)$_smarty_tpl->tpl_vars['user']->value['id']).".".((string)$_smarty_tpl->tpl_vars['user']->value['img']));
}?>
			<div class="col s12 m4 l3">
				<h4 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Preview'];?>
</h4>
				<div class="user card hoverable waves-effect" id="preview" style="min-height: 100px; margin: 10px; min-width: 100%;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php if (isset($_smarty_tpl->tpl_vars['user']->value['img']) && file_exists($_smarty_tpl->tpl_vars['image']->value)) {
echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;
}?>"  id="img-prev" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" id="name-prev" style="margin-top: 20px"><?php if (isset($_smarty_tpl->tpl_vars['user']->value['username'])) {
echo $_smarty_tpl->tpl_vars['user']->value['username'];
} else {
echo $_smarty_tpl->tpl_vars['lang']->value['User'];
}?></h6>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Carga la vista previo del nombre de la marca
			$('#username').on('keyup keypress blur change', function(){
				var content = $('#username').val();
				$('#name-prev').html(content);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var username 	= $('#username');
				var email 		= $('#email');
				var password 	= $('#password');
				var status 		= $('#status');
				var language 	= $('#language');

				if (!username.val())
				{
					username.addClass('invalid');
					username.focus();
					return false;
				}

				if (!email.val())
				{
					email.addClass('invalid');
					email.focus();
					return false;
				}

				if (!password.val())
				{
					password.addClass('invalid');
					password.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('username', username.val());
				formData.append('email', email.val());
				formData.append('password', password.val());
				formData.append('status', status.val());
				formData.append('language', language.val());
				formData.append('img', $('input[type=file]')[0].files[0]);

				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['privileges']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
				formData.append('privilege_<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
', $('#<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
').is(':checked'));
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


				$.ajax({
					url: '<?php echo URL;?>
user/add',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
user/?message=added';
						}
						else if (data == 2)
						{
							window.location = '<?php echo URL;?>
user/?message=noimage';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});
			<?php if (isset($_smarty_tpl->tpl_vars['user']->value)) {?>
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var username 	= $('#username');
				var email 		= $('#email');
				var password 	= $('#password');
				var status 		= $('#status');
				var language 	= $('#language');

				if (!username.val())
				{
					username.addClass('invalid');
					username.focus();
					return false;
				}

				if (!email.val())
				{
					email.addClass('invalid');
					email.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('username', username.val());
				formData.append('email', email.val());
				formData.append('password', password.val());
				formData.append('status', status.val());
				formData.append('language', language.val());
				formData.append('img', $('input[type=file]')[0].files[0]);

				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['privileges']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
				formData.append('privilege_<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
', $('#<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
').is(':checked'));
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


				$.ajax({
					url: '<?php echo URL;?>
user/edit/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
/?img=<?php echo $_smarty_tpl->tpl_vars['user']->value['img'];?>
',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
user/?message=edited';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('#preview').parent().fadeIn();
							$('form').fadeIn();
							$('#loading').hide();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			<?php }?>
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
