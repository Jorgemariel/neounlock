<?php
/* Smarty version 3.1.31, created on 2017-05-14 20:48:56
  from "/opt/lampp/htdocs/neounlock/Views/template/Public/nav.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5918a69804d6b8_43905474',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '79c6937663f0102d0961ee4adacfd6e1f4abb7b7' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Public/nav.tpl',
      1 => 1494787025,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/_tracking.tpl' => 1,
  ),
),false)) {
function content_5918a69804d6b8_43905474 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Dropdown Structure -->
<ul id="languages" class="dropdown-content">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<li>
		<a data-url="<?php echo URL;?>
home/language/<?php echo $_smarty_tpl->tpl_vars['l']->value['short_name'];?>
" class="grey-text text-darken-3 lang">
			<img src="<?php echo URL;?>
Views/img/languages/<?php echo $_smarty_tpl->tpl_vars['l']->value['img'];?>
.png" style="padding-right: 10px;">
			<?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>

		</a>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>

<ul id="languagesMobile" class="dropdown-content">
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<li>
		<a data-url="<?php echo URL;?>
home/language/<?php echo $_smarty_tpl->tpl_vars['l']->value['short_name'];?>
" class="grey-text text-darken-3 lang">
			<img src="<?php echo URL;?>
Views/img/languages/<?php echo $_smarty_tpl->tpl_vars['l']->value['img'];?>
.png" style="padding-right: 10px;">
			<?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>

		</a>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<nav class="blue darken-1">
	<div class="nav-wrapper">
		<a href="<?php echo URL;?>
" class="brand-logo">
			<img class="responsive-img" src="<?php echo URL;?>
Views/img/logo.png" style="height: 50px; margin: -4px 0 0 10px; vertical-align: middle;">
		</a>
		<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		<a class="dropdown-button hide-on-large-only right" data-beloworigin="true" data-constrainwidth="false" data-activates="languagesMobile" style="cursor: pointer;">
			<img src="<?php echo URL;?>
Views/img/languages/<?php echo mb_strtolower($_SESSION['language'], 'UTF-8');?>
.png" style="margin-right: 5px;">
			<?php echo mb_strtoupper($_SESSION['language'], 'UTF-8');?>

			<i class="material-icons right" style="margin-left: 0px;">arrow_drop_down</i>
		</a>
		<ul class="right hide-on-med-and-down">
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'home') {?> class="active"<?php }?>>
				<a href="<?php echo URL;?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Home'];?>
</a>
			</li>
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'tracking') {?> class="active"<?php }?>>
				<a class="modal-trigger" href="#trackingmodal"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Tracking'];?>
</a>
			</li>
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'faqs') {?> class="active"<?php }?>>
				<a href="<?php echo URL;?>
faqs"><?php echo $_smarty_tpl->tpl_vars['lang']->value['FAQs'];?>
</a>
			</li>
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'contact') {?> class="active"<?php }?>>
				<a href="<?php echo URL;?>
contact"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Contact'];?>
</a>
			</li>
			<li>
				<a class="dropdown-button" data-beloworigin="true" data-constrainwidth="false" data-activates="languages"><img src="<?php echo URL;?>
Views/img/languages/<?php echo mb_strtolower($_SESSION['language'], 'UTF-8');?>
.png" style="margin-right: 5px;"><?php echo mb_strtoupper($_SESSION['language'], 'UTF-8');?>
<i class="material-icons right" style="margin-left: 0px;">arrow_drop_down</i></a>
			</li>
		</ul>
		<ul class="side-nav" id="mobile-demo">
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'home') {?> class="active"<?php }?>>
				<a href="<?php echo URL;?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Home'];?>
</a>
			</li>
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'tracking') {?> class="active"<?php }?>>
				<a class="modal-trigger" href="#trackingmodal"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Tracking'];?>
</a>
			</li>
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'faqs') {?> class="active"<?php }?>>
				<a href="<?php echo URL;?>
faqs"><?php echo $_smarty_tpl->tpl_vars['lang']->value['FAQs'];?>
</a>
			</li>
			<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'contact') {?> class="active"<?php }?>>
				<a href="<?php echo URL;?>
contact"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Contact'];?>
</a>
			</li>
		</ul>
	</div>
</nav>
<?php $_smarty_tpl->_subTemplateRender('file:Public/_tracking.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
