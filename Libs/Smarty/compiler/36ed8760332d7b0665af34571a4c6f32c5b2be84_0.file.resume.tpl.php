<?php
/* Smarty version 3.1.31, created on 2017-11-29 08:08:13
  from "/opt/lampp/htdocs/neounlock/Views/template/Home/resume.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a1e5cdd0684b5_80939199',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '36ed8760332d7b0665af34571a4c6f32c5b2be84' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Home/resume.tpl',
      1 => 1499280231,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/_terms.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5a1e5cdd0684b5_80939199 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderResume'];?>
</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Order'];?>
 <strong><?php echo $_smarty_tpl->tpl_vars['order']->value['tracking'];?>
</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>
			<div class="center">
				<div style="display: inline-flex">
					<h4 style="margin-right: 5px">USD</h4>
					<h1 style="margin-top: 13px"><?php echo $_smarty_tpl->tpl_vars['order']->value['service_price'];?>
</h1>
				</div>
			</div>

			<div class="row">
				<div class="col l8 m10 s12 offset-l2 offset-m1">
					<ul class="collection">
						<li class="collection-item">
							<div><?php echo $_smarty_tpl->tpl_vars['lang']->value['AverageDelay'];?>
<a href="#!" class="secondary-content">FALTA TERMINAR</a></div>
						</li>
						<li class="collection-item">
							<div><?php echo $_smarty_tpl->tpl_vars['lang']->value['MaxDelay'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['service_delay'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['order']->value['delay_type']];?>
</a></div>
						</li>
						<li class="collection-item">
							<div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brand'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['brand'];?>
</a></div>
						</li>
						<?php if (isset($_smarty_tpl->tpl_vars['order']->value['model']) && !empty($_smarty_tpl->tpl_vars['order']->value['model'])) {?>
						<li class="collection-item">
							<div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['model'];?>
</a></div>
						</li>
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['order']->value['country']) && !empty($_smarty_tpl->tpl_vars['order']->value['country'])) {?>
						<li class="collection-item">
							<div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['country'];?>
</a></div>
						</li>
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['order']->value['company']) && !empty($_smarty_tpl->tpl_vars['order']->value['company'])) {?>
						<li class="collection-item">
							<div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['company'];?>
</a></div>
						</li>
						<?php }?>
						<li class="collection-item">
							<div>IMEI<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['imei'];?>
</a></div>
						</li>
						<?php if (isset($_smarty_tpl->tpl_vars['order']->value['mep']) && !empty($_smarty_tpl->tpl_vars['order']->value['mep'])) {?>
						<li class="collection-item">
							<div>MEP<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['mep'];?>
</a></div>
						</li>
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['order']->value['prd']) && !empty($_smarty_tpl->tpl_vars['order']->value['prd'])) {?>
						<li class="collection-item">
							<div>PRD<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['prd'];?>
</a></div>
						</li>
						<?php }?>
						<li class="collection-item">
							<div><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockType'];?>
<a href="#!" class="secondary-content">FALTA TERMINAR</a></div>
						</li>
					</ul>
				</div>
			</div>

			<div class="right-align">
				<a href="<?php echo URL;?>
home/payment/<?php echo $_smarty_tpl->tpl_vars['order']->value['tracking'];?>
" class="waves-effect waves-light btn-large green">
					<i class="material-icons right">send</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['ContinueToPay'];?>

				</a>
			</div>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/_terms.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	
	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){
			
		});
	<?php echo '</script'; ?>
>
	

</body>
</html><?php }
}
