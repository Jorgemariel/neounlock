<?php
/* Smarty version 3.1.31, created on 2017-05-01 02:34:25
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Admin\User\_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59069eb1c8c938_92695149',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9fc4ea3893e036142f54ab346b1019ba8fbb6c6f' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Admin\\User\\_list.tpl',
      1 => 1493606053,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59069eb1c8c938_92695149 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m3 strong">
				<a href="#" class="hide-on-small-only orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['User'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByName"><i class="material-icons">account_circle</i></a>
			</div>
			<div class="col hide-on-small-only m5 strong">
				<a href="#" class="hide-on-small-only orderByEmail"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Email'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByEmail"><i class="material-icons">mail</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<?php $_smarty_tpl->_assignInScope('image', "Views/img/users/".((string)$_smarty_tpl->tpl_vars['l']->value['id']).".".((string)$_smarty_tpl->tpl_vars['l']->value['img']));
?>
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m3"><?php echo $_smarty_tpl->tpl_vars['l']->value['username'];?>
</div>
				<div class="col hide-on-small-only m5"><?php echo $_smarty_tpl->tpl_vars['l']->value['email'];?>
</div>
				<div class="col s2 m2 l2">
					<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
				<div class="col s2 m2 l2">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['status']) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12 m8">
					<h5><?php echo $_smarty_tpl->tpl_vars['lang']->value['Privileges'];?>
: </h5>
					<ul>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['privileges'], 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
						<li><strong><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['p']->value['area']];?>
: </strong><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['p']->value['privilege']];?>
</li>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					</ul>
				</div>
				<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
				<div class="col s12 m4 center" style="margin-top: 3%">
					<img src="<?php echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;?>
" style="max-height:70px;">
				</div>
				<?php }?>
				<div class="col s12">
					<a href="<?php echo URL;?>
user/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="col waves-effect waves-teal btn-flat right"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
					<a href="<?php echo URL;?>
user/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class=" col waves-effect waves-teal btn-flat right"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
