<?php
/* Smarty version 3.1.31, created on 2018-07-01 01:31:39
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/_help.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b38592b120d49_58091567',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd7fc43c32c3572a545e8d9f43f30101f0033d1a4' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/_help.tpl',
      1 => 1530419497,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b38592b120d49_58091567 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="helpingmodal" class="modal">
	<div class="modal-content">
		<h4><?php echo $_smarty_tpl->tpl_vars['lang']->value['Help'];?>
</h4>
		<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['AskYourQuestion'];?>
</p>
        <div class="input-field" style="margin-top: 2rem;">
            <textarea id="textarea" name="textarea" class="materialize-textarea"></textarea>
            <label for="textarea"><?php echo $_smarty_tpl->tpl_vars['lang']->value['YourQuestion'];?>
</label>
        </div>
	</div>
	<div class="modal-footer">
		<button id="helping-go" type="submit" class="modal-action waves-effect btn green darken-2" style="margin: 5px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Send'];?>
</button>
		<a class="modal-action modal-close waves-effect btn-flat modal-close" style="margin: 5px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Close'];?>
</a>
	</div>
</div>
<?php }
}
