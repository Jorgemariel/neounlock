<?php
/* Smarty version 3.1.31, created on 2018-06-04 07:42:24
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Faq/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b14d140526618_54072578',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e04781ee7535adf2837835e74fe7cfc55a16c76' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Faq/index.tpl',
      1 => 1528089999,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Faq/_list.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b14d140526618_54072578 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<h5 class="col s10 expand" style="cursor: pointer;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Filter'];?>
</h5>
					<h5 class="col s2 right-align expand" style="cursor: pointer;"><i class="material-icons small expandArrow">keyboard_arrow_down</i></h5>
					<div class="input-field col s12 m6 l3" style="display: none;">
						<input id="searchName" name="searchName" type="text" class="validate">
						<label for="searchName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Faq'];?>
</label>
					</div>
					<div class="input-field col s8 m4 l2" style="display: none;">
						<input id="searchPosition" name="searchPosition" type="number" class="validate">
						<label for="searchPosition"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Position'];?>
</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchImage" name="searchImage">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchStatus" name="searchStatus">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchModel" name="searchModel">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchCountry" name="searchCountry">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchCompany" name="searchCompany">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchPRD" name="searchPRD">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label>PRD</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchMEP" name="searchMEP">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label>MEP</label>
					</div>
					<div class="col s12 right-align" style="display: none;"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	<?php if (isset($_GET['message'])) {?>
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text <?php echo $_smarty_tpl->tpl_vars['messageColor']->value;?>
">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h6>
			</div>
		</div>
	</div>
	<?php }?>

	<div id="list"><?php $_smarty_tpl->_subTemplateRender('file:Admin/Faq/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="<?php echo URL;?>
faqs/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			<?php if (array_search(41,$_SESSION['user']['privileges'])) {?>
			<li><a href="<?php echo URL;?>
faqs/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
			<?php }?>
		</ul>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});
		});

		var sort = 'desc';
		var orderBy = 'position'

		$('#list').on('click', 'a.orderByName', function(e){
			orderBy = 'name';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByPosition', function(e){
			orderBy = 'position';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByImage', function(e){
			orderBy = 'img';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		$('#searchForm').children().change(function(){
			search();
		});

		$('.expand').click(function(){
			expand();
		});

		function expand(){
			if ($('#searchForm').children().not('.expand').is(":visible")) {
				$('#searchForm').children().not('.expand').hide();
				$('#searchForm').css("marginBottom", '-5px');
				$('.expandArrow').text('keyboard_arrow_up');
			} else {
				$('#searchForm').children().not('.expand').show();
				$('#searchForm').css("marginBottom", '-40px');
				$('.expandArrow').text('keyboard_arrow_down');
			}
		};

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var searchName = $('#searchName');
			var searchPosition = $('#searchPosition');
			var searchImage = $('#searchImage');
			var searchStatus = $('#searchStatus');
			var searchModel = $('#searchModel');
			var searchCountry = $('#searchCountry');
			var searchCompany = $('#searchCompany');
			var searchMEP = $('#searchMEP');
			var searchPRD = $('#searchPRD');

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '<?php echo URL;?>
faq/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	<?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
