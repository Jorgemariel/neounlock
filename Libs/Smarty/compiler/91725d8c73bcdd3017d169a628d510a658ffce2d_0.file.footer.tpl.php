<?php
/* Smarty version 3.1.31, created on 2017-03-08 03:32:54
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Public\footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58bf7b66c650c2_40890938',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '91725d8c73bcdd3017d169a628d510a658ffce2d' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Public\\footer.tpl',
      1 => 1488943972,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58bf7b66c650c2_40890938 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Libs\\Smarty\\plugins\\modifier.date_format.php';
?>
<footer class="page-footer blue darken-1">
	<div class="container">
		<div class="row">
			<div class="col l6 m6 s12">
				<h5 class="white-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Contact'];?>
</h5><br>
				<a href="" class="grey-text text-lighten-3"><img src="<?php echo URL;?>
Views/img/facebook.png" class="responsive-img" style="width: 30px; margin-bottom: -10px"> /NeoUnlockOfficial</a><br><br>
				<a href="" class="grey-text text-lighten-3"><img src="<?php echo URL;?>
Views/img/whatsapp.png" class="responsive-img" style="width: 30px; margin-bottom: -10px"> +54 9 3515144316</a>
			</div>
			<div class="col l4 m4 offset-l2 offset-m2 s12">
				<h5 class="white-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['MoreInfo'];?>
</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="#!"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Terms'];?>
</a></li>
					<li><a class="grey-text text-lighten-3" href="#!"><?php echo $_smarty_tpl->tpl_vars['lang']->value['ReportProblem'];?>
</a></li>
					<li><a class="grey-text text-lighten-3" href="#!"><?php echo $_smarty_tpl->tpl_vars['lang']->value['AboutNeoUnlock'];?>
</a></li>
					<li><a class="grey-text text-lighten-3" href="#!"><?php echo $_smarty_tpl->tpl_vars['lang']->value['HowWork'];?>
</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright light-blue">
		<div class="container">
			© <?php echo smarty_modifier_date_format(time(),"%Y");?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value['AllRightReserved'];?>
 - NeoUnlock
			<a class="grey-text text-lighten-4 right" target="_blank" href="http://www.jorgemariel.com"><?php echo $_smarty_tpl->tpl_vars['lang']->value['MadeBy'];?>
 Jorge Mariel</a>
		</div>
	</div>
</footer><?php }
}
