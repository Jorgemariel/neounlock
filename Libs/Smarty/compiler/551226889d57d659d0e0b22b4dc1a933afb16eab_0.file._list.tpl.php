<?php
/* Smarty version 3.1.31, created on 2017-03-03 02:19:30
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Company\_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58b8d2b2a8f468_66463302',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '551226889d57d659d0e0b22b4dc1a933afb16eab' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Company\\_list.tpl',
      1 => 1488481742,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58b8d2b2a8f468_66463302 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m8 l8 strong">
				<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
</a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<?php $_smarty_tpl->_assignInScope('image', "Views/img/companies/".((string)$_smarty_tpl->tpl_vars['l']->value['id']).".".((string)$_smarty_tpl->tpl_vars['l']->value['img']));
?>
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m8 l8"><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</div>
				<div class="col s2 m2 l2 center" style="max-height: 42px">
					<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
				<div class="col s2 m2 l2">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['status']) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px 20px 0; margin-bottom: 0;">
				<div class="col s12 m8">
					<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['countries'])) {?>
						<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
:</strong><br>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['countries'], 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<?php echo $_smarty_tpl->tpl_vars['c']->value['name_en'];?>
<br>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php }?>
				</div>
				<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
				<div class="col s12 m4 center" style="margin-top: 3%">
					<img src="<?php echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;?>
" style="max-height:70px;">
				</div>
				<?php }?>
				<div class="col s12 right-align" style="margin: 10px 0">
					<a href="<?php echo URL;?>
company/countries/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['AdminCountries'];?>
</a>
					<a href="<?php echo URL;?>
company/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
					<a href="<?php echo URL;?>
company/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
