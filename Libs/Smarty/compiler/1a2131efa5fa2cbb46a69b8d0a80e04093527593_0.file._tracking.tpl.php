<?php
/* Smarty version 3.1.31, created on 2018-05-30 06:15:32
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Public/_tracking.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b0e2564f40238_01972766',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1a2131efa5fa2cbb46a69b8d0a80e04093527593' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Public/_tracking.tpl',
      1 => 1527653728,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b0e2564f40238_01972766 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="trackingmodal" class="modal">
	<div class="modal-content">
		<h4><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingOrder'];?>
</h4>
		<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingOrderHelp'];?>
</p>
		<div class="input-field" style="margin-top: 2rem;">
			<input id="order-code" name="order-code" type="text" class="validate" placeholder="XXXXXX">
			<label for="last_name" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingCodeValid'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingCode'];?>
</label>
		</div>
	</div>
	<div class="modal-footer">
		<button id="tracking-go" type="submit" class="modal-action waves-effect btn green darken-2" style="margin: 5px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Consult'];?>
</button>
		<a class="modal-action modal-close waves-effect btn-flat modal-close" style="margin: 5px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Close'];?>
</a>
	</div>
</div>
<?php }
}
