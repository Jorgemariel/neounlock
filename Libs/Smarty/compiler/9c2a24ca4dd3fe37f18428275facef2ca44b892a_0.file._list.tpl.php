<?php
/* Smarty version 3.1.31, created on 2018-04-27 10:35:30
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ae2e0d27a7d66_99202166',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9c2a24ca4dd3fe37f18428275facef2ca44b892a' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/_list.tpl',
      1 => 1524818122,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ae2e0d27a7d66_99202166 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s4 m3 l2 strong">
				<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Name'];?>
</a>
			</div>
			<div class="col s6 m7 l8 strong">
				<a href="#" class="orderByDescription"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Description'];?>
</a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<li data-id="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
">
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s4 m3 l2"><?php echo $_smarty_tpl->tpl_vars['l']->value['email'];?>
</div>
				<div class="col s6 m7 l8"><?php echo $_smarty_tpl->tpl_vars['l']->value['description'];?>
</div>
				<div class="col s2 m2 l2 center"><?php echo $_smarty_tpl->tpl_vars['l']->value['count'];?>
/<?php echo count($_smarty_tpl->tpl_vars['languages']->value);?>
</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12" style="margin-bottom: 10px;">
					<?php if (array_search(38,$_SESSION['user']['privileges'])) {?>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['languages'], 'la');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['la']->value) {
?>
					<a href="#content" class="waves-effect waves-teal btn-flat modal-trigger" data-content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['la']->value['content'], ENT_QUOTES, 'UTF-8', true);?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['la']->value['id_content'];?>
" data-language="<?php echo $_smarty_tpl->tpl_vars['la']->value['id_language'];?>
">
						<?php echo $_smarty_tpl->tpl_vars['la']->value['language'];?>

						<?php if (($_smarty_tpl->tpl_vars['la']->value['content'] != '' || $_smarty_tpl->tpl_vars['la']->value['content'] != null)) {?>
						<i class="material-icons green-text" style="display: inline-flex; vertical-align: middle; margin-bottom: 5px;">check</i>
						<?php } else { ?>
						<i class="material-icons red-text" style="display: inline-flex; vertical-align: middle; margin-bottom: 5px;">close</i> <?php }?>
					</a>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php }?>
				</div>
				<div class="col s12">
					<div class="right">
						<?php if (array_search(29,$_SESSION['user']['privileges'])) {?>
						<a href="<?php echo URL;?>
email/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="col waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
						<?php }?>
						<?php if (array_search(30,$_SESSION['user']['privileges'])) {?>
						<a href="<?php echo URL;?>
email/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class=" col waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
						<?php }?>

					</div>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
