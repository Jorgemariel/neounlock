<?php
/* Smarty version 3.1.31, created on 2017-07-07 02:06:48
  from "/opt/lampp/htdocs/neounlock/Views/template/Public/foot.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_595ed09879aa31_55096561',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8866dfd20bf5d9e800a6f6eef6b0792f3065e8cb' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Public/foot.tpl',
      1 => 1499386007,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_595ed09879aa31_55096561 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript" src="http://code.jquery.com/jquery-3.1.1.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
$(document).ready(function(){
	$(".button-collapse").sideNav();
	$(".dropdown-button").dropdown();
	$('.modal-trigger').leanModal();
});

$('#tracking-go').click(function(){trackingGo(); return false;});

$('.lan').click(function(){
	$('.dropdown-button').dropdown('open');
});

$('.lang').click(function(){
	$.ajax({
		url: $(this).data('url'),
		type: 'POST',
		success: function(data) {
			if (data == '1')
			{
				location.reload();
			}
			else
			{
				$('#loading').hide();
				$('form').fadeIn();
				$('#error-text').html(data);
				$('#error').fadeIn().delay(5000).slideUp('slow');
				var errorDiv =  $("#error").offset().top - 20;
				$(window).scrollTop(errorDiv);
			}
		},
		error: function(data) {
			alert(data);
		}
	});
	return false;
});

function trackingGo() {
	var order = $('#order-code');

	if (!order.val()) {
		order.next().attr('data-error', '<?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingCodeValid'];?>
');
		order.addClass('invalid');
		order.focus();
		return false;
	}

	if (order.val().length != 6) {
		order.next().attr('data-error', '<?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderLength6'];?>
');
		order.addClass('invalid');
		order.focus();
		return false;
	}

	window.location = '<?php echo URL;?>
tracking/resume/' + order.val();
}
<?php echo '</script'; ?>
><?php }
}
