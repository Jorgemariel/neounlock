<?php
/* Smarty version 3.1.31, created on 2017-05-14 20:48:56
  from "/opt/lampp/htdocs/neounlock/Views/template/Home/introduction.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5918a698061473_19345773',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ff205a906fcf4f1e19a8baead1a9559f18c5a024' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Home/introduction.tpl',
      1 => 1494787025,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5918a698061473_19345773 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="carousel carousel-slider center" data-indicators="true">
	<div class="carousel-fixed-item center">
		<a class="btn waves-effect white blue-text blue-text-2">Liberar ahora</a>
	</div>

	
	<div class="carousel-item light-blue white-text center-text">
		<h2>Libera tu celular</h2>
		<p class="white-text">En solo 3 simples pasos</p>
		<div class="container">
			<div class="hide-on-large-only red-text" style="margin-top: 30px">.</div>
			<div class="row">
				<div class="col s2 offset-s2 center">
					<div class="circle2 z-depth-2" style="background-color: #438ecb">
						<i class="material-icons medium white-text accent-text-4">assignment</i>
					</div>
					<h6 class="white-text" style="margin: 15px -15px 0 -15px;">Completa con tus datos</h6>
				</div>
				<div class="col s1"><i class="material-icons medium arrow">arrow_forward</i></div>
				<div class="col s2">
					<div class="circle2 z-depth-2" style="background-color: #fdc113">
						<i class="material-icons medium white-text accent-text-4">email</i>
					</div>
					<h6 class="white-text" style="margin: 15px -15px 0 -15px;">Recibe tu código de liberación</h6>
				</div>
				<div class="col s1"><i class="material-icons medium arrow">arrow_forward</i></div>
				<div class="col s2">
					<div class="circle2 z-depth-2" style="background-color: #f68f1e">
						<i class="material-icons medium white-text accent-text-4">lock_open</i>
					</div>
					<h6 class="white-text" style="margin: 15px -15px 0 -15px;">Libera tu celular</h6>
				</div>
			</div>
		</div>
	</div>

	<div class="carousel-item teal accent-4 white-text center-text">
		<h2>Libera tu celular</h2>
		<p class="white-text">En solo 3 simples pasos</p>
		<div class="container">
			<div class="hide-on-large-only red-text" style="margin-top: 30px">.</div>
			<div class="row">
				<div class="col s2 offset-s2 center">
					<div class="circle2 z-depth-2">
						<i class="material-icons medium blue-text accent-text-4">assignment</i>
					</div>
					<h6 class="white-text" style="margin: 15px -15px 0 -15px;">Completa con tus datos</h6>
				</div>
				<div class="col s1"><i class="material-icons medium arrow">arrow_forward</i></div>
				<div class="col s2">
					<div class="circle2 z-depth-2">
						<i class="material-icons medium blue-text accent-text-4">email</i>
					</div>
					<h6 class="white-text" style="margin: 15px -15px 0 -15px;">Recibe tu código de liberación</h6>
				</div>
				<div class="col s1"><i class="material-icons medium arrow">arrow_forward</i></div>
				<div class="col s2">
					<div class="circle2 z-depth-2">
						<i class="material-icons medium blue-text accent-text-4">lock_open</i>
					</div>
					<h6 class="white-text" style="margin: 15px -15px 0 -15px;">Libera tu celular</h6>
				</div>
			</div>
		</div>
	</div>
</div><?php }
}
