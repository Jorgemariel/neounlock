<?php
/* Smarty version 3.1.31, created on 2018-04-27 21:45:31
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/_content.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ae37ddb34fc60_98378485',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4030beb6f3739d96bcce8587185b54e2707ddafc' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/_content.tpl',
      1 => 1524820670,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/_loading.tpl' => 1,
  ),
),false)) {
function content_5ae37ddb34fc60_98378485 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="content" class="modal modal-fixed-footer">
	<div class="modal-content contentData">
		<h4><?php echo $_smarty_tpl->tpl_vars['lang']->value['Email'];?>
</h4>
		<div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="error-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['ErrorOcurred'];?>
</h6>
		</div>
		<div id="success" class="card-panel green white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="success-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['EmailTestSent'];?>
</h6>
		</div>
		<form id="contentForm" class="row">
			<div class="input-field col s12">
				<textarea id="contentText" class="materialize-textarea validate"></textarea>
			</div>
		</form>
	</div>
	<div class="modal-footer contentData">
		<?php if (array_search(39,$_SESSION['user']['privileges'])) {?>
		<a id="contentFormSend" class="modal-action waves-effect waves-green btn green" style="margin: 0 10px;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Update'];?>
</a>
		<?php }?>
		<a id="contentFormClose" class="modal-action modal-close waves-effect btn-flat" style="margin: 0 10px;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Close'];?>
</a>
		<?php if (array_search(39,$_SESSION['user']['privileges'])) {?>
		<a id="contentFormTest" class="modal-action waves-effect btn-flat" style="margin: 0 10px;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Test'];?>
</a>
		<?php }?>
	</div>
	<div id="contentFormLoading" class="center" style="display: none; padding-top: 200px; padding-bottom: 200px"><?php $_smarty_tpl->_subTemplateRender('file:Public/_loading.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
</div>
<?php }
}
