<?php
/* Smarty version 3.1.31, created on 2017-05-14 23:35:42
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Order/_codes.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5918cdae79eba6_47287168',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0bd600917b0bf807b5bbba8b84097a62ab08a0fe' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Order/_codes.tpl',
      1 => 1494787025,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/_loading.tpl' => 1,
  ),
),false)) {
function content_5918cdae79eba6_47287168 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="codes" class="modal">
	<div class="modal-content codesData">
		<h4><?php echo $_smarty_tpl->tpl_vars['lang']->value['SendUnlockCodes'];?>
</h4>
		<div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="error-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['ErrorOcurred'];?>
</h6>
		</div>
		<form id="codesForm" class="row">
			<div class="input-field col s12 m6">
				<input id="code1" name="code1" type="number" class="validate">
				<label for="code1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 1</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code2" name="code2" type="number" class="">
				<label for="code2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 2</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code3" name="code3" type="number" class="">
				<label for="code3"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 3</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code4" name="code4" type="text" class="">
				<label for="code4"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 4</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code5" name="code5" type="text" class="">
				<label for="code5"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 5</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code6" name="code6" type="text" class="">
				<label for="code6"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Code'];?>
 6</label>
			</div>
		</form>
	</div>
	<div class="modal-footer row codesData">
		<a id="codesSend" class="col modal-action waves-effect waves-green btn green" style="margin: 0 10px;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Send'];?>
</a>
		<a is="codesClose" class="col modal-action modal-close waves-effect btn-flat modal-close" style="margin: 0 10px;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Close'];?>
</a>
	</div>
	<div id="codesLoading" class="center" style="display: none; padding-top: 100px; padding-bottom: 100px"><?php $_smarty_tpl->_subTemplateRender('file:Public/_loading.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
</div><?php }
}
