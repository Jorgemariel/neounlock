<?php
/* Smarty version 3.1.31, created on 2017-05-01 02:34:25
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Admin\Public\sidenav.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59069eb1b742f9_44217831',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4b079c8cae495f05ef34d4c1689cc936aa8cef34' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Admin\\Public\\sidenav.tpl',
      1 => 1493606052,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59069eb1b742f9_44217831 (Smarty_Internal_Template $_smarty_tpl) {
?>
<ul id="slide-out" class="side-nav fixed">
	<div class="userView">
		<div class="background">
			<img src="<?php echo URL;?>
Views/img/background.jpg" class="responsive-img">
		</div>
		<a href="<?php echo URL;?>
"><img class="responsive-img" src="<?php echo URL;?>
Views/img/logo.png"></a>
	</div>

	<li <?php if (!isset($_smarty_tpl->tpl_vars['nav']->value)) {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
admin"><i class="material-icons">home</i>Inicio</a>
	</li>

	<li><div class="divider"></div></li>

	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'brands') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
brand"><i class="material-icons">style</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brands'];?>
</a>
	</li>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'models') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
model"><i class="material-icons">stay_current_portrait</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Models'];?>
</a>
	</li>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'countries') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
country"><i class="material-icons">explore</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
</a>
	</li>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'companies') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
company"><i class="material-icons">work</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
</a>
	</li>

	<li><div class="divider"></div></li>

	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'services') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
service"><i class="material-icons">book</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Services'];?>
</a>
	</li>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'providers') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
provider"><i class="material-icons">cloud</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Providers'];?>
</a>
	</li>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'orders') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
order"><i class="material-icons">list</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Orders'];?>
</a>
	</li>

	<li><div class="divider"></div></li>

	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'emails') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
email"><i class="material-icons">mail</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Emails'];?>
</a>
	</li>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'users') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
user"><i class="material-icons">face</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Users'];?>
</a>
	</li>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'languages') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
language"><i class="material-icons">language</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Languages'];?>
</a>
	</li>
</ul><?php }
}
