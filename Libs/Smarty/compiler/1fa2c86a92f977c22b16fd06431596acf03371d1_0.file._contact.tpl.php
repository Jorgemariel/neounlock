<?php
/* Smarty version 3.1.31, created on 2018-06-02 01:51:16
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Faq/_contact.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b11dbf430bf97_73414201',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1fa2c86a92f977c22b16fd06431596acf03371d1' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Faq/_contact.tpl',
      1 => 1527897072,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/_loading.tpl' => 1,
  ),
),false)) {
function content_5b11dbf430bf97_73414201 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container" style="margin-top: 50px">
  <div class="card-panel">
    <h3>¿Tienes otra consulta?</h3>
    <p>Haz tu pregunta y te contestaremos a la brevedad.</p><br>

    <div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="error-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['ErrorOcurred'];?>
</h6>
		</div>

    <div id="success" class="card-panel green white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="success-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['EmailSent'];?>
</h6>
		</div>

    <form id="contactForm" class="row" style="margin: 25px; margin-bottom: 0;">
      <div class="input-field col s12">
        <i class="material-icons prefix">email</i>
        <input id="email" name="email" type="email" class="validate">
        <label for="email">Mail de contacto</label>
      </div>
      <div class="input-field col s12">
        <i class="material-icons prefix">mode_edit</i>
        <textarea id="textarea" name="textarea" class="materialize-textarea"></textarea>
        <label for="textarea">Su consulta</label>
      </div>
      <div class="center">
        <button class="btn waves-effect waves-light green darken-2" type="button" id="send" name="send">Enviar
          <i class="material-icons right">send</i>
        </button>
      </div>
    </form>
    <div class="row">
      <div id="sendLoading" class="center" style="display: none; padding-top: 200px; padding-bottom: 200px"><?php $_smarty_tpl->_subTemplateRender('file:Public/_loading.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
    </div>
  </div>
</div>
<?php }
}
