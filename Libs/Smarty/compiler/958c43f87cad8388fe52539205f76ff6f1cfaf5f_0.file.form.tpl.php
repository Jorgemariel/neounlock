<?php
/* Smarty version 3.1.31, created on 2018-04-28 03:37:29
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Home/form.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ae3d05906dca2_26928138',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '958c43f87cad8388fe52539205f76ff6f1cfaf5f' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Home/form.tpl',
      1 => 1524878968,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/_loading.tpl' => 1,
    'file:Public/_terms.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5ae3d05906dca2_26928138 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['UnlockingForm'];?>
</h2>
			<!-- <p>Completa con tus datos personales y los de tu celular para continuar con la liberación.</p> -->
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px;">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">Liberación <?php echo $_smarty_tpl->tpl_vars['brand']->value['name'];?>
</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
				<i class="material-icons valign" style="margin-right: 5px">info</i>
				<h6 id="error-text"><?php echo $_smarty_tpl->tpl_vars['lang']->value['ErrorOcurred'];?>
</h6>
			</div>

			<form style="margin: 25px; display: none;">
				<!-- NOMBRE -->
				<div class="row">
					<div class="input-field col s11 m6 l6">
						<i class="material-icons prefix">account_circle</i>
						<input id="name" type="text" class="validate" <?php if (isset($_smarty_tpl->tpl_vars['name']->value) && !empty($_smarty_tpl->tpl_vars['name']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
"<?php }?>>
						<label for="name" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['YourNameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Name'];?>
</label>
					</div>
				</div>

				<!-- EMAIL -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">mail</i>
						<input id="email" type="email" class="validate" <?php if (isset($_smarty_tpl->tpl_vars['email']->value) && !empty($_smarty_tpl->tpl_vars['email']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
"<?php }?>>
						<label for="email" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['EmailRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Email'];?>
</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['EmailHelp'];?>
</div>
				</div>

				<?php if (isset($_smarty_tpl->tpl_vars['countries']->value) && ($_smarty_tpl->tpl_vars['brand']->value['showCountry'] || $_smarty_tpl->tpl_vars['brand']->value['showCompany'] || (isset($_smarty_tpl->tpl_vars['model']->value['showCountry']) && $_smarty_tpl->tpl_vars['model']->value['showCountry']) || (isset($_smarty_tpl->tpl_vars['model']->value['showCompany']) && $_smarty_tpl->tpl_vars['model']->value['showCompany']))) {?>
				<!-- PAÍS -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">language</i>
						<select id="countries">
							<option value="" data-companies='[]' <?php if (!isset($_smarty_tpl->tpl_vars['id_country']->value) || empty($_smarty_tpl->tpl_vars['id_country']->value)) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['SelectCountry'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['countries']->value, 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<?php ob_start();
echo mb_strtolower($_smarty_tpl->tpl_vars['c']->value['iso2'], 'UTF-8');
$_prefixVariable1=ob_get_clean();
$_smarty_tpl->_assignInScope('image', "Views/img/countries/".$_prefixVariable1.".png");
?>
							<option
								value="<?php echo $_smarty_tpl->tpl_vars['c']->value['id'];?>
"
								data-companies='<?php echo json_encode($_smarty_tpl->tpl_vars['c']->value['companies']);?>
'
								class="left"
								<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?> data-icon="<?php echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;?>
" <?php }?>
								<?php if (isset($_smarty_tpl->tpl_vars['id_country']->value) && !empty($_smarty_tpl->tpl_vars['id_country']->value) && $_smarty_tpl->tpl_vars['id_country']->value == $_smarty_tpl->tpl_vars['c']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['c']->value['name_en'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['CountryHelp'];?>
</div>
				</div>
				<?php }?>

				<?php if (isset($_smarty_tpl->tpl_vars['countries']->value) && ($_smarty_tpl->tpl_vars['brand']->value['showCompany'] || (isset($_smarty_tpl->tpl_vars['model']->value['showCompany']) && $_smarty_tpl->tpl_vars['model']->value['showCompany']))) {?>
				<!-- EMPRESAS -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">sim_card</i>
						<select id="companies">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['SelectCompany'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['CompanyHelp'];?>
</div>

				</div>
				<?php }?>

				<!-- IMEI -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">lock</i>
						<input id="imei" class="validate" placeholder="XXXXXXXXXXXXXXX" <?php if (isset($_smarty_tpl->tpl_vars['imei']->value) && !empty($_smarty_tpl->tpl_vars['imei']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['imei']->value;?>
"<?php }?>>
						<label for="imei" class="active" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['imeiRequired'];?>
">IMEI</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['IMEIHelp'];?>
</div>
				</div>

				<?php if ($_smarty_tpl->tpl_vars['brand']->value['showMEP'] || (isset($_smarty_tpl->tpl_vars['model']->value['showMEP']) && $_smarty_tpl->tpl_vars['model']->value['showMEP'])) {?>
				<!-- MEP -->
				<div class="row">
					<div class="input-field col s11 m6 l6">
						<i class="material-icons prefix">edit</i>
						<input id="mep" class="validate" placeholder="MEP-XXXXX-XXX"<?php if (isset($_smarty_tpl->tpl_vars['mep']->value) && !empty($_smarty_tpl->tpl_vars['mep']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['mep']->value;?>
"<?php }?>>
						<label for="mep" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
" class="active">MEP</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['MEPHelp'];?>
</div>
				</div>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['brand']->value['showPRD'] || (isset($_smarty_tpl->tpl_vars['model']->value['showPRD']) && $_smarty_tpl->tpl_vars['model']->value['showPRD'])) {?>
				<!-- PRD -->
				<div class="row">
					<div class="input-field col s11 m6 l6">
						<i class="material-icons prefix">edit</i>
						<input id="prd" type="text" class="validate" placeholder="PRD-XXXXX-XXX"<?php if (isset($_smarty_tpl->tpl_vars['prd']->value) && !empty($_smarty_tpl->tpl_vars['prd']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['prd']->value;?>
"<?php }?>>
						<label for="prd" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
" class="active">PRD</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['PRDHelp'];?>
</div>
				</div>
				<?php }?>

				<div class="row">
					<!-- TERMS & CONDITIONS -->
					<div class="col s12">
						<p class="center">
							<input type="checkbox" id="terms" />
							<label for="terms"><?php echo $_smarty_tpl->tpl_vars['lang']->value['IAcept'];?>
<a class="modal-trigger" href="#termsmodal"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Terms'];?>
</a></label>
						</p>
					</div>

					<!-- ACTIONS -->
					<div class="col s12 center" style="margin-top: 10px">
						<a href="<?php echo URL;?>
" class="waves-effect waves-teal btn-flat" id="back"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
						<button class="waves-effect waves-light btn green" id="submit"><i class="material-icons right">send</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Continue'];?>
</button>
					</div>
				</div>
			</form>
			<div id="loading" class="center" style="padding: 30px">
				<?php $_smarty_tpl->_subTemplateRender('file:Public/_loading.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			</div>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/_terms.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php echo '<script'; ?>
 src="<?php echo URL;?>
Views/js/jquery.formatter.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
 type="text/javascript">
		
		$(document).ready(function(){
			$('select').material_select();

			$('#imei').formatter({ 'pattern': '{{999999999999999}}' });
			$('#mep').formatter({ 'pattern': 'MEP-{{99999}}-{{999}}' });
			$('#prd').formatter({ 'pattern': 'PRD-{{99999}}-{{999}}' });

			$('#loading').hide();
			$('form').fadeIn();
			showCompanies();
		});
		

		$('#countries').change(function(){
			showCompanies();
		});

		function showCompanies(){
			<?php if (isset($_smarty_tpl->tpl_vars['countries']->value) && ($_smarty_tpl->tpl_vars['brand']->value['showCompany'] || (isset($_smarty_tpl->tpl_vars['model']->value['showCompany']) && $_smarty_tpl->tpl_vars['model']->value['showCompany']))) {?>
			$('#companies').material_select('destroy');

			var companies = '<option value="" disabled><?php echo $_smarty_tpl->tpl_vars['lang']->value['SelectCompany'];?>
</option>';
			var data = $('#countries :selected').attr('data-companies');
			data = JSON.parse(data);
			$.each(data, function(im, company) {
				var selected = '';
				<?php if (isset($_smarty_tpl->tpl_vars['id_company']->value) && !empty($_smarty_tpl->tpl_vars['id_company']->value)) {?>
				if(data[im].id == <?php echo $_smarty_tpl->tpl_vars['id_company']->value;?>
) selected = ' selected';
				<?php }?>
				companies += "<option value=" + data[im].id + selected + ">" + data[im].name + "</option>";
			});
			$('#companies').html(companies);
			$('#companies').material_select();
			<?php }?>
		};

		$('#submit').click(function(e){
			e.preventDefault();

			var brand = <?php echo $_smarty_tpl->tpl_vars['brand']->value['id'];?>
;
			var model = <?php if (isset($_smarty_tpl->tpl_vars['model']->value)) {
echo $_smarty_tpl->tpl_vars['model']->value['id'];
} else { ?>null<?php }?>;
			var name = $('#name');
			var email = $('#email');
			var country = $('#countries :selected');
			var company = $('#companies :selected');
			var imei = $('#imei');
			var prd = $('#prd');
			var mep = $('#mep');

			if (!validateForm())
			{
				return false;
			}

			$('#error').hide();
			$('form').hide();
			$('#loading').fadeIn();

			var formData = {
				'brand': brand,
				'model': model,
				'name': name.val(),
				'email': email.val(),
				'country': country.val(),
				'company': company.val(),
				'imei': imei.val(),
				'prd': prd.val(),
				'mep': mep.val()
			};

			$.ajax({
				url: '<?php echo URL;?>
home/form/<?php echo $_smarty_tpl->tpl_vars['brand']->value["id"];?>
',
				type: 'POST',
				data: formData,
				dataType: 'json',
				success: function(data) {
					if (data.response == '0')
					{
						window.location = '<?php echo URL;?>
home/noservice';
					}
					else if (data.response == '1')
					{
						window.location = '<?php echo URL;?>
home/resume/' + data.tracking;
					}
					else if (data.response == '2')
					{
						window.location = '<?php echo URL;?>
home/services';
					}
					else
					{
						$('#loading').hide();
						$('form').fadeIn();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						var errorDiv =  $("#error").offset().top - 20;
						$(window).scrollTop(errorDiv);
					}
				},
				error: function(data) {
					alert('Error');
					console.log(data);
				}
			});
			return false;
		});

		function Validar(variable){
			if (variable.val() == "") {
			variable.addClass('invalid');
			variable.focus();
			return false;
			}
		}

		
		function validateEmail($email)
		{
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			return emailReg.test( $email );
		}

		function validateImei(imei)
		{
			if (!/^[0-9]{15}$/.test(imei)) {return false;}

			var sum = 0, factor = 2, checkDigit, multipliedDigit;

			for (var i = 13, li = 0; i >= li; i--) {
				multipliedDigit = parseInt(imei.charAt(i), 10) * factor;
				sum += (multipliedDigit >= 10 ? ((multipliedDigit % 10) + 1) : multipliedDigit);
				(factor === 1 ? factor++ : factor--);
			}
			checkDigit = ((10 - (sum % 10)) % 10);

			return !(checkDigit !== parseInt(imei.charAt(14), 10))
		}
		

		function validateForm()
		{
			var brand = <?php echo $_smarty_tpl->tpl_vars['brand']->value['id'];?>
;
			var model = <?php if (isset($_smarty_tpl->tpl_vars['model']->value)) {
echo $_smarty_tpl->tpl_vars['model']->value['id'];
} else { ?>null<?php }?>;
			var name = $('#name');
			var email = $('#email');
			var country = $('#countries :selected');
			var company = $('#companies :selected');
			var imei = $('#imei');
			var prd = $('#prd');
			var mep = $('#mep');

			if (!name.val())
			{
				name.addClass('invalid');
				name.focus();
				return false;
			}
			if (!email.val() || !validateEmail(email.val()))
			{
				email.addClass('invalid');
				email.focus();
				return false;
			}

			if ($('#countries').length)
			{
				if (!country.val())
				{
					$('#error-text').html("<?php echo $_smarty_tpl->tpl_vars['lang']->value['countryError'];?>
");
					$('#error').fadeIn().delay(5000).slideUp('slow');
					var errorDiv =  $("#error").offset().top - 20;
					$(window).scrollTop(errorDiv);
					return false;
				}
			}

			if ($('#companies').length)
			{
				if (!company.val())
				{
					$('#error-text').html("<?php echo $_smarty_tpl->tpl_vars['lang']->value['companyError'];?>
");
					$('#error').fadeIn().delay(5000).slideUp('slow');
					var errorDiv =  $("#error").offset().top - 20;
					$(window).scrollTop(errorDiv);
					return false;
				}
			}

			if (!imei.val())
			{
				imei.addClass('invalid');
				imei.focus();
				return false;
			}

			if (!validateImei(imei.val()))
			{
				imei.next().attr('data-error', "<?php echo $_smarty_tpl->tpl_vars['lang']->value['imeiWrong'];?>
");
				imei.addClass('invalid');
				imei.focus();
				return false;
			}

			if ($('#prd').length)
			{
				if (!prd.val())
				{
					prd.addClass('invalid');
					prd.focus();
					return false;
				}
			}

			if ($('#mep').length)
			{
				if (!mep.val())
				{
					mep.addClass('invalid');
					mep.focus();
					return false;
				}
			}

			if (!$('#terms').is(':checked'))
			{
				$('#error-text').html("<?php echo $_smarty_tpl->tpl_vars['lang']->value['termsError'];?>
");
				$('#error').fadeIn().delay(5000).slideUp('slow');
				var errorDiv =  $("#error").offset().top - 20;
				$(window).scrollTop(errorDiv);
				return false;
			}

			return true;
		}

	<?php echo '</script'; ?>
>

</body>
</html>
<?php }
}
