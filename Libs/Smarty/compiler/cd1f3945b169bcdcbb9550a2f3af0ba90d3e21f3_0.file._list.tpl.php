<?php
/* Smarty version 3.1.31, created on 2018-04-25 22:02:07
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Service/_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ae0debf70f208_08116126',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd1f3945b169bcdcbb9550a2f3af0ba90d3e21f3' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Service/_list.tpl',
      1 => 1498946116,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ae0debf70f208_08116126 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 strong">
				<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Services'];?>
</a>
			</div>
			<div class="col s2 strong center">
				<a href="#" class="hide-on-small-only orderByDescription"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Descriptions'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByDescription"><i class="material-icons">edit</i></a>
			</div>
			<div class="col s2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8"><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</div>
				<div class="col s2">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['checkDescriptions']) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
				<div class="col s2">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['status']) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px 20px 0; margin-bottom: 0;">
				<div class="col s12">
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Price'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['price'];?>
 U$D<br>
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delay'];?>
: </strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['delay'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['l']->value['delay_type']];?>

				</div>
				<div class="divider col s12" style="margin: 20px 0;"></div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brands'];?>
:</strong><br>
					<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['brands'])) {?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['brands'], 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
<br>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php } else { ?>
						<?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
<br>
					<?php }?>
				</div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Models'];?>
:</strong><br>
					<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['models'])) {?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['models'], 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
<br>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php } else { ?>
						<?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
<br>
					<?php }?>
				</div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
:</strong><br>
					<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['countries'])) {?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['countries'], 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<?php echo $_smarty_tpl->tpl_vars['c']->value['name_en'];?>
<br>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php } else { ?>
						<?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
<br>
					<?php }?>
				</div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
:</strong><br>
					<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['companies'])) {?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['l']->value['companies'], 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
<br>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php } else { ?>
						<?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
<br>
					<?php }?>
				</div>
				<div class="col s12 right-align" style="margin: 10px 0">
					<?php if (array_search(36,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
service/descriptions/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Descriptions'];?>
</a>
					<?php }?>
					<?php if (array_search(25,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
service/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
					<?php }?>
					<?php if (array_search(26,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
service/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
					<?php }?>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
