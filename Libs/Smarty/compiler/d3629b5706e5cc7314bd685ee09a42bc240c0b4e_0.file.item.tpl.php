<?php
/* Smarty version 3.1.31, created on 2017-03-03 00:38:11
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Service\item.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58b8baf335f1c8_69933048',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd3629b5706e5cc7314bd685ee09a42bc240c0b4e' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Service\\item.tpl',
      1 => 1488501451,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/sidenav.tpl' => 1,
    'file:Public/navbar.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58b8baf335f1c8_69933048 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12 m8">
							<input id="name" name="name" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['service']->value['name'])) {
echo $_smarty_tpl->tpl_vars['service']->value['name'];
}?>">
							<label for="name" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Service'];?>
</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="price" name="price" type="number" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['service']->value['price'])) {
echo $_smarty_tpl->tpl_vars['service']->value['price'];
}?>">
							<label for="price" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['PriceRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Price'];?>
</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="delay" name="delay" type="number" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['service']->value['delay'])) {
echo $_smarty_tpl->tpl_vars['service']->value['delay'];
}?>">
							<label for="delay" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['DelayRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delay'];?>
</label>
						</div>
						<div class="input-field col s12 m4">
							<select id="delayType" name="delayType" value="">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['delayTypes']->value, 'd');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['d']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['d']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['service']->value['id_delay_type']) && $_smarty_tpl->tpl_vars['service']->value['id_delay_type'] == $_smarty_tpl->tpl_vars['d']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['d']->value['name']];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['DelayType'];?>
</label>
						</div>
						<div class="input-field col s12 m4">
							<select id="status" name="status" value="<?php if (isset($_smarty_tpl->tpl_vars['service']->value['status'])) {
echo $_smarty_tpl->tpl_vars['service']->value['status'];
}?>">
								<option value="1" <?php if (isset($_smarty_tpl->tpl_vars['service']->value['status']) && $_smarty_tpl->tpl_vars['service']->value['status'] == 1) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Active'];?>
</option>
								<option value="0" <?php if (isset($_smarty_tpl->tpl_vars['service']->value['status']) && $_smarty_tpl->tpl_vars['service']->value['status'] == 0) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['Inactive'];?>
</option>
							</select>
							<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="brands" name="brands" value="">
								<option value="" disabled selected data-models="[]"><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brands']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" data-models='<?php echo json_encode($_smarty_tpl->tpl_vars['l']->value['models']);?>
' <?php if (isset($_smarty_tpl->tpl_vars['l']->value['checked']) && $_smarty_tpl->tpl_vars['l']->value['checked'] == '1') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label class="left-align"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brands'];?>
</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="models" name="models" value="">
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['models']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['l']->value['checked']) && $_smarty_tpl->tpl_vars['l']->value['checked'] == '1') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label class="left-align"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Models'];?>
</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="countries" name="countries" value="">
								<option value="" disabled selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['countries']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['l']->value['checked']) && $_smarty_tpl->tpl_vars['l']->value['checked'] == '1') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name_en'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label class="left-align"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="companies" name="companies" value="">
								<option value="" disabled selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['companies']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
								<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['l']->value['checked']) && $_smarty_tpl->tpl_vars['l']->value['checked'] == '1') {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

							</select>
							<label class="left-align"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
</label>
						</div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="<?php echo URL;?>
service" class="waves-effect waves-light btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
							<?php if (!isset($_smarty_tpl->tpl_vars['service']->value)) {?>
							<a id="save" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
							<?php } else { ?>
							<a id="update" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Update'];?>
</a>
							<?php }?>
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function()
		{
			showModels();

			//Materialize: selector
			$('select').material_select();

			$('#brands').change(function(){
				showModels();
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var price 		= $('#price');
				var delay 		= $('#delay');
				var delayType 	= $('#delayType');
				var status 		= $('#status');
				var brands 		= $('#brands');
				var models 		= $('#models');
				var countries 	= $('#countries');
				var companies 	= $('#companies');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!price.val())
				{
					price.addClass('invalid');
					price.focus();
					return false;
				}

				if (!delay.val())
				{
					delay.addClass('invalid');
					delay.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('price', price.val());
				formData.append('delay', delay.val());
				formData.append('delayType', delayType.val());
				formData.append('status', status.val());
				formData.append('brands', JSON.stringify(brands.val()));
				formData.append('models', JSON.stringify(models.val()));
				formData.append('countries', JSON.stringify(countries.val()));
				formData.append('companies', JSON.stringify(companies.val()));

				$.ajax({
					url: '<?php echo URL;?>
service/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
service/?message=added';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			function showModels(){
				$('#models').material_select('destroy');

				var data = '';
				var models = '<option value="" disabled selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>';

				$('#brands :selected').each(function(ib, brand){
					data = $(brand).attr('data-models');
					data = JSON.parse(data);
					$.each(data, function(im, model) {
						var selected = '';
						if (data[im].selected == '1') selected = ' selected ';
						models += "<option value=" + data[im].id + ' " ' + selected + ">" + '<strong>' + $(brand).text() + '</strong> ' + data[im].name + "</option>";
					})
				});
				$('#models').html(models);
				$('#models').material_select();
			};

			<?php if (isset($_smarty_tpl->tpl_vars['service']->value)) {?>
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var price 		= $('#price');
				var delay 		= $('#delay');
				var delayType 	= $('#delayType');
				var status 		= $('#status');
				var brands 		= $('#brands');
				var models 		= $('#models');
				var countries 	= $('#countries');
				var companies 	= $('#companies');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!price.val())
				{
					price.addClass('invalid');
					price.focus();
					return false;
				}

				if (!delay.val())
				{
					delay.addClass('invalid');
					delay.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('price', price.val());
				formData.append('delay', delay.val());
				formData.append('delayType', delayType.val());
				formData.append('status', status.val());
				formData.append('brands', JSON.stringify(brands.val()));
				formData.append('models', JSON.stringify(models.val()));
				formData.append('countries', JSON.stringify(countries.val()));
				formData.append('companies', JSON.stringify(companies.val()));

				$.ajax({
					url: '<?php echo URL;?>
service/edit/<?php echo $_smarty_tpl->tpl_vars['service']->value['id'];?>
',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
service/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			<?php }?>
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
