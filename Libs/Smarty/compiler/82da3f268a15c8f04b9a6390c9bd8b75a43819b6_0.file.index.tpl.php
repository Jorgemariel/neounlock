<?php
/* Smarty version 3.1.31, created on 2017-05-14 23:35:42
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Order/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5918cdae6e43a4_70796654',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '82da3f268a15c8f04b9a6390c9bd8b75a43819b6' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Order/index.tpl',
      1 => 1494787025,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Order/_list.tpl' => 1,
    'file:Admin/Order/_codes.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5918cdae6e43a4_70796654 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<h5 class="col s10 expand" style="cursor: pointer;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Filter'];?>
</h5>
					<h5 class="col s2 right-align expand" style="cursor: pointer;"><i class="material-icons small expandArrow">keyboard_arrow_down</i></h5>
					<div class="input-field col s12 m6 l6" style="display: none;">
						<input id="searchName" name="searchName" type="text" class="validate" style="margin-bottom: 19px;">
						<label for="searchName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Search'];?>
</label>
					</div>
					<div class="input-field col s12 m6 l3" style="display: none;">
						<select id="searchStatus" name="searchStatus">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['status']->value, 's');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['s']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['s']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['s']->value['name']];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</label>
					</div>
					<div class="input-field col s6 m4 l3" style="display: none;">
						<select id="searchService" name="searchService">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 's');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['s']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['s']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['s']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Service'];?>
</label>
					</div>
					<div class="input-field col s6 m4 l4" style="display: none;">
						<select id="searchLanguage" name="searchLanguage">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchBrand" name="searchBrand">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brands']->value, 'b');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['b']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['b']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['b']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brand'];?>
</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchModel" name="searchModel">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['models']->value, 'm');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['m']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['m']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['m']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchCountry" name="searchCountry">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['countries']->value, 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['c']->value['name_en'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchCompany" name="searchCompany">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['companies']->value, 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
</label>
					</div>
					<div class="col s12 right-align" style="display: none;"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	<?php if (isset($_GET['message'])) {?>
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text <?php echo $_smarty_tpl->tpl_vars['messageColor']->value;?>
">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h6>
			</div>
		</div>
	</div>
	<?php }?>
	
	<div id="list"><?php $_smarty_tpl->_subTemplateRender('file:Admin/Order/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="<?php echo URL;?>
order/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			<li><a href="<?php echo URL;?>
order/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
		</ul>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Order/_codes.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();
			$('.modal-trigger').leanModal({
				complete: function() {$('#codesForm').trigger("reset"); }
			});

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});
		});

		$('#codesSend').click(function(){
			$('.codesData').hide();
			$('#codesLoading').fadeIn();
			var id = $('#list').find('.active').data('id');
			var formData = $('#codesForm').serialize();
			//console.log(formData);

			$.ajax({
				url: '<?php echo URL;?>
order/completed/' + id,
				type: 'POST',
				data: formData,
				cache: false,
				success: function(data) {
					if (data == 1)
					{
						window.location = '<?php echo URL;?>
order/?message=codes';
					} else {
						$('#codesLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('.codesData').fadeIn();
						
					}
				},
				error: function(data) {
					$('#codesLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('.codesData').fadeIn();
				}
			});
			return false;
		})

		var sort = 'desc';
		var orderBy = 'o.id'

		$('#list').on('click', 'a.orderById', function(e){
			orderBy = 'o.id';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByTracking', function(e){
			orderBy = 'tracking';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByService', function(e){
			orderBy = 'service';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'o.id_status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		$('#searchForm').children().change(function(){
			search();
		});

		$('.expand').click(function(){
			expand();
		});

		function expand(){
			if ($('#searchForm').children().not('.expand').is(":visible")) {
				$('#searchForm').children().not('.expand').hide();
				$('#searchForm').css("marginBottom", '-5px');
				$('.expandArrow').text('keyboard_arrow_up');
			} else {
				$('#searchForm').children().not('.expand').show();
				$('#searchForm').css("marginBottom", '-40px');
				$('.expandArrow').text('keyboard_arrow_down');
			}
		};

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var searchName = $('#searchName');
			var searchPosition = $('#searchPosition');
			var searchImage = $('#searchImage');
			var searchStatus = $('#searchStatus');
			var searchModel = $('#searchModel');
			var searchCountry = $('#searchCountry');
			var searchCompany = $('#searchCompany');
			var searchMEP = $('#searchMEP');
			var searchPRD = $('#searchPRD');

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '<?php echo URL;?>
order/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
