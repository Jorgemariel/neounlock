<?php
/* Smarty version 3.1.31, created on 2017-07-01 18:26:31
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Public/navbar.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5957cd374e48f7_34685006',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef7ae6efc5bb71eed925895e3a5926038d4cbb11' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Public/navbar.tpl',
      1 => 1498926389,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5957cd374e48f7_34685006 (Smarty_Internal_Template $_smarty_tpl) {
?>
<nav class="admin responsive-img light-blue lighten-2">
	<div class="nav-wrapper">
		<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
		<a href="#" class="logo"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</a>
		<ul class="right">
			<li>
				<a href="" class="dropdown-button hide-on-small-only" data-beloworigin="true" data-alignment="right" data-constrainwidth="false" data-activates='user'><?php echo $_SESSION['user']['username'];?>
<img class="circle responsive-img" src="<?php echo URL;?>
Views/img/users/<?php echo $_SESSION['user']['id'];?>
.<?php echo $_SESSION['user']['img'];?>
" style="max-width: 40px; margin-bottom: -15px; margin-left: 15px"></a>
				<a href="" class="dropdown-button hide-on-med-and-up" data-beloworigin="true" data-alignment="right" data-constrainwidth="false" data-activates='user'><img class="circle responsive-img" src="<?php echo URL;?>
Views/img/users/<?php echo $_SESSION['user']['id'];?>
.<?php echo $_SESSION['user']['img'];?>
" style="max-width: 40px; margin-bottom: -15px; margin-left: 15px"></a>
			</li>
		</ul>
	</div>
</nav>

<ul id='user' class='dropdown-content'>
	<li><a href="" class="waves-effect waves-light"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Configuration'];?>
</a></li>
	<li><a href="<?php echo URL;?>
login/logout" class="waves-effect waves-light"><?php echo $_smarty_tpl->tpl_vars['lang']->value['LogOut'];?>
</a></li>
</ul>

<main class="admin" style="padding-bottom: 90px;"><?php }
}
