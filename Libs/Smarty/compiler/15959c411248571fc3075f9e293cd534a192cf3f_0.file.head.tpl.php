<?php
/* Smarty version 3.1.31, created on 2018-07-15 07:21:25
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Public/head.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b4ad9d55bfea5_76911216',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '15959c411248571fc3075f9e293cd534a192cf3f' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Public/head.tpl',
      1 => 1530493652,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b4ad9d55bfea5_76911216 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Jorge Mariel">

	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 - Admin NeoUnlock</title>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css" />
	
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo URL;?>
Views/css/global.css"><?php }
}
