<?php
/* Smarty version 3.1.31, created on 2017-07-02 05:44:00
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Index/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59586c000b6ac0_97797272',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3668513314cd59bba70d20b4f81d568381b2ba42' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Index/index.tpl',
      1 => 1498966993,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_59586c000b6ac0_97797272 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/opt/lampp/htdocs/neounlock/Libs/Smarty/plugins/modifier.date_format.php';
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<title>Admin - NeoUnlock</title>
</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	
	<div style="margin: 20px 30px 0;">
		<div class="row">
			<div class="col s12 m4">
				<div class="card center purple darken-2 white-text">
					<div class="card-title">Ingresos <?php echo smarty_modifier_date_format(time(),"%b");?>
</div>
					<div class="card-content">
						<div class="value"><h3>U$D <?php echo sprintf("%d",$_smarty_tpl->tpl_vars['profitMonth']->value);?>
</h3></div>
						<div class="compare">
							<span>
								<i class="material-icons"><?php if ($_smarty_tpl->tpl_vars['profitMonthCompare']->value > 0) {?>trending_up<?php } else { ?>trending_down<?php }?></i> <?php echo sprintf("%d",$_smarty_tpl->tpl_vars['profitMonthCompare']->value);?>
%
							</span>
						</div>
					</div>
					<div id="myfirstchart" style="height: 250px;"></div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center purple darken-2 white-text">
					<div class="card-title">Ingresos <?php echo smarty_modifier_date_format(time(),"%G");?>
</div>
					<div class="card-content">
						<div class="value"><h3>U$D <?php echo sprintf("%d",$_smarty_tpl->tpl_vars['profitYear']->value);?>
</h3></div>
						<div class="compare">
							<span>
								<i class="material-icons"><?php if ($_smarty_tpl->tpl_vars['profitYearCompare']->value > 0) {?>trending_up<?php } else { ?>trending_down<?php }?></i> <?php echo sprintf("%d",$_smarty_tpl->tpl_vars['profitYearCompare']->value);?>
%
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center purple darken-2 white-text">
					<div class="card-title">Ingresos TOTAL</div>
					<div class="card-content">
						<div class="value"><h3>U$D <?php echo sprintf("%d",$_smarty_tpl->tpl_vars['profitTotal']->value);?>
</h3></div>
						<div class="compare purple-text text-darken-2">
							<i class="material-icons">trending_up</i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m4">
				<div class="card center teal darken-3 white-text">
					<div class="card-title">Pedidos <?php echo smarty_modifier_date_format(time(),"%b");?>
</div>
					<div class="card-content">
						<div class="value"><h3>24</h3></div>
						<div class="compare">
							<i class="material-icons">trending_up</i> 15%
							<br>
							<i class="material-icons">done_all</i> 4 (-12%)
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center teal darken-3 white-text">
					<div class="card-title">Pedidos <?php echo smarty_modifier_date_format(time(),"%G");?>
</div>
					<div class="card-content">
						<div class="value"><h3>294</h3></div>
						<div class="compare">
							<i class="material-icons">trending_up</i> 15%
							<br>
							<i class="material-icons">done_all</i> 29 (-12%)
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center teal darken-3 white-text">
					<div class="card-title">Pedidos TOTAL</div>
					<div class="card-content">
						<div class="value"><h3>627</h3></div>
						<div class="compare">
							<i class="material-icons teal-text text-darken-3">trending_up</i>
							<br>
							<i class="material-icons">done_all</i> 103
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php echo '<script'; ?>
 src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
>
	
		$(document).ready(function(){
			$('.collapsible').collapsible();

		});
		new Morris.Line({
		  // ID of the element in which to draw the chart.
		  element: 'myfirstchart',
		  // Chart data records -- each entry in this array corresponds to a point on
		  // the chart.
		  data: [
		    { year: '2008', value: 20 },
		    { year: '2009', value: 10 },
		    { year: '2010', value: 5 },
		    { year: '2011', value: 5 },
		    { year: '2012', value: 20 }
		  ],
		  // The name of the data record attribute that contains x-values.
		  xkey: 'date',
		  // A list of names of data record attributes that contain y-values.
		  ykeys: ['income'],
		  // Labels for the ykeys -- will be displayed when you hover over the
		  // chart.
		  labels: ['income']
		});
	
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
