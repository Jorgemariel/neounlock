<?php
/* Smarty version 3.1.31, created on 2017-03-08 18:27:05
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Index\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58c04cf92a0db9_70181897',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'efd0172377b98826cb76c1e5951fd3fe08af160f' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Index\\index.tpl',
      1 => 1488997622,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Index/introduction.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58c04cf92a0db9_70181897 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Index/introduction.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row valign-wrapper title">
		<h4 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['SelectBrand'];?>
</h4>
		<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
			<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
			<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
			<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
		</div>
	</div>

	<div class="row">
		<?php if (isset($_smarty_tpl->tpl_vars['brands']->value)) {?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brands']->value, 'b');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['b']->value) {
?>
		<div class="col s12 m4 l3">
			<div data-id="<?php echo $_smarty_tpl->tpl_vars['b']->value['id'];?>
" data-url="<?php if ($_smarty_tpl->tpl_vars['b']->value['showModel'] == 1) {?>models<?php } else { ?>form<?php }?>" class="brand card hoverable waves-effect" style="margin: 15px 5px">
				<div class="card-content" style="text-align: center; padding-bottom: 1px">
					<img src="<?php echo URL;?>
Views/img/brands/<?php echo $_smarty_tpl->tpl_vars['b']->value['id'];?>
.<?php echo $_smarty_tpl->tpl_vars['b']->value['img'];?>
" style="max-width: 90%">
					<h6 class="blue-text darken-text-1" style="margin-top: 20px"><?php echo $_smarty_tpl->tpl_vars['b']->value['name'];?>
</h6>
				</div>
			</div>
		</div>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

		<?php } else { ?>
		<div class="col m12 center">
			<h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoResult'];?>
</h3>
		</div>
		<?php }?>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){
			$(".button-collapse").sideNav();
			$('.modal-trigger').leanModal();
			$('#tracking-go').click(function(){TrackingGo(); return false;});

			$('.carousel.carousel-slider').carousel({
				full_width: true,
				indicators: false
			});

			$('.brand').click(function(){
				window.location = '<?php echo URL;?>
index/'+ $(this).data('url') + '/' + $(this).data('id');
				return false;
			});
		});
	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
