<?php
/* Smarty version 3.1.31, created on 2017-07-01 20:44:00
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Public/sidenav.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5957ed70649738_98573560',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '81e28ba0fa48e2d3caf78eee7c4fbe0ca82b62fa' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Public/sidenav.tpl',
      1 => 1498934633,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5957ed70649738_98573560 (Smarty_Internal_Template $_smarty_tpl) {
?>
<ul id="slide-out" class="side-nav fixed">
	<div class="userView">
		<div class="background">
			<img src="<?php echo URL;?>
Views/img/background.jpg" class="responsive-img">
		</div>
		<a href="<?php echo URL;?>
"><img class="responsive-img" src="<?php echo URL;?>
Views/img/logo.png"></a>
	</div>
	<?php if (array_search(4,$_SESSION['user']['privileges'])) {?>
	<li <?php if (!isset($_smarty_tpl->tpl_vars['nav']->value)) {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
admin"><i class="material-icons">home</i>Inicio</a>
	</li>
	<?php }?>

	<li><div class="divider"></div></li>

	<?php if (array_search(1,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'brands') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
brand"><i class="material-icons">style</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brands'];?>
</a>
	</li>
	<?php }?>

	<?php if (array_search(2,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'models') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
model"><i class="material-icons">stay_current_portrait</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Models'];?>
</a>
	</li>
	<?php }?>

	<?php if (array_search(3,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'countries') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
country"><i class="material-icons">explore</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Countries'];?>
</a>
	</li>
	<?php }?>

	<?php if (array_search(5,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'companies') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
company"><i class="material-icons">work</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Companies'];?>
</a>
	</li>
	<?php }?>

	<li><div class="divider"></div></li>

	<?php if (array_search(6,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'services') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
service"><i class="material-icons">book</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Services'];?>
</a>
	</li>
	<?php }?>

	<?php if (array_search(7,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'providers') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
provider"><i class="material-icons">cloud</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Providers'];?>
</a>
	</li>
	<?php }?>

	<?php if (array_search(8,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'orders') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
order"><i class="material-icons">list</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Orders'];?>
</a>
	</li>
	<?php }?>

	<li><div class="divider"></div></li>

	<?php if (array_search(9,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'emails') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
email"><i class="material-icons">mail</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Emails'];?>
</a>
	</li>
	<?php }?>

	<?php if (array_search(10,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'users') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
user"><i class="material-icons">face</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Users'];?>
</a>
	</li>
	<?php }?>

	<?php if (array_search(11,$_SESSION['user']['privileges'])) {?>
	<li <?php if (isset($_smarty_tpl->tpl_vars['nav']->value) && $_smarty_tpl->tpl_vars['nav']->value == 'languages') {?> class="active"<?php }?>>
		<a class="waves-effect" href="<?php echo URL;?>
language"><i class="material-icons">language</i><?php echo $_smarty_tpl->tpl_vars['lang']->value['Languages'];?>
</a>
	</li>
	<?php }?>
</ul><?php }
}
