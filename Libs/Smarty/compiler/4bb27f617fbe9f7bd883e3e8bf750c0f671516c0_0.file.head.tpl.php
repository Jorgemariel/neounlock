<?php
/* Smarty version 3.1.31, created on 2017-05-14 20:48:55
  from "/opt/lampp/htdocs/neounlock/Views/template/Public/head.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5918a698014c84_32003011',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4bb27f617fbe9f7bd883e3e8bf750c0f671516c0' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Public/head.tpl',
      1 => 1494787025,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5918a698014c84_32003011 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Jorge Mariel">

	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 - NeoUnlock</title>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo URL;?>
Views/css/global.css"><?php }
}
