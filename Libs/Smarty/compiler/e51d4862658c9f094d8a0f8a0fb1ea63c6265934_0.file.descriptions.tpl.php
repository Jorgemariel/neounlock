<?php
/* Smarty version 3.1.31, created on 2018-05-30 06:02:05
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Service/descriptions.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b0e223d3b4b41_74008195',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e51d4862658c9f094d8a0f8a0fb1ea63c6265934' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Service/descriptions.tpl',
      1 => 1494797824,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b0e223d3b4b41_74008195 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	
	<div class="admin-container">
		<div class="row" id="list">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<ul class="collapsible popout" data-collapsible="accordion">
					<li class="collapsible-header">
						<div class="row" style="padding:0px;margin:0px;">
							<div class="col s10 strong">
								<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</a>
							</div>
							<div class="col s2 strong center">
								<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
								<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
							</div>
						</div>
					</li>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['descriptions']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
					<?php $_smarty_tpl->_assignInScope('image', "Views/img/countries/".((string)$_smarty_tpl->tpl_vars['l']->value['img']).".png");
?>
					<li>
						<div class="collapsible-header">
							<div class="row" style="margin: 0;">
								<div class="col s10">
									<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?><img src="<?php echo URL;
echo $_smarty_tpl->tpl_vars['image']->value;?>
" style="margin-right: 10px;"><?php }?> <?php echo $_smarty_tpl->tpl_vars['l']->value['language'];?>

								</div>
								<div class="col s2">
									<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['content'])) {?>
									<i class="material-icons center-icon green-text">check</i>
									<?php } else { ?>
									<i class="material-icons center-icon red-text">block</i>
									<?php }?>
								</div>
							</div>
						</div>
						<div class="collapsible-body">
							<textarea class="editor" data-language="<?php echo $_smarty_tpl->tpl_vars['l']->value['id_lang'];?>
" id="a<?php echo $_smarty_tpl->tpl_vars['l']->value['id_lang'];?>
">
								<?php if (!empty($_smarty_tpl->tpl_vars['l']->value['content'])) {?>
									<?php echo $_smarty_tpl->tpl_vars['l']->value['content'];?>

								<?php } else { ?>
									<?php echo $_smarty_tpl->tpl_vars['lang']->value['AddDescription'];?>

								<?php }?>
							</textarea>
						</div>
					</li>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

				</ul>
			</div>
			<div class="col s12 center">
				<a href="<?php echo URL;?>
service" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
				<a id="save" class="waves-effect waves-light btn-large"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
			</div>
		</div>
		<div class="center" id="loading" style="display: none; height: 200px;">
			<div class="preloader-wrapper big active" style="margin-top: 68px">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php echo '<script'; ?>
 src="http://cloud.tinymce.com/stable/tinymce.min.js?apiKey=835mpwvoa9jxi8di3gqker4w8h54myafuvydw171zjztih33"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="<?php echo URL;?>
Views/js/tinymce.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			//var data = tinymce.get('editor').getContent();
		});

		var data = [];

		$('#save').click(function(){
			data = [];
			$('textarea').each(function(i, el){
				// console.log($(el).attr('id'));
				// console.log(tinymce.get($(el).attr('id')).getContent());
				var desc = '';

				if (tinymce.get($(el).attr('id')).getContent() != '<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['AddDescription'];?>
</p>') {
					desc = tinymce.get($(el).attr('id')).getContent();
				}

				data.push({
					id_language: $(el).attr('data-language'),
					content: desc
				});
			});

			$('#list').hide();
			$('#loading').fadeIn();

			$.ajax({
				url: '<?php echo URL;?>
service/descriptions/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
',
				type: 'POST',
				data: {data},
				success: function(data) {
					if (data == 1)
					{
						window.location = '<?php echo URL;?>
service/?message=descriptions';
					}
					else
					{
						$('#loading').hide();
						$('#list').fadeIn();
					}
				},
				error: function(data) {
					alert(data);
				}
			});

			return false;
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
