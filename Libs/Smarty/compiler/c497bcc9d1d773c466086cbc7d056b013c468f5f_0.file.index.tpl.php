<?php
/* Smarty version 3.1.31, created on 2017-11-29 08:02:08
  from "/opt/lampp/htdocs/neounlock/Views/template/Admin/Country/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a1e5b705d6380_96123868',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c497bcc9d1d773c466086cbc7d056b013c468f5f' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Admin/Country/index.tpl',
      1 => 1498935102,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Country/_list.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5a1e5b705d6380_96123868 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<h5 class="col s10 expand" style="cursor: pointer;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Filter'];?>
</h5>
					<h5 class="col s2 right-align expand" style="cursor: pointer;"><i class="material-icons small expandArrow">keyboard_arrow_down</i></h5>
					<div class="input-field col s12 m6" style="display: none;">
						<input id="searchName" name="searchName" type="text" class="validate">
						<label for="searchName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
</label>
					</div>
					<div class="input-field col s12 m2" style="display: none;">
						<select id="searchLanguage" name="searchLanguage">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['l']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</label>
					</div>
					<div class="input-field col s6 m2" style="display: none;">
						<select id="searchImage" name="searchImage">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Image'];?>
</label>
					</div>
					<div class="input-field col s6 m2" style="display: none;">
						<select id="searchHighlighted" name="searchHighlighted">
							<option value="" selected><?php echo $_smarty_tpl->tpl_vars['lang']->value['All'];?>
</option>
							<option value="1"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Yes'];?>
</option>
							<option value="0"><?php echo $_smarty_tpl->tpl_vars['lang']->value['No'];?>
</option>
						</select>
						<label><?php echo $_smarty_tpl->tpl_vars['lang']->value['Highlighted'];?>
</label>
					</div>
					<div class="col s12 right-align" style="display: none;"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	<?php if (isset($_GET['message'])) {?>
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text <?php echo $_smarty_tpl->tpl_vars['messageColor']->value;?>
">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h6>
			</div>
		</div>
	</div>
	<?php }?>
	
	<div id="list"><?php $_smarty_tpl->_subTemplateRender('file:Admin/Country/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="<?php echo URL;?>
country/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			<?php if (array_search(18,$_SESSION['user']['privileges'])) {?>
			<li><a href="<?php echo URL;?>
country/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
			<?php }?>
		</ul>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});
		});

		var sort = 'desc';
		var orderBy = 'highlighted';

		$('#list').on('click', 'a.orderByIso3', function(e){
			orderBy = 'iso3';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByLanguage', function(e){
			orderBy = 'id_language';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByImage', function(e){
			orderBy = 'img';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByHighlighted', function(e){
			orderBy = 'highlighted';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		$('#searchForm').children().change(function(){
			search();
		});

		$('.expand').click(function(){
			expand();
		});

		function expand(){
			if ($('#searchForm').children().not('.expand').is(":visible")) {
				$('#searchForm').children().not('.expand').hide();
				$('#searchForm').css("marginBottom", '-5px');
				$('.expandArrow').text('keyboard_arrow_up');
			} else {
				$('#searchForm').children().not('.expand').show();
				$('#searchForm').css("marginBottom", '-40px');
				$('.expandArrow').text('keyboard_arrow_down');
			}
		};

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '<?php echo URL;?>
country/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
