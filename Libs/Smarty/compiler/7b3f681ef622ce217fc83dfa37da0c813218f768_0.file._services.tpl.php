<?php
/* Smarty version 3.1.31, created on 2017-03-22 19:08:43
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Home\_services.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58d2cbbb1b2739_57021318',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b3f681ef622ce217fc83dfa37da0c813218f768' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Home\\_services.tpl',
      1 => 1490209711,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/_terms.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58d2cbbb1b2739_57021318 (Smarty_Internal_Template $_smarty_tpl) {
?>




<?php $_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px">Servicios disponibles</h2>
			<!-- <p>Completa con tus datos personales y los de tu celular para continuar con la liberación.</p> -->
		</div>
	</div>
	<div class="" style="padding: 15px;">
		<div class="row" style="margin-top: -120px;">
			<?php if (count($_smarty_tpl->tpl_vars['services']->value) == 2) {?> <div class="col m2"></div> <?php }?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 's', false, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['s']->value) {
?>
			<div class="col s12 m4" style="padding: 0 20px;">
				<div class="card hoverable">
					<div class="<?php if ($_smarty_tpl->tpl_vars['i']->value == 0) {?>purple<?php } elseif ($_smarty_tpl->tpl_vars['i']->value == 1) {?>cyan<?php } else { ?>teal<?php }?> waves-effect" style="width: 100%;">
						<div class="card-title white-text">ECONÓMICO</div>
						<div class="center white-text">
							<div style="display: inline-flex; margin-top: 10px;">
								<h4 style="margin-right: 5px">USD</h4>
								<h2 style="margin-top: 13px"><?php echo $_smarty_tpl->tpl_vars['s']->value['price'];?>
</h2>
							</div>
						</div>
					</div>
					<div class="card-content">
						<ul class="collection">
							<li class="collection-item">
								<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['AverageDelay'];?>
</strong><br>
								<?php echo $_smarty_tpl->tpl_vars['s']->value['delay'];?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value[$_smarty_tpl->tpl_vars['s']->value['delay_type']];?>

							</li>
							<li class="collection-item">
								<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['MaxDelay'];?>
</strong><br>
								FALTA TERMINAR
							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

			<!-- <div class="col s12 m4" style="padding: 0 20px;">
				<div class="card hoverable">
					<div class="cyan waves-effect" style="width: 100%;">
						<div class="card-title white-text">STANDARD</div>
						<div class="center white-text">
							<div style="display: inline-flex; margin-top: 10px;">
								<h4 style="margin-right: 5px">USD</h4>
								<h2 style="margin-top: 13px">15</h2>
							</div>
						</div>
					</div>
					<div class="card-content">
						<ul class="collection">
							<li class="collection-item">
								<strong>Demora estimada</strong><br>
								3 horas
							</li>
							<li class="collection-item">
								<strong>Demora máxima</strong><br>
								24 horas
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col s12 m4" style="padding: 0 20px;">
				<div class="card hoverable">
					<div class="teal darken-1 waves-effect" style="width: 100%;">
						<div class="card-title white-text">PREMIUM</div>
						<div class="center white-text">
							<div style="display: inline-flex; margin-top: 10px;">
								<h4 style="margin-right: 5px">USD</h4>
								<h2 style="margin-top: 13px">20</h2>
							</div>
						</div>
					</div>
					<div class="card-content">
						<ul class="collection">
							<li class="collection-item">
								<strong>Demora estimada</strong><br>
								20 minutos
							</li>
							<li class="collection-item">
								<strong>Demora máxima</strong><br>
								60 minutos
							</li>
						</ul>
					</div>
				</div>
			</div> -->
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/_terms.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</body>
</html><?php }
}
