<?php
/* Smarty version 3.1.31, created on 2017-04-17 06:08:25
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Admin\Email\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58f45bd9993c07_93343045',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8fac9e20a254d9d13744cd6fce011f85b591c232' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Admin\\Email\\index.tpl',
      1 => 1492409304,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Email/_list.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_58f45bd9993c07_93343045 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<div class="input-field col s9 m10 l11">
						<input id="searchName" name="searchName" type="text" class="validate">
						<label for="searchName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Search'];?>
</label>
					</div>
					<div class="col s3 m2 l1 center"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	<div class="row messageArea" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel white-text orange">
			<i class="material-icons right messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['lang']->value['OnlyInSpanish'];?>
</h6>
			</div>
		</div>
	</div>

	<?php if (isset($_GET['message'])) {?>
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text <?php echo $_smarty_tpl->tpl_vars['messageColor']->value;?>
">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h6>
			</div>
		</div>
	</div>
	<?php }?>
	
	<div id="list"><?php $_smarty_tpl->_subTemplateRender('file:Admin/Email/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="<?php echo URL;?>
email/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			<li><a href="<?php echo URL;?>
email/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
		</ul>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});

			$('.messageClose').click(function(){
				$('.messageArea').fadeOut();
			});
		});

		var sort = 'desc';
		var orderBy = 'position'

		$('#list').on('click', 'a.orderByName', function(e){
			orderBy = 'name';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderById', function(e){
			orderBy = 'id';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		$(document).keypress(function(e) {
			if(e.which == 13) {
				e.preventDefault();
				search();
			}
		});

		$('#searchForm').children().change(function(){
			search();
		});

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var searchName = $('#searchName');
			var searchPosition = $('#searchPosition');
			var searchImage = $('#searchImage');
			var searchStatus = $('#searchStatus');
			var searchModel = $('#searchModel');
			var searchCountry = $('#searchCountry');
			var searchCompany = $('#searchCompany');
			var searchMEP = $('#searchMEP');
			var searchPRD = $('#searchPRD');

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '<?php echo URL;?>
email/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
