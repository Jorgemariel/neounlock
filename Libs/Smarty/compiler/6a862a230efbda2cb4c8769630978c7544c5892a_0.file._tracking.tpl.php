<?php
/* Smarty version 3.1.31, created on 2017-03-31 07:18:04
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Public\_tracking.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58de02acc2a850_90913781',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6a862a230efbda2cb4c8769630978c7544c5892a' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Public\\_tracking.tpl',
      1 => 1490943513,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58de02acc2a850_90913781 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="trackingmodal" class="modal">
	<div class="modal-content">
		<h4><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingOrder'];?>
</h4>
		<p><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingOrderHelp'];?>
</p>
		<div class="input-field col s6">
			<input id="order-code" name="order-code" type="text" class="validate">
			<label for="last_name" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingCodeValid'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingCode'];?>
</label>
		</div>
	</div>
	<div class="modal-footer">
		<button id="tracking-go" class="modal-action waves-effect btn" style="margin: 5px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Consult'];?>
</button>
		<a class="modal-action modal-close waves-effect btn-flat modal-close" style="margin: 5px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Close'];?>
</a>
	</div>
</div><?php }
}
