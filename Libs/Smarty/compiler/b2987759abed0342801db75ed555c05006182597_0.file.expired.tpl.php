<?php
/* Smarty version 3.1.31, created on 2018-04-24 02:03:58
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/expired.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5adebabe24faa1_18227648',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b2987759abed0342801db75ed555c05006182597' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/expired.tpl',
      1 => 1499590082,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5adebabe24faa1_18227648 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingStatus'];?>
</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Order'];?>
 <strong><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['order']->value['tracking'], 'UTF-8');?>
</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel teal darken-3 white-text valign-wrapper" style="margin: 0 20px">
				<i class="material-icons valign" style="margin-right: 5px">info</i>
				<h6><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderExpired'];?>
</h6>
			</div>

			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><div>IMEI<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['imei'];?>
</a></div></li>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['brand']) && !empty($_smarty_tpl->tpl_vars['order']->value['brand'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brand'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['brand'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['model']) && !empty($_smarty_tpl->tpl_vars['order']->value['model'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['model'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['country']) && !empty($_smarty_tpl->tpl_vars['order']->value['country'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['country'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['company']) && !empty($_smarty_tpl->tpl_vars['order']->value['company'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['company'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['mep']) && !empty($_smarty_tpl->tpl_vars['order']->value['mep'])) {?>
				<li class="collection-item"><div>MEP<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['mep'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['prd']) && !empty($_smarty_tpl->tpl_vars['order']->value['prd'])) {?>
				<li class="collection-item"><div>PRD<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['prd'];?>
</a></div></li>
				<?php }?>
			</ul>

			<div class="row" style="margin-top: 20px">
				<div class="col s12 m6 center">
					<a class="waves-effect waves-light btn-large green" style="margin: 10px; width: 85%"><i class="material-icons left">attach_money</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['ReorderRequest'], 'UTF-8');?>
</a>
				</div>
				<div class="col s12 m6 center">
					<a class="waves-effect waves-light btn-large red" style="margin: 10px; width: 85%"><i class="material-icons left">help</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['Help'], 'UTF-8');?>
</a>
				</div>
			</div>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){

		});
	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
