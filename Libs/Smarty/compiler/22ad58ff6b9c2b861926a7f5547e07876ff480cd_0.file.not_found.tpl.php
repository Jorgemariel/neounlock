<?php
/* Smarty version 3.1.31, created on 2018-05-30 06:45:58
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/not_found.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b0e2c86cf8050_13797339',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '22ad58ff6b9c2b861926a7f5547e07876ff480cd' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Tracking/not_found.tpl',
      1 => 1527655557,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b0e2c86cf8050_13797339 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NotFound'];?>
</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Order'];?>
 <strong><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['tracking']->value, 'UTF-8');?>
</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel red white-text valign-wrapper" style="margin: 0 20px 20px 20px;">
				<i class="material-icons valign" style="margin-right: 5px">error</i>
				<h6><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderNotFound'];?>
</h6>
			</div>
      <div class="center" style="width: 100%">
        <a href="<?php echo URL;?>
" class="btn green">
          <?php echo $_smarty_tpl->tpl_vars['lang']->value['Back'];?>

        </a>
      </div>

		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){

		});
	<?php echo '</script'; ?>
>

</body>
</html>
<?php }
}
