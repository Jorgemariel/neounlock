<?php
/* Smarty version 3.1.31, created on 2017-03-29 17:52:16
  from "C:\wamp64\www\CodigoFacilito_PHP_POO\NeoUnlock\Views\template\Home\payment.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_58dbf450ed3e57_14853185',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '66a9156d22a42a37b3b9dfc7b878cbbe907a91ad' => 
    array (
      0 => 'C:\\wamp64\\www\\CodigoFacilito_PHP_POO\\NeoUnlock\\Views\\template\\Home\\payment.tpl',
      1 => 1490720160,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_58dbf450ed3e57_14853185 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper">
		<div class="container valign">
			<h2 class="animated fadeInLeft"><?php echo $_smarty_tpl->tpl_vars['lang']->value['PaymentMethod'];?>
</h2>
		</div>
	</div>

	<div class="container" style="margin-top: 35px">


		<div class="row">
			<div class="col s12">
				<div class="card-panel">
					<select class="icons">
						<option value="" disabled selected>Selecciona tu país</option>
						<option value="" data-icon="<?php echo URL;?>
Views/img/payments/arg.png" class="left circle">Argentina</option>
						<option value="" data-icon="<?php echo URL;?>
Views/img/payments/chi.png" class="left circle">Chile</option>
						<option value="" data-icon="<?php echo URL;?>
Views/img/payments/uru.jpg" class="left circle">Uruguay</option>
					</select>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<a id="mercadopago" name="MP-Checkout" class="brand card hoverable waves-effect" mp-mode="modal" style="margin: 15px 5px;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px;">
						<img src="<?php echo URL;?>
Views/img/payments/mercadopago.png" style="max-width: 90%;">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px;">MercadoPago Argentina</h6>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/rapipago.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">RapiPago Argentina</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/pagofacil.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">PagoFacil Argentina</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/redcompra.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">RedCompra Chile</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/paypal.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">PayPal</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/bitcoin.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">Bitcoin</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/payu.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">Pay U</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/webpay.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">WebPay Chile</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="<?php echo URL;?>
Views/img/payments/servipag.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">ServiPag Chile</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php echo '<script'; ?>
 src="https://secure.mlstatic.com/mptools/render.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
 type="text/javascript">
		
		$(document).ready(function(){
			$('.modal-trigger').leanModal();
			$('#tracking-go').click(function(){TrackingGo(); return false;});
			$('select').material_select();
		});	
		

		$('#mercadopago').click(function(){

			$.ajax({
				type: 'POST',
				url: "<?php echo URL;?>
payment/mercadopago/<?php echo $_smarty_tpl->tpl_vars['order']->value['tracking'];?>
",
				success: function(result){
					$MPC.openCheckout ({
						url: result,
						mode: "modal",
						onreturn: function(data) {
							// execute_my_onreturn (Sólo modal)
							console.log(data);
							// FALTA TERMINAR
						}
					});
					// $('#mercadopago').attr("href", result);
				}
			});

		});
	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
