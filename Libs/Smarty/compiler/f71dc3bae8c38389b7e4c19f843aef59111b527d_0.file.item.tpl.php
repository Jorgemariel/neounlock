<?php
/* Smarty version 3.1.31, created on 2018-04-25 00:17:00
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/item.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5adfacdcd4f920_05951740',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f71dc3bae8c38389b7e4c19f843aef59111b527d' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/item.tpl',
      1 => 1524608212,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5adfacdcd4f920_05951740 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12">
							<input id="name" name="name" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['email']->value['name'])) {
echo $_smarty_tpl->tpl_vars['email']->value['name'];
}?>">
							<label for="name" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['NameRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Name'];?>
</label>
						</div>
						<div class="input-field col s12">
							<input id="description" name="description" type="text" class="validate" value="<?php if (isset($_smarty_tpl->tpl_vars['email']->value['name'])) {
echo $_smarty_tpl->tpl_vars['email']->value['name'];
}?>">
							<label for="description" data-error="<?php echo $_smarty_tpl->tpl_vars['lang']->value['DescriptionRequired'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Description'];?>
</label>
						</div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="<?php echo URL;?>
email" class="waves-effect waves-light btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Cancel'];?>
</a>
							<?php if (!isset($_smarty_tpl->tpl_vars['email']->value)) {?>
							<a id="save" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Save'];?>
</a>
							<?php } else { ?>
							<a id="update" class="waves-effect waves-light btn blue"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Update'];?>
</a>
							<?php }?>
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
>
		$(document).ready(function()
		{
			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault();

				var name 				= $('#name');
				var description = $('#description');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!description.val())
				{
					description.addClass('invalid');
					description.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('description', description.val());

				$.ajax({
					url: '<?php echo URL;?>
email/add',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
email/?message=added';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			<?php if (isset($_smarty_tpl->tpl_vars['email']->value)) {?>
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault();

				var name 				= $('#name');
				var description = $('#description');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!description.val())
				{
					description.addClass('invalid');
					description.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('description', description.val());

				$.ajax({
					url: '<?php echo URL;?>
email/edit/<?php echo $_smarty_tpl->tpl_vars['email']->value['id'];?>
',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '<?php echo URL;?>
email/?message=edited';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#loading').hide();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			<?php }?>
		});
	<?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
