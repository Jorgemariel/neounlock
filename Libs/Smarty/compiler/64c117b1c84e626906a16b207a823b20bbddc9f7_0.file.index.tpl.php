<?php
/* Smarty version 3.1.31, created on 2018-04-23 20:59:24
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Login/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ade2d0c94aac3_98588749',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '64c117b1c84e626906a16b207a823b20bbddc9f7' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Login/index.tpl',
      1 => 1498872724,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5ade2d0c94aac3_98588749 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<title>Login - NeoUnlock</title>
</head>
<body>	
	<div class="container" style="margin-top: 100px;">
		<div class="animated fadeIn container row" style="margin:5% auto;">
			<div class="center">
				<h3 id="title" class="grey-text text-darken-4">Iniciar sesión</h3>
			</div>
			<div class="card-panel z-depth-3 col s12 m10 l8 offset-m1 offset-l2" style="padding-top: 50px">

				<!-- Mensaje de error -->
				<div class="card-panel red white-text" id="formLoginError" style="padding: 10px" hidden>
					<h6 id="message">Hubo un error y no se pudo iniciar sesión</h6>
					<i class="material-icons right" id="closeMessage" style="margin-top: -25px">close</i>
				</div>

				<!-- Formulario de inicio de sesion -->
				<form action="" method="post" class="row" id="formLogin" style="padding:0 5%">
					<div class="input-field col s12">
						<i class="material-icons prefix">account_circle</i>
						<input id="username" name="username" type="text" class="validate">
						<label for="username" data-error="Ingrese su nombre de usuario">Nombre de usuario</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">lock</i>
						<input id="password" name="password" type="password" class="validate">
						<label for="password" data-error="Ingrese su contraseña">Contraseña</label>
					</div>
					<div class="right" style="margin-top: 20px">
						<a id="login" class="btn waves-effect waves-light blue" type="submit" name="action">Ingresar</a>
					</div>
				</form>

				<!-- Loading ajax -->
				<div id="loading" class="center-align" hidden style="padding:1%">
					<h4>Iniciando sesión</h4>
					<div class="preloader-wrapper active" style="margin-bottom:40px">
						<div class="spinner-layer spinner-red-only">
							<div class="circle-clipper left">
								<div class="circle"></div>
							</div>
							<div class="gap-patch">
								<div class="circle"></div>
							</div>
							<div class="circle-clipper right">
								<div class="circle"></div>
							</div>
						</div>
					</div>
				</div>

				<div id="formFooter">
					<div class="divider" style="margin-top: 30px;"></div>
					<div class="center" style="padding: 10px 0 10px 0; margin: 0 -25px">
						<a style="cursor:pointer" class="waves-teal blue-text" target="_blank" href="http://www.jorgemariel.com">Creado por Jorge Mariel</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function () {
		});

		$('#login').click(function(e){
			e.preventDefault();
			var username = $('#username');
			var password = $('#password');

			if(username.val() == "") {
				username.addClass('invalid');
				username.focus();
				return false;
			}
			else if(password.val() == "") {
				password.addClass('invalid');
				password.focus();
				return false;
			}

			$('#title').hide();
			$('#description').hide();
			$('#formLoginError').hide();
			$('#formLogin').hide();
			$('#formFooter').hide();
			$('#loading').fadeIn('slow', 'swing');

			$.ajax({
				type: 'POST',
				url: '<?php echo URL;?>
login',
				data: $('#formLogin').serialize(),
				timeout: 5000,
				success: function(msg){
					if ( msg == 1 ){
						setTimeout(function(){
							window.location.href = "<?php echo URL;?>
admin";
						},(500));
					} else {
						$('#loading').hide();
						$('#title').fadeIn();
						$('#description').fadeIn();
						$('#message').html(msg);
						$('#formLoginError').fadeIn();
						$('#formLogin').fadeIn();
						$('#formFooter').fadeIn();
					}
				},
				fail: function(msg){
					$('#loading').hide();
					$('#title').fadeIn();
					$('#description').fadeIn();
					$('#message').html(msg);
					$('#formLoginError').fadeIn();
					$('#formLogin').fadeIn();
					$('#formFooter').fadeIn();
				}
			})
		});

		$('#closeMessage').click(function(e){
			$('#formLoginError').fadeOut();
		});
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
