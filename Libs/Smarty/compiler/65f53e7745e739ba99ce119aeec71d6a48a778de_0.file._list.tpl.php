<?php
/* Smarty version 3.1.31, created on 2018-06-04 08:30:07
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Faq/_list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b14dc6f9adfa9_04148110',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '65f53e7745e739ba99ce119aeec71d6a48a778de' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Faq/_list.tpl',
      1 => 1528093805,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b14dc6f9adfa9_04148110 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['list']->value) && !empty($_smarty_tpl->tpl_vars['list']->value)) {?>
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m8 l8 strong">
				<a href="#" class="orderByName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Question'];?>
</a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByPosition"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Language'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByPosition"><i class="material-icons">language</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Status'];?>
</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'l');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['l']->value) {
?>
	<?php $_smarty_tpl->_assignInScope('image', "Views/img/languages/".((string)$_smarty_tpl->tpl_vars['l']->value['language_img']).".png");
?>
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m6 l8"><?php echo $_smarty_tpl->tpl_vars['l']->value['question'];?>
</div>
				<div class="col s2 m2 l2">
					<?php if (file_exists($_smarty_tpl->tpl_vars['image']->value)) {?>
          <div class="center-icon" style="text-align: center;">
            <img src="<?php echo URL;?>
Views/img/languages/<?php echo $_smarty_tpl->tpl_vars['l']->value['language_img'];?>
.png" alt="">
          </div>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
				<div class="col s2 m2 l2">
					<?php if ($_smarty_tpl->tpl_vars['l']->value['status']) {?>
					<i class="material-icons center-icon green-text">check</i>
					<?php } else { ?>
					<i class="material-icons center-icon red-text">block</i>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12">
					<strong><?php echo $_smarty_tpl->tpl_vars['lang']->value['Answer'];?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['l']->value['answer'];?>

				</div>
				<div class="col s12 right-align">
					<?php if (array_search(42,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
faqs/edit/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Edit'];?>
</a>
					<?php }?>
					<?php if (array_search(43,$_SESSION['user']['privileges'])) {?>
					<a href="<?php echo URL;?>
faqs/delete/<?php echo $_smarty_tpl->tpl_vars['l']->value['id'];?>
" class="waves-effect waves-teal btn-flat"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Delete'];?>
</a>
					<?php }?>
				</div>
			</div>
		</div>
	</li>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul>
<?php } else { ?>
<h3 class="center"><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoItems'];?>
</h3>
<?php }
}
}
