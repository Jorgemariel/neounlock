<?php
/* Smarty version 3.1.31, created on 2018-06-04 08:54:13
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Faq/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b14e2153b7681_36452328',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dc0c0fbcfcaa261e90cd87782efb6e65aaa6c0e9' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Faq/index.tpl',
      1 => 1528095251,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Faq/_search.tpl' => 1,
    'file:Faq/_list.tpl' => 1,
    'file:Faq/_contact.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5b14e2153b7681_36452328 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px" class="animated fadeInLeft"><?php echo $_smarty_tpl->tpl_vars['lang']->value['FrecuentQuestions'];?>
</h2>
			<!-- <p>
				Navega entre las preguntas mas realizadas por nuestros clientes y busca aquellas que sean de tu interés. <br>
				Si no encuentras la respuesta a tu pregunta en el listado, te invitamos a realizarla al final de la página. <br>
				Estamos para ayudarte.
			</p> -->
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Faq/_search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Faq/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Faq/_contact.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript">

		var faqs = [];

		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['faqs']->value, 'f');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['f']->value) {
?>
			var item = {question: '<?php echo $_smarty_tpl->tpl_vars['f']->value['question'];?>
', answer: '<?php echo $_smarty_tpl->tpl_vars['f']->value['answer'];?>
'};
			faqs.push(item);
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


		$(document).ready(function(){
			renderFaqs(faqs);
		});

		$('#search').keyup(search);
		$('#search-button').click(search);

		$('#send').click(function(){
			$('#contactForm').hide();
			$('#sendLoading').fadeIn();
			var formData = $('#contactForm').serialize();
			console.log(formData);

			$.ajax({
				url: '<?php echo URL;?>
contact/send',
				type: 'POST',
				data: formData,
				cache: false,
				success: function(data) {
					if (data == 1)
					{
						$('#sendLoading').hide();
						$('#success').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
						$('#contactForm')[0].reset();
					}
					else
					{
						$('#sendLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
					}
				},
				error: function(data) {
					$('#sendLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('#contactForm').fadeIn();
				}
			});
			return false;
		});

		function search() {
			var search = $('#search').val();
			var faqsFiltered = [];

			faqs.forEach(function(i) {
				var question = i.question.toLowerCase();
				var answer = i.answer.toLowerCase();

				if(question.includes(search.toLowerCase()) || answer.includes(search.toLowerCase())) {
					//console.log(i);
					faqsFiltered.push(i);
				}
			});

			renderFaqs(faqsFiltered);
		}
		function renderFaqs(list) {
			var faqhtml = '';

			list.forEach(function(i) {
				faqhtml += '<li class="faq">'+
	 										'<div class="collapsible-header question">' + i.question + '</div>'+
	 										'<div class="collapsible-body answer"><p>' + i.answer + '</p></div>'+
	 										'</li>';
			});

			if(faqhtml == '') {
				faqhtml = '<div class="center" style="padding-bottom: 5px;"><h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['NoResult'];?>
</h3></div>'
			}

			$('#faqs').html(faqhtml);
		}
	<?php echo '</script'; ?>
>

</body>
</html>
<?php }
}
