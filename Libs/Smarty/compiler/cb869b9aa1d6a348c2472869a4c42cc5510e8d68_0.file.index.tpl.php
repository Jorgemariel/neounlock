<?php
/* Smarty version 3.1.31, created on 2018-04-29 07:30:02
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ae5585ac27ad7_22689355',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cb869b9aa1d6a348c2472869a4c42cc5510e8d68' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Admin/Email/index.tpl',
      1 => 1524979787,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Admin/Public/head.tpl' => 1,
    'file:Admin/Public/sidenav.tpl' => 1,
    'file:Admin/Public/navbar.tpl' => 1,
    'file:Admin/Email/_list.tpl' => 1,
    'file:Admin/Email/_content.tpl' => 1,
    'file:Admin/Public/foot.tpl' => 1,
  ),
),false)) {
function content_5ae5585ac27ad7_22689355 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Admin/Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</head>
<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/sidenav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<div class="input-field col s9 m10 l11">
						<input id="searchName" name="searchName" type="text" class="validate">
						<label for="searchName"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Search'];?>
</label>
					</div>
					<div class="col s3 m2 l1 center"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	<div class="row messageArea" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel white-text orange">
			<i class="material-icons right messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['lang']->value['OnlyInSpanish'];?>
</h6>
			</div>
		</div>
	</div>

	<?php if (isset($_GET['message'])) {?>
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text <?php echo $_smarty_tpl->tpl_vars['messageColor']->value;?>
">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h6>
			</div>
		</div>
	</div>
	<?php }?>

	<div id="list"><?php $_smarty_tpl->_subTemplateRender('file:Admin/Email/_list.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="<?php echo URL;?>
email/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			<?php if (array_search(28,$_SESSION['user']['privileges'])) {?>
			<li><a href="<?php echo URL;?>
email/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
			<?php }?>
		</ul>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Email/_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php $_smarty_tpl->_subTemplateRender('file:Admin/Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo URL;?>
/Views/js/jquery.crp.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo URL;?>
/Views/js/jquery.md5.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo URL;?>
/Views/js/jquery.base64.min.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();

			$('.modal-trigger').leanModal({
				complete: function() {$('#contentForm').trigger("reset"); }
			});

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});

			$('.messageClose').click(function(){
				$('.messageArea').fadeOut();
			});
		});

		$('.modal-trigger').click(function() {
			var content = $(this).data('content');

			$('#contentText').val(content);
			$('#contentText').trigger('autoresize');

			var id = $(this).data('id');
			var id_language = $(this).data('language');

			$('#content').data('id', id);
			$('#content').data('language', id_language);
		});

		$('#contentFormSend').click(function() {
			$('.contentData').hide();
			$('#contentFormLoading').fadeIn();

			var idContent = $('#content').data('id') || 0;
			var content = $('#contentText').val();
			var idEmail = $('#list').find('.active').data('id');
			var idLanguage = $('#content').data('language');

			content = encodeURIComponent(content);

			var key = 'neounlock';
			content = $.crp.crypte(content, key);

			$.ajax({
				url: '<?php echo URL;?>
email/editContent/' + idEmail,
				type: 'POST',
				data: {content: content, id_content: idContent, id_language: idLanguage},
				cache: false,
				success: function(data) {
					if (data == '1')
					{
						window.location = '<?php echo URL;?>
email/?message=success';
					} else {
						$('#contentFormLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('.contentData').fadeIn();

					}
				},
				error: function(data) {
					$('#contentFormLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('.contentData').fadeIn();
				}
			});

			return false;
		});

		$('#contentFormTest').click(function() {
			$('.contentData').hide();
			$('#contentFormLoading').fadeIn();

			var content = $('#contentText').val();
			
			content = encodeURIComponent(content);

			var key = 'neounlock';
			content = $.crp.crypte(content, key);

			$.ajax({
				url: '<?php echo URL;?>
email/testContent',
				type: 'POST',
				data: {content: content},
				cache: false,
				success: function(data) {
					if (data == '1')
					{
						$('#contentFormLoading').hide();
						$('#success').fadeIn().delay(5000).slideUp('slow');
						$('.contentData').fadeIn();
					}
					else
					{
						$('#contentFormLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('.contentData').fadeIn();

					}
				},
				error: function(data) {
					console.log('error');

					$('#contentFormLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('.contentData').fadeIn();
				}
			});

			return false;
		});



		var sort = 'desc';
		var orderBy = 'position'

		$('#list').on('click', 'a.orderByName', function(e){
			orderBy = 'name';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderById', function(e){
			orderBy = 'id';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		// $(document).keypress(function(e) {
		// 	if(e.which == 13) {
		// 		e.preventDefault();
		// 		search();
		// 	}
		// });

		$('#searchForm').children().change(function(){
			search();
		});

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var searchName = $('#searchName');
			var searchPosition = $('#searchPosition');
			var searchImage = $('#searchImage');
			var searchStatus = $('#searchStatus');
			var searchModel = $('#searchModel');
			var searchCountry = $('#searchCountry');
			var searchCompany = $('#searchCompany');
			var searchMEP = $('#searchMEP');
			var searchPRD = $('#searchPRD');

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '<?php echo URL;?>
email/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	<?php echo '</script'; ?>
>
</body>
</html>
<?php }
}
