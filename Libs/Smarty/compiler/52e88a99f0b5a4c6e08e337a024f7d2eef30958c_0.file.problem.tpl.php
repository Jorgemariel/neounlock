<?php
/* Smarty version 3.1.31, created on 2017-07-09 20:05:56
  from "/opt/lampp/htdocs/neounlock/Views/template/Tracking/problem.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5962b6d47a7320_26594936',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '52e88a99f0b5a4c6e08e337a024f7d2eef30958c' => 
    array (
      0 => '/opt/lampp/htdocs/neounlock/Views/template/Tracking/problem.tpl',
      1 => 1499640570,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:Public/head.tpl' => 1,
    'file:Public/nav.tpl' => 1,
    'file:Public/footer.tpl' => 1,
    'file:Public/foot.tpl' => 1,
  ),
),false)) {
function content_5962b6d47a7320_26594936 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:Public/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<link rel="stylesheet" type="text/css" href="<?php echo URL;?>
Views/css/tracking/problem-chat.css">
	<style>
		.message-wrapper.them .circle-wrapper {
		  background-image: url('<?php echo URL;?>
Views/img/logo.png');
		}
		.message-wrapper.me .circle-wrapper {
		  background-image: url('<?php echo URL;?>
Views/img/tracking/face.png');
		}
	</style>
</head>

<body>
	<?php $_smarty_tpl->_subTemplateRender('file:Public/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px"><?php echo $_smarty_tpl->tpl_vars['lang']->value['TrackingStatus'];?>
</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Order'];?>
 <strong><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['order']->value['tracking'], 'UTF-8');?>
</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel teal darken-3 white-text valign-wrapper" style="margin: 0 20px">
				<i class="material-icons valign" style="margin-right: 5px">info</i>
				<h6><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderUnderReview'];?>
</h6>
			</div>

			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><p style="text-align: justify;"><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderUnderReviewText'];?>
</p></li>
			</ul>

			<div class="row" style="margin-top: 20px">
				<div class="col s12 m8 l6 offset-m2 offset-l3 center">
					<a class="waves-effect waves-light btn-large green" style="margin: 10px; width: 85%"><i class="material-icons left">done</i><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['lang']->value['ProblemSolved'], 'UTF-8');?>
</a>
				</div>
			</div>

			<!-- <div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['Report'];?>
</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="wrapper" style="margin: 20px">
				<div id="inner" class="inner">
					<div id="content" class="content"></div>
				</div>
				<div id="bottom" class="bottom">
					<textarea id="input" class="input"></textarea>
					<div id="send" class="send btn-floating btn-large"></div>
				</div>
			</div> -->

			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['OrderDetail'];?>
</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<ul class="collection" style="margin: 20px">

				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['brand']) && !empty($_smarty_tpl->tpl_vars['order']->value['brand'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Brand'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['brand'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['model']) && !empty($_smarty_tpl->tpl_vars['order']->value['model'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Model'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['model'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['country']) && !empty($_smarty_tpl->tpl_vars['order']->value['country'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Country'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['country'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['company']) && !empty($_smarty_tpl->tpl_vars['order']->value['company'])) {?>
				<li class="collection-item"><div><?php echo $_smarty_tpl->tpl_vars['lang']->value['Company'];?>
<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['company'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['mep']) && !empty($_smarty_tpl->tpl_vars['order']->value['mep'])) {?>
				<li class="collection-item"><div>MEP<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['mep'];?>
</a></div></li>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['order']->value['prd']) && !empty($_smarty_tpl->tpl_vars['order']->value['prd'])) {?>
				<li class="collection-item"><div>PRD<a href="#!" class="secondary-content"><?php echo $_smarty_tpl->tpl_vars['order']->value['prd'];?>
</a></div></li>
				<?php }?>
			</ul>
		</div>
	</div>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php $_smarty_tpl->_subTemplateRender('file:Public/foot.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo URL;?>
Views/js/tracking/problem-chat.js"><?php echo '</script'; ?>
>

	<?php echo '<script'; ?>
 type="text/javascript">
		$(document).ready(function(){
			$('.message-wrapper.them .circle-wrapper').css('background-image', "url('<?php echo URL;?>
Views/img/logo.png')");
		});

	<?php echo '</script'; ?>
>

</body>
</html><?php }
}
