<?php
/* Smarty version 3.1.31, created on 2018-04-27 21:36:40
  from "/home/jorge/Dev/Proyectos/neounlock/Views/template/Public/_terms.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ae37bc869d311_89812014',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cdbdba540605380af58c3f6ceb1261826c4be524' => 
    array (
      0 => '/home/jorge/Dev/Proyectos/neounlock/Views/template/Public/_terms.tpl',
      1 => 1494797824,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ae37bc869d311_89812014 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="termsmodal" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h3>Condiciones particulares, uso del servicio de liberación y aviso legal</h3>
		<p>
			Las condiciones particulares descritas a continuación establecen las características del servicio de
			liberaciones de terminales (teléfonos y otros dispositivos móviles) prestado por NeoUnlock a sus
			clientes a través de la página web www.neounlock.com (en adelante "NeoUnlock") y deberán ser
			aceptadas junto a las condiciones generales de los servicios hospedados en NeoUnlock y como tal,
			no reemplazarán o anularan las condiciones establecidas en éstas sino que las complementan.
			NeoUnlock puede ocasionalmente modificar los artículos de las Condiciones Particulares, por lo
			que se recomienda su lectura previa a la contratación del servicio de liberaciones.
		</p>
		<h4>Definición del servicio</h4>
		<p>
			El servicio de liberación de terminales de NeoUnlock es ofrecido a sus clientes como mero
			intermediario entre una serie de proveedores de servicios especializados tanto nacionales como
			internacionales y el cliente final.
		</p>
		<p>
			Este servicio se ofrece con la modalidad "Liberación por IMEI".<br>
			La liberación por IMEI de terminales se realiza mediante la introducción de un código a través del
			teclado del teléfono, ya sea físico o virtual.
			El pedido y el pago se tramitarán a través de la página web www.neounlock.com. Posteriormente,
			NeoUnlock se comunicará por correo electrónico con el cliente para hacer entrega del código de
			liberación.
		</p>
		<h4>Comprobaciones y requisitos</h4>
		<p>
			NeoUnlock exige a su cliente que lleve a cabo las siguientes comprobaciones antes de realizar el
			pedido de liberación:
		</p>
		<ul>
			<li>
				Verificar que no se haya superado el límite máximo de intentos de liberación del terminal.
				En caso contrario el terminal no se podrá liberar por este medio.
			</li>
			<li>
				Verificar que el IMEI del terminal no esté bloqueado por el operador por pérdida, robo o
				impago. En este caso, la liberación del terminal no será posible.
			</li>
			<li>
				Verificar que el terminal no se haya liberado anteriormente.
			</li>
			<li>
				Verificar que el teléfono pide el código de liberación. El cliente deberá consultar la web de
				NeoUnlock específica a cada modelo o al servicio de atención al cliente de NeoUnlock
				cualquier duda previa a la realización del pedido.
			</li>
			<li>
				Verificar que el terminal no tiene ni ha tenido el software modificado, ya que solo se
				garantizará el correcto funcionamiento con el software original del fabricante y operador.
			</li>
			<li>
				Verificar que en el caso de los terminales Apple éstos no estén bloqueados mediante la ID
				y contraseña de Apple (iCloud) o la opción de seguridad "Buscar mi iPhone" o "Find mi
				iPhone". En este caso, la liberación del terminal no será posible. Si el cliente no logra
				recordar sus claves, es recomendable contactar el punto de venta.
			</li>
			<li>
				Verificar que se conoce cualquier clave personal o contraseña de seguridad, tales como
				PUK, PIN, Vodafone Protect o patrones de seguridad. La liberación o desbloqueo no serán
				posibles ante el desconocimiento de las mismas y no se realizará reembolso alguno al ser
				una incidencia responsabilidad del cliente. NeoUnlock se compromete a asistir en
				cualquier duda en la liberación una vez que el cliente recuerde o recupere dichas claves.
			</li>
			<li>
				En algunos casos se indicarán debidamente al cliente que deberá introducir una tarjeta
				SIM de un operador distinto al asociado al terminal para completar la liberación por IMEI.
				Si el cliente tuviera cualquier duda o no supiera cómo obtener esta información, le recomendamos
				que se ponga en contacto con el servicio técnico de NeoUnlock en la dirección de correo
				electrónico info@neounlock.com
			</li>
		</ul>
		<h4>Pedidos</h4>
		<p>
			La tramitación del código de liberación solo se hará efectiva tras la finalización del pago. Una vez
			efectuado el pago, el cliente recibirá un correo electrónico de confirmación del mismo en la
			dirección de e-mail que haya indicado en el pedido. En caso de no recibirlo, se recomienda revisar
			el buzón de correo no deseado (spam).
		</p>
		<p>
			NeoUnlock se reserva el derecho de rechazar la tramitación de pedidos en cualquier momento si
			estima que pudieran existir actividades ilícitas o fraudulentas durante el transcurso de los mismos.
		</p>
		<h4>Plazos de tramitación y entrega</h4>
		<p>
			Los plazos de entrega de códigos varían dependiendo del terminal seleccionado.
			NeoUnlock no se hace responsable de retrasos en la entrega del código causados por proveedores
			externos. Sin embargo hará todo lo posible para entregar el código con la mayor diligencia y en el
			menor plazo posible.
		</p>
		<h4>Tiempo medio y plazo garantizado</h4>
		<p>
			El tiempo medio especifica la media de retraso en la entrega de los últimos pedidos del terminal
			seleccionado en función de marca, modelo, país y operador.
		</p>
		<p>
			El plazo garantizado especifica el retraso máximo que se genera en la entrega del código solicitado
			según la marca, modelo, país y operador del terminal.
		</p>
		<p>
			NeoUnlock se compromete a realizar la devolución del importe abonado si el plazo garantizado es
			superado. El cliente deberá solicitarlo mediante correo electrónico enviando su código de
			seguimiento.
		</p>
		<h4>Horario laboral</h4>
		<p>
			El horario laboral al que hacen mención los tiempos de entrega será el comprendido entre las
			10am a 10pm GMT -3, solo días hábiles.
		</p>
		<h4>Precios y ofertas</h4>
		<p>
			Los precios por los servicios ofertados estarán debidamente indicados en la web y se muestran en
			dólares.
		</p>
		<p>
			Los precios y las ofertas podrían variar de manera temporal o permanente según el criterio de
			NeoUnlock.
		</p>
		<p>
			Las tarifas de los servicios ofertados por NeoUnlock podrán ser modificadas. No obstante, al
			realizar el pago de un servicio se garantiza el precio especificado en dicho momento.
		</p>
		<h4>Cancelación del pedido</h4>
		<p>
			Los pedidos de liberación por IMEI se tramitan de manera automática. Una vez efectuado el pago,
			las solicitudes son automáticamente ejecutadas y no podrán cancelarse ni reembolsarse.
		</p>
		<h4>Devoluciones</h4>
		<p>
			NeoUnlock se compromete a devolver el importe íntegro del servicio prestado en los siguientes
			casos.
		</p>
		<ol>
			<li>
				<h5>El código no existe</h5>
				Si no existe código de liberación alguno asociado al pedido, NeoUnlock se compromete a realizar
				la devolución íntegra del pago realizado por el cliente siempre y cuando no se trate de un caso
				expuesto en los puntos del apartado A. Subapartados "1", "2", "3", "4", expuestos a continuación.
			</li>
			<li>
				<h5>El código de liberación o las instrucciones son incorrectas.</h5>
				<p>
					Si en el proceso de liberación el terminal indica que el código es erróneo el cliente deberá enviar
					un video demostrativo sin cortes que muestre el IMEI (marcando *#06#) y cómo introduce el
					código en el terminal. La imagen deberá ser suficientemente nítida para distinguir las teclas y los
					textos de la pantalla y deberá mostrar el terminal completo en todo momento. En caso de
					detectar un error a la hora de introducir el código a través del video, NeoUnlock se pondrá en
					contacto con el cliente vía correo electrónico detallando la solución al problema. En caso
					contrario, si se tratase de un error real en el código o en las instrucciones NeoUnlock se
					compromete a realizar la devolución íntegra del pago siempre y cuando el error no se deba a los
					casos expuestos a continuación.
				</p>
				<p>
					A NeoUnlock no realizará devoluciones en los casos en los que el terminal no se libere
					correctamente por causas ajenas a NeoUnlock, incluidos sin carácter limitativo:
				</p>
			</li>
			<li>
				<h6>IMEI Bloqueado por pérdida, robo o impago</h6>
				<p>
					NeoUnlock podrá liberar el terminal pero no deshacer el bloqueo de IMEI que impone la
					operadora (proveedor de telefonía vinculado al terminal) en casos de pérdida, robo o impago. En
					estos casos el terminal con IMEI bloqueado no se podrá conectar a ninguna red para realizar
					llamadas pese a estar liberado correctamente. Antes de solicitar la liberación de un terminal de
					origen desconocido, compruebe que no tiene el IMEI bloqueado. NeoUnlock no realizará la
					devolución de una liberación correcta por problemas relacionados con el bloqueo de IMEI del
					terminal. Asimismo NeoUnlock declara que no prestará servicios relacionados con la modificación
					del IMEI bajo ningún concepto.
				</p>
			</li>
			<li>
				<h6>Contador de Intentos Agotado</h6>
				<p>
					Si el terminal no se puede liberar por IMEI porque tiene un contador que limita el número de
					intentos y éste está agotado.
				</p>
			</li>
			<li>
				<h6>El terminal estaba libre con anterioridad.</h6>
				<p>
					NeoUnlock no puede comprobar qué teléfonos estaban libres previamente a los pedidos recibidos.
					Corresponde al cliente verificar si está ya libre para que el servicio contratado tenga la utilidad
					deseada.
				</p>
			</li>
			<li>
				<h6>Errores en el pedido</h6>
				<p>
					Si los siguientes datos introducidos por cliente al realizar el pedido no son correctos: Número de
					IMEI, número de serie, marca, modelo u operador del terminal y dirección de correo de contacto.
				</p>
			</li>
			<li>
				<h6>Alteraciones de software y/o hardware</h6>
				<p>
					Cualquier modificación del software o hardware original del equipo que interfiera en el correcto
					funcionamiento y aplicación del código e instrucciones suministrados por NeoUnlock.
				</p>
			</li>
		</ol>
		<p>
			NeoUnlock realizará los reintegros en la cuenta de PayPal, MercadoPago y DineroMail (PayU)
			utilizada para la compra o podrá entregar "Créditos NeoUnlock" para su posterior uso en servicios
			brindados en la página web. Estos se tramitarán en un plazo de entre 48 y 72 horas laborables,
			dependiendo de la entidad del usuario.
		</p>
		<p>
			Las devoluciones en Créditos ("Créditos de Abono") permitirán al cliente acumular "Créditos" para
			realizar otras compras en NeoUnlock sin incurrir en gastos ni comisione. Los "Créditos de Abono"
			no tienen caducidad.
		</p>
		<h4>Formas de pago</h4>
		<p>
			NeoUnlock pone a su disposición varias modalidades de pago en función del país desde el que se
			realice:
		</p>
		<ul>
			<li>PayPal</li>
			<li>DineroMail (PayU)</li>
			<li>MercadoPago: Rapipago, PagoFacil y Tarjeta de crédito</li>
			<li>BitCoin</li>
		</ul>
		<h4>Responsabilidades</h4>
		<p>
			En el servicio de liberaciones por IMEI, NeoUnlock actúa meramente de intermediario entre los
			proveedores de códigos y el cliente. NeoUnlock no se responsabiliza en relación con los
			procedimientos ni los métodos empleados por los proveedores de códigos para la obtención de
			estos. Lo advierte con carácter previo al cliente, y le exige que en caso de estar en desacuerdo, no
			acepte estas condiciones particulares y por tanto, no solicite el servicio.
			NeoUnlock no será responsable de las consecuencias legales en las que se pudiera incurrir por la
			liberación del terminal móvil frente a terceros (incluidos dentro de estos terceros, a título
			meramente enunciativo, los incumplimientos del cliente en relación con los acuerdos de
			permanencia que pudiera tener suscritos con las operadoras de telefonía móvil y fabricantes de
			terminales).
		</p>
		<p>
			NeoUnlock podrá ceder información del pedido a miembros de la Policía Judicial incluyendo, sin
			carácter limitativo: IP, ubicación, IMEI del teléfono y dirección de correo desde la cual se originó el
			pedido. Esta información será almacenada en ficheros específicos para su posterior tratamiento
			con el fin de reprimir infracciones penales como pueden ser el robo o utilización fraudulenta de
			una tarjeta de crédito.
		</p>
		<p>
			El cliente, mediante la aceptación de estas Condiciones Particulares, acepta eximir de cualquier
			responsabilidad a NeoUnlock como consecuencia de la obtención de los códigos de liberación de
			terminales.
		</p>

	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect btn-flat modal-close" style="margin: 5px">Cerrar</a>
	</div>
</div><?php }
}
