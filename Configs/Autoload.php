<?php namespace Configs;

	class Autoload
	{
		public static function run()
		{
			spl_autoload_register(function($class){
				//var_dump($class);
				//var_dump(str_replace('\\', '/', $class) . '.php');
				include_once str_replace('\\', '/', $class) . '.php';
			});
		}
	}

?>