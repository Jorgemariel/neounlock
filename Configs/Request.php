<?php namespace Configs;

	class Request
	{
		private $controller;
		private $method;
		private $argument;

		public function __construct()
		{
			if (isset($_GET['url']))
			{
				//Toma la URL y la parte en un array
				$link = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);
				$link = explode("/", $link);
				$link = array_filter($link);

				//Si no hay un controller, te lleva al indexController
				if ($link[0] == "index.php") {
					$this->controller = "index";
				}
				else
				{
					//Carga en controller de la URL
					$this->controller = strtolower(array_shift($link));
				}

				//Carga el metodo de la URL. Si no lo encuentra carga index
				if (!$this->method = strtolower(array_shift($link)))
				{
					$this->method = 'index';
				}

				//Toda la URL restante son tratados como argumentos.
				$this->argument = $link;
			}
			else
			{
				$this->controller = "home";
				$this->method = "index";
			}

			// var_dump($this->controller, $this->method, $this->argument);
		}

		public function getController()
		{
			return $this->controller;
		}

		public function getMethod()
		{
			return $this->method;
		}

		public function getArgument()
		{
			return $this->argument;
		}
	}
?>