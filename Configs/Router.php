<?php namespace Configs;

	class Router
	{
		public static function run(Request $request)
		{

			// var_dump($request->getController());
			// var_dump($request->getMethod());
			// var_dump($request->getArgument());

			//Define donde y como se llaman los controllers
			$controller = $request->getController() . 'Controller';
			$link = ROOT . "Controllers" . DS . $controller . ".php";

			//Define el nombre del method. En este caso no varia.
			$method = $request->getMethod();
			if ($method == "index.php")
			{
				$method = 'index';
			}

			//Define el nombre del argumento. En este caso no varia.
			$argument = $request->getArgument();

			//Controla que el controller sea legible
			if (is_readable($link))
			{
				require_once $link;
				$show = "Controllers\\" . $controller;
				$controller = new $show;

				if (!isset($argument)) //?????????????????????????????
				{
					$datos = call_user_func(array($controller, $method));
				}
				else
				{
					$datos = call_user_func_array(array($controller, $method), $argument);
				}
			}

			//Cargar vista

			// $link = ROOT . "Views" . DS . $request->getController() . DS . $request->getMethod() . ".php";
			// if (is_readable($link))
			// {
			// 	require_once $link;
			// }
			// else
			// {
			// 	var_dump($link);
			// 	echo 'no se encontro la ruta';
			// }
		}
	}

?>