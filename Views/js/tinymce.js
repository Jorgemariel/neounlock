tinymce.init({
	selector: "textarea",
	height: 300,
	autosave_ask_before_unload: false,
	plugins: [
	"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
	"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	"table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern"
	],

	toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect | searchreplace | bullist numlist | outdent indent | link unlink image code | preview forecolor table | removeformat | emoticons print spellchecker visualblocks",

	menubar: false,
	toolbar_items_size: 'small',

	style_formats: [{
		title: 'Bold text',
		inline: 'b'
	}, {
		title: 'Red text',
		inline: 'span',
		styles: {
			color: '#ff0000'
		}
	}, {
		title: 'Red header',
		block: 'h1',
		styles: {
			color: '#ff0000'
		}
	}, {
		title: 'Example 1',
		inline: 'span',
		classes: 'example1'
	}, {
		title: 'Example 2',
		inline: 'span',
		classes: 'example2'
	}, {
		title: 'Table styles'
	}, {
		title: 'Table row 1',
		selector: 'tr',
		classes: 'tablerow1'
	}],
	content_css: [
	'//www.tinymce.com/css/codepen.min.css'
	]
});