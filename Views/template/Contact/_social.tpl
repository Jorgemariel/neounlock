<div class="container">
	<div class="card-panel" style="margin-top: -100px;">
		<div class="row valign-wrapper title">
			<h5 class="col s12 m10 l10">{$lang['SocialNetworks']}</h5>
			<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
				<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
				<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
				<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
			</div>
		</div>
		<div id="social-links" class="row" style="margin: 10px; margin-bottom: 0">
			<div class="col l4 m12 s12 center hoverable" style="padding: 15px">
				<div class="">
					<a href="https://www.facebook.com/neounlockofficial" target="_blank">
						<button class="waves-effect waves-light btn-floating btn-large float-large" style="background: #3B5C95">
							<img src="{URL}Views/img/facebook-lite.png" class="responsive-img" style="height: 30px; margin-bottom: -10px">
						</button>
						<h6 style="float: left; margin: 20px; margin-right: 0;">/NeoUnlockOfficial</h6>
					</a>
				</div>
			</div>

      <div class="col l4 m12 s12 center hoverable" style="padding: 15px">
				<a href="https://api.whatsapp.com/send?phone=5493515144316" target="_blank">
					<button class="waves-effect waves-light btn-floating btn-large float-large" style="background: #68B859">
            <img src="{URL}Views/img/whatsapp-plane.png" class="responsive-img" style="height: 40px; margin-bottom: -15px">
          </button>
          <h6 style="float: left; margin: 20px; margin-right: 0;">+54 9 3515 144 316</h6>
				</a>
			</div>

      <div class="col l4 m12 s12 center hoverable" style="padding: 15px">
				<a href="mailto:info@neounlock.com">
					<button class="waves-effect waves-light btn-floating btn-large float-large" style="background: #00AAF2">
            <i class="material-icons" style="font-size: 2rem;">mail</i>
          </button>
          <h6 style="float: left; margin: 20px; margin-right: 0;">info@neounlock.com</h6>
				</a>
			</div>
		</div>
	</div>
</div>
