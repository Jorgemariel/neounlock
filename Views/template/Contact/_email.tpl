<div class="container" style="margin-top: 50px">
  <div class="card-panel">
    <div class="row valign-wrapper title">
			<h5 class="col s12 m10 l10">{$lang['Queries']}</h5>
			<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
				<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
				<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
				<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
			</div>
		</div>

    <div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="error-text">{$lang['ErrorOcurred']}</h6>
		</div>

    <div id="success" class="card-panel green white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="success-text">{$lang['EmailSent']}</h6>
		</div>

    <form id="contactForm" class="row" style="margin: 25px; margin-bottom: 0;">
      <div class="input-field col s12">
        <i class="material-icons prefix">email</i>
        <input id="email" name="email" type="email" class="validate">
        <label for="email">Mail de contacto</label>
      </div>
      <div class="input-field col s12">
        <i class="material-icons prefix">mode_edit</i>
        <textarea id="textarea" name="textarea" class="materialize-textarea"></textarea>
        <label for="textarea">Su consulta</label>
      </div>
      <div class="center">
        <button class="btn waves-effect waves-light green darken-2" type="button" id="send" name="send">Enviar
          <i class="material-icons right">send</i>
        </button>
      </div>
    </form>
    <div class="row">
      <div id="sendLoading" class="center" style="display: none; padding-top: 200px; padding-bottom: 200px">{include 'Public/_loading.tpl'}</div>
    </div>

  </div>
</div>
