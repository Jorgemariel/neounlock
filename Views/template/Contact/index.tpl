{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px" class="animated fadeInLeft">{$lang['Contact']}</h2>
			<!-- <p>
				Navega entre las preguntas mas realizadas por nuestros clientes y busca aquellas que sean de tu interés. <br>
				Si no encuentras la respuesta a tu pregunta en el listado, te invitamos a realizarla al final de la página. <br>
				Estamos para ayudarte.
			</p> -->
		</div>
	</div>

	{include 'Contact/_social.tpl'}

	{include 'Contact/_email.tpl'}

	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	<script type="text/javascript">
		$(document).ready(function(){

		});

		$('#send').click(function(){
			$('#contactForm').hide();
			$('#sendLoading').fadeIn();
			var formData = $('#contactForm').serialize();
			console.log(formData);

			$.ajax({
				url: '{URL}contact/send',
				type: 'POST',
				data: formData,
				cache: false,
				success: function(data) {
					if (data == 1)
					{
						$('#sendLoading').hide();
						$('#success').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
						$('#contactForm')[0].reset();
					}
					else
					{
						$('#sendLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
					}
				},
				error: function(data) {
					$('#sendLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('#contactForm').fadeIn();
				}
			});
			return false;
		});
	</script>

</body>
</html>
