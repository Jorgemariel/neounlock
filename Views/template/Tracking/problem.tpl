{include 'Public/head.tpl'}
	<link rel="stylesheet" type="text/css" href="{URL}Views/css/tracking/problem-chat.css">
	<style>
		.message-wrapper.them .circle-wrapper {
		  background-image: url('{URL}Views/img/logo.png');
		}
		.message-wrapper.me .circle-wrapper {
		  background-image: url('{URL}Views/img/tracking/face.png');
		}
	</style>
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px">{$lang['TrackingStatus']}</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">{$lang['Order']} <strong>{$order['tracking']|upper}</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel teal darken-3 white-text valign-wrapper" style="margin: 0 20px">
				<i class="material-icons valign" style="margin-right: 5px">info</i>
				<h6>{$lang['OrderUnderReview']}</h6>
			</div>

			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><p style="text-align: justify;">{$lang['OrderUnderReviewText']}</p></li>
			</ul>

			<div class="row" style="margin-top: 20px">
				<div class="col s12 m8 l6 offset-m2 offset-l3 center">
					<a class="waves-effect waves-light btn-large green" style="margin: 10px; width: 85%"><i class="material-icons left">done</i>{$lang['ProblemSolved']|upper}</a>
				</div>
			</div>

			<!-- <div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">{$lang['Report']}</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="wrapper" style="margin: 20px">
				<div id="inner" class="inner">
					<div id="content" class="content"></div>
				</div>
				<div id="bottom" class="bottom">
					<textarea id="input" class="input"></textarea>
					<div id="send" class="send btn-floating btn-large"></div>
				</div>
			</div> -->

			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">{$lang['OrderDetail']}</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<ul class="collection" style="margin: 20px">

				{if isset($order['brand']) && !empty($order['brand'])}
				<li class="collection-item"><div>{$lang['Brand']}<a href="#!" class="secondary-content">{$order['brand']}</a></div></li>
				{/if}
				{if isset($order['model']) && !empty($order['model'])}
				<li class="collection-item"><div>{$lang['Model']}<a href="#!" class="secondary-content">{$order['model']}</a></div></li>
				{/if}
				{if isset($order['country']) && !empty($order['country'])}
				<li class="collection-item"><div>{$lang['Country']}<a href="#!" class="secondary-content">{$order['country']}</a></div></li>
				{/if}
				{if isset($order['company']) && !empty($order['company'])}
				<li class="collection-item"><div>{$lang['Company']}<a href="#!" class="secondary-content">{$order['company']}</a></div></li>
				{/if}
				{if isset($order['mep']) && !empty($order['mep'])}
				<li class="collection-item"><div>MEP<a href="#!" class="secondary-content">{$order['mep']}</a></div></li>
				{/if}
				{if isset($order['prd']) && !empty($order['prd'])}
				<li class="collection-item"><div>PRD<a href="#!" class="secondary-content">{$order['prd']}</a></div></li>
				{/if}
			</ul>
		</div>
	</div>

	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}
	<script type="text/javascript" src="{URL}Views/js/tracking/problem-chat.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.message-wrapper.them .circle-wrapper').css('background-image', "url('{URL}Views/img/logo.png')");
		});

	</script>

</body>
</html>