{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px">{$lang['TrackingStatus']}</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">{$lang['Order']} <strong>{$order['tracking']|upper}</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel orange white-text valign-wrapper" style="margin: 0 20px">
				<i class="material-icons valign" style="margin-right: 5px">access_time</i>
				<h6>{$lang['WaitingPayment']}</h6>
			</div>

			<ul class="collection" style="margin: 20px">
				<li class="collection-item"><div>IMEI<a href="#!" class="secondary-content">{$order['imei']}</a></div></li>
				<li class="collection-item"><div>{$lang['Service']}<a href="#!" class="secondary-content">{$order['service']}</a></div></li>
				<li class="collection-item"><div>{$lang['Price']}<a href="#!" class="secondary-content">USD {$order['service_price']}</a></div></li>
				<li class="collection-item"><div>{$lang['Delay']}<a href="#!" class="secondary-content">{$order['service_delay']} {$lang[$order['delay_type']]}</a></div></li>
			</ul>

			<div class="row" style="margin-top: 20px">
				<div class="col s12 m6 l4 center">
					<a href="{URL}home/payment/{$order['tracking']}" target="_blank" class="waves-effect waves-light btn-large blue tracking-button" style="margin: 10px; width: 85%">
						<i class="material-icons left">attach_money</i>{$lang['Pay']|upper}
					</a>
				</div>
				<div class="col s12 m6 l4 center">
					<a class="waves-effect waves-light btn-large green tracking-button" style="margin: 10px; width: 85%"><i class="material-icons left">attach_money</i>{$lang['ReportPayment']|upper}</a>
				</div>
				<div class="col s12 m6 l4 offset-m3 center">
					<a class="waves-effect waves-light btn-large red tracking-button modal-trigger" href="#helpingmodal" style="margin: 10px; width: 85%"><i class="material-icons left">help</i>{$lang['Help']|upper}</a>
				</div>
			</div>

			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">{$lang['OrderDetail']}</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>
			<ul class="collection" style="margin: 20px">

				{if isset($order['brand']) && !empty($order['brand'])}
				<li class="collection-item"><div>{$lang['Brand']}<a href="#!" class="secondary-content">{$order['brand']}</a></div></li>
				{/if}
				{if isset($order['model']) && !empty($order['model'])}
				<li class="collection-item"><div>{$lang['Model']}<a href="#!" class="secondary-content">{$order['model']}</a></div></li>
				{/if}
				{if isset($order['country']) && !empty($order['country'])}
				<li class="collection-item"><div>{$lang['Country']}<a href="#!" class="secondary-content">{$order['country']}</a></div></li>
				{/if}
				{if isset($order['company']) && !empty($order['company'])}
				<li class="collection-item"><div>{$lang['Company']}<a href="#!" class="secondary-content">{$order['company']}</a></div></li>
				{/if}
				{if isset($order['mep']) && !empty($order['mep'])}
				<li class="collection-item"><div>MEP<a href="#!" class="secondary-content">{$order['mep']}</a></div></li>
				{/if}
				{if isset($order['prd']) && !empty($order['prd'])}
				<li class="collection-item"><div>PRD<a href="#!" class="secondary-content">{$order['prd']}</a></div></li>
				{/if}
			</ul>
		</div>
	</div>

	{include 'Tracking/_help.tpl'}


	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	<script type="text/javascript">
		{literal}
		$('#helping-go').click(function(){
			helpingGo(); 
			return false;
		});
		{/literal}

		function helpingGo() {
			console.log('falta terminar consulta');
		}
	</script>

</body>
</html>
