<div id="helpingmodal" class="modal">
	<div class="modal-content">
		<h4>{$lang['Help']}</h4>
		<p>{$lang['AskYourQuestion']}</p>
        <div class="input-field" style="margin-top: 2rem;">
            <textarea id="textarea" name="textarea" class="materialize-textarea"></textarea>
            <label for="textarea">{$lang['YourQuestion']}</label>
        </div>
	</div>
	<div class="modal-footer">
		<button id="helping-go" type="submit" class="modal-action waves-effect btn green darken-2" style="margin: 5px">{$lang['Send']}</button>
		<a class="modal-action modal-close waves-effect btn-flat modal-close" style="margin: 5px">{$lang['Close']}</a>
	</div>
</div>
