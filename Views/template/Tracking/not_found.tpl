{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px">{$lang['NotFound']}</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">{$lang['Order']} <strong>{$tracking|upper}</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div class="card-panel red white-text valign-wrapper" style="margin: 0 20px 20px 20px;">
				<i class="material-icons valign" style="margin-right: 5px">error</i>
				<h6>{$lang['OrderNotFound']}</h6>
			</div>
      <div class="center" style="width: 100%">
        <a href="{URL}" class="btn green">
          {$lang['Back']}
        </a>
      </div>

		</div>
	</div>

	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	<script type="text/javascript">
		$(document).ready(function(){

		});
	</script>

</body>
</html>
