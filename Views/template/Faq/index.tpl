{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px" class="animated fadeInLeft">{$lang['FrecuentQuestions']}</h2>
			<!-- <p>
				Navega entre las preguntas mas realizadas por nuestros clientes y busca aquellas que sean de tu interés. <br>
				Si no encuentras la respuesta a tu pregunta en el listado, te invitamos a realizarla al final de la página. <br>
				Estamos para ayudarte.
			</p> -->
		</div>
	</div>

	{include 'Faq/_search.tpl'}

	{include 'Faq/_list.tpl'}

	{include 'Faq/_contact.tpl'}

	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	<script type="text/javascript">

		var faqs = [];

		{foreach from = $faqs item = f}
			var item = {literal}{{/literal}question: '{$f.question}', answer: '{$f.answer}'{literal}}{/literal};
			faqs.push(item);
		{/foreach}

		$(document).ready(function(){
			renderFaqs(faqs);
		});

		$('#search').keyup(search);
		$('#search-button').click(search);

		$('#send').click(function(){
			$('#contactForm').hide();
			$('#sendLoading').fadeIn();
			var formData = $('#contactForm').serialize();
			console.log(formData);

			$.ajax({
				url: '{URL}contact/send',
				type: 'POST',
				data: formData,
				cache: false,
				success: function(data) {
					if (data == 1)
					{
						$('#sendLoading').hide();
						$('#success').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
						$('#contactForm')[0].reset();
					}
					else
					{
						$('#sendLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('#contactForm').fadeIn();
					}
				},
				error: function(data) {
					$('#sendLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('#contactForm').fadeIn();
				}
			});
			return false;
		});

		function search() {
			var search = $('#search').val();
			var faqsFiltered = [];

			faqs.forEach(function(i) {
				var question = i.question.toLowerCase();
				var answer = i.answer.toLowerCase();

				if(question.includes(search.toLowerCase()) || answer.includes(search.toLowerCase())) {
					//console.log(i);
					faqsFiltered.push(i);
				}
			});

			renderFaqs(faqsFiltered);
		}
		function renderFaqs(list) {
			var faqhtml = '';

			list.forEach(function(i) {
				faqhtml += '<li class="faq">'+
	 										'<div class="collapsible-header question">' + i.question + '</div>'+
	 										'<div class="collapsible-body answer"><p>' + i.answer + '</p></div>'+
	 										'</li>';
			});

			if(faqhtml == '') {
				faqhtml = '<div class="center" style="padding-bottom: 5px;"><h3>{$lang['NoResult']}</h3></div>'
			}

			$('#faqs').html(faqhtml);
		}
	</script>

</body>
</html>
