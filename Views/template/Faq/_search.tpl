<div class="container">
	<div class="card-panel" style="margin-top: -100px;">
		<div class="row valign-wrapper title">
			<h5 class="col s12 m10 l10">{$lang['Search']}</h5>
			<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
				<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
				<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
				<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
			</div>
		</div>
		<div class="row" style="margin: 25px">
			<!-- <div id="help" class="card-panel red white-text valign-wrapper" style="margin: 0 20px;">
				<i class="material-icons valign" style="margin-right: 5px">info</i>
				<h6 id="help-text">Ingresa las palabras claves de tu consulta para filtrar el listado de preguntas frecuentes</h6>
			</div> -->
			<div class="input-field col m10 l11">
				<input id="search" type="text" class="validate" required>
				<label for="search">{$lang['InsertKeyWords']}</label>
			</div>
			<div class="input-field col m2 l1">
				<div class="center">
					<a id="search-button" class="waves-effect waves-light btn-floating btn-large green darken-2"><i class="material-icons left">search</i></a>
				</div>
			</div>
		</div>
	</div>
</div>
