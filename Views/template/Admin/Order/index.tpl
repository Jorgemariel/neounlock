{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}

	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<h5 class="col s10 expand" style="cursor: pointer;">{$lang['Filter']}</h5>
					<h5 class="col s2 right-align expand" style="cursor: pointer;"><i class="material-icons small expandArrow">keyboard_arrow_down</i></h5>
					<div class="input-field col s12 m6 l6" style="display: none;">
						<input id="searchName" name="searchName" type="text" class="validate" style="margin-bottom: 19px;">
						<label for="searchName">{$lang['Search']}</label>
					</div>
					<div class="input-field col s12 m6 l3" style="display: none;">
						<select id="searchStatus" name="searchStatus">
							<option value="" selected>{$lang['All']}</option>
							{foreach from = $status item = s}
							<option value="{$s.id}">{$lang[$s.name]}</option>
							{/foreach}
						</select>
						<label>{$lang['Status']}</label>
					</div>
					<div class="input-field col s6 m4 l3" style="display: none;">
						<select id="searchService" name="searchService">
							<option value="" selected>{$lang['All']}</option>
							{foreach from = $services item = s}
							<option value="{$s.id}">{$s.name}</option>
							{/foreach}
						</select>
						<label>{$lang['Service']}</label>
					</div>
					<div class="input-field col s6 m4 l4" style="display: none;">
						<select id="searchLanguage" name="searchLanguage">
							<option value="" selected>{$lang['All']}</option>
							{foreach from = $languages item = l}
							<option value="{$l.id}">{$l.name}</option>
							{/foreach}
						</select>
						<label>{$lang['Language']}</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchBrand" name="searchBrand">
							<option value="" selected>{$lang['All']}</option>
							{foreach from = $brands item = b}
							<option value="{$b.id}">{$b.name}</option>
							{/foreach}
						</select>
						<label>{$lang['Brand']}</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchModel" name="searchModel">
							<option value="" selected>{$lang['All']}</option>
							{foreach from = $models item = m}
							<option value="{$m.id}">{$m.name}</option>
							{/foreach}
						</select>
						<label>{$lang['Model']}</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchCountry" name="searchCountry">
							<option value="" selected>{$lang['All']}</option>
							{foreach from = $countries item = c}
							<option value="{$c.id}">{$c.name_en}</option>
							{/foreach}
						</select>
						<label>{$lang['Country']}</label>
					</div>
					<div class="input-field col s6 m4 l2" style="display: none;">
						<select id="searchCompany" name="searchCompany">
							<option value="" selected>{$lang['All']}</option>
							{foreach from = $companies item = c}
							<option value="{$c.id}">{$c.name}</option>
							{/foreach}
						</select>
						<label>{$lang['Company']}</label>
					</div>
					<div class="col s12 right-align" style="display: none;"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	{if isset($smarty.get.message)}
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text {$messageColor}">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message">{$message}</h6>
			</div>
		</div>
	</div>
	{/if}

	<div id="list">{include 'Admin/Order/_list.tpl'}</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="{URL}order/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			<li><a href="{URL}order/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
		</ul>
	</div>

	{include 'Admin/Order/_codes.tpl'}
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();
			$('.modal-trigger').leanModal({
				complete: function() {literal}{$('#codesForm').trigger("reset"); }{/literal}
			});

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});
		});

		$('#codesSend').click(function(){
			$('.codesData').hide();
			$('#codesLoading').fadeIn();
			var id = $('#list').find('.active').data('id');
			var formData = $('#codesForm').serialize();
			//console.log(formData);

			$.ajax({
				url: '{URL}order/completed/' + id,
				type: 'POST',
				data: formData,
				cache: false,
				success: function(data) {
					if (data == 1)
					{
						window.location = '{URL}order/?message=codes';
					} else {
						$('#codesLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('.codesData').fadeIn();

					}
				},
				error: function(data) {
					$('#codesLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('.codesData').fadeIn();
				}
			});
			return false;
		});

		var sort = 'desc';
		var orderBy = 'o.id'

		$('#list').on('click', 'a.orderById', function(e){
			orderBy = 'o.id';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByTracking', function(e){
			orderBy = 'tracking';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByService', function(e){
			orderBy = 'service';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'o.id_status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		$('#searchForm').children().change(function(){
			search();
		});

		$('.expand').click(function(){
			expand();
		});

		function expand(){
			if ($('#searchForm').children().not('.expand').is(":visible")) {
				$('#searchForm').children().not('.expand').hide();
				$('#searchForm').css("marginBottom", '-5px');
				$('.expandArrow').text('keyboard_arrow_up');
			} else {
				$('#searchForm').children().not('.expand').show();
				$('#searchForm').css("marginBottom", '-40px');
				$('.expandArrow').text('keyboard_arrow_down');
			}
		};

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var searchName = $('#searchName');
			var searchPosition = $('#searchPosition');
			var searchImage = $('#searchImage');
			var searchStatus = $('#searchStatus');
			var searchModel = $('#searchModel');
			var searchCountry = $('#searchCountry');
			var searchCompany = $('#searchCompany');
			var searchMEP = $('#searchMEP');
			var searchPRD = $('#searchPRD');

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '{URL}order/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	</script>
</body>
</html>
