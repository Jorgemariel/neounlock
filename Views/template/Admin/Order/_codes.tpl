<div id="codes" class="modal">
	<div class="modal-content codesData">
		<h4>{$lang['SendUnlockCodes']}</h4>
		<div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="error-text">{$lang['ErrorOcurred']}</h6>
		</div>
		<form id="codesForm" class="row">
			<div class="input-field col s12 m6">
				<input id="code1" name="code1" type="number" class="validate">
				<label for="code1">{$lang['Code']} 1</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code2" name="code2" type="number" class="">
				<label for="code2">{$lang['Code']} 2</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code3" name="code3" type="number" class="">
				<label for="code3">{$lang['Code']} 3</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code4" name="code4" type="text" class="">
				<label for="code4">{$lang['Code']} 4</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code5" name="code5" type="text" class="">
				<label for="code5">{$lang['Code']} 5</label>
			</div>
			<div class="input-field col s12 m6">
				<input id="code6" name="code6" type="text" class="">
				<label for="code6">{$lang['Code']} 6</label>
			</div>
		</form>
	</div>
	<div class="modal-footer row codesData">
		<a id="codesSend" class="col modal-action waves-effect waves-green btn green" style="margin: 0 10px;">{$lang['Send']}</a>
		<a id="codesClose" class="col modal-action modal-close waves-effect btn-flat modal-close" style="margin: 0 10px;">{$lang['Close']}</a>
	</div>
	<div id="codesLoading" class="center" style="display: none; padding-top: 100px; padding-bottom: 100px">{include 'Public/_loading.tpl'}</div>
</div>
