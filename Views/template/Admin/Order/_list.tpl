{if isset($list) and !empty($list)}
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header" style="padding-left: 0;">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col hide-on-small-only m1 strong center">
				<a href="#" class="orderById">ID</a>
			</div>
			<div class="col s4 m2 strong center-align">
				<a href="#" class="hide-on-small-only orderByTracking">{$lang['Tracking']}</a>
				<a href="#" class="hide-on-med-and-up center-icon orderByTracking"><i class="material-icons">code</i> </a>
			</div>
			<div class="col s6 m5 strong">
				<a href="#" class="hide-on-small-only orderByService">{$lang['Service']}</a>
				<a href="#" class="hide-on-med-and-up orderByService center-align"><i class="material-icons">book</i></a>
			</div>
			<div class="col s2 m3 strong right-align">
				<a href="#" class="hide-on-small-only orderByStatus">{$lang['Status']}</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">content_paste</i></a>
			</div>
		</div>
	</li>
	{foreach from = $list item = l}
	<li data-id="{$l.tracking}">
		<div class="collapsible-header" style="padding-left: 0;">
			<div class="row" style="margin: 0;">
				<div class="col hide-on-small-only m1 center">{"%05d"|sprintf:$l.id}</div>
				<div class="col s4 m2 center-align">{$l.tracking}</div>
				<div class="col s6 m5 truncate">{$l.service}</div>
				<div class="col s2 m3 right">
					{if $l.id_status == 1}
					<i class="material-icons yellow-text">timer</i>
					{else if $l.id_status == 2}
					<i class="material-icons orange-text">timer_off</i>
					{else if $l.id_status == 3}
					<i class="material-icons green-text">check</i>
					{else if $l.id_status == 4}
					<i class="material-icons blue-text">done_all</i>
					{else if $l.id_status == 5}
					<i class="material-icons red-text">report_problem</i>
					{else if $l.id_status == 6}
					<i class="material-icons black-text">cancel</i>
					{/if}
					<div class="hide-on-med-and-down">{$lang[$l.status]}</div>
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 5px 20px; margin-bottom: 0;">
				<div class="col s12 m6">
					<h5>{$lang['PhoneData']}</h5>
					{if isset($l.imei)} <strong>IMEI: </strong> {$l.imei} <br> {/if}
					{if isset($l.brand)} <strong>{$lang['Brand']}: </strong> {$l.brand} <br> {/if}
					{if isset($l.model)} <strong>{$lang['Model']}: </strong> {$l.model} <br> {/if}
					{if isset($l.country)} <strong>{$lang['Country']}: </strong> {$l.country} <br> {/if}
					{if isset($l.company)} <strong>{$lang['Company']}: </strong> {$l.company} <br> {/if}
					{if isset($l.mep) and !empty($l.mep)} <strong>MEP: </strong> {$l.mep} <br> {/if}
					{if isset($l.prd) and !empty($l.prd)} <strong>PRD: </strong> {$l.prd} <br> {/if}
				</div>
				<div class="col s12 m6">
					<h5>{$lang['OrderData']}</h5>
					{if isset($l.entry_date)} <strong>{$lang['EntryDate']}: </strong> {$l.entry_date} <br> {/if}
					{if isset($l.tracking)} <strong>{$lang['Tracking']}: </strong> {$l.tracking} <br> {/if}
					{if isset($l.dalivery_date)} <strong>{$lang['DeliveryDate']}: </strong> {$l.dalivery_date} <br> {/if}
					{if isset($l.survey_date)} <strong>{$lang['SurveyDate']}: </strong> {$l.survey_date} <br> {/if}
				</div>
			</div>
			<div class="row" style="padding: 5px 20px; margin-bottom: 0;">
				<div class="col s12 m6">
					<h5>{$lang['ClientData']}</h5>
					{if isset($l.name)} <strong>{$lang['Name']}: </strong> {$l.name} <br> {/if}
					{if isset($l.email)} <strong>{$lang['Email']}: </strong> <a href="mailto:{$l.email}">{$l.email}</a> <br> {/if}
					{if isset($l.ip)} <strong>IP: </strong> {$l.ip} <br> {/if}
					{if isset($l.language)} <strong>{$lang['Language']}: </strong> {$l.language} <br> {/if}
				</div>
				<div class="col s12 m6">
					<h5>{$lang['ServiceData']}</h5>
					{if isset($l.service)} <strong>{$lang['Service']}: </strong> {$l.service} <br> {/if}
					{if isset($l.service_price)} <strong>{$lang['Price']}: </strong>USD {$l.service_price} <br> {/if}
					{if isset($l.service_delay)} <strong>{$lang['Delay']}: </strong> {$l.service_delay} {$lang[$l.delay_type]}<br> {/if}
				</div>
			</div>
			<div class="row" style="padding: 5px 20px; margin-bottom: 0;">
				{if $l.id_status >= 3}
				<div class="col s12 m6">
					<h5>{$lang['PaymentData']}</h5>
					{if isset($l.payment_date)} <strong>{$lang['PaymentDate']}: </strong> {$l.payment_date} <br> {/if}
					{if isset($l.payment_method)} <strong>{$lang['PaymentMethod']}: </strong> {$l.payment_method} <br> {/if}
					{if isset($l.id_mercadopago)} <strong>ID (MP): </strong>{$l.id_mercadopago} <br> {/if}
					{if isset($l.id_paypal)} <strong>ID (PP): </strong> {$l.id_paypal} <br> {/if}
				</div>
				{/if}
				{if $l.id_status >= 4}
				<div class="col s12 m6">
					<h5>{$lang['Codes']}</h5>
					{if isset($l.code1)} <strong>{$lang['Code']} 1: </strong> {$l.code1} <br> {/if}
					{if isset($l.code2)} <strong>{$lang['Code']} 2: </strong> {$l.code2} <br> {/if}
					{if isset($l.code3)} <strong>{$lang['Code']} 3: </strong> {$l.code3} <br> {/if}
					{if isset($l.code4)} <strong>{$lang['Code']} 4: </strong> {$l.code4} <br> {/if}
					{if isset($l.code5)} <strong>{$lang['Code']} 5: </strong> {$l.code5} <br> {/if}
					{if isset($l.code6)} <strong>{$lang['Code']} 6: </strong> {$l.code6} <br> {/if}
				</div>
				{/if}
			</div>
			<div class="row">
				<div class="col s12 m12">
					<div class="right">
						{if $l.id_status == 3 and 37|array_search:$smarty.session.user.privileges}
						<a href="#codes" class="waves-effect waves-teal btn-flat modal-trigger">{$lang['SendCodes']}</a>
						{/if}
						{if 27|array_search:$smarty.session.user.privileges}
						<a href="{URL}order/edit/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Edit']}</a>
						{/if}
					</div>
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
<h5 class="center">{$lang['ResultsFound']}: {count($list)}</h5>
{else}
<h3 class="center">{$lang['NoItems']}</h3>
{/if}