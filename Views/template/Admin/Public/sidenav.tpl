<ul id="slide-out" class="side-nav fixed">
	<div class="userView">
		<div class="background">
			<img src="{URL}Views/img/background.jpg" class="responsive-img">
		</div>
		<a href="{URL}"><img class="responsive-img" src="{URL}Views/img/logo.png"></a>
	</div>
	{if 4|array_search:$smarty.session.user.privileges}
	<li {if !isset($nav)} class="active"{/if}>
		<a class="waves-effect" href="{URL}admin"><i class="material-icons">home</i>Inicio</a>
	</li>
	{/if}

	<li><div class="divider"></div></li>

	{if 1|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='brands'} class="active"{/if}>
		<a class="waves-effect" href="{URL}brand"><i class="material-icons">style</i>{$lang['Brands']}</a>
	</li>
	{/if}

	{if 2|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='models'} class="active"{/if}>
		<a class="waves-effect" href="{URL}model"><i class="material-icons">stay_current_portrait</i>{$lang['Models']}</a>
	</li>
	{/if}

	{if 3|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='countries'} class="active"{/if}>
		<a class="waves-effect" href="{URL}country"><i class="material-icons">explore</i>{$lang['Countries']}</a>
	</li>
	{/if}

	{if 5|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='companies'} class="active"{/if}>
		<a class="waves-effect" href="{URL}company"><i class="material-icons">work</i>{$lang['Companies']}</a>
	</li>
	{/if}

	<li><div class="divider"></div></li>

	{if 6|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='services'} class="active"{/if}>
		<a class="waves-effect" href="{URL}service"><i class="material-icons">book</i>{$lang['Services']}</a>
	</li>
	{/if}

	{if 7|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='providers'} class="active"{/if}>
		<a class="waves-effect" href="{URL}provider"><i class="material-icons">cloud</i>{$lang['Providers']}</a>
	</li>
	{/if}

	{if 8|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='orders'} class="active"{/if}>
		<a class="waves-effect" href="{URL}order"><i class="material-icons">list</i>{$lang['Orders']}</a>
	</li>
	{/if}

	<li><div class="divider"></div></li>

	{if 9|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='emails'} class="active"{/if}>
		<a class="waves-effect" href="{URL}email"><i class="material-icons">mail</i>{$lang['Emails']}</a>
	</li>
	{/if}

	{if 10|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='users'} class="active"{/if}>
		<a class="waves-effect" href="{URL}user"><i class="material-icons">face</i>{$lang['Users']}</a>
	</li>
	{/if}

	{if 11|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='languages'} class="active"{/if}>
		<a class="waves-effect" href="{URL}language"><i class="material-icons">language</i>{$lang['Languages']}</a>
	</li>
	{/if}

	{if 40|array_search:$smarty.session.user.privileges}
	<li {if isset($nav) && $nav=='faqs'} class="active"{/if}>
		<a class="waves-effect" href="{URL}faqs/list"><i class="material-icons">info</i>{$lang['Questions']}</a>
	</li>
	{/if}
</ul>
