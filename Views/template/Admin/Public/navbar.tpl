<nav class="admin responsive-img light-blue lighten-2">
	<div class="nav-wrapper">
		<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
		<a href="#" class="logo">{$title}</a>
		<ul class="right">
			<li>
				<a href="" class="dropdown-button hide-on-small-only" data-beloworigin="true" data-alignment="right" data-constrainwidth="false" data-activates='user'>{$smarty.session.user.username}<img class="circle responsive-img" src="{URL}Views/img/users/{$smarty.session.user.id}.{$smarty.session.user.img}" style="max-width: 40px; margin-bottom: -15px; margin-left: 15px"></a>
				<a href="" class="dropdown-button hide-on-med-and-up" data-beloworigin="true" data-alignment="right" data-constrainwidth="false" data-activates='user'><img class="circle responsive-img" src="{URL}Views/img/users/{$smarty.session.user.id}.{$smarty.session.user.img}" style="max-width: 40px; margin-bottom: -15px; margin-left: 15px"></a>
			</li>
		</ul>
	</div>
</nav>

<ul id='user' class='dropdown-content'>
	<li><a href="" class="waves-effect waves-light">{$lang['Configuration']}</a></li>
	<li><a href="{URL}login/logout" class="waves-effect waves-light">{$lang['LogOut']}</a></li>
</ul>

<main class="admin" style="padding-bottom: 90px;">