{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}

	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
            <div class="input-field col s12 m6">
							<select id="language" name="language" value="">
								{foreach from = $languages item = l}
								<option value="{$l.id}" {if isset($faq.id_language) && $faq.id_language == $l.id}selected{/if}>{$l.name}</option>
								{/foreach}
							</select>
							<label>{$lang['Language']}</label>
						</div>
            <div class="input-field col s12 m6">
							<select id="status" name="status" value="{if isset($faq.status)}{$faq.status}{/if}">
								<option value="1" {if isset($faq.status) && $faq.status == 1}selected{/if}>{$lang['Active']}</option>
								<option value="0" {if isset($faq.status) && $faq.status == 0}selected{/if}>{$lang['Inactive']}</option>
							</select>
							<label>{$lang['Status']}</label>
						</div>
						<div class="input-field col s12">
							<input id="question" name="question" type="text" class="validate" value="{if isset($faq.question)}{$faq.question}{/if}">
							<label for="question" data-error="{$lang['QuestionRequired']}">{$lang['Question']}</label>
						</div>
            <div class="input-field col s12">
              <textarea id="answer" name="answer" class="materialize-textarea">{if isset($faq.answer)}{$faq.answer}{/if}</textarea>
              <label for="answer">{$lang['Answer']}</label>
            </div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}faqs/list" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($faq)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault();

				var language 		= $('#language');
        var status 		= $('#status');
				var question 		= $('#question');
				var answer 		= $('#answer');

				if (!question.val())
				{
					question.addClass('invalid');
					question.focus();
					return false;
				}

        if (!answer.val())
				{
          $('#error').html("{$lang['AnswerRequired']}");
          $('#errorArea').fadeIn();
					answer.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('language', language.val());
        formData.append('status', status.val());
				formData.append('question', question.val());
				formData.append('answer', answer.val());

				$.ajax({
					url: '{URL}faqs/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}faqs/list/?message=added';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			{if isset($faq)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault();

        var language 		= $('#language');
        var status 		= $('#status');
				var question 		= $('#question');
				var answer 		= $('#answer');

				if (!question.val())
				{
					question.addClass('invalid');
					question.focus();
					return false;
				}

				if (!answer.val())
				{
          $('#error').html("{$lang['AnswerRequired']}");
          $('#errorArea').fadeIn();
					answer.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

        formData.append('language', language.val());
        formData.append('status', status.val());
				formData.append('question', question.val());
				formData.append('answer', answer.val());

				$.ajax({
					url: '{URL}faqs/edit/{$faq.id}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}faqs/list/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>
