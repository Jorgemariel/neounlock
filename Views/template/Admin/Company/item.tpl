{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12 m8 l9">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12">
							<input id="name" name="name" type="text" class="validate" value="{if isset($company.name)}{$company.name}{/if}">
							<label for="name" data-error="{$lang['NameRequired']}">{$lang['Company']}</label>
						</div>
						<div class="file-field input-field col s12 m6">
							<div class="btn blue">
								<span>{$lang['Image']}</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<div class="input-field col s12 m6">
							<select id="status" name="status" value="{if isset($company.status)}{$company.status}{/if}">
								<option value="1" {if isset($company.status) && $company.status == 1}selected{/if}>{$lang['Active']}</option>
								<option value="0" {if isset($company.status) && $company.status == 0}selected{/if}>{$lang['Inactive']}</option>
							</select>
							<label>{$lang['Status']}</label>
						</div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}company" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($company)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{if isset($company.img)}{assign var="image" value="Views/img/companies/{$company.id}.{$company.img}"}{/if}
			<div class="col s12 m4 l3">
				<h4 class="center">{$lang['Preview']}</h4>
				<div class="brand card hoverable waves-effect" id="preview" style="min-height: 100px; margin: 10px; min-width: 100%;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{if isset($company.img) && file_exists($image)}{URL}{$image}{/if}"  id="img-prev" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" id="name-prev" style="margin-top: 20px">{if isset($company.name)}{$company.name}{else}{$lang['Company']}{/if}</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Carga la vista previo del nombre de la marca
			$('#name').on('keyup keypress blur change', function(){
				var content = $('#name').val();
				$('#name-prev').html(content);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name 	= $('#name');
				var status 	= $('#status');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('status', status.val());

				$.ajax({
					url: '{URL}company/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}company/?message=added';
						}
						else if (data == 2)
						{
							window.location = '{URL}company/?message=noimage';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			{if isset($company)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name	= $('#name');
				var status 	= $('#status');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#preview').parent().hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('status', status.val());

				$.ajax({
					url: '{URL}company/edit/{$company.id}/?img={$company.img}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}company/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>