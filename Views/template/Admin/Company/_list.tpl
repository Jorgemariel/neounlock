{if isset($list) and !empty($list)}
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m8 l8 strong">
				<a href="#" class="orderByName">{$lang['Companies']}</a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage">{$lang['Image']}</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus">{$lang['Status']}</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	{foreach from = $list item = l}
	{assign var="image" value="Views/img/companies/{$l.id}.{$l.img}"}
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m8 l8">{$l.name}</div>
				<div class="col s2 m2 l2 center" style="max-height: 42px">
					{if file_exists($image)}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
				<div class="col s2 m2 l2">
					{if $l.status}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px 20px 0; margin-bottom: 0;">
				<div class="col s12 m8">
					{if !empty($l['countries'])}
						<strong>{$lang['Countries']}:</strong><br>
						{foreach from = $l['countries'] item = c}
							{$c['name_en']}<br>
						{/foreach}
					{/if}
				</div>
				{if file_exists($image)}
				<div class="col s12 m4 center" style="margin-top: 3%">
					<img src="{URL}{$image}" style="max-height:70px;">
				</div>
				{/if}
				<div class="col s12 right-align" style="margin: 10px 0">
					{if 35|array_search:$smarty.session.user.privileges}
					<a href="{URL}company/countries/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['AdminCountries']}</a>
					{/if}
					{if 22|array_search:$smarty.session.user.privileges}
					<a href="{URL}company/edit/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Edit']}</a>
					{/if}
					{if 23|array_search:$smarty.session.user.privileges}
					<a href="{URL}company/delete/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Delete']}</a>
					{/if}
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
{else}
<h3 class="center">{$lang['NoItems']}</h3>
{/if}