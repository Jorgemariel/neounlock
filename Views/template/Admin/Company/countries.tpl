{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}

	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12" id="errorArea" hidden>
			<div class="card-panel red white-text">
			<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
			</div>
		</div>
		<div class="col s12"><h3>{$companyName}</h3></div>
		<div class="col s12 m6 l6">
			<div class="card-panel">
				<form class="">
					<div class="input-field">
						<select multiple id="countries" name="countries" value="">
							{foreach from = $countries item = l}
							<option value="{$l.id}" {if isset($l.checked) && $l.checked == '1'}selected{/if}>{$l.name_en}</option>
							{/foreach}
						</select>
						<label class="left-align">{$lang['Country']}</label>
					</div>
					<div class="right-align">
						<a href="{URL}company" class="waves-effect waves-light btn-flat">{$lang['Back']}</a>
						<a id="save" class="waves-effect waves-light btn">{$lang['Save']}</a>
					</div>
				</form>
			</div>
		</div>
		<div class="col s12 m6 l6">
			<div class="card-panel">
				<h5>{$lang['Selected']}</h5>
				<ul id="list"></ul>
			</div>
		</div>
	</div>
	
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>
	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function(){
			$('select').material_select();
			loadList();
		});

		$('#countries').change(function(){
			loadList();
		});

		//Muestra el listado de paises seleccionados
		function loadList(){
			var list = '';
			$('#countries :selected').each(function(i, el){
				list += '<li> ' + $(el).text() + '</li> ';
			});
			$('#list').html(list);
		};

		$('#save').click(function(){

			$('form').parent().parent().parent().hide();
			$('#loading').fadeIn();
			var formData = $('#countries').val();

			$.ajax({
					url: '{URL}company/countries/{$id}',
					type: 'POST',
					data:  {literal}{'countries': formData}{/literal},
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}company/?message=countries';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').parent().parent().parent().hide();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
		});
	</script>
</body>
</html>