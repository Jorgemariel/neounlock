{if isset($list) and !empty($list)}
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m6 l6 strong">
				<a href="#" class="orderByName">{$lang['Model']}</a>
			</div>
			<div class="col hide-on-small-only m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByPosition">{$lang['Position']}</a>
				<a href="#" class="hide-on-med-and-up orderByPosition"><i class="material-icons">swap_vert</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage">{$lang['Image']}</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus">{$lang['Status']}</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	{foreach from = $list item = l}
	{assign var="image" value="Views/img/models/{$l.id}.{$l.img}"}
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m6 l6"><strong class="hide-on-small-only">{$l.brand} </strong>{$l.name}</div>
				<div class="col hide-on-small-only m2 l2 center">{$l.position}</div>
				<div class="col s2 m2 l2">
					{if file_exists($image)}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
				<div class="col s2 m2 l2">
					{if $l.status}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12 m4">
					<strong>Position: </strong> {$l.position}
				</div>
				<div class="col s12 m4">
					{if $l.showCountry || $l.showCompany || $l.showPRD || $l.showMEP}
					<strong>Show:</strong>
					<ul>
						{if $l.showCountry}<li>{$lang['Country']}</li>{/if}
						{if $l.showCompany}<li>{$lang['Company']}</li>{/if}
						{if $l.showPRD}<li>PRD</li>{/if}
						{if $l.showMEP}<li>MEP</li>{/if}
					</ul>
					{/if}
				</div>
				{if file_exists($image)}
				<div class="col s12 m4 center" style="margin-top: 3%">
					<img src="{URL}{$image}" style="max-height:70px;">
				</div>
				{/if}
				<div class="col s12 m6">
					{if 16|array_search:$smarty.session.user.privileges}
					<a href="{URL}model/edit/{$l.id}" class="col waves-effect waves-teal btn-flat">{$lang['Edit']}</a>
					{/if}
					{if 17|array_search:$smarty.session.user.privileges}
					<a href="{URL}model/delete/{$l.id}" class=" col waves-effect waves-teal btn-flat">{$lang['Delete']}</a>
					{/if}
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
{else}
<h3 class="center">{$lang['NoItems']}</h3>
{/if}