{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12 m8 l9">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s6">
							<input id="name" name="name" type="text" class="validate" value="{if isset($model.name)}{$model.name}{/if}">
							<label for="name" data-error="{$lang['NameRequired']}">{$lang['Model']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="brand" name="brand" value="">
								{foreach from = $listBrands item = b}
								<option value="{$b.id}" {if isset($model.id_brand) && $model.id_brand == $b.id}selected{/if}>{$b.name}</option>
								{/foreach}
							</select>
							<label>{$lang['Brand']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="status" name="status" value="{if isset($model.status)}{$model.status}{/if}">
								<option value="1" {if isset($model.status) && $model.status == 1}selected{/if}>{$lang['Active']}</option>
								<option value="0" {if isset($model.status) && $model.status == 0}selected{/if}>{$lang['Inactive']}</option>
							</select>
							<label>{$lang['Status']}</label>
						</div>
						<div class="input-field col s12 m6">
							<input id="position" name="position" type="number" class="validate" value="{if isset($model.position)}{$model.position}{/if}">
							<label for="position" data-error="{$lang['PositionRequired']}">{$lang['Position']}</label>
						</div>
						<div class="file-field input-field col s12">
							<div class="btn blue">
								<span>{$lang['Image']}</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<h5 class="col s12" style="margin-top: 20px;">{$lang['CheckNeeded']}</h5>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showCountry" id="showCountry" {if isset($model.showCountry) && $model.showCountry == 1}checked{/if}/>
							<label for="showCountry">{$lang['Country']}</label>
						</p>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showCompany" id="showCompany" {if isset($model.showCompany) && $model.showCompany == 1}checked{/if}/>
							<label for="showCompany">{$lang['Company']}</label>
						</p>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showPRD" id="showPRD" {if isset($model.showPRD) && $model.showPRD == 1}checked{/if}/>
							<label for="showPRD">PRD</label>
						</p>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showMEP" id="showMEP" {if isset($model.showMEP) && $model.showMEP == 1}checked{/if}/>
							<label for="showMEP">MEP</label>
						</p>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}model" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($model)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{if isset($model.img)}{assign var="image" value="Views/img/models/{$model.id}.{$model.img}"}{/if}
			<div class="col s12 m4 l3">
				<h4 class="center">{$lang['Preview']}</h4>
				<div class="model card hoverable waves-effect" id="preview" style="min-height: 100px; margin: 10px; min-width: 100%;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{if isset($model.img) && file_exists($image)}{URL}{$image}{/if}"  id="img-prev" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" id="name-prev" style="margin-top: 20px">{if isset($model.name)}{$model.name}{else}{$lang['Model']}{/if}</h6>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Carga la vista previo del nombre de la marca
			$('#name').on('keyup keypress blur change', function(){
				var content = $('#name').val();
				$('#name-prev').html(content);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var brand 		= $('#brand');
				var status 		= $('#status');
				var position 	= $('#position');
				var showCountry = $('#showCountry');
				var showCompany = $('#showCompany');
				var showPRD 	= $('#showPRD');
				var showMEP 	= $('#showMEP');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!position.val())
				{
					position.addClass('invalid');
					position.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('brand', brand.val());
				formData.append('status', status.val());
				formData.append('position', position.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('showCountry', showCountry.prop('checked'));
				formData.append('showCompany', showCompany.prop('checked'));
				formData.append('showPRD', showPRD.prop('checked'));
				formData.append('showMEP', showMEP.prop('checked'));

				$.ajax({
					url: '{URL}model/add',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}model/?message=added';
						}
						else if (data == 2)
						{
							window.location = '{URL}model/?message=noimage';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});
			{if isset($model)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var brand 		= $('#brand');
				var status 		= $('#status');
				var position 	= $('#position');
				var showModel 	= $('#showModel');
				var showCountry = $('#showCountry');
				var showCompany = $('#showCompany');
				var showPRD 	= $('#showPRD');
				var showMEP 	= $('#showMEP');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!position.val())
				{
					position.addClass('invalid');
					position.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('brand', brand.val());
				formData.append('status', status.val());
				formData.append('position', position.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('showModel', showModel.prop('checked'));
				formData.append('showCountry', showCountry.prop('checked'));
				formData.append('showCompany', showCompany.prop('checked'));
				formData.append('showPRD', showPRD.prop('checked'));
				formData.append('showMEP', showMEP.prop('checked'));

				$.ajax({
					url: '{URL}model/edit/{$model.id}/?img={$model.img}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}model/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>