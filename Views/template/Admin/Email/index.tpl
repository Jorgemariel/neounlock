{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}

	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<div class="input-field col s9 m10 l11">
						<input id="searchName" name="searchName" type="text" class="validate">
						<label for="searchName">{$lang['Search']}</label>
					</div>
					<div class="col s3 m2 l1 center"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	<div class="row messageArea" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel white-text orange">
			<i class="material-icons right messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message">{$lang['OnlyInSpanish']}</h6>
			</div>
		</div>
	</div>

	{if isset($smarty.get.message)}
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text {$messageColor}">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message">{$message}</h6>
			</div>
		</div>
	</div>
	{/if}

	<div id="list">{include 'Admin/Email/_list.tpl'}</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="{URL}email/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			{if 28|array_search:$smarty.session.user.privileges}
			<li><a href="{URL}email/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
			{/if}
		</ul>
	</div>

	{include 'Admin/Email/_content.tpl'}

	{include 'Admin/Public/foot.tpl'}

	<script type="text/javascript" src="{URL}/Views/js/jquery.crp.min.js"></script>
	<script type="text/javascript" src="{URL}/Views/js/jquery.md5.min.js"></script>
	<script type="text/javascript" src="{URL}/Views/js/jquery.base64.min.js"></script>

	<script>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();

			$('.modal-trigger').leanModal({
				complete: function() {literal}{$('#contentForm').trigger("reset"); }{/literal}
			});

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});

			$('.messageClose').click(function(){
				$('.messageArea').fadeOut();
			});
		});

		$('.modal-trigger').click(function() {
			var content = $(this).data('content');

			$('#contentText').val(content);
			$('#contentText').trigger('autoresize');

			var id = $(this).data('id');
			var id_language = $(this).data('language');

			$('#content').data('id', id);
			$('#content').data('language', id_language);
		});

		$('#contentFormSend').click(function() {
			$('.contentData').hide();
			$('#contentFormLoading').fadeIn();

			var idContent = $('#content').data('id') || 0;
			var content = $('#contentText').val();
			var idEmail = $('#list').find('.active').data('id');
			var idLanguage = $('#content').data('language');

			content = encodeURIComponent(content);

			var key = 'neounlock';
			content = $.crp.crypte(content, key);

			$.ajax({
				url: '{URL}email/editContent/' + idEmail,
				type: 'POST',
				data: {literal}{content: content, id_content: idContent, id_language: idLanguage}{/literal},
				cache: false,
				success: function(data) {
					if (data == '1')
					{
						window.location = '{URL}email/?message=success';
					} else {
						$('#contentFormLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('.contentData').fadeIn();

					}
				},
				error: function(data) {
					$('#contentFormLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('.contentData').fadeIn();
				}
			});

			return false;
		});

		$('#contentFormTest').click(function() {
			$('.contentData').hide();
			$('#contentFormLoading').fadeIn();

			var content = $('#contentText').val();
			
			content = encodeURIComponent(content);

			var key = 'neounlock';
			content = $.crp.crypte(content, key);

			$.ajax({
				url: '{URL}email/testContent',
				type: 'POST',
				data: {literal}{content: content}{/literal},
				cache: false,
				success: function(data) {
					if (data == '1')
					{
						$('#contentFormLoading').hide();
						$('#success').fadeIn().delay(5000).slideUp('slow');
						$('.contentData').fadeIn();
					}
					else
					{
						$('#contentFormLoading').hide();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						$('.contentData').fadeIn();

					}
				},
				error: function(data) {
					console.log('error');

					$('#contentFormLoading').hide();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					$('.contentData').fadeIn();
				}
			});

			return false;
		});



		var sort = 'desc';
		var orderBy = 'position'

		$('#list').on('click', 'a.orderByName', function(e){
			orderBy = 'name';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderById', function(e){
			orderBy = 'id';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		// $(document).keypress(function(e) {
		// 	if(e.which == 13) {
		// 		e.preventDefault();
		// 		search();
		// 	}
		// });

		$('#searchForm').children().change(function(){
			search();
		});

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var searchName = $('#searchName');
			var searchPosition = $('#searchPosition');
			var searchImage = $('#searchImage');
			var searchStatus = $('#searchStatus');
			var searchModel = $('#searchModel');
			var searchCountry = $('#searchCountry');
			var searchCompany = $('#searchCompany');
			var searchMEP = $('#searchMEP');
			var searchPRD = $('#searchPRD');

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '{URL}email/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	</script>
</body>
</html>
