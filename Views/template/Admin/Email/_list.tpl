{if isset($list) and !empty($list)}
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s4 m3 l2 strong">
				<a href="#" class="orderByName">{$lang['Name']}</a>
			</div>
			<div class="col s6 m7 l8 strong">
				<a href="#" class="orderByDescription">{$lang['Description']}</a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus">{$lang['Status']}</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	{foreach from = $list item = l}
	<li data-id="{$l.id}">
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s4 m3 l2">{$l.email}</div>
				<div class="col s6 m7 l8">{$l.description}</div>
				<div class="col s2 m2 l2 center">{$l.count}/{count($languages)}</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12" style="margin-bottom: 10px;">
					{if 38|array_search:$smarty.session.user.privileges}
					{foreach from = $l.languages item = $la}
					<a href="#content" class="waves-effect waves-teal btn-flat modal-trigger" data-content="{$la.content|escape}" data-id="{$la.id_content}" data-language="{$la.id_language}">
						{$la.language}
						{if ($la.content != '' || $la.content != null)}
						<i class="material-icons green-text" style="display: inline-flex; vertical-align: middle; margin-bottom: 5px;">check</i>
						{else}
						<i class="material-icons red-text" style="display: inline-flex; vertical-align: middle; margin-bottom: 5px;">close</i> {/if}
					</a>
					{/foreach}
					{/if}
				</div>
				<div class="col s12">
					<div class="right">
						{if 29|array_search:$smarty.session.user.privileges}
						<a href="{URL}email/edit/{$l.id}" class="col waves-effect waves-teal btn-flat">{$lang['Edit']}</a>
						{/if}
						{if 30|array_search:$smarty.session.user.privileges}
						<a href="{URL}email/delete/{$l.id}" class=" col waves-effect waves-teal btn-flat">{$lang['Delete']}</a>
						{/if}

					</div>
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
{else}
<h3 class="center">{$lang['NoItems']}</h3>
{/if}
