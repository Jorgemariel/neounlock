{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}

	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12">
							<input id="name" name="name" type="text" class="validate" value="{if isset($email.name)}{$email.name}{/if}">
							<label for="name" data-error="{$lang['NameRequired']}">{$lang['Name']}</label>
						</div>
						<div class="input-field col s12">
							<input id="description" name="description" type="text" class="validate" value="{if isset($email.name)}{$email.name}{/if}">
							<label for="description" data-error="{$lang['DescriptionRequired']}">{$lang['Description']}</label>
						</div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}email" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($email)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault();

				var name 				= $('#name');
				var description = $('#description');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!description.val())
				{
					description.addClass('invalid');
					description.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('description', description.val());

				$.ajax({
					url: '{URL}email/add',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}email/?message=added';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			{if isset($email)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault();

				var name 				= $('#name');
				var description = $('#description');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!description.val())
				{
					description.addClass('invalid');
					description.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('description', description.val());

				$.ajax({
					url: '{URL}email/edit/{$email.id}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}email/?message=edited';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#loading').hide();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>
