<div id="content" class="modal modal-fixed-footer">
	<div class="modal-content contentData">
		<h4>{$lang['Email']}</h4>
		<div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="error-text">{$lang['ErrorOcurred']}</h6>
		</div>
		<div id="success" class="card-panel green white-text valign-wrapper" style="margin: 0 20px; display: none;">
			<i class="material-icons valign" style="margin-right: 5px">info</i>
			<h6 id="success-text">{$lang['EmailTestSent']}</h6>
		</div>
		<form id="contentForm" class="row">
			<div class="input-field col s12">
				<textarea id="contentText" class="materialize-textarea validate"></textarea>
			</div>
		</form>
	</div>
	<div class="modal-footer contentData">
		{if 39|array_search:$smarty.session.user.privileges}
		<a id="contentFormSend" class="modal-action waves-effect waves-green btn green" style="margin: 0 10px;">{$lang['Update']}</a>
		{/if}
		<a id="contentFormClose" class="modal-action modal-close waves-effect btn-flat" style="margin: 0 10px;">{$lang['Close']}</a>
		{if 39|array_search:$smarty.session.user.privileges}
		<a id="contentFormTest" class="modal-action waves-effect btn-flat" style="margin: 0 10px;">{$lang['Test']}</a>
		{/if}
	</div>
	<div id="contentFormLoading" class="center" style="display: none; padding-top: 200px; padding-bottom: 200px">{include 'Public/_loading.tpl'}</div>
</div>
