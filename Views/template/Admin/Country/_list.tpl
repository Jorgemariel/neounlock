{if isset($list) and !empty($list)}
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m6 l6 strong">
				<a href="#" class="orderByIso3">{$lang['Countries']}</a>
			</div>
			<div class="col hide-on-small-only m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByLanguage">{$lang['Language']}</a>
				<a href="#" class="hide-on-med-and-up orderByLanguage"><i class="material-icons">language</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage">{$lang['Image']}</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByHighlighted">{$lang['Highlighted']}</a>
				<a href="#" class="hide-on-med-and-up orderByHighlighted"><i class="material-icons">star</i></a>
			</div>
		</div>
	</li>
	{foreach from = $list item = l}
	{assign var="image" value="Views/img/countries/{$l.iso2|lower}.png"}
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m6 l6"><strong>{$l.iso3}</strong> {$l.name_en}</div>
				<div class="col hide-on-small-only m2 l2 center">{$l.language}</div>
				<div class="col s2 m2 l2 center" style="max-height: 42px">
					{if file_exists($image)}
					<img src="{URL}{$image}" class="responsive-img" style="max-height: 35px;">
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
				<div class="col s2 m2 l2">
					{if $l.highlighted}
					<i class="material-icons center-icon amber-text text-accent-3">star</i>
					{else}
					<i class="material-icons center-icon">star_border</i>
					{/if}
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px 20px 0; margin-bottom: 0;">
				<div class="col s12 m6">
					<strong>{$lang['Language']}: </strong> {$l.language} <br>
					<strong>English: </strong> {$l.name_en} <br>
					<strong>Español: </strong> {$l.name_es} <br>
					<strong>ISO 2: </strong> {$l.iso2} <br>
					<strong>ISO 3: </strong> {$l.iso3} <br>
					<strong>{$lang['Coin']}: </strong> {$l.coin} <br>
				</div>
				<div class="col s12 m6">
					{if !empty($l['companies'])}
						<strong>{$lang['Companies']}:</strong><br>
						{foreach from = $l['companies'] item = c}
							{$c['name']}<br>
						{/foreach}
					{/if}
				</div>
				<div class="col s12 right-align" style="margin: 10px 0">
					{if 34|array_search:$smarty.session.user.privileges}
					<a href="{URL}country/companies/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['AdminCompanies']}</a>
					{/if}
					{if 19|array_search:$smarty.session.user.privileges}
					<a href="{URL}country/edit/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Edit']}</a>
					{/if}
					{if 20|array_search:$smarty.session.user.privileges}
					<a href="{URL}country/delete/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Delete']}</a>
					{/if}
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
{else}
<h3 class="center">{$lang['NoItems']}</h3>
{/if}