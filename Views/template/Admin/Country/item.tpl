{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12 m6 l6">
							<input id="name_en" name="name_en" type="text" class="validate" value="{if isset($country.name_en)}{$country.name_en}{/if}">
							<label for="name_en" data-error="{$lang['NameRequired']}">{$lang['Country']} (English)</label>
						</div>
						<div class="input-field col s12 m6 l6">
							<input id="name_es" name="name_es" type="text" class="validate" value="{if isset($country.name_es)}{$country.name_es}{/if}">
							<label for="name_es" data-error="{$lang['NameRequired']}">{$lang['Country']} (Español)</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="iso2" name="iso2" type="text" class="validate" value="{if isset($country.iso2)}{$country.iso2}{/if}">
							<label for="iso2" data-error="{$lang['NameRequired']}">ISO (2)</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="iso3" name="iso3" type="text" class="validate" value="{if isset($country.iso3)}{$country.iso3}{/if}">
							<label for="iso3" data-error="{$lang['NameRequired']}">ISO (3)</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="coin" name="coin" type="text" class="validate" value="{if isset($country.coin)}{$country.coin}{/if}">
							<label for="coin" data-error="{$lang['NameRequired']}">{$lang['Coin']}</label>
						</div>
						<div class="file-field input-field col s12 m6">
							<div class="btn blue">
								<span>{$lang['Image']}</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<div class="input-field col s12 m6">
							<select id="language" name="language" value="">
								{foreach from = $languages item = l}
								<option value="{$l.id}" {if isset($country.id_language) && $country.id_language == $l.id}selected{/if}>{$l.name}</option>
								{/foreach}
							</select>
							<label>{$lang['Language']}</label>
						</div>
						<p class="input-field col s12">
							<input type="checkbox" name="highlighted" id="highlighted" {if isset($country.highlighted) && $country.highlighted == 1}checked{/if}/>
							<label for="highlighted">{$lang['Highlighted']}</label>
						</p>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}country" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($country)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name_en		= $('#name_en');
				var name_es		= $('#name_es');
				var iso2 		= $('#iso2');
				var iso3 		= $('#iso3');
				var coin 		= $('#coin');
				var language 	= $('#language');
				var highlighted = $('#highlighted');

				if (!name_en.val())
				{
					name_en.addClass('invalid');
					name_en.focus();
					return false;
				}

				if (!name_es.val())
				{
					name_es.addClass('invalid');
					name_es.focus();
					return false;
				}

				if (!iso2.val())
				{
					iso2.addClass('invalid');
					iso2.focus();
					return false;
				}

				if (!iso3.val())
				{
					iso3.addClass('invalid');
					iso3.focus();
					return false;
				}

				if (!coin.val())
				{
					coin.addClass('invalid');
					coin.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name_en', name_en.val());
				formData.append('name_es', name_es.val());
				formData.append('iso2', iso2.val());
				formData.append('iso3', iso3.val());
				formData.append('coin', coin.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('language', language.val());
				formData.append('highlighted', highlighted.is(':checked'));

				$.ajax({
					url: '{URL}country/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}country/?message=added';
						}
						else if (data == 2)
						{
							window.location = '{URL}country/?message=noimage';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			{if isset($country)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name_en		= $('#name_en');
				var name_es		= $('#name_es');
				var iso2 		= $('#iso2');
				var iso3 		= $('#iso3');
				var coin 		= $('#coin');
				var language 	= $('#language');
				var highlighted = $('#highlighted');

				if (!name_en.val())
				{
					name_en.addClass('invalid');
					name_en.focus();
					return false;
				}

				if (!name_es.val())
				{
					name_es.addClass('invalid');
					name_es.focus();
					return false;
				}

				if (!iso2.val())
				{
					iso2.addClass('invalid');
					iso2.focus();
					return false;
				}

				if (!iso3.val())
				{
					iso3.addClass('invalid');
					iso3.focus();
					return false;
				}

				if (!coin.val())
				{
					coin.addClass('invalid');
					coin.focus();
					return false;
				}

				$('#errorArea').hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name_en', name_en.val());
				formData.append('name_es', name_es.val());
				formData.append('iso2', iso2.val());
				formData.append('iso3', iso3.val());
				formData.append('coin', coin.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('language', language.val());
				formData.append('highlighted', highlighted.is(':checked'));

				$.ajax({
					url: '{URL}country/edit/{$country.id}/?img={$country.img}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}country/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>