{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}

	<div class="row" style="margin: 20px 30px 0;">
		<div class="col s12">
			<div class="card-panel">
				<form class="row" id="searchForm" style="margin-bottom: -5px">
					<h5 class="col s10 expand" style="cursor: pointer;">{$lang['Filter']}</h5>
					<h5 class="col s2 right-align expand" style="cursor: pointer;"><i class="material-icons small expandArrow">keyboard_arrow_down</i></h5>
					<div class="input-field col s12 m6 l3" style="display: none;">
						<input id="searchName" name="searchName" type="text" class="validate">
						<label for="searchName">{$lang['User']}</label>
					</div>
					<div class="input-field col s8 m4 l2" style="display: none;">
						<input id="searchPosition" name="searchPosition" type="number" class="validate">
						<label for="searchPosition">{$lang['Position']}</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchImage" name="searchImage">
							<option value="" selected>{$lang['All']}</option>
							<option value="1">{$lang['Yes']}</option>
							<option value="0">{$lang['No']}</option>
						</select>
						<label>{$lang['Image']}</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchStatus" name="searchStatus">
							<option value="" selected>{$lang['All']}</option>
							<option value="1">{$lang['Yes']}</option>
							<option value="0">{$lang['No']}</option>
						</select>
						<label>{$lang['Status']}</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchModel" name="searchModel">
							<option value="" selected>{$lang['All']}</option>
							<option value="1">{$lang['Yes']}</option>
							<option value="0">{$lang['No']}</option>
						</select>
						<label>{$lang['Model']}</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchCountry" name="searchCountry">
							<option value="" selected>{$lang['All']}</option>
							<option value="1">{$lang['Yes']}</option>
							<option value="0">{$lang['No']}</option>
						</select>
						<label>{$lang['Country']}</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchCompany" name="searchCompany">
							<option value="" selected>{$lang['All']}</option>
							<option value="1">{$lang['Yes']}</option>
							<option value="0">{$lang['No']}</option>
						</select>
						<label>{$lang['Company']}</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchPRD" name="searchPRD">
							<option value="" selected>{$lang['All']}</option>
							<option value="1">{$lang['Yes']}</option>
							<option value="0">{$lang['No']}</option>
						</select>
						<label>PRD</label>
					</div>
					<div class="input-field col s4 m2 l1" style="display: none;">
						<select id="searchMEP" name="searchMEP">
							<option value="" selected>{$lang['All']}</option>
							<option value="1">{$lang['Yes']}</option>
							<option value="0">{$lang['No']}</option>
						</select>
						<label>MEP</label>
					</div>
					<div class="col s12 right-align" style="display: none;"><a id="search" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">search</i></a></div>
				</form>
			</div>
		</div>
	</div>

	{if isset($smarty.get.message)}
	<div class="row" style="margin: 20px 30px 0;" id="messageArea">
		<div class="col s12">
			<div class="card-panel white-text {$messageColor}">
			<i class="material-icons right" id="messageClose" style="margin-top: 3px; cursor: pointer;">close</i>
				<h6 id="message">{$message}</h6>
			</div>
		</div>
	</div>
	{/if}
	
	<div id="list">{include 'Admin/User/_list.tpl'}</div>
	<div class="center" id="loading" style="display: none; height: 200px;">
		<div class="preloader-wrapper big active" style="margin-top: 68px">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-action-btn vertical click-to-toggle">
		<a class="btn-floating btn-large red">
			<i class="material-icons">settings</i>
		</a>
		<ul>
			<li><a href="{URL}user/export" class="btn-floating blue"><i class="material-icons">file_download</i></a></li>
			{if 31|array_search:$smarty.session.user.privileges}
			<li><a href="{URL}user/add" class="btn-floating green"><i class="material-icons">add</i></a></li>
			{/if}
		</ul>
	</div>
	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function(){
			$('.collapsible').collapsible();
			$('select').material_select();

			$('#messageClose').click(function(){
				$('#messageArea').fadeOut();
			});
		});

		var sort = 'desc';
		var orderBy = 'position'

		$('#list').on('click', 'a.orderByName', function(e){
			orderBy = 'name';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByPosition', function(e){
			orderBy = 'position';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByImage', function(e){
			orderBy = 'img';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#list').on('click', 'a.orderByStatus', function(e){
			orderBy = 'status';
			if (sort == 'asc') { sort = 'desc'; } else { sort = 'asc'; }
			search();
		});

		$('#search').click(function(e){
			e.preventDefault();
			search();
		});

		$('#searchForm').children().change(function(){
			search();
		});

		$('.expand').click(function(){
			expand();
		});

		function expand(){
			if ($('#searchForm').children().not('.expand').is(":visible")) {
				$('#searchForm').children().not('.expand').hide();
				$('#searchForm').css("marginBottom", '-5px');
				$('.expandArrow').text('keyboard_arrow_up');
			} else {
				$('#searchForm').children().not('.expand').show();
				$('#searchForm').css("marginBottom", '-40px');
				$('.expandArrow').text('keyboard_arrow_down');
			}
		};

		function search(){
			$('#list').hide();
			$('#loading').fadeIn();

			var searchName = $('#searchName');
			var searchPosition = $('#searchPosition');
			var searchImage = $('#searchImage');
			var searchStatus = $('#searchStatus');
			var searchModel = $('#searchModel');
			var searchCountry = $('#searchCountry');
			var searchCompany = $('#searchCompany');
			var searchMEP = $('#searchMEP');
			var searchPRD = $('#searchPRD');

			var formData = $('form').serialize() + '&order=' + orderBy + '&sort=' + sort;

			$.ajax({
				url: '{URL}user/',
				type: 'GET',
				data: formData,
				cache: false,
				success: function(data) {
					$('#loading').hide();
					$('#list').html(data);
					$('#list').fadeIn();
					$('.collapsible').collapsible();
				},
				error: function(data) {
					alert(data);
				}
			});
			return false;
		}
	</script>
</body>
</html>