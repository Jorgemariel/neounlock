{if isset($list) and !empty($list)}
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 m3 strong">
				<a href="#" class="hide-on-small-only orderByName">{$lang['User']}</a>
				<a href="#" class="hide-on-med-and-up orderByName"><i class="material-icons">account_circle</i></a>
			</div>
			<div class="col hide-on-small-only m5 strong">
				<a href="#" class="hide-on-small-only orderByEmail">{$lang['Email']}</a>
				<a href="#" class="hide-on-med-and-up orderByEmail"><i class="material-icons">mail</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByImage">{$lang['Image']}</a>
				<a href="#" class="hide-on-med-and-up orderByImage"><i class="material-icons">image</i></a>
			</div>
			<div class="col s2 m2 l2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus">{$lang['Status']}</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	{foreach from = $list item = l}
	{assign var="image" value="Views/img/users/{$l.id}.{$l.img}"}
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8 m3">{$l.username}</div>
				<div class="col hide-on-small-only m5">{$l.email}</div>
				<div class="col s2 m2 l2">
					{if file_exists($image)}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
				<div class="col s2 m2 l2">
					{if $l.status}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px; margin-bottom: 0;">
				<div class="col s12 m8">
					<h5>{$lang['Privileges']}: </h5>
					<ul>
						{foreach from = $l.privileges item = p}
						<li><strong>{$lang[$p.area]}: </strong>{$lang[$p.privilege]}</li>
						{/foreach}
					</ul>
				</div>
				{if file_exists($image)}
				<div class="col s12 m4 center" style="margin-top: 3%">
					<img src="{URL}{$image}" style="max-height:70px;">
				</div>
				{/if}
				<div class="col s12">
					{if 32|array_search:$smarty.session.user.privileges}
					<a href="{URL}user/edit/{$l.id}" class="col waves-effect waves-teal btn-flat right">{$lang['Edit']}</a>
					{/if}
					{if 33|array_search:$smarty.session.user.privileges}
					<a href="{URL}user/delete/{$l.id}" class=" col waves-effect waves-teal btn-flat right">{$lang['Delete']}</a>
					{/if}
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
{else}
<h3 class="center">{$lang['NoItems']}</h3>
{/if}