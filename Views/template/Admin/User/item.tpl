{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12 m8 l9">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12">
							<input id="username" name="username" type="text" class="validate" value="{if isset($user.username)}{$user.username}{/if}">
							<label for="username" data-error="{$lang['NameRequired']}">{$lang['User']}</label>
						</div>
						<div class="input-field col s12">
							<input id="email" name="email" type="email" class="validate" value="{if isset($user.email)}{$user.email}{/if}">
							<label for="email" data-error="{$lang['EmailRequired']}">{$lang['Email']}</label>
						</div>
						<div class="input-field col s12">
							<input id="password" name="password" type="password" class="validate" {if isset($user.password)}placeholder="{$lang['LeaveEmptyNotChange']}"{/if}>
							<label for="password" data-error="{$lang['PasswordRequired']}">{$lang['Password']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="status" name="status" value="{if isset($user.status)}{$user.status}{/if}">
								<option value="1" {if isset($user.status) && $user.status == 1}selected{/if}>{$lang['Active']}</option>
								<option value="0" {if isset($user.status) && $user.status == 0}selected{/if}>{$lang['Inactive']}</option>
							</select>
							<label>{$lang['Status']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="language" name="language" value="{if isset($user.id_language)}{$user.id_language}{/if}">
								{foreach from = $languages item = l}
								<option value="{$l.id}" {if isset($user.id_language) && $user.id_language == $l.id}selected{/if}>{$l.name}</option>
								{/foreach}
							</select>
							<label>{$lang['Language']}</label>
						</div>
						<div class="file-field input-field col s12">
							<div class="btn blue">
								<span>{$lang['Image']}</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<h5 class="col s12" style="margin-top: 20px;">{$lang['Privileges']}</h5>
						{if isset($user)}
						{foreach from = $user.privileges item = p}
						<p class="input-field col s12 m6 l4">
							<input class="privileges" type="checkbox" name="{$p.id}" id="{$p.id}" {if isset($p.checked) && $p.checked == '1'}checked{/if}/>
							<label for="{$p.id}"><strong>{$lang[$p.area]}: </strong>{$lang[$p.name]}</label>
						</p>
						{/foreach}
						{else}
						{foreach from = $privileges item = p}
						<p class="input-field col s12 m6 l4">
							<input class="privileges" type="checkbox" name="{$p.id}" id="{$p.id}"/>
							<label for="{$p.id}"><strong>{$lang[$p.area]}: </strong>{$lang[$p.name]}</label>
						</p>
						{/foreach}
						{/if}

						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}user" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($user)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{if isset($user.img)}{assign var="image" value="Views/img/users/{$user.id}.{$user.img}"}{/if}
			<div class="col s12 m4 l3">
				<h4 class="center">{$lang['Preview']}</h4>
				<div class="user card hoverable waves-effect" id="preview" style="min-height: 100px; margin: 10px; min-width: 100%;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{if isset($user.img) && file_exists($image)}{URL}{$image}{/if}"  id="img-prev" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" id="name-prev" style="margin-top: 20px">{if isset($user.username)}{$user.username}{else}{$lang['User']}{/if}</h6>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Carga la vista previo del nombre de la marca
			$('#username').on('keyup keypress blur change', function(){
				var content = $('#username').val();
				$('#name-prev').html(content);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var username 	= $('#username');
				var email 		= $('#email');
				var password 	= $('#password');
				var status 		= $('#status');
				var language 	= $('#language');

				if (!username.val())
				{
					username.addClass('invalid');
					username.focus();
					return false;
				}

				if (!email.val())
				{
					email.addClass('invalid');
					email.focus();
					return false;
				}

				if (!password.val())
				{
					password.addClass('invalid');
					password.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('username', username.val());
				formData.append('email', email.val());
				formData.append('password', password.val());
				formData.append('status', status.val());
				formData.append('language', language.val());
				formData.append('img', $('input[type=file]')[0].files[0]);

				{foreach from = $privileges item = p}
				formData.append('privilege_{$p.id}', $('#{$p.id}').is(':checked'));
				{/foreach}

				$.ajax({
					url: '{URL}user/add',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}user/?message=added';
						}
						else if (data == 2)
						{
							window.location = '{URL}user/?message=noimage';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});
			{if isset($user)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var username 	= $('#username');
				var email 		= $('#email');
				var password 	= $('#password');
				var status 		= $('#status');
				var language 	= $('#language');

				if (!username.val())
				{
					username.addClass('invalid');
					username.focus();
					return false;
				}

				if (!email.val())
				{
					email.addClass('invalid');
					email.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('username', username.val());
				formData.append('email', email.val());
				formData.append('password', password.val());
				formData.append('status', status.val());
				formData.append('language', language.val());
				formData.append('img', $('input[type=file]')[0].files[0]);

				{foreach from = $privileges item = p}
				formData.append('privilege_{$p.id}', $('#{$p.id}').is(':checked'));
				{/foreach}

				$.ajax({
					url: '{URL}user/edit/{$user.id}/?img={$user.img}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}user/?message=edited';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('#preview').parent().fadeIn();
							$('form').fadeIn();
							$('#loading').hide();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>