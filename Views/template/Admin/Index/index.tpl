{include 'Admin/Public/head.tpl'}
	<title>Admin - NeoUnlock</title>
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	
	<div style="margin: 20px 30px 0;">
		<div class="row">
			<div class="col s12 m4">
				<div class="card center purple darken-2 white-text">
					<div class="card-title">Ingresos {$smarty.now|date_format:"%b"}</div>
					<div class="card-content">
						<div class="value"><h3>U$D {$profitMonth|string_format:"%d"}</h3></div>
						<div class="compare">
							<span>
								<i class="material-icons">{if $profitMonthCompare > 0}trending_up{else}trending_down{/if}</i> {$profitMonthCompare|string_format:"%d"}%
							</span>
						</div>
					</div>
					<div id="myfirstchart" style="height: 250px;"></div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center purple darken-2 white-text">
					<div class="card-title">Ingresos {$smarty.now|date_format:"%G"}</div>
					<div class="card-content">
						<div class="value"><h3>U$D {$profitYear|string_format:"%d"}</h3></div>
						<div class="compare">
							<span>
								<i class="material-icons">{if $profitYearCompare > 0}trending_up{else}trending_down{/if}</i> {$profitYearCompare|string_format:"%d"}%
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center purple darken-2 white-text">
					<div class="card-title">Ingresos TOTAL</div>
					<div class="card-content">
						<div class="value"><h3>U$D {$profitTotal|string_format:"%d"}</h3></div>
						<div class="compare purple-text text-darken-2">
							<i class="material-icons">trending_up</i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m4">
				<div class="card center teal darken-3 white-text">
					<div class="card-title">Pedidos {$smarty.now|date_format:"%b"}</div>
					<div class="card-content">
						<div class="value"><h3>24</h3></div>
						<div class="compare">
							<i class="material-icons">trending_up</i> 15%
							<br>
							<i class="material-icons">done_all</i> 4 (-12%)
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center teal darken-3 white-text">
					<div class="card-title">Pedidos {$smarty.now|date_format:"%G"}</div>
					<div class="card-content">
						<div class="value"><h3>294</h3></div>
						<div class="compare">
							<i class="material-icons">trending_up</i> 15%
							<br>
							<i class="material-icons">done_all</i> 29 (-12%)
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m4">
				<div class="card center teal darken-3 white-text">
					<div class="card-title">Pedidos TOTAL</div>
					<div class="card-content">
						<div class="value"><h3>627</h3></div>
						<div class="compare">
							<i class="material-icons teal-text text-darken-3">trending_up</i>
							<br>
							<i class="material-icons">done_all</i> 103
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{include 'Admin/Public/foot.tpl'}
	<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

	<script>
	{literal}
		$(document).ready(function(){
			$('.collapsible').collapsible();

		});
		new Morris.Line({
		  // ID of the element in which to draw the chart.
		  element: 'myfirstchart',
		  // Chart data records -- each entry in this array corresponds to a point on
		  // the chart.
		  data: [
		    { year: '2008', value: 20 },
		    { year: '2009', value: 10 },
		    { year: '2010', value: 5 },
		    { year: '2011', value: 5 },
		    { year: '2012', value: 20 }
		  ],
		  // The name of the data record attribute that contains x-values.
		  xkey: 'date',
		  // A list of names of data record attributes that contain y-values.
		  ykeys: ['income'],
		  // Labels for the ykeys -- will be displayed when you hover over the
		  // chart.
		  labels: ['income']
		});
	{/literal}
	</script>
</body>
</html>