{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12 m8 l9">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12">
							<input id="name" name="name" type="text" class="validate" value="{if isset($brand.name)}{$brand.name}{/if}">
							<label for="name" data-error="{$lang['NameRequired']}">{$lang['Brand']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="status" name="status" value="{if isset($brand.status)}{$brand.status}{/if}">
								<option value="1" {if isset($brand.status) && $brand.status == 1}selected{/if}>{$lang['Active']}</option>
								<option value="0" {if isset($brand.status) && $brand.status == 0}selected{/if}>{$lang['Inactive']}</option>
							</select>
							<label>{$lang['Status']}</label>
						</div>
						<div class="input-field col s12 m6">
							<input id="position" name="position" type="number" class="validate" value="{if isset($brand.position)}{$brand.position}{/if}">
							<label for="position" data-error="{$lang['PositionRequired']}">{$lang['Position']}</label>
						</div>
						<div class="file-field input-field col s12">
							<div class="btn blue">
								<span>{$lang['Image']}</span>
								<input type="file" name="img" id="img">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text">
							</div>
						</div>
						<h5 class="col s12" style="margin-top: 20px;">{$lang['CheckNeeded']}</h5>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showModel" id="showModel" {if isset($brand.showModel) && $brand.showModel == 1}checked{/if}/>
							<label for="showModel">{$lang['Model']}</label>
						</p>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showCountry" id="showCountry" {if isset($brand.showCountry) && $brand.showCountry == 1}checked{/if}/>
							<label for="showCountry">{$lang['Country']}</label>
						</p>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showCompany" id="showCompany" {if isset($brand.showCompany) && $brand.showCompany == 1}checked{/if}/>
							<label for="showCompany">{$lang['Company']}</label>
						</p>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showPRD" id="showPRD" {if isset($brand.showPRD) && $brand.showPRD == 1}checked{/if}/>
							<label for="showPRD">PRD</label>
						</p>
						<p class="input-field col s12 m6 l4">
							<input type="checkbox" name="showMEP" id="showMEP" {if isset($brand.showMEP) && $brand.showMEP == 1}checked{/if}/>
							<label for="showMEP">MEP</label>
						</p>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}brand" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($brand)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{if isset($brand.img)}{assign var="image" value="Views/img/brands/{$brand.id}.{$brand.img}"}{/if}
			<div class="col s12 m4 l3">
				<h4 class="center">{$lang['Preview']}</h4>
				<div class="brand card hoverable waves-effect" id="preview" style="min-height: 100px; margin: 10px; min-width: 100%;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{if isset($brand.img) && file_exists($image)}{URL}{$image}{/if}"  id="img-prev" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" id="name-prev" style="margin-top: 20px">{if isset($brand.name)}{$brand.name}{else}{$lang['Brand']}{/if}</h6>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			//Materialize: selector
			$('select').material_select();

			//Carga la vista previa de la imagen
			$('#img').change(function(){
				var reader = new FileReader();
				reader.onload = function (e)
				{
					$('#img-prev').attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
			});

			//Carga la vista previo del nombre de la marca
			$('#name').on('keyup keypress blur change', function(){
				var content = $('#name').val();
				$('#name-prev').html(content);
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var status 		= $('#status');
				var position 	= $('#position');
				var showModel 	= $('#showModel');
				var showCountry = $('#showCountry');
				var showCompany = $('#showCompany');
				var showPRD 	= $('#showPRD');
				var showMEP 	= $('#showMEP');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!position.val())
				{
					position.addClass('invalid');
					position.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('status', status.val());
				formData.append('position', position.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('showModel', showModel.prop('checked'));
				formData.append('showCountry', showCountry.prop('checked'));
				formData.append('showCompany', showCompany.prop('checked'));
				formData.append('showPRD', showPRD.prop('checked'));
				formData.append('showMEP', showMEP.prop('checked'));

				$.ajax({
					url: '{URL}brand/add',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}brand/?message=added';
						}
						else if (data == 2)
						{
							window.location = '{URL}brand/?message=noimage';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});
			{if isset($brand)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var status 		= $('#status');
				var position 	= $('#position');
				var showModel 	= $('#showModel');
				var showCountry = $('#showCountry');
				var showCompany = $('#showCompany');
				var showPRD 	= $('#showPRD');
				var showMEP 	= $('#showMEP');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!position.val())
				{
					position.addClass('invalid');
					position.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('status', status.val());
				formData.append('position', position.val());
				formData.append('img', $('input[type=file]')[0].files[0]);
				formData.append('showModel', showModel.prop('checked'));
				formData.append('showCountry', showCountry.prop('checked'));
				formData.append('showCompany', showCompany.prop('checked'));
				formData.append('showPRD', showPRD.prop('checked'));
				formData.append('showMEP', showMEP.prop('checked'));

				$.ajax({
					url: '{URL}brand/edit/{$brand.id}/?img={$brand.img}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}brand/?message=edited';
						}
						else
						{
							$('#error').html(data);
							$('#errorArea').fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>