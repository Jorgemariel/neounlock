{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	
	<div class="admin-container">
		<div class="row">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<div class="card-panel">
					<form class="row" enctype="multipart/form-data">
						<div class="input-field col s12 m8">
							<input id="name" name="name" type="text" class="validate" value="{if isset($service.name)}{$service.name}{/if}">
							<label for="name" data-error="{$lang['NameRequired']}">{$lang['Service']}</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="price" name="price" type="number" class="validate" value="{if isset($service.price)}{$service.price}{/if}">
							<label for="price" data-error="{$lang['PriceRequired']}">{$lang['Price']}</label>
						</div>
						<div class="input-field col s12 m4">
							<input id="delay" name="delay" type="number" class="validate" value="{if isset($service.delay)}{$service.delay}{/if}">
							<label for="delay" data-error="{$lang['DelayRequired']}">{$lang['Delay']}</label>
						</div>
						<div class="input-field col s12 m4">
							<select id="delayType" name="delayType" value="">
								{foreach from = $delayTypes item = d}
								<option value="{$d.id}" {if isset($service.id_delay_type) && $service.id_delay_type == $d.id}selected{/if}>{$lang[$d.name]}</option>
								{/foreach}
							</select>
							<label>{$lang['DelayType']}</label>
						</div>
						<div class="input-field col s12 m4">
							<select id="status" name="status" value="{if isset($service.status)}{$service.status}{/if}">
								<option value="1" {if isset($service.status) && $service.status == 1}selected{/if}>{$lang['Active']}</option>
								<option value="0" {if isset($service.status) && $service.status == 0}selected{/if}>{$lang['Inactive']}</option>
							</select>
							<label>{$lang['Status']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="brands" name="brands" value="">
								<option value="" disabled selected data-models="[]">{$lang['All']}</option>
								{foreach from = $brands item = l}
								<option value="{$l.id}" data-models='{$l.models|@json_encode}' {if isset($l.checked) && $l.checked == '1'}selected{/if}>{$l.name}</option>
								{/foreach}
							</select>
							<label class="left-align">{$lang['Brands']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="models" name="models" value="">
								{foreach from = $models item = l}
								<option value="{$l.id}" {if isset($l.checked) && $l.checked == '1'}selected{/if}>{$l.name}</option>
								{/foreach}
							</select>
							<label class="left-align">{$lang['Models']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="countries" name="countries" value="">
								<option value="" disabled selected>{$lang['All']}</option>
								{foreach from = $countries item = l}
								<option value="{$l.id}" {if isset($l.checked) && $l.checked == '1'}selected{/if}>{$l.name_en}</option>
								{/foreach}
							</select>
							<label class="left-align">{$lang['Countries']}</label>
						</div>
						<div class="input-field col s12 m6">
							<select multiple id="companies" name="companies" value="">
								<option value="" disabled selected>{$lang['All']}</option>
								{foreach from = $companies item = l}
								<option value="{$l.id}" {if isset($l.checked) && $l.checked == '1'}selected{/if}>{$l.name}</option>
								{/foreach}
							</select>
							<label class="left-align">{$lang['Companies']}</label>
						</div>
						<div class="col s12 right-align" style="margin-top: 40px">
							<a href="{URL}service" class="waves-effect waves-light btn-flat">{$lang['Cancel']}</a>
							{if !isset($service)}
							<a id="save" class="waves-effect waves-light btn blue">{$lang['Save']}</a>
							{else}
							<a id="update" class="waves-effect waves-light btn blue">{$lang['Update']}</a>
							{/if}
						</div>
					</form>
					<div class="center" id="loading" style="display: none; height: 200px;">
						<div class="preloader-wrapper big active" style="margin-top: 68px">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{include 'Admin/Public/foot.tpl'}

	<script>
		$(document).ready(function()
		{
			showModels();

			//Materialize: selector
			$('select').material_select();

			$('#brands').change(function(){
				showModels();
			});

			//Toma el form y lo envia al Controller
			$("#save").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var price 		= $('#price');
				var delay 		= $('#delay');
				var delayType 	= $('#delayType');
				var status 		= $('#status');
				var brands 		= $('#brands');
				var models 		= $('#models');
				var countries 	= $('#countries');
				var companies 	= $('#companies');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!price.val())
				{
					price.addClass('invalid');
					price.focus();
					return false;
				}

				if (!delay.val())
				{
					delay.addClass('invalid');
					delay.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('price', price.val());
				formData.append('delay', delay.val());
				formData.append('delayType', delayType.val());
				formData.append('status', status.val());
				formData.append('brands', JSON.stringify(brands.val()));
				formData.append('models', JSON.stringify(models.val()));
				formData.append('countries', JSON.stringify(countries.val()));
				formData.append('companies', JSON.stringify(companies.val()));

				$.ajax({
					url: '{URL}service/add',
					type: 'POST',
					data: formData,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}service/?message=added';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});

			//Muentra la respuesta de PHP
			$('#errorClose').click(function(){
				$('#errorArea').fadeOut();
			});

			function showModels(){
				$('#models').material_select('destroy');

				var data = '';
				var models = '<option value="" disabled selected>{$lang['All']}</option>';

				$('#brands :selected').each(function(ib, brand){
					data = $(brand).attr('data-models');
					data = JSON.parse(data);
					$.each(data, function(im, model) {
						var selected = '';
						if (data[im].selected == '1') selected = ' selected ';
						models += "<option value=" + data[im].id + ' " ' + selected + ">" + '<strong>' + $(brand).text() + '</strong> ' + data[im].name + "</option>";
					})
				});
				$('#models').html(models);
				$('#models').material_select();
			};

			{if isset($service)}
			//Toma el form y actualiza la marca
			$("#update").click(function(e) {
				e.preventDefault(); 

				var name 		= $('#name');
				var price 		= $('#price');
				var delay 		= $('#delay');
				var delayType 	= $('#delayType');
				var status 		= $('#status');
				var brands 		= $('#brands');
				var models 		= $('#models');
				var countries 	= $('#countries');
				var companies 	= $('#companies');

				if (!name.val())
				{
					name.addClass('invalid');
					name.focus();
					return false;
				}

				if (!price.val())
				{
					price.addClass('invalid');
					price.focus();
					return false;
				}

				if (!delay.val())
				{
					delay.addClass('invalid');
					delay.focus();
					return false;
				}

				$('#errorArea').hide();
				$('#preview').parent().hide();
				$('form').hide();
				$('#loading').fadeIn();

				var formData = new FormData();

				formData.append('name', name.val());
				formData.append('price', price.val());
				formData.append('delay', delay.val());
				formData.append('delayType', delayType.val());
				formData.append('status', status.val());
				formData.append('brands', JSON.stringify(brands.val()));
				formData.append('models', JSON.stringify(models.val()));
				formData.append('countries', JSON.stringify(countries.val()));
				formData.append('companies', JSON.stringify(companies.val()));

				$.ajax({
					url: '{URL}service/edit/{$service.id}',
					type: 'POST',
					data: formData,
					// async: false,
					cache: false,
					contentType: false,  // tell jQuery not to set contentType
					//enctype: 'multipart/form-data',
					processData: false,  // tell jQuery not to process the data
					success: function(data) {
						if (data == 1)
						{
							window.location = '{URL}service/?message=edited';
						}
						else
						{
							$('#loading').hide();
							$('#error').html(data);
							$('#errorArea').fadeIn();
							$('form').fadeIn();
							$('#preview').parent().fadeIn();
						}
					},
					error: function(data) {
						alert(data);
					}
				});
				return false;
			});
			{/if}
		});
	</script>
</body>
</html>