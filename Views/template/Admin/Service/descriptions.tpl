{include 'Admin/Public/head.tpl'}
</head>
<body>
	{include 'Admin/Public/sidenav.tpl'}
	{include 'Admin/Public/navbar.tpl'}
	
	<div class="admin-container">
		<div class="row" id="list">
			<div class="col s12" id="errorArea" hidden>
				<div class="card-panel red white-text">
				<i class="material-icons right" id="errorClose" style="margin-top: 3px; cursor: pointer;">close</i>
					<h6 id="error">Error al ingresar los datos. Intentelo nuevamente.</h6>
				</div>
			</div>
			<div class="col s12">
				<ul class="collapsible popout" data-collapsible="accordion">
					<li class="collapsible-header">
						<div class="row" style="padding:0px;margin:0px;">
							<div class="col s10 strong">
								<a href="#" class="orderByName">{$lang['Language']}</a>
							</div>
							<div class="col s2 strong center">
								<a href="#" class="hide-on-small-only orderByStatus">{$lang['Status']}</a>
								<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
							</div>
						</div>
					</li>
					{foreach from = $descriptions item = l}
					{assign var="image" value="Views/img/countries/{$l.img}.png"}
					<li>
						<div class="collapsible-header">
							<div class="row" style="margin: 0;">
								<div class="col s10">
									{if file_exists($image)}<img src="{URL}{$image}" style="margin-right: 10px;">{/if} {$l.language}
								</div>
								<div class="col s2">
									{if !empty($l.content)}
									<i class="material-icons center-icon green-text">check</i>
									{else}
									<i class="material-icons center-icon red-text">block</i>
									{/if}
								</div>
							</div>
						</div>
						<div class="collapsible-body">
							<textarea class="editor" data-language="{$l.id_lang}" id="a{$l.id_lang}">
								{if !empty($l.content)}
									{$l.content}
								{else}
									{$lang['AddDescription']}
								{/if}
							</textarea>
						</div>
					</li>
					{/foreach}
				</ul>
			</div>
			<div class="col s12 center">
				<a href="{URL}service" class="waves-effect waves-teal btn-flat">{$lang['Cancel']}</a>
				<a id="save" class="waves-effect waves-light btn-large">{$lang['Save']}</a>
			</div>
		</div>
		<div class="center" id="loading" style="display: none; height: 200px;">
			<div class="preloader-wrapper big active" style="margin-top: 68px">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{include 'Admin/Public/foot.tpl'}
	<script src="http://cloud.tinymce.com/stable/tinymce.min.js?apiKey=835mpwvoa9jxi8di3gqker4w8h54myafuvydw171zjztih33"></script>
	<script src="{URL}Views/js/tinymce.js"></script>

	<script>
		$(document).ready(function(){
			//var data = tinymce.get('editor').getContent();
		});

		var data = [];

		$('#save').click(function(){
			data = [];
			$('textarea').each(function(i, el){
				// console.log($(el).attr('id'));
				// console.log(tinymce.get($(el).attr('id')).getContent());
				var desc = '';

				if (tinymce.get($(el).attr('id')).getContent() != '<p>{$lang['AddDescription']}</p>') {
					desc = tinymce.get($(el).attr('id')).getContent();
				}

				data.push({
					id_language: $(el).attr('data-language'),
					content: desc
				});
			});

			$('#list').hide();
			$('#loading').fadeIn();

			$.ajax({
				url: '{URL}service/descriptions/{$id}',
				type: 'POST',
				data: {literal}{data}{/literal},
				success: function(data) {
					if (data == 1)
					{
						window.location = '{URL}service/?message=descriptions';
					}
					else
					{
						$('#loading').hide();
						$('#list').fadeIn();
					}
				},
				error: function(data) {
					alert(data);
				}
			});

			return false;
		});
	</script>
</body>
</html>