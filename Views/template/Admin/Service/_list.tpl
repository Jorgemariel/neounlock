{if isset($list) and !empty($list)}
<ul class="collapsible popout admin-container" data-collapsible="accordion" style="margin-top: 20px;">
	<li class="collapsible-header">
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col s8 strong">
				<a href="#" class="orderByName">{$lang['Services']}</a>
			</div>
			<div class="col s2 strong center">
				<a href="#" class="hide-on-small-only orderByDescription">{$lang['Descriptions']}</a>
				<a href="#" class="hide-on-med-and-up orderByDescription"><i class="material-icons">edit</i></a>
			</div>
			<div class="col s2 strong center">
				<a href="#" class="hide-on-small-only orderByStatus">{$lang['Status']}</a>
				<a href="#" class="hide-on-med-and-up orderByStatus"><i class="material-icons">visibility</i></a>
			</div>
		</div>
	</li>
	{foreach from = $list item = l}
	<li>
		<div class="collapsible-header">
			<div class="row" style="margin: 0;">
				<div class="col s8">{$l.name}</div>
				<div class="col s2">
					{if $l.checkDescriptions}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
				<div class="col s2">
					{if $l.status}
					<i class="material-icons center-icon green-text">check</i>
					{else}
					<i class="material-icons center-icon red-text">block</i>
					{/if}
				</div>
			</div>
		</div>
		<div class="collapsible-body">
			<div class="row" style="padding: 20px 20px 0; margin-bottom: 0;">
				<div class="col s12">
					<strong>{$lang['Price']}: </strong> {$l['price']} U$D<br>
					<strong>{$lang['Delay']}: </strong> {$l['delay']} {$lang[$l['delay_type']]}
				</div>
				<div class="divider col s12" style="margin: 20px 0;"></div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong>{$lang['Brands']}:</strong><br>
					{if !empty($l['brands'])}
						{foreach from = $l['brands'] item = c}
							{$c['name']}<br>
						{/foreach}
					{else}
						{$lang['All']}<br>
					{/if}
				</div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong>{$lang['Models']}:</strong><br>
					{if !empty($l['models'])}
						{foreach from = $l['models'] item = c}
							{$c['name']}<br>
						{/foreach}
					{else}
						{$lang['All']}<br>
					{/if}
				</div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong>{$lang['Countries']}:</strong><br>
					{if !empty($l['countries'])}
						{foreach from = $l['countries'] item = c}
							{$c['name_en']}<br>
						{/foreach}
					{else}
						{$lang['All']}<br>
					{/if}
				</div>
				<div class="col s6 m3" style="margin-bottom: 10px;">
					<strong>{$lang['Companies']}:</strong><br>
					{if !empty($l['companies'])}
						{foreach from = $l['companies'] item = c}
							{$c['name']}<br>
						{/foreach}
					{else}
						{$lang['All']}<br>
					{/if}
				</div>
				<div class="col s12 right-align" style="margin: 10px 0">
					{if 36|array_search:$smarty.session.user.privileges}
					<a href="{URL}service/descriptions/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Descriptions']}</a>
					{/if}
					{if 25|array_search:$smarty.session.user.privileges}
					<a href="{URL}service/edit/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Edit']}</a>
					{/if}
					{if 26|array_search:$smarty.session.user.privileges}
					<a href="{URL}service/delete/{$l.id}" class="waves-effect waves-teal btn-flat">{$lang['Delete']}</a>
					{/if}
				</div>
			</div>
		</div>
	</li>
	{/foreach}
</ul>
{else}
<h3 class="center">{$lang['NoItems']}</h3>
{/if}