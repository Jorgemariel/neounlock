{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="row valign-wrapper title">
		<h4 class="col s12 m10 l10">{$brand['name']} <a href="{URL}" style="font-size: 0.7em;">({$lang['Change']})</a></h4>
		<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
			<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
			<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
			<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
		</div>
	</div>

	<div class="row">
		{if isset($models) and !empty($models)}
		<h5 class="col s12">
			<div class="card-panel white-text lime darken-1 center">
				{$lang['SelectModel']}
			</div>
		</h5>
		{foreach from = $models item = m}
		<div class="col s12 m4 l3">
			<div data-id="{$m.id}" class="model card hoverable waves-effect" style="margin: 15px 5px">
				<div class="card-content" style="text-align: center; padding-bottom: 1px">
					<img src="{URL}Views/img/models/{$m.id}.{$m.img}" style="max-width: 90%">
					<h6 class="blue-text darken-text-1" style="margin-top: 20px">{$m.name}</h6>
				</div>
			</div>
		</div>
		{/foreach}
		{else}
		<div class="col m12 center">
			<h3>{$lang['NoResult']}</h3>
		</div>
		{/if}
	</div>

	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	<script type="text/javascript">
		$(document).ready(function(){

			$('.model').click(function(){
				window.location = '{URL}home/form/{$brand['id']}/' + $(this).data('id');
				return false;
			});
		});
	</script>

</body>
</html>