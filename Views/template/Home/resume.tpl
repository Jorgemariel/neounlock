{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -50px">{$lang['OrderResume']}</h2>
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">{$lang['Order']} <strong>{$order['tracking']}</strong></h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>
			<div class="center">
				<div style="display: inline-flex">
					<h4 style="margin-right: 5px">USD</h4>
					<h1 style="margin-top: 13px">{$order['service_price']}</h1>
				</div>
			</div>

			<div class="row">
				<div class="col l8 m10 s12 offset-l2 offset-m1">
					<ul class="collection">
						<li class="collection-item">
							<div>{$lang['AverageDelay']}<a href="#!" class="secondary-content">FALTA TERMINAR</a></div>
						</li>
						<li class="collection-item">
							<div>{$lang['MaxDelay']}<a href="#!" class="secondary-content">{$order['service_delay']} {$lang[$order['delay_type']]}</a></div>
						</li>
						<li class="collection-item">
							<div>{$lang['Brand']}<a href="#!" class="secondary-content">{$order['brand']}</a></div>
						</li>
						{if isset($order['model']) and !empty($order['model'])}
						<li class="collection-item">
							<div>{$lang['Model']}<a href="#!" class="secondary-content">{$order['model']}</a></div>
						</li>
						{/if}
						{if isset($order['country']) and !empty($order['country'])}
						<li class="collection-item">
							<div>{$lang['Country']}<a href="#!" class="secondary-content">{$order['country']}</a></div>
						</li>
						{/if}
						{if isset($order['company']) and !empty($order['company'])}
						<li class="collection-item">
							<div>{$lang['Company']}<a href="#!" class="secondary-content">{$order['company']}</a></div>
						</li>
						{/if}
						<li class="collection-item">
							<div>IMEI<a href="#!" class="secondary-content">{$order['imei']}</a></div>
						</li>
						{if isset($order['mep']) and !empty($order['mep'])}
						<li class="collection-item">
							<div>MEP<a href="#!" class="secondary-content">{$order['mep']}</a></div>
						</li>
						{/if}
						{if isset($order['prd']) and !empty($order['prd'])}
						<li class="collection-item">
							<div>PRD<a href="#!" class="secondary-content">{$order['prd']}</a></div>
						</li>
						{/if}
						<li class="collection-item">
							<div>{$lang['UnlockType']}<a href="#!" class="secondary-content">FALTA TERMINAR</a></div>
						</li>
					</ul>
				</div>
			</div>

			<div class="right-align">
				<a href="{URL}home/payment/{$order['tracking']}" class="waves-effect waves-light btn-large green">
					<i class="material-icons right">send</i>{$lang['ContinueToPay']}
				</a>
			</div>
		</div>
	</div>

	{include 'Public/_terms.tpl'}
	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			
		});
	</script>
	{/literal}

</body>
</html>