{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	{include 'Home/introduction.tpl'}

	<div class="row valign-wrapper title">
		<h4 class="col s12 m10 l10">{$lang['SelectBrand']}</h4>
		<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
			<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
			<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
			<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
		</div>
	</div>

	<div class="row" style="padding: 0 5px;">
		{if isset($brands)}
		{foreach from = $brands item = b}
		<div class="col s12 m4 l3">
			<div data-id="{$b.id}" data-url="{if $b.showModel == 1}models{else}form{/if}" class="brand card hoverable waves-effect" style="margin: 15px 5px">
				<div class="card-content" style="text-align: center; padding-bottom: 1px">
					<img src="{URL}Views/img/brands/{$b.id}.{$b.img}" style="max-width: 90%">
					<h6 class="blue-text darken-text-1" style="margin-top: 20px">{$b.name}</h6>
				</div>
			</div>
		</div>
		{/foreach}
		{else}
		<div class="col m12 center">
			<h3>{$lang['NoResult']}</h3>
		</div>
		{/if}
	</div>

	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	<script type="text/javascript">
		$(document).ready(function(){
			$('.carousel.carousel-slider').carousel({
				full_width: true,
				indicators: false
			});

			$('.brand').click(function(){
				window.location = '{URL}home/'+ $(this).data('url') + '/' + $(this).data('id');
				return false;
			});
		});
	</script>

</body>
</html>