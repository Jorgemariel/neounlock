{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper">
		<div class="container valign">
			<h2 class="animated fadeInLeft">{$lang['PaymentMethod']}</h2>
		</div>
	</div>

	<div class="container" style="margin-top: 35px">


		<div class="row">
			<div class="col s12 m6 l4">
				<a id="mercadopago" name="MP-Checkout" class="brand card hoverable waves-effect" mp-mode="modal" style="margin: 15px 5px;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px;">
						<img src="{URL}Views/img/payments/mercadopago.png" style="max-width: 90%;">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px;">MercadoPago</h6>
					</div>
				</a>
			</div>
			<!-- <div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{URL}Views/img/payments/rapipago.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">RapiPago Argentina</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{URL}Views/img/payments/pagofacil.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">PagoFacil Argentina</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{URL}Views/img/payments/redcompra.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">RedCompra Chile</h6>
					</div>
				</div>
			</div> -->
			<div class="col s12 m6 l4">
				<a href="{URL}payment/paypal/{$order['tracking']}" target="_blank" class="brand card hoverable waves-effect" style="margin: 15px 5px;">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<!-- <div id="paypal-button"></div> -->
						<img src="{URL}Views/img/payments/paypal.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">PayPal</h6>
					</div>
				</a>
			</div>
			<!-- <div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{URL}Views/img/payments/bitcoin.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">Bitcoin</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{URL}Views/img/payments/payu.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">Pay U</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{URL}Views/img/payments/webpay.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">WebPay Chile</h6>
					</div>
				</div>
			</div>
			<div class="col s12 m6 l4">
				<div class="brand card hoverable waves-effect" style="margin: 15px 5px">
					<div class="card-content" style="text-align: center; padding-bottom: 1px">
						<img src="{URL}Views/img/payments/servipag.png" style="max-width: 90%">
						<h6 class="blue-text darken-text-1" style="margin-top: 20px">ServiPag Chile</h6>
					</div>
				</div>
			</div> -->
		</div>
	</div>
	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}
	<script src="https://secure.mlstatic.com/mptools/render.js"></script>
	<script src="https://www.paypalobjects.com/api/checkout.js"></script>

	<script type="text/javascript">
		{literal}
		$(document).ready(function(){
			$('select').material_select();
		});
		{/literal}

		$('#mercadopago').click(function(){

			$.ajax({
				type: 'POST',
				url: "{URL}payment/mercadopago/{$order['tracking']}",
				success: function(result){
					$MPC.openCheckout ({
						url: result,
						mode: "modal",
						onreturn: function(data) {
							// execute_my_onreturn (Sólo modal)
							console.log(data);
							// FALTA TERMINAR
						}
					});
					// $('#mercadopago').attr("href", result);
				}
			});

		});


	</script>

</body>
</html>
