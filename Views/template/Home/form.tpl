{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px">{$lang['UnlockingForm']}</h2>
			<!-- <p>Completa con tus datos personales y los de tu celular para continuar con la liberación.</p> -->
		</div>
	</div>
	<div class="container">
		<div class="card-panel" style="margin-top: -100px;">
			<div class="row valign-wrapper title">
				<h5 class="col s12 m10 l10">Liberación {$brand['name']}</h5>
				<div class="col hide-on-small-only m2 l2 valign right-align" style="margin-top: 15px">
					<div class="circle-dot z-depth-1" style="background: #438ecb"></div>
					<div class="circle-dot z-depth-1" style="background: #fdc113"></div>
					<div class="circle-dot z-depth-1" style="background: #f68f1e"></div>
				</div>
			</div>

			<div id="error" class="card-panel red white-text valign-wrapper" style="margin: 0 20px; display: none;">
				<i class="material-icons valign" style="margin-right: 5px">info</i>
				<h6 id="error-text">{$lang['ErrorOcurred']}</h6>
			</div>

			<form style="margin: 25px; display: none;">
				<!-- NOMBRE -->
				<div class="row">
					<div class="input-field col s11 m6 l6">
						<i class="material-icons prefix">account_circle</i>
						<input id="name" type="text" class="validate" {if isset($name) and !empty($name)}value="{$name}"{/if}>
						<label for="name" data-error="{$lang['YourNameRequired']}">{$lang['Name']}</label>
					</div>
				</div>

				<!-- EMAIL -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">mail</i>
						<input id="email" type="email" class="validate" {if isset($email) and !empty($email)}value="{$email}"{/if}>
						<label for="email" data-error="{$lang['EmailRequired']}">{$lang['Email']}</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px">{$lang['EmailHelp']}</div>
				</div>

				{if isset($countries) and (
					$brand['showCountry'] or
					$brand['showCompany'] or
					(	isset($model['showCountry']) and $model['showCountry']) or
					(	isset($model['showCompany']) and $model['showCompany'])
				)}
				<!-- PAÍS -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">language</i>
						<select id="countries">
							<option value="" data-companies='[]' {if !isset($id_country) or empty($id_country)}selected{/if}>{$lang['SelectCountry']}</option>
							{foreach from = $countries item = c}
							{assign var="image" value="Views/img/countries/{$c.iso2|lower}.png"}
							<option
								value="{$c.id}"
								data-companies='{$c.companies|@json_encode}'
								class="left"
								{if file_exists($image)} data-icon="{URL}{$image}" {/if}
								{if isset($id_country) and !empty($id_country) and $id_country == $c.id}selected{/if}>{$c.name_en}</option>
							{/foreach}
						</select>
						<label>{$lang['Countries']}</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px">{$lang['CountryHelp']}</div>
				</div>
				{/if}

				{if isset($countries) and (
					$brand['showCompany'] or
					( isset($model['showCompany']) and $model['showCompany'])
				)}
				<!-- EMPRESAS -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">sim_card</i>
						<select id="companies">
							<option value="" selected>{$lang['SelectCompany']}</option>
						</select>
						<label>{$lang['Companies']}</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px">{$lang['CompanyHelp']}</div>

				</div>
				{/if}

				<!-- IMEI -->
				<div class="row">
					<div class="input-field col s12 m6 l6">
						<i class="material-icons prefix">lock</i>
						<input id="imei" class="validate" placeholder="XXXXXXXXXXXXXXX" {if isset($imei) and !empty($imei)}value="{$imei}"{/if}>
						<label for="imei" class="active" data-error="{$lang['imeiRequired']}">IMEI</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px">{$lang['IMEIHelp']}</div>
				</div>

				{if $brand['showMEP'] or (isset($model['showMEP']) and $model['showMEP'])}
				<!-- MEP -->
				<div class="row">
					<div class="input-field col s11 m6 l6">
						<i class="material-icons prefix">edit</i>
						<input id="mep" class="validate" placeholder="MEP-XXXXX-XXX"{if isset($mep) and !empty($mep)}value="{$mep}"{/if}>
						<label for="mep" data-error="{$lang['NameRequired']}" class="active">MEP</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px">{$lang['MEPHelp']}</div>
				</div>
				{/if}

				{if $brand['showPRD'] or (isset($model['showPRD']) and $model['showPRD'])}
				<!-- PRD -->
				<div class="row">
					<div class="input-field col s11 m6 l6">
						<i class="material-icons prefix">edit</i>
						<input id="prd" type="text" class="validate" placeholder="PRD-XXXXX-XXX"{if isset($prd) and !empty($prd)}value="{$prd}"{/if}>
						<label for="prd" data-error="{$lang['NameRequired']}" class="active">PRD</label>
					</div>
					<div class="col s12 m6 l6 card-panel light-blue white-text" style="padding: 15px">{$lang['PRDHelp']}</div>
				</div>
				{/if}

				<div class="row">
					<!-- TERMS & CONDITIONS -->
					<div class="col s12">
						<p class="center">
							<input type="checkbox" id="terms" />
							<label for="terms">{$lang['IAcept']}<a class="modal-trigger" href="#termsmodal">{$lang['Terms']}</a></label>
						</p>
					</div>

					<!-- ACTIONS -->
					<div class="col s12 center" style="margin-top: 10px">
						<a href="{URL}" class="waves-effect waves-teal btn-flat" id="back">{$lang['Cancel']}</a>
						<button class="waves-effect waves-light btn green" id="submit"><i class="material-icons right">send</i>{$lang['Continue']}</button>
					</div>
				</div>
			</form>
			<div id="loading" class="center" style="padding: 30px">
				{include 'Public/_loading.tpl'}
			</div>
		</div>
	</div>

	{include 'Public/_terms.tpl'}
	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}
	<script src="{URL}Views/js/jquery.formatter.js"></script>

	<script type="text/javascript">
		{literal}
		$(document).ready(function(){
			$('select').material_select();

			$('#imei').formatter({ 'pattern': '{{999999999999999}}' });
			$('#mep').formatter({ 'pattern': 'MEP-{{99999}}-{{999}}' });
			$('#prd').formatter({ 'pattern': 'PRD-{{99999}}-{{999}}' });

			$('#loading').hide();
			$('form').fadeIn();
			showCompanies();
		});
		{/literal}

		$('#countries').change(function(){
			showCompanies();
		});

		function showCompanies(){
			{if isset($countries) and (
				$brand['showCompany'] or
				( isset($model['showCompany']) and $model['showCompany'])
			)}
			$('#companies').material_select('destroy');

			var companies = '<option value="" disabled>{$lang['SelectCompany']}</option>';
			var data = $('#countries :selected').attr('data-companies');
			data = JSON.parse(data);
			$.each(data, function(im, company) {
				var selected = '';
				{if isset($id_company) and !empty($id_company)}
				if(data[im].id == {$id_company}) selected = ' selected';
				{/if}
				companies += "<option value=" + data[im].id + selected + ">" + data[im].name + "</option>";
			});
			$('#companies').html(companies);
			$('#companies').material_select();
			{/if}
		};

		$('#submit').click(function(e){
			e.preventDefault();

			var brand = {$brand['id']};
			var model = {if isset($model)}{$model['id']}{else}null{/if};
			var name = $('#name');
			var email = $('#email');
			var country = $('#countries :selected');
			var company = $('#companies :selected');
			var imei = $('#imei');
			var prd = $('#prd');
			var mep = $('#mep');

			if (!validateForm())
			{
				return false;
			}

			$('#error').hide();
			$('form').hide();
			$('#loading').fadeIn();

			var formData = {
				'brand': brand,
				'model': model,
				'name': name.val(),
				'email': email.val(),
				'country': country.val(),
				'company': company.val(),
				'imei': imei.val(),
				'prd': prd.val(),
				'mep': mep.val()
			};

			$.ajax({
				url: '{URL}home/form/{$brand["id"]}',
				type: 'POST',
				data: formData,
				dataType: 'json',
				success: function(data) {
					if (data.response == '0')
					{
						window.location = '{URL}home/noservice';
					}
					else if (data.response == '1')
					{
						window.location = '{URL}home/resume/' + data.tracking;
					}
					else if (data.response == '2')
					{
						window.location = '{URL}home/services';
					}
					else
					{
						$('#loading').hide();
						$('form').fadeIn();
						$('#error-text').html(data);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						var errorDiv =  $("#error").offset().top - 20;
						$(window).scrollTop(errorDiv);
					}
				},
				error: function(data) {
					alert('Error');
					console.log(data);
				}
			});
			return false;
		});

		function Validar(variable){
			if (variable.val() == "") {
			variable.addClass('invalid');
			variable.focus();
			return false;
			}
		}

		{literal}
		function validateEmail($email)
		{
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			return emailReg.test( $email );
		}

		function validateImei(imei)
		{
			if (!/^[0-9]{15}$/.test(imei)) {return false;}

			var sum = 0, factor = 2, checkDigit, multipliedDigit;

			for (var i = 13, li = 0; i >= li; i--) {
				multipliedDigit = parseInt(imei.charAt(i), 10) * factor;
				sum += (multipliedDigit >= 10 ? ((multipliedDigit % 10) + 1) : multipliedDigit);
				(factor === 1 ? factor++ : factor--);
			}
			checkDigit = ((10 - (sum % 10)) % 10);

			return !(checkDigit !== parseInt(imei.charAt(14), 10))
		}
		{/literal}

		function validateForm()
		{
			var brand = {$brand['id']};
			var model = {if isset($model)}{$model['id']}{else}null{/if};
			var name = $('#name');
			var email = $('#email');
			var country = $('#countries :selected');
			var company = $('#companies :selected');
			var imei = $('#imei');
			var prd = $('#prd');
			var mep = $('#mep');

			if (!name.val())
			{
				name.addClass('invalid');
				name.focus();
				return false;
			}
			if (!email.val() || !validateEmail(email.val()))
			{
				email.addClass('invalid');
				email.focus();
				return false;
			}

			if ($('#countries').length)
			{
				if (!country.val())
				{
					$('#error-text').html("{$lang['countryError']}");
					$('#error').fadeIn().delay(5000).slideUp('slow');
					var errorDiv =  $("#error").offset().top - 20;
					$(window).scrollTop(errorDiv);
					return false;
				}
			}

			if ($('#companies').length)
			{
				if (!company.val())
				{
					$('#error-text').html("{$lang['companyError']}");
					$('#error').fadeIn().delay(5000).slideUp('slow');
					var errorDiv =  $("#error").offset().top - 20;
					$(window).scrollTop(errorDiv);
					return false;
				}
			}

			if (!imei.val())
			{
				imei.addClass('invalid');
				imei.focus();
				return false;
			}

			if (!validateImei(imei.val()))
			{
				imei.next().attr('data-error', "{$lang['imeiWrong']}");
				imei.addClass('invalid');
				imei.focus();
				return false;
			}

			if ($('#prd').length)
			{
				if (!prd.val())
				{
					prd.addClass('invalid');
					prd.focus();
					return false;
				}
			}

			if ($('#mep').length)
			{
				if (!mep.val())
				{
					mep.addClass('invalid');
					mep.focus();
					return false;
				}
			}

			if (!$('#terms').is(':checked'))
			{
				$('#error-text').html("{$lang['termsError']}");
				$('#error').fadeIn().delay(5000).slideUp('slow');
				var errorDiv =  $("#error").offset().top - 20;
				$(window).scrollTop(errorDiv);
				return false;
			}

			return true;
		}

	</script>

</body>
</html>
