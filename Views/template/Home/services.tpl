



{include 'Public/head.tpl'}
</head>

<body>
	{include 'Public/nav.tpl'}

	<div class="light-blue white-text valign-wrapper" style="height: 300px;">
		<div class="container valign">
			<h2 style="margin-top: -75px">{$lang['AvailableServices']}</h2>
		</div>
	</div>
	<div class="" style="padding: 15px;">
		<div class="row" style="margin-top: -120px;">
			<div id="error" class="col s12 row" style="display: none;">
				<div class="col s12 m8 offset-m2">
					<div class="card-panel red white-text valign-wrapper">
						<i class="material-icons valign" style="margin-right: 5px">info</i>
						<h6 id="error-text">{$lang['ErrorOcurred']}</h6>
					</div>
				</div>
			</div>
			{if count($services) == 2} <div class="col m2"></div> {/if}
			{foreach from = $services key = i item = s}
			<div class="col s12 m4" style="padding: 0 20px;">
				<div class="card hoverable service" data-id="{$s.id}">
					<div class="{if $i==0}purple{elseif $i==1}cyan{else}teal{/if} waves-effect" style="width: 100%;">
						<div class="card-title white-text">FALTA TERMINAR</div>
						<div class="center white-text">
							<div style="display: inline-flex; margin-top: 10px;">
								<h4 style="margin-right: 5px">USD</h4>
								<h2 style="margin-top: 13px">{$s.price}</h2>
							</div>
						</div>
					</div>
					<div class="card-content">
						<ul class="collection">
							<li class="collection-item">
								<strong>{$lang['AverageDelay']}</strong><br>
								FALTA TERMINAR
							</li>
							<li class="collection-item">
								<strong>{$lang['MaxDelay']}</strong><br>
								{$s.delay} {$lang[$s.delay_type]}
							</li>
						</ul>
					</div>
				</div>
			</div>
			{/foreach}
			<div id="loading" class="col s8 m6 l4 offset-m2 offset-l4" style="display: none;">
				<div class="card-panel center" style="padding: 100px;">{include 'Public/_loading.tpl'}</div>
			</div>
		</div>
	</div>
	{include 'Public/footer.tpl'}
	{include 'Public/foot.tpl'}

	<script>
		$('.service').click(function(){
			$('.service').hide();
			$('#loading').fadeIn();
			var id = $(this).data('id');

			$.ajax({
				url: '{URL}home/services',
				type: 'POST',
				data: {literal}{ id_service : id }{/literal},
				dataType: 'json',
				success: function(data) {
					if (data.response == '1')
					{
						window.location = '{URL}home/resume/' + data.tracking;
					}
					else
					{
						$('#loading').hide();
						$('.service').fadeIn();
						$('#error-text').html(data.response);
						$('#error').fadeIn().delay(5000).slideUp('slow');
						var errorDiv =  $("#error").offset().top - 20;
						$(window).scrollTop(errorDiv);
					}
				},
				error: function(data) {
					alert(data.response);
					$('#loading').hide();
					$('.service').fadeIn();
					$('#error-text').html(data);
					$('#error').fadeIn().delay(5000).slideUp('slow');
					var errorDiv =  $("#error").offset().top - 20;
					$(window).scrollTop(errorDiv);
				}
			});
			return false;
		});
	</script>

</body>
</html>