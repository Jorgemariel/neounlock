<script type="text/javascript" src="http://code.jquery.com/jquery-3.1.1.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>

{* <script type="text/javascript" src="{URL}Views/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="{URL}Views/js/materialize.min.js"></script> *}

<script>
$(document).ready(function(){
	$(".button-collapse").sideNav();
	$(".dropdown-button").dropdown();
	$('.modal-trigger').leanModal();
});

{literal}$('#tracking-go').click(function(){trackingGo(); return false;});{/literal}

$('.lan').click(function(){
	$('.dropdown-button').dropdown('open');
});

$('.lang').click(function(){
	$.ajax({
		url: $(this).data('url'),
		type: 'POST',
		success: function(data) {
			if (data == '1')
			{
				location.reload();
			}
			else
			{
				$('#loading').hide();
				$('form').fadeIn();
				$('#error-text').html(data);
				$('#error').fadeIn().delay(5000).slideUp('slow');
				var errorDiv =  $("#error").offset().top - 20;
				$(window).scrollTop(errorDiv);
			}
		},
		error: function(data) {
			alert(data);
		}
	});
	return false;
});

$('#trackingmodal').click(function(){
	$('#order-code').focus();
});

$(document).keypress(function(e) {
    if(e.which == 13 && $("#order-code").is(":focus")) {
			trackingGo();
			return false;
    }
});

$(document).keyup(function(e) {
    if($("#order-code").is(":focus")) {
			$('#order-code').val($('#order-code').val().toUpperCase());
			return false;
    }
});

function trackingGo() {
	var order = $('#order-code');

	if (!order.val()) {
		order.next().attr('data-error', '{$lang['TrackingCodeValid']}');
		order.addClass('invalid');
		order.focus();
		return false;
	}

	if (order.val().length != 6) {
		order.next().attr('data-error', '{$lang['OrderLength6']}');
		order.addClass('invalid');
		order.focus();
		return false;
	}

	window.location = '{URL}tracking/resume/' + order.val();
}
</script>
