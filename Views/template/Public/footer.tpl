<footer class="page-footer blue darken-1">
	<div class="container">
		<div class="row">
			<div class="col l6 m6 s12">
				<h5 class="white-text">{$lang['Contact']}</h5><br>
				<a href="" class="grey-text text-lighten-3"><img src="{URL}Views/img/facebook.png" class="responsive-img" style="width: 30px; margin-bottom: -10px"> /NeoUnlockOfficial</a><br><br>
				<a href="" class="grey-text text-lighten-3"><img src="{URL}Views/img/whatsapp.png" class="responsive-img" style="width: 30px; margin-bottom: -10px"> +54 9 3515144316</a>
			</div>
			<div class="col l4 m4 offset-l2 offset-m2 s12">
				<h5 class="white-text">{$lang['MoreInfo']}</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="#!">{$lang['Terms']}</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">{$lang['ReportProblem']}</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">{$lang['AboutNeoUnlock']}</a></li>
					<li><a class="grey-text text-lighten-3" href="#!">{$lang['HowWork']}</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright light-blue">
		<div class="container">
			© {$smarty.now|date_format:"%Y"} {$lang['AllRightReserved']} - NeoUnlock
			<a class="grey-text text-lighten-4 right" target="_blank" href="http://www.jorgemariel.com">{$lang['MadeBy']} Jorge Mariel</a>
		</div>
	</div>
</footer>