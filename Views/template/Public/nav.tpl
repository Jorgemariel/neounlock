<!-- Dropdown Structure -->
<ul id="languages" class="dropdown-content">
	{foreach from = $languages item = l}
	<li>
		<a data-url="{URL}home/language/{$l.short_name}" class="grey-text text-darken-3 lang">
			<img src="{URL}Views/img/languages/{$l.img}.png" style="padding-right: 10px;">
			{$l.name}
		</a>
	</li>
	{/foreach}
</ul>

<ul id="languagesMobile" class="dropdown-content">
	{foreach from = $languages item = l}
	<li>
		<a data-url="{URL}home/language/{$l.short_name}" class="grey-text text-darken-3 lang">
			<img src="{URL}Views/img/languages/{$l.img}.png" style="padding-right: 10px;">
			{$l.name}
		</a>
	</li>
	{/foreach}
</ul>
<nav class="blue darken-1">
	<div class="nav-wrapper">
		<a href="{URL}" class="brand-logo">
			<img class="responsive-img" src="{URL}Views/img/logo.png" style="height: 50px; margin: -4px 0 0 10px; vertical-align: middle;">
		</a>
		<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		<a class="dropdown-button hide-on-large-only right" data-beloworigin="true" data-constrainwidth="false" data-activates="languagesMobile" style="cursor: pointer;">
			<img src="{URL}Views/img/languages/{$smarty.session.language|lower}.png" style="margin-right: 5px;">
			{$smarty.session.language|upper}
			<i class="material-icons right" style="margin-left: 0px;">arrow_drop_down</i>
		</a>
		<ul class="right hide-on-med-and-down">
			<li {if isset($nav) && $nav=='home'} class="active"{/if}>
				<a href="{URL}">{$lang['Home']}</a>
			</li>
			<li {if isset($nav) && $nav=='tracking'} class="active"{/if}>
				<a class="modal-trigger" href="#trackingmodal">{$lang['Tracking']}</a>
			</li>
			<li {if isset($nav) && $nav=='faqs'} class="active"{/if}>
				<a href="{URL}faqs">{$lang['FAQs']}</a>
			</li>
			<li {if isset($nav) && $nav=='contact'} class="active"{/if}>
				<a href="{URL}contact">{$lang['Contact']}</a>
			</li>
			<li>
				<a class="dropdown-button" data-beloworigin="true" data-constrainwidth="false" data-activates="languages"><img src="{URL}Views/img/languages/{$smarty.session.language|lower}.png" style="margin-right: 5px;">{$smarty.session.language|upper}<i class="material-icons right" style="margin-left: 0px;">arrow_drop_down</i></a>
			</li>
		</ul>
		<ul class="side-nav" id="mobile-demo">
			<li {if isset($nav) && $nav=='home'} class="active"{/if}>
				<a href="{URL}">{$lang['Home']}</a>
			</li>
			<li {if isset($nav) && $nav=='tracking'} class="active"{/if}>
				<a class="modal-trigger" href="#trackingmodal">{$lang['Tracking']}</a>
			</li>
			<li {if isset($nav) && $nav=='faqs'} class="active"{/if}>
				<a href="{URL}faqs">{$lang['FAQs']}</a>
			</li>
			<li {if isset($nav) && $nav=='contact'} class="active"{/if}>
				<a href="{URL}contact">{$lang['Contact']}</a>
			</li>
		</ul>
	</div>
</nav>
{include 'Public/_tracking.tpl'}