<div id="trackingmodal" class="modal">
	<div class="modal-content">
		<h4>{$lang['TrackingOrder']}</h4>
		<p>{$lang['TrackingOrderHelp']}</p>
		<div class="input-field" style="margin-top: 2rem;">
			<input id="order-code" name="order-code" type="text" class="validate" placeholder="XXXXXX">
			<label for="last_name" data-error="{$lang['TrackingCodeValid']}">{$lang['TrackingCode']}</label>
		</div>
	</div>
	<div class="modal-footer">
		<button id="tracking-go" type="submit" class="modal-action waves-effect btn green darken-2" style="margin: 5px">{$lang['Consult']}</button>
		<a class="modal-action modal-close waves-effect btn-flat modal-close" style="margin: 5px">{$lang['Close']}</a>
	</div>
</div>
