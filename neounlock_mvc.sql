-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 26-04-2018 a las 19:53:58
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `neounlock_mvc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `showModel` tinyint(1) DEFAULT NULL,
  `showCountry` tinyint(1) DEFAULT NULL,
  `showCompany` tinyint(1) DEFAULT NULL,
  `showPRD` tinyint(1) DEFAULT NULL,
  `showMEP` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `brands`
--

INSERT INTO `brands` (`id`, `name`, `img`, `status`, `position`, `showModel`, `showCountry`, `showCompany`, `showPRD`, `showMEP`) VALUES
(9, 'Samsung', 'png', 1, 90, 0, 1, 0, 0, 0),
(10, 'LG', 'png', 1, 91, 0, 0, 0, 0, 0),
(11, 'Nokia', 'png', 0, 11, 1, 1, 1, 0, 1),
(23, 'Blackberry', 'png', 1, 3, 0, 0, 0, 1, 1),
(35, 'Apple', 'png', 1, 1, 1, 0, 0, 0, 0),
(36, 'Motorola', 'png', 1, 2, 0, 0, 0, 0, 0),
(37, 'Sony', 'png', 1, 3, 0, 1, 1, 0, 0),
(38, 'ZTE', 'png', 1, 10, 0, 0, 0, 0, 0),
(33, 'Pantech', 'png', 1, 50, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL,
  `img` varchar(4) COLLATE utf32_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `name`, `img`, `status`) VALUES
(1, 'test company', NULL, 1),
(2, 'test company 2', NULL, 0),
(3, 'afasd', 'png', 1),
(4, 'prueba img', 'png', 0),
(5, 'testing final', 'png', 0),
(6, 'testv2', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies_x_countries`
--

CREATE TABLE `companies_x_countries` (
  `id` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_company` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `companies_x_countries`
--

INSERT INTO `companies_x_countries` (`id`, `id_country`, `id_company`) VALUES
(13, 2, 4),
(52, 4, 4),
(21, 2, 3),
(51, 4, 3),
(50, 4, 2),
(14, 3, 4),
(35, 5, 6),
(34, 5, 2),
(54, 4, 1),
(53, 4, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name_en` varchar(20) COLLATE utf32_unicode_ci NOT NULL,
  `name_es` varchar(20) COLLATE utf32_unicode_ci NOT NULL,
  `img` varchar(4) COLLATE utf32_unicode_ci DEFAULT NULL,
  `iso2` varchar(2) COLLATE utf32_unicode_ci NOT NULL,
  `iso3` varchar(3) COLLATE utf32_unicode_ci NOT NULL,
  `coin` varchar(3) COLLATE utf32_unicode_ci NOT NULL,
  `id_language` int(11) NOT NULL,
  `highlighted` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `name_en`, `name_es`, `img`, `iso2`, `iso3`, `coin`, `id_language`, `highlighted`) VALUES
(2, 'Uruguay', 'Uruguay', '', 'UY', 'URU', 'URS', 1, 0),
(3, 'Chile', 'Chile', '', 'CL', 'CHI', 'CLS', 2, 0),
(5, 'sadf', 'asd', '', 'aa', 'aaa', 'aaa', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `delay_types`
--

CREATE TABLE `delay_types` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `delay_types`
--

INSERT INTO `delay_types` (`id`, `name`) VALUES
(1, 'Minutes'),
(2, 'Hours'),
(3, 'WorkingDays'),
(4, 'Days');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descriptions`
--

CREATE TABLE `descriptions` (
  `id` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `content` text COLLATE utf32_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `descriptions`
--

INSERT INTO `descriptions` (`id`, `id_language`, `id_service`, `content`) VALUES
(51, 1, 30, '<p>hola manola</p>'),
(53, 1, 32, '<p>Agrega la descripci&oacute;n del servicio.sdfgsdf</p>'),
(52, 2, 30, '<p>hola ingles</p>'),
(54, 2, 32, ''),
(55, 1, 34, ''),
(56, 2, 34, '<p>Agrega la descripci&oacute;n del servicio.kdjaskldja</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `emails`
--

INSERT INTO `emails` (`id`, `name`, `description`) VALUES
(1, 'email 1 de prueba', 'description3'),
(2, 'email 2', NULL),
(3, 'email 3', NULL),
(5, 'jose es puto', 'muuuuy puto'),
(6, 'Tutorial Moto', 'Entrega de codigo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails_content`
--

CREATE TABLE `emails_content` (
  `id` int(11) NOT NULL,
  `id_email` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `emails_content`
--

INSERT INTO `emails_content` (`id`, `id_email`, `id_language`, `content`) VALUES
(1, 1, 1, '<p>hola mundo</p>sdf'),
(2, 1, 2, 'sadfas'),
(3, 2, 2, '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n<head>\n	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n	<title>A Simple Responsive HTML Email</title>\n	{literal}\n	<style type=\"text/css\">\n		body {margin: 0; padding: 0; min-width: 100%!important;}\n		img {height: auto;}\n		.content {width: 100%; max-width: 600px;}\n		.header {padding: 40px 30px 20px 30px;}\n		.innerpadding {padding: 30px 30px 30px 30px;}\n		.borderbottom {border-bottom: 1px solid #f2eeed;}\n		.subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}\n		.h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}\n		.h1 {font-size: 33px; line-height: 38px; font-weight: bold;}\n		.h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}\n		.bodycopy {font-size: 16px; line-height: 22px;}\n		.button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}\n		.button a {color: #ffffff; text-decoration: none;}\n		.footer {padding: 20px 30px 15px 30px;}\n		.footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}\n		.footercopy a {color: #ffffff; text-decoration: underline;}\n		@media only screen and (max-width: 550px), screen and (max-device-width: 550px) {\n			body[yahoo] .hide {display: none!important;}\n			body[yahoo] .buttonwrapper {background-color: transparent!important;}\n			body[yahoo] .button {padding: 0px!important;}\n			body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}\n			body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}\n		}\n/*@media only screen and (min-device-width: 601px) {\n.content {width: 600px !important;}\n.col425 {width: 425px!important;}\n.col380 {width: 380px!important;}\n}*/\n	</style>\n	{/literal}\n</head>\n\n<body yahoo bgcolor=\"#f6f8f1\">\n<table width=\"100%\" bgcolor=\"#f6f8f1\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n<tr>\n<td>\n	<!--[if (gte mso 9)|(IE)]>\n	<table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n		<tr>\n			<td>\n	<![endif]-->     \n	<table bgcolor=\"#ffffff\" class=\"content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n		<tr>\n			<td bgcolor=\"#1e88e5\" class=\"header\">\n				<table width=\"70\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">  \n					<tr>\n						<td height=\"70\">\n							<img class=\"\" src=\"{URL}Views/img/logo.png\" width=\"90\" height=\"70\" border=\"0\" alt=\"\" />\n						</td>\n					</tr>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td class=\"innerpadding borderbottom\">\n				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n					<tr>\n						<td class=\"h2\">\n							Has ingresado un nuevo pedido de liberación\n						</td>\n					</tr>\n					<tr>\n						<td class=\"bodycopy\">\n							{$name}, ya falta poco para liberar tu celular.<br>\n\n							<div class=\"button\" style=\"margin: 20px; margin-bottom: 0; padding: 20px; text-align: center; background-color: green; color: white;\">\n								<a href=\"{URL}order/tracking/{$tracking}\">Código de seguimiento: <strong>{$tracking}</strong></a>\n							</div>\n						</td>\n					</tr>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td class=\"innerpadding borderbottom\">\n				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n					<tr>\n						<td class=\"h2\">\n							Datos del pedido\n						</td>\n					</tr>\n					<tr>\n						<td class=\"bodycopy\">\n							{if isset($imei) and !empty($imei)}IMEI: <strong>{$imei}</strong><br>{/if}\n							{if isset($brand) and !empty($brand)}Marca: <strong>{$brand}</strong><br>{/if}\n							{if isset($model) and !empty($model)}Modelo: <strong>{$model}</strong><br>{/if}\n							{if isset($country) and !empty($country)}País: <strong>{$country}</strong><br>{/if}\n							{if isset($company) and !empty($company)}Empresa: <strong>{$company}</strong><br>{/if}\n							{if isset($mep) and !empty($mep)}MEP: <strong>{$mep}</strong><br>{/if}\n							{if isset($prd) and !empty($prd)}PRD: <strong>{$prd}</strong><br>{/if}\n						</td>\n					</tr>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td class=\"innerpadding borderbottom\">\n				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n					<tr>\n						<td class=\"bodycopy\">\n							Recuerda realizar el pago dentro de los primeros 15 días. <br>\n							NeoUnlock no cambiará el precio del servicio en este período solo para ti. <br>\n							Si ya realizaste el pago, ignora este mensaje.\n						</td>\n					</tr>\n					<tr>\n						<td style=\"padding: 20px 0 0 0; text-align: center;\">\n							<table class=\"buttonwrapper\" bgcolor=\"#1565c0\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n								<tr>\n									<td class=\"button\" height=\"45\">\n										<a href=\"{URL}home/payment/{$tracking}\">Pagar ahora</a>\n									</td>\n								</tr>\n							</table>\n						</td>\n					</tr>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td class=\"innerpadding bodycopy\">\n				En NeoUnlock brindamos un servicio de liberación garantizado.\n			</td>\n		</tr>\n		<tr>\n			<td class=\"footer\" bgcolor=\"#1e88e5\">\n				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n					<tr>\n						<td align=\"center\" class=\"footercopy\">\n							NeoUnlock - Liberación de celulares<br/>\n							<span class=\"hide\">{\"%Y\"|strftime}</span>\n						</td>\n					</tr>\n					<tr>\n						<td align=\"center\" style=\"padding: 20px 0 0 0;\">\n							<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n								<tr>\n									<td width=\"37\" style=\"text-align: center; padding: 0 10px 0 10px;\">\n										<a href=\"http://www.facebook.com/neounlockofficial\">\n											<img src=\"{URL}Views/img/facebook.png\" width=\"37\" height=\"37\" alt=\"Facebook\" border=\"0\" />\n										</a>\n									</td>\n									<td width=\"37\" style=\"text-align: center; padding: 0 10px 0 10px;\">\n										<a href=\"tel: +5493515144316\">\n											<img src=\"{URL}Views/img/whatsapp.png\" width=\"37\" height=\"37\" alt=\"Whatsapp\" border=\"0\" />\n										</a>\n									</td>\n								</tr>\n							</table>\n						</td>\n					</tr>\n				</table>\n			</td>\n		</tr>\n	</table>\n			<!--[if (gte mso 9)|(IE)]>\n      		</td>\n    	</tr>\n	</table>\n	<![endif]-->\n</td>\n</tr>\n</table>\n</body>\n</html>'),
(4, 3, 1, NULL),
(5, 1, 4, 'kfalsjdhfashfdlak'),
(6, 4, 4, 'dfgsd'),
(7, 4, 2, 'ertwe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL,
  `short_name` varchar(3) COLLATE utf32_unicode_ci NOT NULL,
  `img` varchar(4) COLLATE utf32_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `languages`
--

INSERT INTO `languages` (`id`, `name`, `short_name`, `img`) VALUES
(1, 'Español', 'ES', 'es'),
(2, 'English', 'EN', 'en'),
(4, 'Italiano', 'IT', 'it');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `models`
--

CREATE TABLE `models` (
  `id` int(11) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL,
  `img` varchar(4) COLLATE utf32_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `showCountry` tinyint(1) NOT NULL,
  `showCompany` tinyint(1) NOT NULL,
  `showPRD` tinyint(1) NOT NULL,
  `showMEP` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `models`
--

INSERT INTO `models` (`id`, `id_brand`, `name`, `img`, `status`, `position`, `showCountry`, `showCompany`, `showPRD`, `showMEP`) VALUES
(2, 35, 'iPhone 5', '', 1, 2, 1, 0, 0, 1),
(3, 38, 'test img', 'png', 1, 23, 0, 0, 0, 0),
(4, 35, 'iPhone 7', 'png', 0, 1, 1, 1, 0, 0),
(5, 35, 'iPhone X', '', 1, 3, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `tracking` varchar(45) COLLATE utf32_unicode_ci NOT NULL,
  `id_status` int(11) DEFAULT '1',
  `id_language` int(11) DEFAULT '1',
  `imei` bigint(15) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `id_model` int(11) DEFAULT NULL,
  `id_country` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `mep` varchar(45) COLLATE utf32_unicode_ci DEFAULT NULL,
  `prd` varchar(45) COLLATE utf32_unicode_ci DEFAULT NULL,
  `id_service` int(11) NOT NULL,
  `service_price` decimal(5,2) NOT NULL,
  `service_delay` int(11) NOT NULL,
  `service_delay_type` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf32_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf32_unicode_ci NOT NULL,
  `ip` varchar(45) CHARACTER SET utf32 NOT NULL,
  `entry_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_mercadopago` varchar(80) COLLATE utf32_unicode_ci DEFAULT NULL,
  `id_paypal` varchar(80) COLLATE utf32_unicode_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `payment_method` varchar(45) CHARACTER SET utf32 DEFAULT NULL,
  `payment_data` varchar(1000) CHARACTER SET utf32 DEFAULT NULL,
  `code1` int(11) DEFAULT NULL,
  `code2` int(11) DEFAULT NULL,
  `code3` int(11) DEFAULT NULL,
  `code4` varchar(45) COLLATE utf32_unicode_ci DEFAULT NULL,
  `code5` varchar(45) COLLATE utf32_unicode_ci DEFAULT NULL,
  `code6` varchar(45) COLLATE utf32_unicode_ci DEFAULT NULL,
  `survey_score` int(1) DEFAULT NULL,
  `survey_coment` mediumtext CHARACTER SET utf32,
  `survey_date` datetime DEFAULT NULL,
  `id_survey_refered` int(11) DEFAULT NULL,
  `surveyReferedOther` varchar(45) COLLATE utf32_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `tracking`, `id_status`, `id_language`, `imei`, `id_brand`, `id_model`, `id_country`, `id_company`, `mep`, `prd`, `id_service`, `service_price`, `service_delay`, `service_delay_type`, `name`, `email`, `ip`, `entry_date`, `id_mercadopago`, `id_paypal`, `payment_date`, `delivery_date`, `payment_method`, `payment_data`, `code1`, `code2`, `code3`, `code4`, `code5`, `code6`, `survey_score`, `survey_coment`, `survey_date`, `id_survey_refered`, `surveyReferedOther`) VALUES
(1, 'AAAAAA', 1, 1, 352858067901632, 1, 1, 1, 1, NULL, NULL, 1, '1.00', 1, 1, '1', '1', '3245', '2017-03-23 16:38:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'AAAAAb', 2, 2, 222, 2, 2, 2, 2, NULL, NULL, 2, '2.00', 2, 2, '2', '2', '23452', '2017-03-23 17:00:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'JNV4KB', 3, 1, 352858067901632, 36, NULL, NULL, NULL, 'null', 'null', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-23 22:58:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '4QX089', 3, 1, 352858067901632, 36, NULL, NULL, NULL, 'null', 'null', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-23 22:58:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '4Q0089', 3, 1, 352858067901632, 36, NULL, NULL, NULL, 'null', 'null', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-23 22:58:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'L2XY2R', 4, 1, 352858067901632, 36, NULL, NULL, NULL, 'null', 'null', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-23 22:59:23', NULL, NULL, NULL, NULL, NULL, NULL, 123, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'HP92KV', 3, 1, 352858067901632, 36, NULL, NULL, NULL, 'null', 'null', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 00:00:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'NXFA03', 3, 1, 352858067901632, 36, NULL, NULL, NULL, NULL, NULL, 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 00:01:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'VO2OXW', 4, 1, 352858067901632, 9, NULL, 3, NULL, NULL, NULL, 34, '30.00', 3, 3, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 00:08:36', NULL, NULL, NULL, NULL, NULL, NULL, 12341234, 1234123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'R0IHTO', 3, 1, 352858067901632, 35, 4, 4, 1, NULL, NULL, 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 00:09:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'TWCX7E', 2, 1, 352858067901632, 35, 2, 4, NULL, NULL, NULL, 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '186.153.226.220', '2017-03-24 01:07:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'TWCT7E', 3, 1, 352858067901632, 35, 2, 4, NULL, 'MEP-34523-452', NULL, 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '186.153.226.220', '2017-03-24 01:07:40', '179184830-fb8f0d15-74a3-4c52-9267-4caabcd4e046', '', NULL, NULL, 'MercadoPago', NULL, 23476895, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'FBKWUX', 4, 1, 352858067901632, 35, 2, 3, NULL, 'MEP-23452-345', '', 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 01:09:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'K366S1', 5, 1, 352858067901632, 35, 2, 3, NULL, 'MEP-23452-345', '', 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 01:09:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'D83P5S', 6, 1, 352858067901632, 35, 2, 3, NULL, 'MEP-23452-345', '', 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 01:09:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'U0VD8D', 1, 1, 352858067901632, 35, 2, 3, NULL, 'MEP-23452-345', '', 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 13:21:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'QFI7Y7', 1, 1, 352858067901632, 35, 2, 4, NULL, 'MEP-23532-452', '', 31, '11.00', 11, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 13:21:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'E6TFYS', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 13:22:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'QJOAO7', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 13:22:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'PQPGY7', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 13:43:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '5DMVQ0', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 13:44:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'QIGLSE', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 13:44:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'HV5O72', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 19:30:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'M4QIY5', 1, 1, 352858067901632, 9, NULL, 4, NULL, '', '', 34, '30.00', 3, 3, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 20:04:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'CD27OT', 1, 1, 352858067901632, 9, NULL, 4, NULL, '', '', 30, '202.00', 101, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 20:11:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'G7OFD4', 1, 1, 352858067901632, 9, NULL, 4, NULL, '', '', 30, '202.00', 101, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 20:11:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '5T612M', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 20:12:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'S0ZCS3', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-24 20:26:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'K1JK2F', 1, 1, 352858067901632, 9, NULL, 4, NULL, '', '', 30, '202.00', 101, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-25 01:26:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '4QBDOI', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-25 02:06:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '4I1VXH', 1, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-26 03:01:38', '179184830-8d6ec779-0bc9-45a5-9a21-86ca76cf540e', NULL, NULL, NULL, NULL, NULL, 12222, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'S31V92', 4, 1, 352858067901632, 9, NULL, 3, NULL, '', '', 34, '30.00', 3, 3, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-26 16:18:49', NULL, NULL, NULL, NULL, NULL, NULL, 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'ZHAMJG', 4, 1, 352858067901632, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-29 14:52:02', NULL, NULL, NULL, NULL, NULL, NULL, 11111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'KM5QSH', 4, 1, 352858067901632, 9, NULL, 4, NULL, '', '', 30, '202.00', 101, 1, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-03-29 14:53:31', NULL, NULL, NULL, NULL, NULL, NULL, 11231, 2, 3, '4', '5', '6', NULL, NULL, NULL, NULL, NULL),
(35, 'VXZRXN', 1, 1, 352858067901632, 9, NULL, 4, NULL, '', '', 34, '30.00', 3, 3, 'Fernanda Abarca', 'fernanda.abarca.cortes@gmail.com', '::1', '2017-04-03 09:43:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '7IKUNN', 1, 1, 352858067901632, 37, NULL, 4, 1, '', '', 35, '1.00', 12, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-04-04 21:22:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'R2EH3W', 1, 1, 352858067901632, 37, NULL, 4, 6, '', '', 35, '1.00', 12, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '::1', '2017-04-04 21:24:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'RM2HZJ', 1, 1, 0, 9, NULL, 3, NULL, '', '', 34, '30.00', 3, 3, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '186.125.118.220', '2017-07-01 17:01:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'T07YZ9', 1, 1, 0, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Jorge Mariel', 'jorgemariel.s@gmail.com', '190.139.35.237', '2017-09-08 01:45:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'F4MNMM', 1, 1, 0, 36, NULL, NULL, NULL, '', '', 33, '15.00', 24, 2, 'Genesis', 'gedainegu@gmail.com', '186.93.183.94', '2017-09-08 01:45:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privileges`
--

CREATE TABLE `privileges` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf32_unicode_ci NOT NULL,
  `id_area` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `privileges`
--

INSERT INTO `privileges` (`id`, `name`, `id_area`) VALUES
(1, 'ViewBrands', 1),
(2, 'ViewModels', 2),
(3, 'ViewCountries', 3),
(4, 'ViewHome', 4),
(5, 'ViewCompanies', 5),
(6, 'ViewServices', 6),
(7, 'ViewProviders', 7),
(8, 'ViewOrders', 8),
(9, 'ViewEmails', 9),
(10, 'ViewUsers', 10),
(11, 'ViewLanguages', 11),
(12, 'AddBrand', 1),
(13, 'EditBrand', 1),
(14, 'DeleteBrand', 1),
(15, 'AddModel', 2),
(16, 'EditModel', 2),
(17, 'DeleteModel', 2),
(18, 'AddCountry', 3),
(19, 'EditCountry', 3),
(20, 'DeleteCountry', 3),
(21, 'AddCompany', 4),
(22, 'EditCompany', 4),
(23, 'DeleteCompany', 4),
(24, 'AddService', 6),
(25, 'EditService', 6),
(26, 'DeleteService', 6),
(27, 'EditOrder', 8),
(28, 'AddEmail', 9),
(29, 'EditEmail', 9),
(30, 'DeleteEmail', 9),
(31, 'AddUser', 10),
(32, 'EditUser', 10),
(33, 'DeleteUser', 10),
(34, 'SelectCompanies', 3),
(35, 'SelectCountries', 4),
(36, 'ServiceDescriptions', 6),
(37, 'CompleteOrder', 8),
(38, 'ViewEmailContent', 9),
(39, 'EditEmailContent', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilege_areas`
--

CREATE TABLE `privilege_areas` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf32_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `privilege_areas`
--

INSERT INTO `privilege_areas` (`id`, `name`) VALUES
(1, 'Brands'),
(2, 'Models'),
(3, 'Countries'),
(4, 'Home'),
(5, 'Companies'),
(6, 'Services'),
(7, 'Providers'),
(8, 'Orders'),
(9, 'Emails'),
(10, 'Users'),
(11, 'Languages'),
(12, 'Faqs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `delay` int(4) NOT NULL,
  `id_delay_type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id`, `name`, `status`, `price`, `delay`, `id_delay_type`) VALUES
(34, 'Samsung All', 1, '30', 3, 3),
(35, 'sony', 1, '1', 12, 2),
(32, 'testing zte chile', 1, '12', 22, 3),
(33, 'Motorola All', 1, '15', 24, 2),
(31, 'testing', 1, '11', 11, 1),
(30, 'Samsung Argentina 2', 1, '202', 101, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_x_brands`
--

CREATE TABLE `services_x_brands` (
  `id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_brand` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `services_x_brands`
--

INSERT INTO `services_x_brands` (`id`, `id_service`, `id_brand`) VALUES
(63, 35, 37),
(62, 34, 9),
(59, 33, 36),
(58, 32, 38),
(57, 31, 35),
(61, 30, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_x_companies`
--

CREATE TABLE `services_x_companies` (
  `id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_company` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_x_countries`
--

CREATE TABLE `services_x_countries` (
  `id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_country` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `services_x_countries`
--

INSERT INTO `services_x_countries` (`id`, `id_service`, `id_country`) VALUES
(27, 32, 3),
(28, 30, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_x_emails`
--

CREATE TABLE `services_x_emails` (
  `id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_email` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_x_models`
--

CREATE TABLE `services_x_models` (
  `id` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `id_model` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `services_x_models`
--

INSERT INTO `services_x_models` (`id`, `id_service`, `id_model`) VALUES
(31, 31, 4),
(30, 31, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf32_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Expired'),
(3, 'Paid'),
(4, 'Completed'),
(5, 'Problem'),
(6, 'Cancelled');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(45) COLLATE utf32_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf32_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf32_unicode_ci NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `img` varchar(4) CHARACTER SET utf32 DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `id_type`, `id_language`, `img`, `status`) VALUES
(1, 'jorgemariel', 'jorgemariel.s@gmail.com', '$2y$10$x4gciKx3WzNkH1CCkmfC6uskQWFooOBDaoW8RBJPKOz7iB8zGo83m', 1, 1, 'jpg', 1),
(2, 'fer', 'fernanda.abarca.cortes@gmail.com', '$2y$10$uYPR22nlbQd0wbzhEBPZXe1wigtW46kbYvi/DzOaTngfMnSUOfCMa', 2, 1, 'jpg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_x_privileges`
--

CREATE TABLE `users_x_privileges` (
  `id_user` int(11) NOT NULL,
  `id_privilege` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `users_x_privileges`
--

INSERT INTO `users_x_privileges` (`id_user`, `id_privilege`) VALUES
(0, 1),
(0, 2),
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(2, 1),
(2, 2),
(2, 3),
(5, 1),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(9, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf32_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `companies_x_countries`
--
ALTER TABLE `companies_x_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_country` (`id_country`),
  ADD KEY `fk_company` (`id_company`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_language` (`id_language`);

--
-- Indices de la tabla `delay_types`
--
ALTER TABLE `delay_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UQ_description` (`id_language`,`id_service`),
  ADD KEY `fk_service` (`id_service`);

--
-- Indices de la tabla `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `emails_content`
--
ALTER TABLE `emails_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_email_idx` (`id_email`),
  ADD KEY `fk_language_idx` (`id_language`);

--
-- Indices de la tabla `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_brand` (`id_brand`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tracking_UNIQUE` (`tracking`),
  ADD KEY `id_serviceDelayType_idx` (`service_delay_type`),
  ADD KEY `id_language_idx` (`id_language`),
  ADD KEY `id_status_idx` (`id_status`);

--
-- Indices de la tabla `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_area_idx` (`id_area`);

--
-- Indices de la tabla `privilege_areas`
--
ALTER TABLE `privilege_areas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_delay_type` (`id_delay_type`);

--
-- Indices de la tabla `services_x_brands`
--
ALTER TABLE `services_x_brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_service` (`id_service`),
  ADD KEY `fk_brand` (`id_brand`);

--
-- Indices de la tabla `services_x_companies`
--
ALTER TABLE `services_x_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_service` (`id_service`),
  ADD KEY `fk_company` (`id_company`);

--
-- Indices de la tabla `services_x_countries`
--
ALTER TABLE `services_x_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_service` (`id_service`),
  ADD KEY `fk_country` (`id_country`);

--
-- Indices de la tabla `services_x_emails`
--
ALTER TABLE `services_x_emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_service` (`id_service`),
  ADD KEY `fk_email` (`id_email`);

--
-- Indices de la tabla `services_x_models`
--
ALTER TABLE `services_x_models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_service` (`id_service`),
  ADD KEY `fk_models` (`id_model`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `fk_type_idx` (`id_type`),
  ADD KEY `fk_language_idx` (`id_language`);

--
-- Indices de la tabla `users_x_privileges`
--
ALTER TABLE `users_x_privileges`
  ADD PRIMARY KEY (`id_user`,`id_privilege`),
  ADD KEY `fk_privilege_idx` (`id_privilege`);

--
-- Indices de la tabla `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `companies_x_countries`
--
ALTER TABLE `companies_x_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `delay_types`
--
ALTER TABLE `delay_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `descriptions`
--
ALTER TABLE `descriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `emails_content`
--
ALTER TABLE `emails_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `models`
--
ALTER TABLE `models`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `privilege_areas`
--
ALTER TABLE `privilege_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `services_x_brands`
--
ALTER TABLE `services_x_brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `services_x_companies`
--
ALTER TABLE `services_x_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `services_x_countries`
--
ALTER TABLE `services_x_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `services_x_emails`
--
ALTER TABLE `services_x_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `services_x_models`
--
ALTER TABLE `services_x_models`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
