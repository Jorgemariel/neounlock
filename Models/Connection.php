<?php namespace Models;

	class Connection extends \mysqli
	{
		private $data = array(
			'host' 	=> DB_HOST,
			'user' 	=> DB_USERNAME,
			'pass'	=> DB_PASSWORD,
			'db' 	=> DB_NAME
		);

		public function __construct()
		{
			parent::__construct(
				$this->data['host'], 
				$this->data['user'], 
				$this->data['pass'],
				$this->data['db']
			);
			$this->query("SET NAMES 'utf8'");
		}

		public function simpleQuery($sql)
		{
			$this->query($sql);
		}

		public function returnQuery($sql)
		{
			$data = $this->query($sql);
			return $data;
		}

		public function multiQuery($sql)
		{
			$this->multi_query($sql);
		}

		public function lastInsertId()
		{
			$data = $this->insert_id;
			return $data;
		}
	}

?>