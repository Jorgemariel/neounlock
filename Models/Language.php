<?php namespace Models;

	class Language
	{
		public $lang;

		private $id;
		private $name;
		private $short_name;
		private $img;

		private $db;

		public function __construct()
		{
			//var_dump($_SESSION);
			if (!isset($_SESSION['language']) or empty($_SESSION['language']))
			{
				$_SESSION['language'] = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
			}

			if (file_exists('Lang/'.strtolower($_SESSION['language']).'.php'))
			{
				include_once 'Lang/'.strtolower($_SESSION['language']).'.php';
			}
			else
			{
				include_once 'Lang/es.php';
			}
			$this->lang = $lang;

			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function toList()
		{
			$query = "select * from languages";
			$data = $this->db->returnQuery($query);

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			return $rows;
		}

		public function view()
		{
			$query = "	select
							c.*,
							s.name as 'status',
							l.name as 'language'
						from languages c
							join status s on s.id = c.id_status
							join languages l on l.id = c.id_language
						where id = $this->id";

			$data = $this->db->returnQuery($query);
			// var_dump($query);

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}
			return $rows;
		}

		public function add()
		{
			$query = "insert into languages (name, image, id_status, id_language, position)
					values ('$this->name', '$this->image', $this->id_status, $this->id_language, $this->position)";

			$this->db->simpleQuery($query);
		}

		public function edit()
		{
			$query = "	update languages set
							name = '$this->name',
							image = '$this->image',
							id_status = $this->id_status,
							id_language = $this->id_language,
							position = $this->position
						where id = $this->id";

			$this->db->simpleQuery($query);
		}

		public function delete()
		{
			$query = "delete from languages where id = $this->id";
			$this->db->simpleQuery($query);
		}
	}

?>
