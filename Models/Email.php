<?php namespace Models;

	class Email
	{
		private $id;
		private $name;
		private $description;

		private $order;
		private $sort;
		private $db;

		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function toList()
		{
			$query = "	select
								e.id as 'id',
								e.name as 'email',
								e.description as 'description',
								count(ec.content) as 'count'
							from emails e
								left join emails_content ec on ec.id_email = e.id and ec.content != ''
							group by e.id";

			$data = $this->db->returnQuery($query);

			$emails = array();
			while($row = $data->fetch_array())
			{
				$emails[] = $row;
			}


			foreach ($emails as &$e) {
				$query = "select
							l.name as 'language',
							l.id as 'id_language',
							ec.id as 'id_content',
							ec.content
						from languages l
							left join emails_content ec on l.id = ec.id_language and ec.id_email = '$e[id]'";
				$data = $this->db->returnQuery($query);

				$languages = array();
				while($row = $data->fetch_array())
				{
					$languages[] = $row;
				}

				$e['languages'] = $languages;
			}

			//print_r(json_encode($emails));

			return $emails;
		}

		public function search() //Falta programar
		{
			$filter = '';

			if ($this->name) {
				$filter .= "where e.name like '%".$this->name."%' or e.description like '%".$this->name."%' or e.id = '$this->name' ";
			}

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = '';

			$query = "	select
							e.id,
							e.name,
							e.description
						from emails e
						$filter
						$orderBy";

			// var_dump($query);

			$data = $this->db->returnQuery($query);

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			$this->name = null;
			$this->id = null;
			$this->order = null;
			$this->sort = null;

			return $rows;
		}

		public function view()
		{
			$query = "	select
							name,
							id,
							description
						from emails
						where id = $this->id";

			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);

			// var_dump($row);
			return $row;
		}

		public function add()
		{
			$query = "	insert into emails(
							name,
							description
						)
						values (
							'$this->name',
							'$this->description'
						)";

			$this->db->simpleQuery($query);
			return $this->db->lastInsertId();
		}

		public function edit()
		{
			$query = "	update emails set
							name 				= '$this->name',
							description = '$this->description'
						where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function delete()
		{
			$query = "delete from emails where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function editContent($id, $content, $id_language)
		{
			$query = "";

			if($id)
			{
				$query = "update emails_content set
					content = '$content'
				where id = $id";
			}
			else
			{
				$query = "insert into emails_content (
					id_email,
					id_language,
					content)
				values (
					$this->id,
					$id_language,
					'$content'
					)";
			}

			$this->db->simpleQuery($query);
		}

		public function viewContent()
		{
			$query = "	select
										ec.content,
										l.short_name as 'language'
									from emails_content ec
										join languages l on l.id = ec.id_language
									where ec.id = $this->id";

			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
		}

		public function viewContentByIds($language)
		{
			$query = "	select content
									from emails_content
									where id_email = $this->id
										and id_language = $language";


			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			
			return $row['content'];
		}
	}




?>
