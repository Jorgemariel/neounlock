<?php namespace Models;

	class Company
	{
		private $id;
		private $name;
		private $img;
		private $status;

		private $order;
		private $sort;
		private $db;
		
		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function search()
		{
			$filter = '';

			if ($this->name) {
				$filter .= "name like '%".$this->name."%'";
			}

			if ($this->img === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'img != \'\' ';
			} elseif ($this->img === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'img = \'\' ';
			}

			if ($this->status === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'status = 1 ';
			} elseif ($this->status === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'status = 0 ';
			}

			$where = 'where ';
			($filter) ? $where .= $filter : $where = '';

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = '';

			$query = "	select *
						from companies
						$where
						$orderBy";

			//var_dump($query);

			$data = $this->db->returnQuery($query);
			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			$this->name = null;
			$this->img = null;
			$this->status = null;

			return $rows;
		}

		public function view()
		{
			$query = "select * from companies where id = $this->id";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			return $row;
		}

		public function add()
		{
			$query = "	insert into companies
						(
							name, 
							img, 
							status
						)
						values 
						(
							'$this->name', 
							'$this->img', 
							$this->status
						)";
			$this->db->simpleQuery($query);
			return $this->db->lastInsertId();
		}

		public function edit()
		{
			$query = "	update companies set
							name = '$this->name',
							img = '$this->img',
							status = $this->status
						where id = $this->id";
						
			$this->db->simpleQuery($query);
		}

		public function delete()
		{
			$query = "delete from companies where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function countriesPerCompany()
		{
			$query = "	select *
						from countries c
							join companies_x_countries cxc on cxc.id_country = c.id
						where cxc.id_company = $this->id";
			$data = $this->db->returnQuery($query);

			//Declaro la variable donde se guardaran los datos
			$rows = array();

			//Transformo los datos obtenidos en un array
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}
			return $rows;
		}

		public function countriesAllAndChecked()
		{
			$query = "	select c.*, IF(
						(
							select COUNT(*)
							from companies_x_countries cxc
							where cxc.id_country = c.id
								and cxc.id_company = $this->id
						) > 0, '1', '0') as 'checked'
						from countries c";
			$data = $this->db->returnQuery($query);

			//Declaro la variable donde se guardaran los datos
			$rows = array();

			//Transformo los datos obtenidos en un array
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}
			return $rows;
		}

		public function updateCountries($list){

			$query = "delete from companies_x_countries where id_company = $this->id; ";
			$query .= "insert into companies_x_countries (id_company, id_country) values ";

			$last_key = end($list);
			foreach ($list as $n) {
				if ($n == $last_key) {
					// last element
					$query .= "($this->id, $n); ";
				} else {
					// not last element
					$query .= "($this->id, $n), ";
				}
			}
			$this->db->multiQuery($query);
		}
	}

?>