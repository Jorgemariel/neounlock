<?php namespace Models;

	class Order
	{
		// Seguimiento
		private $id;
		private $tracking;
		private $id_status;
		private $id_language;

		// Datos del pedido
		private $imei;
		private $id_brand;
		private $id_model = 'null';
		private $id_country = 'null';
		private $id_company = 'null';
		private $mep = '';
		private $prd = '';

		// Datos del servicio
		private $id_service;
		private $service_price;
		private $service_delay;
		private $service_delay_type;

		// Datos del cliente
		private $name;
		private $email;
		private $ip;

		// Fechas
		private $entry_date;
		private $payment_date;
		private $delivery_date;

		// Pagos
		private $payment_method;
		private $id_mercadopago;
		private $id_paypal;
		private $payment_data;

		// Códigos de liberación
		private $code1;
		private $code2;
		private $code3;
		private $code4;
		private $code5;
		private $code6;

		// Encuesta
		private $survey_score;
		private $survey_coment;
		private $survey_date;
		private $id_survey_refered;
		private $survey_refered_other;

		// Otros
		private $general;
		private $order;
		private $sort;
		private $db;

		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function search()
		{
			$filter = '';

			if ($this->id) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.id like '%".$this->id."%' ";
			}

			if ($this->ip) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.ip like '%".$this->ip."%' ";
			}

			if ($this->tracking) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "tracking like '%". strtoupper($this->tracking)."%' ";
			}

			if ($this->id_status) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "id_status = $this->id_status ";
			}

			if ($this->imei) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "imei like '%". $this->imei."%' ";
			}

			if ($this->id_brand) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.id_brand = $this->id_brand ";
			}

			if ($this->id_model and $this->id_model!=='null') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.id_model = $this->id_model ";
			}

			if ($this->id_country and $this->id_country!=='null') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.id_country = $this->id_country ";
			}

			if ($this->id_company and $this->id_company!=='null') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.id_company = $this->id_company ";
			}

			if ($this->mep) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "mep like '%". $this->mep."%' ";
			}

			if ($this->prd) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "prd like '%". $this->prd."%' ";
			}

			if ($this->id_service) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.id_service = $this->id_service ";
			}

			if ($this->name) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "o.name like '%". $this->name."%' ";
			}

			if ($this->email) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "email like '%". $this->email."%' ";
			}

			if ($this->payment_method) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "payment_method = $this->payment_method ";
			}

			if ($this->code1) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "(	code1 = '$this->code1' or
								code2 = '$this->code1' or
								code3 = '$this->code1' or
								code4 = '$this->code1' or
								code5 = '$this->code1' or
								code6 = '$this->code1') ";
			}

			if ($this->general) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "(	o.name 		like '%". $this->general."%' or
								email 		like '%". $this->general."%' or
								ip 			like '%". $this->general."%' or
								o.id 			like '%". $this->general."%' or
								imei 		like '%". $this->general."%' or
								tracking 	like '%". $this->general."%' or
								code1 		like '%". $this->general."%' or
								code2 		like '%". $this->general."%' or
								code3 		like '%". $this->general."%' or
								code4 		like '%". $this->general."%' or
								code5 		like '%". $this->general."%' or
								code6 		like '%". $this->general."%') ";
			}

			$where = 'where ';
			($filter) ? $where .= $filter : $where = '';

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = 'order by o.id desc';

			$query = "	select
							o.*,
							b.name as 'brand',
							m.name as 'model',
							c.name_en as 'country',
							comp.name as 'company',
							s.name as 'status',
							l.name as 'language',
							serv.name as 'service',
							dt.name as 'delay_type'
						from orders o
							left join services serv on serv.id = o.id_service
							left join languages l on l.id = o.id_language
							left join status s on s.id = o.id_status
							left join brands b on b.id = o.id_brand
							left join models m on m.id = o.id_model
							left join countries c on c.id = o.id_country
							left join companies comp on comp.id = o.id_company
							left join delay_types dt on dt.id = o.service_delay_type
						$where
						$orderBy";

			// print_r($query);

			$data = $this->db->returnQuery($query);

			$this->id = null;
			$this->tracking = null;
			$this->id_status = null;
			$this->imei = null;
			$this->id_brand = null;
			$this->id_model = null;
			$this->id_country = null;
			$this->id_company = null;
			$this->mep = null;
			$this->prd = null;
			$this->id_service = null;
			$this->name = null;
			$this->email = null;
			$this->payment_method = null;
			$this->code1 = null;

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			return $rows;

		}

		public function statusList()
		{
			$query = "select * from status";
			$data = $this->db->returnQuery($query);
			return $data;
		}

		public function view()
		{
			$query = "	select
							o.*,
							d.name as 'delay_type',
							b.name as 'brand',
							m.name as 'model',
							c.name_es as 'country',
							com.name as 'company',
							s.name as 'service',
							l.short_name as 'lang_short',
							st.name as 'status'
						from orders o
							left join delay_types d on d.id = o.service_delay_type
							left join brands b on b.id = o.id_brand
							left join models m on m.id = o.id_model
							left join countries c on c.id = o.id_country
							left join companies com on com.id = o.id_company
							left join services s on s.id = o.id_service
							left join languages l on l.id = o.id_language
							left join status st on st.id = o.id_status
						where o.tracking = '$this->tracking'";
			// print_r($query);

			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);

			return $row;
		}

		public function add()
		{
			$selectedLang = strtoupper($_SESSION['language']);
			$query = "select id from languages where short_name = '$selectedLang'";

			//print_r($query);

			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			$id_language = $row['id'];

			$query = "	insert into orders
						(
							tracking,
							imei,
							id_brand,
							id_model,
							id_country,
							id_company,
							id_service,
							mep,
							prd,
							service_price,
							service_delay,
							service_delay_type,
							name,
							email,
							ip,
							id_language
						)
						values
						(
							'$this->tracking',
							 $this->imei,
							 $this->id_brand,
							 $this->id_model,
							 $this->id_country,
							 $this->id_company,
							 $this->id_service,
							'$this->mep',
							'$this->prd',
							 $this->service_price,
							 $this->service_delay,
							 $this->service_delay_type,
							'$this->name',
							'$this->email',
							'$this->ip',
							 $id_language
						)";

			// print_r($query);
			$this->db->simpleQuery($query);
			$this->id = $this->db->lastInsertId();
		}

		public function edit()
		{

		}

		public function editIdMercadopago()
		{
			$query = "	update orders set id_mercadopago = '$this->id_mercadopago'
						where id = $this->id";
			// print_r($query);
			$this->db->simpleQuery($query);
		}

		public function editStatus()
		{
			$query = "	update orders set status = '$this->id_status'
						where tracking = $this->tracking";
			// print_r($query);
			$this->db->simpleQuery($query);
		}

		public function editCodes()
		{
			$query = '';
			if ($this->code1)
			{
				($query) ? $query .= ', ' : '';
				$query .= "code1 = $this->code1";
			}
			if ($this->code2)
			{
				($query) ? $query .= ', ' : '';
				$query .= "code2 = $this->code2";
			}
			if ($this->code3)
			{
				($query) ? $query .= ', ' : '';
				$query .= "code3 = $this->code3";
			}
			if ($this->code4)
			{
				($query) ? $query .= ', ' : '';
				$query .= "code4 = '$this->code4'";
			}
			if ($this->code5)
			{
				($query) ? $query .= ', ' : '';
				$query .= "code5 = '$this->code5'";
			}
			if ($this->code6)
			{
				($query) ? $query .= ', ' : '';
				$query .= "code6 = '$this->code6'";
			}
			$query = " 	update orders set
							id_status = $this->id_status,
							$query
						where tracking = '$this->tracking'";
			// print_r($query);
			$this->db->simpleQuery($query);
		}

		public function delete()
		{

		}
	}

?>
