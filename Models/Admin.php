<?php namespace Models;

	class Admin
	{
		private $db;
		
		public function __construct()
		{
			$this->db = new Connection();
		}

		public function profitTotal() {
			$query = "	select sum(service_price) as total
						from orders
						where id_status = 4";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			//var_dump($row['total']);
			return $row['total'];
		}

		public function profitYear() {
			$query = "	select sum(service_price) as total
						from orders
						where id_status = 4
						and year(entry_date) = year(now())";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			//var_dump($row['total']);
			return $row['total'];
		}

		public function profitMonth() {
			$query = "	select sum(service_price) as total
						from orders
						where id_status = 4
						and month(entry_date) = month(now())
						and year(entry_date) = year(now())";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			//var_dump($row['total']);
			return $row['total'];
		}

		public function profitYearCompare() {
			$query = "	select sum(service_price) as total
						from orders
						where id_status = 4
						and year(entry_date) = year(now())-1";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			//var_dump($row['total']);
			return $row['total'];
		}

		public function profitMonthCompare() {
			$query = "	select sum(service_price) as total
						from orders
						where id_status = 4
						and month(entry_date) = month(now())-1
						and year(entry_date) = year(now())";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			//var_dump($row['total']);
			return $row['total'];
		}

		public function profitChart() {
			$query = "	select
							year(entry_date) as year,
							month(entry_date) as month,
							sum(service_price) as total
						from orders
						where id_status = 4
						group by year, month";
			$data = $this->db->returnQuery($query);

			$list = array();
			while($row = $data->fetch_array())
			{
				$list[] = $row;
			}

			// var_dump($list[0]['year']);

			$year = array();

			// while (end($year) < date("Y")) {
			// 	array_push($year, end($year)+1);
			// 	//var_dump($year);
			// }

			$month = array();















			return $list;
		}
	}

?>