<?php namespace Models;

	class User
	{
		private $id;
		private $username;
		private $email;
		private $password;
		//private $id_type;
		private $id_status;
		private $id_language;
		private $img;

		// Solo se usa para guardar o editar
		private $privileges;

		private $order;
		private $sort;
		private $db;
		
		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function setPrivileges($p)
		{
			$this->privileges = $p;
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function toList()
		{
			$query = "	select
							u.id,
							u.username,
							u.email,
							u.password,
							u.status,
							t.name as 'type',
							l.name as 'language',
							u.img
						from users u
							left join user_types t on t.id = u.id_type
							left join languages l on l.id = u.id_language";

			$data = $this->db->returnQuery($query);

			$users = array();
			while($row = $data->fetch_array())
			{
				$users[] = $row;
			}

			foreach ($users as &$u) {
				$query = "	select
								p.name as 'privilege',
								a.name as 'area'
							from privileges p
								join users_x_privileges uxp on uxp.id_privilege = p.id
								join privilege_areas a on p.id_area = a.id
							where uxp.id_user = '$u[id]'
							order by a.id";

				$data = $this->db->returnQuery($query);

				$privileges = array();
				while($row = $data->fetch_array())
				{
					$privileges[] = $row;
				}

				$u['privileges'] = $privileges;
			}

			return $users;
		}

		public function toListPrivileges()
		{
			$query = "	select
							p.id,
							p.name,
							a.name as 'area'
						from privileges p
							left join users_x_privileges uxp on uxp.id_privilege = p.id
							left join privilege_areas a on p.id_area = a.id
						group by p.id
						order by a.id";
			$data = $this->db->returnQuery($query);

			$privileges = array();
			while($row = $data->fetch_array())
			{
				$privileges[] = $row;
			}

			return $privileges;
		}

		public function search()
		{
			$filter = '';

			if ($this->name_en) {
				$filter .= "(		c.name_en like '%".$this->name_en."%'
								or 	c.name_es like '%".$this->name_en."%'
								or 	c.iso2 like '%".$this->name_en."%'
								or 	c.iso3 like '%".$this->name_en."%' 
								or 	c.coin like '%".$this->name_en."%'
							) ";
			}
			
			if ($this->id_language) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "c.id_language = $this->id_language ";
			}

			//Este no chequea bien el estado de la imagen
			if ($this->img === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.img != \'\' ';
			} elseif ($this->img === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.img = \'\' ';
			}

			if ($this->highlighted === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.highlighted = 1 ';
			} elseif ($this->highlighted === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.highlighted = 0 ';
			}

			$where = 'where ';
			($filter) ? $where .= $filter : $where = '';

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = '';

			$query = "	select c.*, l.name as 'language'
						from countries c join languages l on l.id = c.id_language
						$where
						$orderBy";

			//var_dump($query);

			$data = $this->db->returnQuery($query);

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			$this->name_en = null;
			$this->id_language = null;
			$this->img = null;
			$this->highlighted = null;

			return $rows;
		}

		public function view()
		{

			$query = "	select
							u.id,
							u.username,
							u.email,
							u.password,
							u.status,
							t.name as 'type',
							l.name as 'language',
							u.img
						from users u
							left join user_types t on t.id = u.id_type
							left join languages l on l.id = u.id_language
						where u.id = $this->id";

			$data = $this->db->returnQuery($query);
			$u = mysqli_fetch_assoc($data);

			$query = "	select
							p.id,
							p.name,
							a.name as 'area',
							if(
								(
									select count(*)
									from users_x_privileges uxp
									where uxp.id_user = '$u[id]'
										and uxp.id_privilege = p.id
							    ) > 0, '1', '0'
							) as 'checked'
						from privileges p
							left join users_x_privileges uxp on uxp.id_privilege = p.id
							left join privilege_areas a on p.id_area = a.id
						group by p.id
						order by a.id";

			// var_dump($query);

			$data = $this->db->returnQuery($query);

			$privileges = array();
			while($row = $data->fetch_array())
			{
				$privileges[] = $row;
			}

			$u['privileges'] = $privileges;

			// var_dump($query);

			return $u;
		}

		public function add()
		{
			$query = "	insert into users (
							username, 
							email, 
							img, 
							status, 
							id_language,
							password
						)
						values (
							'$this->username', 
							'$this->email', 
							'$this->img', 
							$this->id_status, 
							$this->id_language,
							'$this->password'
						)";

			//var_dump($query);
			$this->db->simpleQuery($query);

			$id = $this->db->lastInsertId();

			$last_key = count($this->privileges)-1;
			$i = 0;

			$items = "";
			foreach ($this->privileges as $p) {
				if ($p['status'] == 1) {
					$items .= ($items == "") ? "" : ", ";
					$items .= "($id, $p[id])";
				}

				$i++;
			}
			if ($items != "") {
				$query = "insert into users_x_privileges (id_user, id_privilege) values $items ;";				
			}
			//var_dump($query);

			$this->db->simpleQuery($query);
			return $id;
		}

		public function edit()
		{
			$query = "	update users set
							username = '$this->username', 
							email = '$this->email', 
							img = '$this->img', 
							status = $this->id_status, 
							id_language = $this->id_language";

			$this->password != "" ? $query .= ", password = '$this->password' " : " ";
			$query .= " where id = $this->id; ";

			$query .= "delete from users_x_privileges where id_user = $this->id; ";

			$last_key = count($this->privileges)-1;
			$i = 0;

			$items = "";
			foreach ($this->privileges as $p) {
				if ($p['status'] == 1) {
					$items .= ($items == "") ? "" : ", ";
					$items .= "($this->id, $p[id])";
				}

				$i++;
			}
			if ($items != "") {
				$query .= "insert into users_x_privileges (id_user, id_privilege) values $items ;";				
			}
			//var_dump($query);
			$this->db->multiQuery($query);
		}

		public function delete()
		{
			$query = "delete from users where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function login() {
			// Busco el usuario
			$query = "select id, password, img from users where username = '$this->username'";
			$data = $this->db->returnQuery($query);
			$data = mysqli_fetch_assoc($data);

			// Si el usuario no existe o la contraseña no coincide retorna 0
			if(!$data || !password_verify($this->password, $data['password'])) return false;

			// Cargo los datos del usuario
			$user = array();
			$user['username'] = $this->username;
			$user['time'] = time() + LOGOUTTIME;
			$user['id'] = $data['id'];
			$user['img'] = $data['img'];


			// Busco los privilegios
			$query = "select id_privilege from users_x_privileges where id_user = $data[id]";
			$data = $this->db->returnQuery($query);

			// Guardo los privilegios
			$privileges = array();
			array_push($privileges, 'start'); //Para evitar que el ID sea "0"
			while($row = $data->fetch_array())
			{
				array_push($privileges, $row['id_privilege']);
				//$privileges[] = $row;
			}
			$user['privileges'] = $privileges;
			// print_r($user['privileges']);

			// Cargo los privilegios
			$_SESSION['user'] = $user;

			return true;
		}

		public function logout() {
			unset($_SESSION['user']);
		}

		public function checkSession() {
			if(isset($_SESSION['user'])) {
				if(time() > $_SESSION['user']['time']) {

				    unset($_SESSION['user']);

				    header("Location: " . URL . "login?message=autologout");
					exit();
				} else {
				    $_SESSION['user']['time'] = time() + LOGOUTTIME; //set new time
				}
			} else {
				header("Location: " . URL . "login?message=mustlogin");
				exit();
			}
		}

		public function checkPrivilege($p) {
			if (in_array($p, $_SESSION['user']['privileges'])) {
				return true;
			} else {
				header("Location: " . URL . "login");
				exit();
			}
		}
	}

?>