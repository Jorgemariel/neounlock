<?php namespace Models;

	/**
	* 
	*/
	class Model
	{
		private $id;
		private $id_brand;
		private $name;
		private $img;
		private $status;
		private $position;
		private $showCountry;
		private $showCompany;
		private $showPRD;
		private $showMEP;

		private $order;
		private $sort;
		private $db;

		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function toList()
		{
			$query = "	select m.*, b.name as 'brand' 
						from models m join brands b on b.id = m.id_brand";
			$data = $this->db->returnQuery($query);
			return $data;
		}

		public function search()
		{
			$filter = '';

			if ($this->id) {
				$filter .= "m.id = $this->id ";
			}

			if ($this->name) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "m.name like '%".$this->name."%'"; //como hacer para que busque en marcas tmb
			}
			
			if ($this->position) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "m.position = $this->position ";
			}

			if ($this->id_brand) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "id_brand = $this->id_brand ";
			}

			//Este no chequea bien el estado de la imagen
			if ($this->img === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.img is not null ';
			} elseif ($this->img === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.img is null ';
			}

			if ($this->status === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.status = 1 ';
			} elseif ($this->status === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.status = 0 ';
			}

			if ($this->showCountry === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showCountry = 1 ';
			} elseif ($this->showCountry === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showCountry = 0 ';
			}

			if ($this->showCompany === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showCompany = 1 ';
			} elseif ($this->showCompany === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showCompany = 0 ';
			}

			if ($this->showPRD === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showPRD = 1 ';
			} elseif ($this->showPRD === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showPRD = 0 ';
			}

			if ($this->showMEP === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showMEP = 1 ';
			} elseif ($this->showMEP === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'm.showMEP = 0 ';
			}

			$where = 'where ';
			($filter) ? $where .= $filter : $where = '';

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = '';

			$query = "	select m.*, b.name as 'brand'
						from models m join brands b on b.id = m.id_brand
						$where
						$orderBy";

			//var_dump($query);

			$data = $this->db->returnQuery($query);

			$this->name = null;
			$this->img = null;
			$this->status = null;
			$this->positio = null;
			$this->showCountry = null;
			$this->showCompany = null;
			$this->showPRD = null;
			$this->showMEP = null;
			$this->order = null;
			$this->list = null;
			
			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			return $rows;
		}

		public function view()
		{
			$query = "	select m.*, b.name as 'brand' 
						from models m join brands b on b.id = m.id_brand
						where m.id = $this->id";

			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			return $row;
		}

		public function add()
		{
			$query = "insert into models (
						id_brand,
						name, 
						img, 
						status, 
						position, 
						showCountry, 
						showCompany, 
						showPRD, 
						showMEP
					)
					values (
						$this->id_brand,
						'$this->name', 
						'$this->img', 
						$this->status, 
						$this->position, 
						$this->showCountry,
						$this->showCompany,
						$this->showPRD,
						$this->showMEP
					)";

			$this->db->simpleQuery($query);
			return $this->db->lastInsertId();
		}

		public function edit()
		{
			$query = "	update models set
							id_brand 	= $this->id_brand,
							name 		= '$this->name',
							img 		= '$this->img',
							status 		= $this->status,
							position 	= $this->position,
							showCountry = $this->showCountry,
							showCompany = $this->showCompany,
							showPRD 	= $this->showPRD,
							showMEP 	= $this->showMEP
						where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function delete()
		{
			$query = "delete from models where id = $this->id";
			$this->db->simpleQuery($query);
		}
	}

?>