<?php namespace Models;

	class Faq
	{
    private $id;
    private $id_language;
    private $language;
    private $question;
    private $answer;

    private $db;

    public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

    public function toList()
    {
      $query = " select f.* from faqs f
                  join languages l on l.id = f.id_language
                  where l.short_name = '$this->language'";

      $data = $this->db->returnQuery($query);
			return $data;
    }

		public function search()
    {
      $query = "select
									f.*,
									l.name as language,
									l.short_name as language_short,
									l.img as language_img
								from faqs f
									join languages l on f.id_language = l.id";

      $data = $this->db->returnQuery($query);
			return $data;
    }

		public function view()
		{
			$query = "select * from faqs where id = $this->id";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			return $row;
		}

		public function add()
		{
			$query = "insert into faqs(id_language, status, question, answer)
								values(
									$this->id_language,
	 							 	$this->status,
	 							 	'$this->question',
	 							 	'$this->answer'
									)";

			$this->db->simpleQuery($query);
		}

		public function edit()
		{
			$query = "	update faqs set
							id_language = $this->id_language,
							status = $this->status,
							question = '$this->question',
							answer = '$this->answer'
						where id = $this->id";

			$this->db->simpleQuery($query);
		}

		public function delete()
		{
			$query = "delete from faqs where id = $this->id";
			$this->db->simpleQuery($query);
		}
  }
