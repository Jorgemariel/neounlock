<?php namespace Models;

	class Brand
	{
		private $id;
		private $name;
		private $img;
		private $status;
		private $position;
		private $showModel;
		private $showCountry;
		private $showCompany;
		private $showPRD;
		private $showMEP;

		private $order;
		private $sort;
		private $db;
		
		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function toList()
		{
			$query = "select * from brands order by position asc";
			$data = $this->db->returnQuery($query);
			return $data;
		}

		public function toListModels()
		{
			$query = "select name, id from brands";
			$data = $this->db->returnQuery($query);

			$brands = array();
			while($row = $data->fetch_array())
			{
				$brands[] = $row;
			}

			foreach ($brands as &$b) {
				$query = "select name, id from models where id_brand = '$b[id]'";
				$data = $this->db->returnQuery($query);

				$models = array();
				while($row = $data->fetch_array())
				{
					$models[] = $row;
				}

				$b['models'] = $models;
			}

			return $brands;
		}

		public function search()
		{
			$filter = '';

			if ($this->name) {
				$filter .= "name like '%".$this->name."%' ";
			}
			
			if ($this->position) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "position = $this->position ";
			}

			if ($this->img === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'img is not null ';
			} elseif ($this->img === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'img is null ';
			}

			if ($this->status === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'status = 1 ';
			} elseif ($this->status === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'status = 0 ';
			}

			if ($this->showModel === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showModel = 1 ';
			} elseif ($this->showModel === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showModel = 0 ';
			}

			if ($this->showCountry === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showCountry = 1 ';
			} elseif ($this->showCountry === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showCountry = 0 ';
			}

			if ($this->showCompany === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showCompany = 1 ';
			} elseif ($this->showCompany === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showCompany = 0 ';
			}

			if ($this->showPRD === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showPRD = 1 ';
			} elseif ($this->showPRD === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showPRD = 0 ';
			}

			if ($this->showMEP === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showMEP = 1 ';
			} elseif ($this->showMEP === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'showMEP = 0 ';
			}

			$where = 'where ';
			($filter) ? $where .= $filter : $where = '';

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = '';

			$query = "	select *
						from brands
						$where
						$orderBy";

			//var_dump($query);

			$data = $this->db->returnQuery($query);

			$this->name = null;
			$this->img = null;
			$this->status = null;
			$this->positio = null;
			$this->showModel = null;
			$this->showCountry = null;
			$this->showCompany = null;
			$this->showPRD = null;
			$this->showMEP = null;
			$this->order = null;
			$this->list = null;

			return $data;
		}

		public function view()
		{
			$query = "	select *
						from brands
						where id = $this->id";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			return $row;
		}

		public function add()
		{
			$query = "	insert into brands(
							name, 
							img, 
							status, 
							position, 
							showModel, 
							showCountry, 
							showCompany, 
							showPRD, 
							showMEP
						)
						values (
							'$this->name', 
							'$this->img', 
							 $this->status, 
							 $this->position,
							 $this->showModel,
							 $this->showCountry,
							 $this->showCompany,
							 $this->showPRD,
							 $this->showMEP
						)";
			$this->db->simpleQuery($query);
			return $this->db->lastInsertId();
		}

		public function edit()
		{
			$query = "	update brands set
							name 		= '$this->name',
							img 		= '$this->img',
							status 		= $this->status,
							position 	= $this->position,
							showModel 	= $this->showModel,
							showCountry = $this->showCountry,
							showCompany = $this->showCompany,
							showPRD 	= $this->showPRD,
							showMEP 	= $this->showMEP
						where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function delete()
		{
			$query = "delete from brands where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function countModels()
		{
			$query = "	select count(*)
						from models
						where id_brand = $this->id";
			$data = $this->db->returnQuery($query);
			return $data;
		}
	}

?>