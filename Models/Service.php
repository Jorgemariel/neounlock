<?php namespace Models;

	class Service
	{
		private $id;
		private $name;
		private $status;
		private $price;
		private $delay;
		private $id_delay_type;

		private $brand;
		private $model;
		private $country;
		private $company;

		private $description;
		private $id_language;

		private $order;
		private $sort;
		private $db;
		
		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function toListBrandsAndModelsAllAndCheck()
		{
			$query = "select b.name, b.id, IF(
						(
							select COUNT(*)
							from services_x_brands sxb
							where sxb.id_brand = b.id
								and sxb.id_service = $this->id
						) > 0, '1', '0') as 'checked'
						from brands b";
			$data = $this->db->returnQuery($query);

			$brands = array();
			while($row = $data->fetch_array())
			{
				$brands[] = $row;
			}

			foreach ($brands as &$b) {
				$query = "select name, id, IF(
						(
							select COUNT(*)
							from services_x_models sxm
							where sxm.id_model = m.id
								and sxm.id_service = $this->id
						) > 0, '1', '0') as 'checked'
						 from models m
						 where m.id_brand = '$b[id]'";
				$data = $this->db->returnQuery($query);

				$models = array();
				while($row = $data->fetch_array())
				{
					$models[] = $row;
				}

				$b['models'] = $models;
			}

			return $brands;
		}

		public function toListCountriesAllAndCheck()
		{
			$query = "select c.name_en, c.id, IF(
						(
							select COUNT(*)
							from services_x_countries sxc
							where sxc.id_country = c.id
								and sxc.id_service = $this->id
						) > 0, '1', '0') as 'checked'
						from countries c";
			$data = $this->db->returnQuery($query);

			return $data;
		}

		public function toListCompaniesAllAndCheck()
		{
			$query = "select c.name, c.id, IF(
						(
							select COUNT(*)
							from services_x_companies sxc
							where sxc.id_company = c.id
								and sxc.id_service = $this->id
						) > 0, '1', '0') as 'checked'
						from companies c";
			$data = $this->db->returnQuery($query);

			return $data;
		}

		public function search()
		{
			$filter = '';
			$join = '';
			$having = '';

			if ($this->name) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "s.name like '%".$this->name."%' ";
			}

			if ($this->status === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 's.status = 1 ';
			} elseif ($this->status === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 's.status = 0 ';
			}

			if ($this->price) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "s.price <= $this->price ";
			}

			// No filtra bien, arreglar
			if ($this->delay and $this->id_delay_type) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "s.delay <= $this->delay and s.id_delay_type <= $this->id_delay_type ";
			}

			// Si hay filtro de Marcas, hago un join y limito la consulta con un where
			if ($this->brand) {
				$join .= "left join services_x_brands sxb on sxb.id_service = s.id ";
				($filter) ? $filter .= 'and ' : '';
				$filter .= "(sxb.id_brand = $this->brand or not exists(select id_brand from services_x_brands sxb2 where sxb2.id_service = s.id)) ";
			}

			if ($this->model) {
				$join .= "left join services_x_models sxm on sxm.id_service = s.id ";
				($filter) ? $filter .= 'and ' : '';
				$filter .= "(sxm.id_model = $this->model or not exists(select id_model from services_x_models sxm2 where sxm2.id_service = s.id)) ";
			}

			if ($this->country) {
				$join .= "left join services_x_countries sxc on sxc.id_service = s.id ";
				($filter) ? $filter .= 'and ' : '';
				$filter .= "(sxc.id_country = $this->country or not exists(select id_country from services_x_countries sxc2 where sxc2.id_service = s.id)) ";
			}

			if ($this->company) {
				$join .= "left join services_x_companies sxcomp on sxc.id_service = s.id ";
				($filter) ? $filter .= 'and ' : '';
				$filter .= "(sxcomp.id_company = $this->company or not exists(select id_company from services_x_companies sxcomp2 where sxcomp2.id_service = s.id)) ";
			}

			if ($this->description === '1') {
				$having .= "having checkDescriptions = 1 ";
			} elseif ($this->description === '0') {
				$having .= "having checkDescriptions = 0 ";
			}

			$where = 'where ';
			($filter) ? $where .= $filter : $where = '';

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = '';

			$query = "	select s.*, d.name as 'delay_type', if(
							(
								select count(l.id) from languages l
						    ) = 
						    (
								select count(descr.id) from descriptions descr
						        where descr.id_service = s.id
						        	and descr.content != ''
						    ), '1', '0'
						) as 'checkDescriptions'
						from services s left join delay_types d on d.id = s.id_delay_type
						$join
						$where
						$having
						$orderBy";

			// print_r($query);

			$data = $this->db->returnQuery($query);

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			$this->name = null;
			$this->status = null;
			$this->price = null;
			$this->delay = null;
			$this->id_delay_type = null;

			foreach ($rows as &$r) {
				$query = "	select b.name
							from brands b join services_x_brands sxb on b.id = sxb.id_brand
							where sxb.id_service = '$r[id]' ";
				// var_dump($query);
				$data = $this->db->returnQuery($query);

				$brands = array();
				while($row = $data->fetch_array())
				{
					$brands[] = $row;
				}
				$r['brands'] = $brands;

				$query = "	select m.name
							from models m join services_x_models sxm on m.id = sxm.id_model
							where sxm.id_service = '$r[id]' ";
				// var_dump($query);
				$data = $this->db->returnQuery($query);

				$models = array();
				while($row = $data->fetch_array())
				{
					$models[] = $row;
				}
				$r['models'] = $models;

				$query = "	select c.name_en
							from countries c join services_x_countries sxc on c.id = sxc.id_country
							where sxc.id_service = '$r[id]' ";
				// var_dump($query);
				$data = $this->db->returnQuery($query);

				$countries = array();
				while($row = $data->fetch_array())
				{
					$countries[] = $row;
				}
				$r['countries'] = $countries;

				$query = "	select c.name
							from companies c join services_x_companies sxc on c.id = sxc.id_company
							where sxc.id_service = '$r[id]' ";
				// var_dump($query);
				$data = $this->db->returnQuery($query);

				$companies = array();
				while($row = $data->fetch_array())
				{
					$companies[] = $row;
				}
				$r['companies'] = $companies;
			}

			return $rows;
		}

		public function view()
		{
			$query = "	select s.*, d.name as 'delay_type'
						from services s join delay_types d on d.id = s.id_delay_type 
						where s.id = $this->id";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			// var_dump($row);

			$query = "	select c.name_en
						from countries c join services_x_countries sxc on c.id = sxc.id_country
						where sxc.id_service = '$row[id]' ";
			// var_dump($query);
			$countries = $this->db->returnQuery($query);
			$row['countries'] = $countries;

			$query = "	select c.name
						from companies c join services_x_companies sxc on c.id = sxc.id_company
						where sxc.id_service = '$row[id]' ";
			// var_dump($query);
			$companies = $this->db->returnQuery($query);
			$row['companies'] = $companies;

			return $row;
		}

		public function toListDelayTypes(){
			$query = "select * from delay_types";
			$data = $this->db->returnQuery($query);
			return $data;
		}

		public function toListDescriptions(){
			$query = "	select l.name as 'language', l.short_name, l.img, l.id as 'id_lang', d.*
						from languages l
							left join descriptions d on d.id_language = l.id
							and d.id_service = $this->id; ";
			// var_dump($query);
			$data = $this->db->returnQuery($query);

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			return $rows;
		}

		public function add($brands, $models, $countries, $companies)
		{
			$brands = json_decode($brands);
			$models = json_decode($models);
			$countries = json_decode($countries);
			$companies = json_decode($companies);

			//var_dump($brands, $models, $countries, $companies);

			$query = "	insert into services (
							name, 
							status, 
							price, 
							delay, 
							id_delay_type
						)
						values (
							'$this->name', 
							$this->status, 
							$this->price, 
							$this->delay, 
							$this->id_delay_type
						)";
			//var_dump($query);
			$this->db->simpleQuery($query);
			$this->id = $this->db->lastInsertId();
			$query = '';
			if ($brands) {
				$query .= "insert into services_x_brands (id_service, id_brand) values ";
				$last_key = end($brands);
				foreach ($brands as $b) {
					if ($b == $last_key) {
						// last element
						$query .= "($this->id, $b); ";
					} else {
						// not last element
						$query .= "($this->id, $b), ";
					}
				}
			}

			if ($models) {
				$query .= "insert into services_x_models (id_service, id_model) values ";
				$last_key = end($models);
				foreach ($models as $m) {
					if ($m == $last_key) {
						// last element
						$query .= "($this->id, $m); ";
					} else {
						// not last element
						$query .= "($this->id, $m), ";
					}
				}
			}

			if ($countries) {
				$query .= "insert into services_x_countries (id_service, id_country) values ";
				$last_key = end($countries);
				foreach ($countries as $c) {
					if ($c == $last_key) {
						// last element
						$query .= "($this->id, $c); ";
					} else {
						// not last element
						$query .= "($this->id, $c), ";
					}
				}
			}

			if ($companies) {
				$query .= "insert into services_x_companies (id_service, id_company) values ";
				$last_key = end($companies);
				foreach ($companies as $c) {
					if ($c == $last_key) {
						// last element
						$query .= "($this->id, $c); ";
					} else {
						// not last element
						$query .= "($this->id, $c), ";
					}
				}
			}
			// var_dump($query);
			$this->db->multiQuery($query);
		}

		public function edit($brands, $models, $countries, $companies)
		{
			$brands = json_decode($brands);
			$models = json_decode($models);
			$countries = json_decode($countries);
			$companies = json_decode($companies);

			$query = "	update services set
							name = '$this->name', 
							price = $this->price, 
							status = $this->status, 
							delay = $this->delay, 
							id_delay_type = $this->id_delay_type
						where id = $this->id; ";
			$query .= "delete from services_x_brands where id_service = $this->id; ";
			$query .= "delete from services_x_models where id_service = $this->id; ";
			$query .= "delete from services_x_countries where id_service = $this->id; ";
			$query .= "delete from services_x_companies where id_service = $this->id; ";

			if ($brands) {
				$query .= "insert into services_x_brands (id_service, id_brand) values ";
				$last_key = end($brands);
				foreach ($brands as $b) {
					if ($b == $last_key) {
						// last element
						$query .= "($this->id, $b); ";
					} else {
						// not last element
						$query .= "($this->id, $b), ";
					}
				}
			}

			if ($models) {
				$query .= "insert into services_x_models (id_service, id_model) values ";
				$last_key = end($models);
				foreach ($models as $m) {
					if ($m == $last_key) {
						// last element
						$query .= "($this->id, $m); ";
					} else {
						// not last element
						$query .= "($this->id, $m), ";
					}
				}
			}

			if ($countries) {
				$query .= "insert into services_x_countries (id_service, id_country) values ";
				$last_key = end($countries);
				foreach ($countries as $c) {
					if ($c == $last_key) {
						// last element
						$query .= "($this->id, $c); ";
					} else {
						// not last element
						$query .= "($this->id, $c), ";
					}
				}
			}

			if ($companies) {
				$query .= "insert into services_x_companies (id_service, id_company) values ";
				$last_key = end($companies);
				foreach ($companies as $c) {
					if ($c == $last_key) {
						// last element
						$query .= "($this->id, $c); ";
					} else {
						// not last element
						$query .= "($this->id, $c), ";
					}
				}
			}
			// var_dump($query);
			$this->db->multiQuery($query);
		}

		public function delete()
		{
			$query  = "delete from services where id = $this->id; ";
			$query .= "delete from services_x_brands where id_service = $this->id; ";
			$query .= "delete from services_x_models where id_service = $this->id; ";
			$query .= "delete from services_x_countries where id_service = $this->id; ";
			$query .= "delete from services_x_companies where id_service = $this->id; ";
			$this->db->multiQuery($query);
		}

		public function editDescription()
		{
			$query = "	replace into descriptions (id_language, id_service, content)
							values ($this->id_language, $this->id, '$this->description');";
			// var_dump($query);

    		$this->db->simpleQuery($query);
		}
	}

?>