<?php namespace Models;

	class Country
	{
		private $id;
		private $name_en;
		private $name_es;
		private $img;
		private $iso2;
		private $iso3;
		private $coin;
		private $id_language;
		private $highlighted;

		private $order;
		private $sort;
		private $db;
		
		public function __construct()
		{
			$this->db = new Connection();
		}

		public function set($attribute, $content)
		{
			$this->$attribute = $this->db->escape_string($content);
		}

		public function get($attribute)
		{
			return $this->$attribute;
		}

		public function toList()
		{
			$query = "	select c.*, l.name as 'language'
						from countries c join languages l on l.id = c.id_language
						order by highlighted desc";
			$data = $this->db->returnQuery($query);
			return $data;
		}

		public function toListCompanies()
		{
			$query = "	select *
						from countries
						order by name_en";
			$data = $this->db->returnQuery($query);

			$countries = array();
			while($row = $data->fetch_array())
			{
				$countries[] = $row;
			}

			foreach ($countries as &$c) {
				$query = "	select c.name, c.id
							from companies c join companies_x_countries cxc on cxc.id_company = c.id
							where cxc.id_country = '$c[id]'
								and c.status = '1'";
				$data = $this->db->returnQuery($query);

				$companies = array();
				while($row = $data->fetch_array())
				{
					$companies[] = $row;
				}

				$c['companies'] = $companies;
			}

			return $countries;
		}

		public function search()
		{
			$filter = '';

			if ($this->name_en) {
				$filter .= "(		c.name_en like '%".$this->name_en."%'
								or 	c.name_es like '%".$this->name_en."%'
								or 	c.iso2 like '%".$this->name_en."%'
								or 	c.iso3 like '%".$this->name_en."%' 
								or 	c.coin like '%".$this->name_en."%'
							) ";
			}
			
			if ($this->id_language) {
				($filter) ? $filter .= 'and ' : '';
				$filter .= "c.id_language = $this->id_language ";
			}

			//Este no chequea bien el estado de la imagen
			if ($this->img === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.img != \'\' ';
			} elseif ($this->img === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.img = \'\' ';
			}

			if ($this->highlighted === '1') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.highlighted = 1 ';
			} elseif ($this->highlighted === '0') {
				($filter) ? $filter .= 'and ' : '';
				$filter .= 'c.highlighted = 0 ';
			}

			$where = 'where ';
			($filter) ? $where .= $filter : $where = '';

			$orderBy = 'order by ';
			($this->order) ? $orderBy .= $this->order . ' ' . $this->sort : $orderBy = '';

			$query = "	select c.*, l.name as 'language'
						from countries c join languages l on l.id = c.id_language
						$where
						$orderBy";

			//var_dump($query);

			$data = $this->db->returnQuery($query);

			$rows = array();
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}

			$this->name_en = null;
			$this->id_language = null;
			$this->img = null;
			$this->highlighted = null;

			return $rows;
		}

		public function view()
		{
			$query = "	select c.*, l.name as 'language'
						from countries c join languages l on l.id = c.id_language
						where c.id = $this->id";
			$data = $this->db->returnQuery($query);
			$row = mysqli_fetch_assoc($data);
			return $row;
		}

		public function add()
		{
			$query = "	insert into countries (
							name_en, 
							name_es, 
							img, 
							iso2, 
							iso3,
							coin,
							id_language,
							highlighted
						)
						values (
							'$this->name_en', 
							'$this->name_es', 
							'$this->img', 
							'$this->iso2', 
							'$this->iso3',
							'$this->coin',
							$this->id_language,
							$this->highlighted
						)";
			$this->db->simpleQuery($query);
			return $this->db->lastInsertId();
		}

		public function edit()
		{
			$query = "	update countries set
							name_en = '$this->name_en', 
							name_es = '$this->name_es', 
							img = '$this->img', 
							iso2 = '$this->iso2', 
							iso3 = '$this->iso3',
							coin = '$this->coin',
							id_language = $this->id_language,
							highlighted = $this->highlighted
						where id = $this->id";
						
			$this->db->simpleQuery($query);
		}

		public function delete()
		{
			$query = "delete from countries where id = $this->id";
			$this->db->simpleQuery($query);
		}

		public function companiesPerCountry()
		{
			$query = "	select *
						from companies comp
							join companies_x_countries cxc on cxc.id_company = comp.id
						where cxc.id_country = $this->id";
			$data = $this->db->returnQuery($query);

			//Declaro la variable donde se guardaran los datos
			$rows = array();

			//Transformo los datos obtenidos en un array
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}
			return $rows;
		}

		public function companiesAllAndChecked()
		{
			$query = "	select comp.*, IF(
						(
							select COUNT(*)
							from companies_x_countries cxc
							where cxc.id_company = comp.id
								and cxc.id_country = $this->id
						) > 0, '1', '0') as 'checked'
						from companies comp";
			$data = $this->db->returnQuery($query);

			//Declaro la variable donde se guardaran los datos
			$rows = array();

			//Transformo los datos obtenidos en un array
			while($row = $data->fetch_array())
			{
				$rows[] = $row;
			}
			return $rows;
		}

		public function updateCompanies($list){

			$query = "delete from companies_x_countries where id_country = $this->id; ";
			$query .= "insert into companies_x_countries (id_country, id_company) values ";

			$last_key = end($list);
			foreach ($list as $n) {
				if ($n == $last_key) {
					// last element
					$query .= "($this->id, $n); ";
				} else {
					// not last element
					$query .= "($this->id, $n), ";
				}
			}
			$this->db->multiQuery($query);
		}
	}

?>